<?php
// コーポレートサイト側でJSON作成している
$url = 'https://www.allhouse.co.jp/news/milifeplus_output_api.php';

$option = [
    CURLOPT_RETURNTRANSFER => true, //文字列として返す
    CURLOPT_TIMEOUT        => 5, // タイムアウト時間
];

$ch = curl_init($url);
curl_setopt_array($ch, $option);

$json    = curl_exec($ch);

echo($json);
exit;