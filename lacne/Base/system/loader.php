<?php
/**
 * 必要ファイルのインクルード設定（全システムでの共通利用するファイル）
 *
 * @package  Lacne
 * @author  InVogue Inc.
 * @link  http://lacne.jp
 * @copyright  Copyright 2008- InVogue Inc. All rights reserved.
 */


define("DIR_SYSTEM", dirname(__FILE__)."/");

//CONIFG系読み込み
define("DIR_CONFIG", DIR_SYSTEM."config/");
include_once(DIR_CONFIG.'config.php');
include_once(DIR_CONFIG.'database.php');
include_once(DIR_CONFIG.'option.php');

define("DIR_FUNC", DIR_SYSTEM."functions/");
define("DIR_LACNE", DIR_SYSTEM."lacne/");
define("DIR_VENDORS", DIR_SYSTEM."vendors/");
define("DIR_OPTION", DIR_SYSTEM."option/");
define("DIR_MODEL", DIR_SYSTEM."model/");
define("DIR_LIBRARY", DIR_SYSTEM."library/");

/**
 * コンポーザーの読み込み
 */
require_once(dirname(__FILE__) . '/composer/vendor/autoload.php');

//その他ファイル読み込み用
$_FILE_REQUIRES = array(

    //VENDORS
    //DIR_VENDORS.'class.imgresize.php',

    //Functions
    DIR_FUNC.'common.php'

);

/**
 * 各種ファイルのinclude実行
 */
if(isset($_FILE_REQUIRES) && is_array($_FILE_REQUIRES))
{

    foreach ($_FILE_REQUIRES as $file) {
        if(file_exists($file))
        {
            include_once($file);
        }
    }
}

/**
 * エラー処理関連の設定
 */
if(DEBUG_MODE_SYS){
    ini_set( "display_errors","On" );
    ini_set( "display_startup_errors","On" );
}
else
{
    ini_set( "display_errors","Off" );
    ini_set( "display_startup_errors","Off" );
}

error_reporting(2039);
//error_reporting(E_ALL);
//error_reporting(0);
ini_set( "log_errors", "On" );
ini_set( "error_log", dirname(__FILE__)."/../log/php.log" );


set_error_handler( "error_handler", E_ALL );
function error_handler ($errno, $errstr, $errfile, $errline, $errcontext)
{
    // 処理継続
    return false;
}

register_shutdown_function( "shutdown_handler" );
function shutdown_handler()
{
    if($error = error_get_last())
    {
        switch($error["type"])
        {
            case E_ERROR:
            case E_PARSE:
            case E_CORE_ERROR:
            case E_CORE_WARNING:
            case E_COMPILE_ERROR:
            case E_COMPILE_WARNING:
                if(!DEBUG_MODE_SYS)
                {
                    // 500出して終了
                    header('HTTP/1.1 500 Internal Server Error');
                    exit();
                }
                break;
        }
    }
}