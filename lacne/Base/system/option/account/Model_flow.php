<?php

//LACNE OPTION SETTING
define("LACNE_OPTION_WORKFLOW" , 1);

/**
// ------------------------------------------------------------------------
 *  workflow.php
 *  (管理画面）承認・公開フロー処理用
 * @package		Lacne
 * @author		In Vogue Inc.
 * @link		http://lacne.jp
 * @copyright	        Copyright (c) 2008 - 2011, In Vogue Inc.
 */
// ------------------------------------------------------------------------

class Model_flow extends Model_post
{	
	
        function Model_flow($LACNE , $db){
            parent::Model_post($LACNE , $db);
        }
        
        
        /**
         * 承認待ちのデータ件数を取得する
         * 
         * @return number
         */
        function data_cnt_waiting()
        {
            return $this->data_cnt(array('post.status ='=>'wait'));
        }
        
}   

?>