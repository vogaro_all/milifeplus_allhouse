<?php

/**
 *-------------------------------------------------------------------------
 *  オプション： ソーシャルボタン
 *  social_btn.php
 * 
 *-------------------------------------------------------------------------
 */

//LACNE OPTION SETTING
define("LACNE_OPTION_SOCIAL_BTN" , 1);


class Lib_social_btn_func extends Lib_social_btn 
{
    
    /**
     * Twitter ツイート内にviaアカウント入れる場合、そのアカウント名を設定
     */
    var $twitter_via_account = "";
    
    /*+
     * facebook APP ID
     */
    var $facebook_app_id = "";
    /**
     *  mixi key
     */
    var $mixi_app_id = "";
    
    /**
     * ページ情報
     */
    var $pageinfo = array();
    
    
    function Lib_social_btn_func($LACNE , $db){
        parent::Lib_social_btn($LACNE , $db);
        $this->LACNE->load_library(array('output'));
    }
    
    /**
     *  OGPの情報を記事データから抜き出してセット
     *  @param  object $Output_obj //Lib_outputのインスタンス
     *  $page_info
     *    |-string "title" //ページ名、ページタイトル
     *    |-string "url" //ページURL
     *    |-string "site_name" //サイト名
     *    |-string "type" //article , blog , website
     *    |-string "description" //ページ内容
     *    |-string "image" //画像URL
     */
    function set_pageinfo($Output_Obj){
        $device = $this->LACNE->library["output"]->is_mobile();
        $imgsrc_arr = $this->LACNE->library["output"]->get_imagesrc($Output_Obj->get("body"));
        $this->pageinfo = array(
            "title" => $Output_Obj->get("title"),
            "url"   => $this->LACNE->get_urlpath($this->LACNE->url_setting->get_pageurl($Output_Obj->get("id"),$Output_Obj->get("category"),$device)),
            "site_name" => (defined("SITE_NAME"))?SITE_NAME:"",
            "type" => "website",
            "description" => strip_tags(fn_output_html_txtarea($Output_Obj->get("body"))),
            "image" => (isset($imgsrc_arr[0]))?$this->LACNE->get_urlpath($imgsrc_arr[0]):""
        );
    }
    
    /**
     * Tweet ボタン配置
     */
    function btn_twitter()
    {
        
        $title = $this->pageinfo["title"];
        $url = $this->pageinfo["url"];
        $str_via = "";
        if($this->twitter_via_account)
        {
            $str_via = 'data-via="'.$this->twitter_via_account.'" ';
        }
        
        $code = <<< EOF
<a href="https://twitter.com/share" class="twitter-share-button" data-text="{$title}" data-url="{$url}" {$str_via}data-lang="ja">ツイート</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
EOF;
        
        return $code;
    }
    
    function btn_facebook()
    {
        
        $code = <<< EOF
<iframe src="//www.facebook.com/plugins/like.php?send=false&amp;layout=standard&amp;width=450&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:21px;" allowTransparency="true"></iframe>
EOF;

        return $code;
    }
    
    function btn_mixi()
    {
        
        $code = <<< EOF
<a href="http://mixi.jp/share.pl" class="mixi-check-button" data-key="{$this->mixi_app_id}" data-button="button-1">mixiチェック</a> <script type="text/javascript" src="http://static.mixi.jp/js/share.js"></script>
EOF;
        return $code;
    }
    
    function output_ogp()
    {
        $code = "";
        if($this->pageinfo && isset($this->pageinfo["title"]))
        {
            $code = <<< EOF
<meta property="og:title" content="{$this->pageinfo["title"]}" />
<meta property="og:type" content="{$this->pageinfo["type"]}" />
<meta property="og:url" content="{$this->pageinfo["url"]}" />
<meta property="og:description" content="{$this->pageinfo["description"]}"/>
<meta property="og:image" content="{$this->pageinfo["image"]}" />
<meta property="og:site_name" content="{$this->pageinfo["site_name"]}" />
<meta property="fb:app_id" content="{$this->facebook_app_id}" />
<meta property="og:locale" content="ja_JP" />
EOF;

        }
        return $code;
    }
    
}

?>