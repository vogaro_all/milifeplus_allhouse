<?php

/**
 *-------------------------------------------------------------------------
 *  オプション：簡易LPO
 *  lpo_light.php
 *-------------------------------------------------------------------------
 */

//LACNE OPTION SETTING
define("LACNE_OPTION_LPO_LIGHT" , 1);


class Lib_lpo_light_func extends Lib_lpo_light
{
    
    function Lib_lpo_light_func($LACNE , $db){
        parent::Lib_lpo_light($LACNE , $db);
    }
    
    /**
     *  LPOタグの解析
     *
     *  @param  string $data セッティングデータ
     *  @return object
     */
    function extractSettingData( $data )
    {
            $delimiter = "==";

            $r_data = fn_output_html_txtarea($data);
            $r_data_arr = explode("<br />" , $r_data);

            $lpo_settingdata_arr = array();

            if($r_data_arr && is_array($r_data_arr)) {
                    $cnt = 0;
                    foreach($r_data_arr as $val) {
                            //フィールドネームなら
                            if(substr($val,0,2) == $delimiter) {
                                    $cnt++;
                                    $term_obj = $this->getTermObj( substr($val , 2) );

                                    $lpo_settingdata_arr[$cnt] = array(
                                            "term" => $term_obj["term"],
                                            "from" => $term_obj["from"],
                                            "to"   => $term_obj["to"],
                                            "metabody" => ""
                                    );
                            //フィールド設定データなら
                            }else{
                                    if($val) {
                                            if($lpo_settingdata_arr[$cnt]["metabody"]) {
                                                    $lpo_settingdata_arr[$cnt]["metabody"] .= "<br>";
                                            }
                                            $lpo_settingdata_arr[$cnt]["metabody"] .= $val;
                                    }
                            }
                    }//foreach
            }

            return $lpo_settingdata_arr;

    }

    /**
     *  LPOの条件を解析
     *
     *  @param  string $term 条件文
     *  @return object
     */
    function getTermObj( $term )
    {

            $delimiter_value = ":";
            $delimiter_value_to = "-";

            $term_obj = array();

            if($term) {

                    $term_data = explode($delimiter_value,$term);

                    if(isset($term_data[0]) && $term_data[0]) {

                            if($term_data[0] == "lpo_time") {
                                    $term_obj["term"] = "time";
                            }else if($term_data[0] == "lpo_days") {
                                    $term_obj["term"] = "days";
                            }else{
                                    $term_obj["term"] = "";
                            }

                            $term_value_arr = explode($delimiter_value_to , $term_data[1]);

                            if(isset($term_value_arr[0]) && $term_value_arr[0]) {
                                    $term_obj["from"] = trim($term_value_arr[0]);
                            }else{
                                    $term_obj["from"] = "";
                            }

                            if(isset($term_value_arr[1]) && $term_value_arr[1]) {
                                    $term_obj["to"] = trim($term_value_arr[1]);
                            }else{
                                    $term_obj["to"] = "";
                            }
                    }
            }

            return $term_obj;

    }

    /**
     *  LPOタグの条件にあてはまるコンテンツ内容を返す
     *
     *  @param  object $lpo_settingdata_arr セッティングデータ（解析済みデータ）
     *  @return object
     */
    function getTargetContents( $lpo_settingdata_arr )
    {

            $targetData = array(
                    "default" => array(
                    ),
                    "target" => array(
                    )
            );

            if($lpo_settingdata_arr && is_array($lpo_settingdata_arr)) {

                    foreach($lpo_settingdata_arr as $val) {

                            //fromの空値が一番最初にきたものがデフォルト値とする
                            if(isset($val["term"]) && $val["term"] && (!$val["from"] || $val["from"] == "default")) {

                                    $targetData["default"] = array(
                                            "term"	=> $val["term"],
                                            "from"	=> "",
                                            "to"	=> "",
                                            "metabody"=> $val["metabody"]
                                    );

                            }else if(isset($val["term"]) && $val["term"]){

                                    $check_res = false;

                                    if($val["term"] == "time") {
                                            //from to 条件を渡し、対象となるデータであるか
                                            $check_res = $this->checkTerm_time($val["from"],$val["to"]);
                                    }else if($val["term"] == "days") {
                                            //from to 条件を渡し、対象となるデータであるか
                                            $check_res = $this->checkTerm_days($val["from"],$val["to"]);
                                    }
                                    if($check_res) {
                                            //対象データであれば、コンテンツデータを返却用データに格納し、foreachループを終わる
                                            $targetData["target"] = array(
                                                    "term"	=> $val["term"],
                                                    "from"	=> $val["from"],
                                                    "to"	=> $val["to"],
                                                    "metabody"=> $val["metabody"]
                                            );
                                            return $targetData["target"]["metabody"];
                                    }
                            }
                    }
            }

            if(isset($targetData["default"]["term"]) && $targetData["default"]["term"]){
                    return $targetData["default"]["metabody"];
            }else{
                    return "";
            }

    }

    /**
     *  LPOの時間条件で条件値を検証
     *
     *  @param  mixed $from
     *  @param  mixed $to
     *  @return object
     */
    function checkTerm_time( $from , $to )
    {

            $from = $this->exchangeTimeStr($from);
            if($from) {
                    $from = date("H:i" , strtotime($from)).":00";
            }
            $to = $this->exchangeTimeStr($to);
            if($to) {
                    $to = date("H:i" , strtotime($to)).":59";
            }

            $nowtime = date("H:i:s");

            $return = false;

            if($from && $from <= $nowtime){
                    if($to) {
                            if($to >= $nowtime) {
                                    $return = true;
                            }
                    }else{
                            $return = true;
                    }
            }

            return $return;
    }

    /**
     *  LPOの曜日条件で条件値を検証
     *
     *  @param  mixed $from
     *  @param  mixed $to
     *  @return object
     */
    function checkTerm_days( $from , $to )
    {
            $from = $this->exchangeDaysStr($from);
            $to = $this->exchangeDaysStr($to);

            $nowdays= date("w");

            $return = false;

            if($from && $from <= $nowdays){
                    if($to) {
                            if($to >= $nowdays) {
                                    $return = true;
                            }
                    }else{
                            $return = true;
                    }
            }

            return $return;
    }

    /**
     *  LPOの時間条件値で時間データを整形
     *
     *  @param  string $time
     *  @return string
     */
    function exchangeTimeStr( $time )
    {
            $return_time = "";

            if($time && preg_match('/^[0-9]+$/' , $time)) {

                    $str_len = mb_strlen($time);

                    if($str_len == 1) {
                            $return_time = sprintf("%02d",$time)."00";
                    }else if($str_len == 2) {
                            $return_time = $time."00";
                    }else if($str_len == 4) {
                            $return_time = $time;
                    }
            }
            return $return_time;
    }

    /**
     *  LPOの曜日条件値で時間データを整形
     *
     *  @param  string $days
     *  @return string
     */
    function exchangeDaysStr( $days )
    {
            $return_days = "";

            if($days && preg_match('/^[0-9]+$/' , $days)) {

                    $str_len = mb_strlen($days);

                    if($str_len == 1) {
                            $return_days = substr($days,0,1);
                    }
            }
            return $return_days;
    }
}


?>