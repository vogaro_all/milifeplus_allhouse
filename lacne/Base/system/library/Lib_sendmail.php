<?php
use lacne\core\Library;
/**
// ------------------------------------------------------------------------
 * Lib_sendmail.php
 * 
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

require_once(DIR_VENDORS."/qdmail.php");
require_once(DIR_VENDORS."/qdsmtp.php");

class Lib_sendmail extends Library
{

    /**
     * Lib_sendmail constructor.
     * @param $LACNE
     * @param $db
     */
	function Lib_sendmail($LACNE , $db) {
        parent::__construct($LACNE, $db);
	}
	
        /**
        *  メール送信
        *
        *  @access public
        *  @param  string $to  送信先アドレス
        *  @param  string $subject  題名
        *  @param  array  $body_data  本文データ
        *  @param  string $from  送信元
         * @param  array  $cc
        *  @return boolean
        */
       	function sendmail($to, $subject, $body_data, $from ,$cc = array(), $smtp_param = array()) {
       		
            //メール送信
            /*
            if( qd_send_mail('text', $to, $subject, $body_data, $from) ) {
                    return true;
            }
            return false;
            */
            $mail = & new Qdmail();

            if(!empty($smtp_param)){
            	$mail->smtp(true);
            	$mail->smtpServer($smtp_param);
            }
            $mail -> to( $to );
            $mail -> subject( $subject );
            $mail -> text( $body_data);
            $mail -> from( $from );
            
            if(count($cc)) $mail -> cc( $cc );
            
            return $mail ->send();
        }
        
}

?>