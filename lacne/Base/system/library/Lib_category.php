<?php
use lacne\core\Library;
use lacne\core\model\Category;
/**
// ------------------------------------------------------------------------
 * Lib_category.php
 *
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class Lib_category extends Library
{

	/**
	 * Lib_category constructor.
	 * @param $LACNE
	 * @param $db
	 */
	function Lib_category($LACNE , $db) {
            parent::__construct($LACNE, $db);
	}
}

?>