<?php
use lacne\core\Library;
use lacne\core\Session;
use lacne\core\model\Admin;
use lacne\core\model\AppConnect;
use lacne\core\model\Post;
/**
// ------------------------------------------------------------------------
 * Lib_App_api.php
 *
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

require_once(DIR_VENDORS."/rhaco/Rhaco.php");
require_once(DIR_VENDORS."/rhaco/tag/model/SimpleTag.php");

class Lib_App_api extends Library
{
        /**  @var string  $authority  権限タイプ */
        var $authority;

    /**
     * Lib_App_api constructor.
     * @param $LACNE
     * @param $db
     */
    function Lib_App_api($LACNE , $db)
    {
        parent::__construct($LACNE, $db);

        $this->LACNE->load_library(array('login', 'post','media'));
    }

        /**
         * POSTデータ取得
         * @param string $key
         * @param boolean $escape //エスケープするか
         * @return mixed
         */
        function get_post($key , $escape = true)
        {
            if(isset($_POST[$key]))
            {
                if($escape)
                {
                    return fn_sanitize($_POST[$key]);
                }
                else
                {
                    return $_POST[$key];
                }
            }

            return false;
        }

        /**
         * iphoneアプリ用ログイン認証
         * @return xml data
         */
        function action_login()
        {
            $result = array();

            //※ここでの$_POST["login_id"]は login_account値をあらわす
            if($this->get_post("login_id") && $this->get_post("password"))
            {

                $this->LACNE->session->sessionStart(3);

                $rs = $this->LACNE->library["login"]->checkLogin($this->get_post("login_id") , $this->get_post("password") , false); //認証チェック、成功してもSESSIONセットしない

                if($rs)
                {
                    //認証成功したらアプリ独自の認証情報セットをおこなう（app_connectテーブルへ）

                    //loginユーザーデータ取得
                    $user_info = with(new Admin())->fetchOneByLoginAccount($this->get_post("login_id"));

                    $connection_id = "";
                    if(isset($user_info["login_id"]))
                    {
                        $connection_id = $this->insert_loginSession($user_info["login_id"]);
                    }
                    if($connection_id)
                    {
                        $result = array(
                            $this->_xml("status" , 1),
                            $this->_xml("session_id" , $connection_id),
                            $this->_xml("authority" , $this->LACNE->library["login"]->chk_controll_limit("publish_post" , $user_info["login_id"]))
                        );
                    }
                }
            }

            if($result)
            {
                return $result;
            }

            return $this->err_response("Unauthorized");
        }

        /**
         * コンテンツ一覧取得
         * @param object $contents_list
         */
        function action_get_contents($contents_list)
        {
            $result = array();
            $err_code = "";
            //認証をまずおこなう
            if($this->chk_loginSession())
            {
                $data = array();
                if(is_array($contents_list))
                {
                    foreach($contents_list as $key => $content)
                    {
                        $data[] = $this->_xml("item" , array(
                            $this->_xml("id" , $key),
                            $this->_xml("name" , $content["name"])
                        ));
                    }

                    $result = array(
                        $this->_xml("status" , 1),
                        $this->_xml("data" , $data)
                    );
                }
                else
                {
                    $err_code = "Missing contents list data";
                }
            }
            else
            {
                //認証エラー※未認証またはセッション切れ
                $err_code = "Unauthorized";
            }


            if($result)
            {
                return $result;
            }

            return $this->err_response($err_code);

        }

        /**
         * 接続ユーザーがアプリから投稿した記事データ一覧を返す
         */
        function action_get_data()
        {
            $result = array();
            $err_code = "";
            //認証をまずおこなう
            $login_id = $this->chk_loginSession();
            if($login_id)
            {
                $post_list = with(new Post())->get_appdata_list($login_id);

                $data = array();
                if($post_list && is_array($post_list))
                {
                    foreach($post_list as $post)
                    {
                        $data[] = $this->_xml("item" , array(
                            $this->_xml("id" , $post["id"]),
                            $this->_xml("status" , $post["status_val"]),
                            $this->_xml("modified" , $post["edit_pc"])
                        ));
                    }

                    $result = array(
                        $this->_xml("status" , 1),
                        $this->_xml("data" , $data)
                    );
                }
                else
                {
                    //対象データがない場合はstatusのみ1にして返す
                    $result = array(
                        $this->_xml("status" , 1)
                    );
                }
            }
            else
            {
                //認証エラー※未認証またはセッション切れ
                $err_code = "Unauthorized";
            }

            if($result)
            {
                return $result;
            }

            return $this->err_response($err_code);
        }

        /**
         * カテゴリデータ一覧を返す
         */
        function action_get_category()
        {
            $result = array();
            $err_code = "";
            //認証をまずおこなう
            $login_id = $this->chk_loginSession();
            if($login_id)
            {

                $category_list = with(new Post())->get_category_list();

                $data = array();
                if(is_array($category_list))
                {
                    foreach($category_list as $key => $category)
                    {
                        $data[] = $this->_xml("item" , array(
                            $this->_xml("id" , $key),
                            $this->_xml("name" , $category)
                        ));
                    }

                    $result = array(
                        $this->_xml("status" , 1),
                        $this->_xml("data" , $data)
                    );
                }
                else
                {
                    //対象データがない場合はstatusのみ1にして返す
                    $result = array(
                        $this->_xml("status" , 1)
                    );
                }

            }
            else
            {
                //認証エラー※未認証またはセッション切れ
                $err_code = "Unauthorized";
            }

            if($result)
            {
                return $result;
            }

            return $this->err_response($err_code);
        }


        /**
         * テンプレートファイル一覧取得
         * @param object $template_list
         */
        function action_get_template($template_list)
        {
            $result = array();
            $err_code = "";
            //認証をまずおこなう
            $login_id = $this->chk_loginSession();
            if($login_id)
            {
                $data = array();
                if(is_array($template_list))
                {
                    foreach($template_list as $key => $template)
                    {
                        $data[] = $this->_xml("item" , array(
                            $this->_xml("id" , $key),
                            $this->_xml("name" , $template["name"])
                        ));
                    }

                    $result = array(
                        $this->_xml("status" , 1),
                        $this->_xml("data" , $data)
                    );
                }
                else
                {
                    $err_code = "Missing template list data";
                }
            }
            else
            {
                //認証エラー※未認証またはセッション切れ
                $err_code = "Unauthorized";
            }


            if($result)
            {
                return $result;
            }

            return $this->err_response($err_code);

        }


        function action_img_upload()
        {
            $result = array();
            $err_code = "";
            //認証をまずおこなう
            $login_id = $this->chk_loginSession();
            if($login_id)
            {

                $this->LACNE->library["media"]->set_arrow_filetype(array("jpg","gif","png")); //アップロードできるファイル拡張子

                $image_file = $_FILES['image'];

                //アップロードチェック
                $err = $this->LACNE->library["media"]->check($image_file);

                if(!$err){

                    //アプリからアップロードされる画像ファイル名はすべて image.XXX のため、
                    //ここで、ランダムなファイル名に変更しておく
                    //※アプリ側から画像のアップロードが行われるのは、記事の作成画面で新しく画像を追加した場合、もしくは、
                    // 画像を変更した場合のみである。（記事を編集する場合で、すでに画像が登録されていて、その画像の変更がない場合はアップされない）
                    // そのため、このアップロード処理が行われる度に、ファイル名をランダムな
                    // 名称にして保存させれば、過去にアップされた画像のファイル名とかぶることはないと思われる（2012.08.21)

                    //拡張子取得
                    $file_ext = $this->LACNE->library["media"]->get_file_extension($image_file["name"]);
                    $image_file["name"] = "imgapp_".substr(uniq(), 0 , 10).".".$file_ext;

                    $result = $this->LACNE->library["media"]->do_upload($image_file , "" , true);

                    if(!$result["err_handle"])
                    {
                        //アップロードしたファイルをDBに挿入
                        $insert_id = $this->LACNE->library["media"]->insert_media_data($result);

                        if($insert_id)
                        {
                            $result = array(
                                $this->_xml("status" , 1),
                                $this->_xml("image_id" , $insert_id)
                            );
                        }
                    }

                }

                if(!$result)
                {
                    $err_code = "UploadError";
                }
            }
            else
            {
                //認証エラー※未認証またはセッション切れ
                $err_code = "Unauthorized";
            }

            if($result)
            {
                return $result;
            }

            return $this->err_response($err_code);

        }

        /**
         * 記事データのアップロード
         * @param object $template_list
         */
        function action_data_upload($template_list)
        {
            $result = array();
            $err_code = "";
            //認証をまずおこなう
            $login_id = $this->chk_loginSession();
            if($login_id)
            {

                //記事本文と画像ファイル（あれば）をテンプレートファイルに
                //埋め込み、DBに入れるbodyデータをつくる
                $image = $this->get_post("image");
                if(!$image) $image = "";

                //テンプレートID
                $template_id = 1;
                if($this->get_post("template")) $template_id = $this->get_post("template");

                //テンプレートファイル名
                $viewname = "app";
                if(isset($template_list[$template_id]["viewname"])) $viewname = $template_list[$template_id]["viewname"];

                //Templateファイルのフォルダ指定
                if(isset($template_list[$template_id]["path"]))
                {
                    $this->LACNE->template->setViewDir($template_list[$template_id]["path"]);
                }

                $body_render = array(
                    "title" => $this->get_post("title"),
                    "body"  => nl2br($this->get_post("body" , false)), //body本文はエスケープしない
                    "image_id" => $this->get_post("image")
                );

                $body_data = $this->LACNE->render($viewname , $body_render , true);

                //このデータが編集モードなら記事IDもPOSTデータ内に含まれている
                $post_id = "";
                if($this->get_post("id")) $post_id = $this->get_post ("id");

                $now_datetime = fn_get_date();

                $post_data = array(
                    "id" => $post_id,
                    "title" => $this->get_post("title"),
                    "category" => $this->get_post("category"),
                    "output_date" => $this->get_post("output_date"),
                    "meta_keyword" => $this->get_post("meta_keyword"),
                    "meta_description" => $this->get_post("meta_description"),
                    "body" => $body_data,
                    "app_modified" => $now_datetime,
                    "user_id" => $login_id,

				    /**
				     * ==============================================
				     * 以下、拡張
				     * ==============================================
				     * */
                    "held_date" => $this->get_post("held_date"),
                    "venue" => $this->get_post("venue"),
                    "reservation_flag" => $this->get_post("reservation_flag"),
                );

                //公開フラグ
                $output_flag = 0;
                if($this->get_post("publish") == 1)
                {
                    $output_flag = 1;
                }

                //既存記事のアップデート（編集）なら
                if($post_id)
                {
                    $post_data["output_flag"] = $output_flag;
                    $post_data["modified"] = $now_datetime;
                }
                //新規作成の記事なら
                else
                {
                    $post_data["sort_no"] = 0;
                    $post_data["output_flag"] = $output_flag;
                    $post_data["created"] = $now_datetime;
                    $post_data["modified"] = $now_datetime;
                }

                //DB登録
                $insert_id = $this->LACNE->library['post']->replace_post($post_data , "id" , $login_id);

                //成功
                if($insert_id)
                {

                    //画像ファイル（image）があれば、それをpostmetaに入れる
                    if($this->get_post("image")){
                        $image_id = $this->get_post("image");
                        $image_file = $this->LACNE->library["media"]->get_mediafile($image_id);
                        $_meta_data = array(
                            "img1" => $image_file
                        );

                        $meta_rs = $this->LACNE->library["post"]->replace_meta($insert_id , $_meta_data);
                    }

                    $post_url = $this->LACNE->url_setting->get_pageurl($insert_id,$post_data["category"]); //記事URLを取得
                    $result = array(
                        $this->_xml("status" , 1),
                        $this->_xml("data_id" , $insert_id),
                        $this->_xml("post_url" , $this->LACNE->get_urlpath($post_url)) //記事URLをhttp://からのURLにする
                    );
                }

                if(!$result)
                {
                    $err_code = "UploadError";
                }

            }
            else
            {
                //認証エラー※未認証またはセッション切れ
                $err_code = "Unauthorized";
            }

            if($result)
            {
                return $result;
            }

            return $this->err_response($err_code);
        }


        /**
         * 認証OKでテーブルにそのlogin_idを書き込む
         * @param string $login_id
         */
        function insert_loginSession($login_id)
        {

            //ランダムキー生成
            $connection_id = Session::create_randomkey();

            $ip = fn_getIP();
            $ua = fn_getUA();
            $insert_data = array(
                "connection_id" => $connection_id,
                "login_id"  => $login_id,
                "ip" => ($ip)?$ip:"",
                "ua" => ($ua)?$ua:"",
                "created" => fn_get_date()
            );

            $rs = with(new AppConnect())->replace($insert_data , "id");

            //認証から1時間以上経過しているその他のデータがあればそれも消す
            $limit_time = date("Y-m-d H:i:s",strtotime("-1 hour"));
            $rs = with(new AppConnect())->delete("created <= ?" , array($limit_time));

            return $connection_id;
        }

        /**
         * session_id(connection_id)をKEYとして、有効な接続かチェック
         * ※認証の有効時間は10分程度とする
         * @return string
         */
        function chk_loginSession()
        {
            $session_id = "";
            $login_id = "";
            if($this->get_post("session_id"))
            {
                $session_id = $this->get_post("session_id");
                $rs = "";

                if (preg_match("/^[0-9a-z]*$/i", $session_id)){

                    $rs = with(new AppConnect())->fetchOneByConnectionID($session_id);

                    if($rs && isset($rs["created"]) && isset($rs["login_id"]))
                    {
                        //有効時間内かどうか
                        $created_time = strtotime($rs["created"]);
                        $limit_time = strtotime("-10 minute");

                        if($created_time > $limit_time)
                        {
                            $login_id = $rs["login_id"];
                        }
                    }
                }
            }

            if(!$login_id)
            {
                //認証エラー
                $this->output_xml($this->err_response("Unauthorized"));
            }

            return $login_id;
        }


        function _xml($name="",$value="",$param=array())
        {
            return new SimpleTag($name,$value,$param);
        }

        /**
         * エラーレスポンスデータ（status = 0で固定）
         * @param string $err_code
         * @return object
         */
        function err_response($err_code)
        {
            $result = array(
                $this->_xml("status" , "0"),
                $this->_xml("error" , $err_code)
            );

            return $result;
        }


        function output_xml($obj)
        {
            $xml = $this->_xml("response" , $obj);
            $xml->output();

            exit;
        }

}

?>