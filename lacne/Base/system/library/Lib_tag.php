<?php
use lacne\core\Library;
use lacne\core\model\Tag;
/**
// ------------------------------------------------------------------------
 * Lib_tag.php
 *
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class Lib_tag extends Library
{

	/**
	 * Lib_tag constructor.
	 * @param $LACNE
	 * @param $db
	 */
	function Lib_tag($LACNE , $db) {
        parent::__construct($LACNE, $db);
	}

	// TODO リファクタ　オウンドメディア用でまとめる
	/**
	 * タグ選択プルダウン用のタグ登録リストを取得
	 *
	 * @return object
	 */
	function get_parent_tag_list()
	{
		$result = with(new Tag())->fetchAllParentTag();

		$tag_data = array();
		if($result && count($result))
		{
			foreach($result as $value)
			{
				$tag_data[$value["id"]] = $value["tag_name"];
			}
		}
		return $tag_data;
	}

	function get_tag_id($tag_name)
	{
		$result = with(new Tag())->fetchOneByName($tag_name);
		return (int) $result['id'];
	}

}

?>