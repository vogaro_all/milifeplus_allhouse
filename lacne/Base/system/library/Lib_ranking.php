<?php
use lacne\core\Library;
use lacne\core\model\Ranking;

/**
// ------------------------------------------------------------------------
 * Lib_ranking.php
 * 
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class Lib_ranking extends Library
{

    /** @var string $upload_dir  アップロード先*/
    var $upload_dir;

    /** @var string $upload_path  アップロード先のパス（絶対パス)*/
    var $upload_path;

    /** @var array  $filetype  アップロード可能拡張子*/
    var $filetype = array("csv");

    /** @var number  $maxfilesize  アップロード可能なサイズ*/
    var $maxfilesize = IMG_SAVE_MAX_SIZE;

    //画像以外のファイルを一覧表示させる際のファイルタイプ別アイコン画像
    var $etcfile_thumb_arr = array(
        "pdf" => MEDIA_ICONFILE_PDF
    );

    /** @var string $upload_error */
    var $upload_error = "";

    /**
     * Lib_post constructor.
     * @param $LACNE
     * @param $db
     */
	function Lib_ranking($LACNE , $db) {
        parent::__construct($LACNE, $db);
        $this->upload_dir = realpath(LACNE_APP_UPLOAD_DIR.'/csv/');
        $this->upload_path = LACNE::get_path_docroot($this->upload_dir);
        $this->LACNE->load_library(array('validation', 'gapi'));
	}
    
    /**
     * 
     * @param $LACNE
     * @param $db
     */
    function get_data() {
        return with(new Ranking())->fetchRanking();
    }

    /**
     *
     * @param $LACNE
     * @param $db
     */
    function get_csv_data() {
    	return with(new Ranking())->fetchRankingDownload();
    }
    
    /**
     * download_csv
     * @param $LACNE
     * @param $db
     */
    function download_csv($filename, $data) {
        try {
            $filepass = 'csvdata/'.$filename.'.csv';
            $res = fopen($filepass, 'w+');

            if ($res === FALSE) {
                throw new Exception('ファイルの書き込みに失敗しました。');
            }
            $sub = array('ID','ページ名','URL','PV');

            fputcsv($res, $sub);

            foreach($data as $dataInfo) {
                fputcsv($res, $dataInfo);
            }
            rewind($res);
            $content = str_replace(PHP_EOL, "\r\n", stream_get_contents($res));
            rewind($res);
            ftruncate($res,0); 
            fwrite($res,  mb_convert_encoding($content, 'SJIS-win', 'UTF-8'));
            fclose($res);
            //header('Content-Type: text/csv');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.$filename.'.csv');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: ' . filesize($filepass));
            readfile($filepass);
            //ダウンロード後削除
            unlink($filepass);
            exit;
        } catch(Exception $e) {
            return $e->getMessage();
        }
    }

//     /**
//      * 
//      * @param $LACNE
//      * @param $db
//      */
//     function get_pv_pickup(){
//         return $this->LACNE->library['gapi']->get_pv_pickup_data();
//     }

//     /**
//      * アップロードしたメディアデータ（ファイル名）をDBに保存する
//      * @param string $filename
//      */
//     function insert_ga_pickup_data($ga_data)
//     {
//         foreach ($ga_data as $key => $val) {
//             $insert_data[$key] = array(
//                 "id"       => NULL,
//                 "title"    => $val[0],
//                 "url"      => $val[1],
//                 "pv"       => $val[2]
//             );
//         }
//         if(!empty($insert_data)){
//             with(new Ranking())->delete("1");
//             return with(new Ranking())->replace($insert_data , "id");
//         }
//         return false;
//     }

    /**
     * ファイルのアップロード処理を実行
     * @param object $param
     *  |- post : $_POSTデータ
     *  |- media_file
     *  |- csrf_check
     * @return object
     */
    function upload_process($param, $delete_flag = false)
    {

        // リネームする場合はファイルを置換
        if($param["post"]["rename"] && $param["media_file"]["name"]){
            $ext = $this->get_file_extension($param["media_file"]["name"]);
            $param['media_file']["name"] = $param["post"]["rename"] . "." . $ext;
        }

        //アップロードチェック
        $err =  $this->check($param['media_file']);

        //CSRF TOKENチェック
        if(!empty($param['csrf_check']) && $param['csrf_check'])
        {
            $csrf_check = false;
            if(!empty($param['post']["token"]))
            {
                $csrf_check =  $this->LACNE->request->csrf_check($param['post']["token"]);
            }
            if(!$csrf_check) $err = "データの受け渡しで問題が発生しました。もう一度操作をやり直して下さい。";
        }

        if(!$err && !empty($param['media_file'])){
            $filename = 'rankingCSV'.date('YmdHis').'.csv';
            $result =  $this->do_upload($param['media_file'], $filename);

            if($result && isset($result["filename"]))
            {
                //アップロードしたファイルをDBに挿入
                $insert =  $this->insert_csv_ranking_data($result , fn_esc($param['post']["tag"]));

                //ファイル内容登録後に削除($delete_flagfで判断する)
                if($delete_flag == true){
                    unlink($this->upload_dir.'/'.$filename);
                }

            }
            else
            {
                //アップロード失敗
                $err =  $this->get_upload_report();
            }
        }

        if(!$err && $insert){
            //登録完了フラグ
            return array(
                'status' => 'success',
                'message' => "ファイルをアップロードしました。"
            );
        }
        else
        {
            return array(
                'status' => 'error',
                'err' => $err
            );
        }
    }

    /**
     * ファイルのチェック
     * @param object $fileobj アップロードファイル $_FILE[XXX]
     *
     * @return object
     */
    function check($fileobj)
    {

        $err = "";

        if(empty($fileobj))
        {
            return "ファイルのアップロードに失敗しました。再度ファイルの中身を確認して下さい。";
        }

        if(!isset($fileobj["name"]) || !$fileobj["name"])
        {
            $err = "ファイルを選択して下さい。";
        }

        if(!$err){
            if (!preg_match("/^[a-zA-Z0-9_\-\.]+$/",$fileobj["name"])) {
                $err = "ファイル名は半角英数字記号（ハイフン、アンダーバーのみ）をご利用下さい。";
            }
        }

        //拡張子のチェック
        if(!$err){
            //拡張子
            $file_ext = $this->get_file_extension($fileobj["name"]);
            $match = 0;
            foreach($this->filetype as $type_val)
            {
                if($file_ext == $type_val)
                {
                    $match = 1;
                    break;
                }
            }
            if(!$match)
            {
                $errstr = "アップロードできるファイルは";
                $errstr .= implode("," , $this->filetype);
                $errstr .= "のみです。";
                $err = $errstr;
            }
        }

        //サイズチェック
        if(!$err){
            $filesize = ceil($fileobj['size']/1024);
            if($filesize > $this->maxfilesize){
                    $err = "ファイルサイズが大きすぎます。(容量が".$this->maxfilesize."キロバイトまでとなります。)";
            }
        }

        return $err;
    }

    /**
     *　ファイルのアップロード処理
     * @param object $fileobj  アップロードファイルオブジェクト $_FILE[XXX]
     * @param sstring $filename  アップロード設置する際、ファイル名を書き換えたい場合に指定
     * @param  boolean $overwrite (同名のファイル名がある場合、上書きするか　true , false)
     * @return boolean
     */
    function do_upload($fileobj , $filename = "" , $overwrite = true){

        $this->upload_error = "";

        if(!$this->upload_dir)
        {
            $this->upload_error = "アップロード先が指定されていません。";
        }
        else
        {
            if(!$filename) $filename = 'rankingCSV'.date('YmdHis').'.csv';
            //ファイル名に「.ファイル拡張子」と同じ文字列が混ざっている場合、その部分をリネームする
            $file_ext = substr($filename,-3);
            $filename = str_replace(".".$file_ext , "" , $filename);
            $filename .= ".".$file_ext;

            $upload_file = $this->upload_dir."/".$filename;

            if($overwrite && file_exists($upload_file))
            {
                if(is_file($upload_file))
                {
                    unlink($upload_file);
                }
                else
                {
                    $this->upload_error = "同名のファイルが存在しており、それを削除することができません。";
                }
            }

            if(!file_exists($upload_file)){

                if($fileobj['binary']) {
                    $result = file_put_contents($upload_file, $fileobj['binary']);
                } else {
                    $result = move_uploaded_file($fileobj["tmp_name"], $upload_file);
                }

                if($result){

                    //パーミッション変更
                    chmod($upload_file, 0644);

                    //拡張子
                    $file_ext = $this->get_file_extension($upload_file);

                    return array(
                        "filename" => $filename,
                        "upload_path" => $this->upload_dir,
                        "file_ext" => $file_ext
                    );
                } else {
                    $this->upload_error = "アップロードに失敗しました。再度お試し下さい。";
                    return false;
                }
            } else {
                $this->upload_error = "同名のファイルが存在しており、アップロードすることができません。";
                return false;
            }
        }
    }

    /**
     * アップロード処理の結果を得る
     * エラーがあればエラー文を返し、成功時は空文字が返る
     */
    function get_upload_report()
    {
        return $this->upload_error;
    }

    /**
     * アップロードしたメディアデータ（ファイル名）をDBに保存する
     * @param string $filename
     */
    function insert_csv_ranking_data($fileinfo , $tag_str = "")
    {

        if($fileinfo && isset($fileinfo["filename"]) && isset($fileinfo["file_ext"]))
        {
            $result = array();
            $insert_data = array();
            $filepass = $fileinfo["upload_path"].'/'.$fileinfo['filename'];
            ini_set('auto_detect_line_endings', 1);
            $fp = fopen($filepass, 'r');

            $num = 0;
            while( $csv_data = $this->fgetcsv_reg( $fp, 256 ) ) {
                for($i = 0; $i < count( $csv_data ); ++$i ){
                    mb_convert_variables('UTF-8', 'SJIS-win', $csv_data[$i]);
                    $result[$num][$i] = $csv_data[$i];
                }
                $num++;
            }
            fclose($fp);

            foreach($result as $key => $val) {
                $insert_data[$key] = array(
                    "id"       => NULL,
                    "title"    => $val[1],
                    "url"      => $val[2],
                    "pv"       => $val[3]
                );
            }

            if(count($insert_data) == 1 && $insert_data[0]['title'] == 'ページ名')
            {
                with(new Ranking())->delete("1");
                return 1;
            }

            unset($insert_data[0]);

            if(!empty($insert_data)){
                with(new Ranking())->delete("1");
                return with(new Ranking())->replace($insert_data , "id");
            }
            return false;
        }
        return false;
    }

    /**
     * アップロードファイル容量取得
     */
    function get_maxfilesize()
    {
        return $this->maxfilesize;
    }

    /**
     * アップロードファイル拡張子取得
     */
    function get_file_extension($filepath)
    {
        $file_info = pathinfo($filepath);
        if(isset($file_info["extension"]))
        {
            return strtolower($file_info["extension"]);
        }

        return "";
    }

    /**
     * CSV読み込み時のfget関数の場合、先頭文字が文字化けしたままになるので独自関数実装
     */
    function fgetcsv_reg (&$handle, $length = null, $d = ',', $e = '"') {
        $d = preg_quote($d);
        $e = preg_quote($e);
        $_line = "";
        
        while ($eof != true) {
            $_line .= (empty($length) ? fgets($handle) : fgets($handle, $length));
            $itemcnt = preg_match_all('/'.$e.'/', $_line, $dummy);
            if ($itemcnt % 2 == 0) $eof = true;
        }

        $_csv_line = preg_replace('/(?:\\r\\n|[\\r\\n])?$/', $d, trim($_line));
        $_csv_pattern = '/('.$e.'[^'.$e.']*(?:'.$e.$e.'[^'.$e.']*)*'.$e.'|[^'.$d.']*)'.$d.'/';
        preg_match_all($_csv_pattern, $_csv_line, $_csv_matches);
        $_csv_data = $_csv_matches[1];
        
        for($_csv_i=0;$_csv_i<count($_csv_data);$_csv_i++){
            $_csv_data[$_csv_i]=preg_replace('/^'.$e.'(.*)'.$e.'$/s','$1',$_csv_data[$_csv_i]);
            $_csv_data[$_csv_i]=str_replace($e.$e, $e, $_csv_data[$_csv_i]);
        }

        return empty($_line) ? false : $_csv_data;
    }
}

?>