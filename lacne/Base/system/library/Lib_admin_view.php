<?php
use lacne\core\Library;
use lacne\core\model\Admin;

/**
// ------------------------------------------------------------------------
 * Lib_social_btn.php
 * ソーシャルボタン設置用
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class Lib_admin_view extends Library
{
	
    //各一覧表示画面の表示数
    //スマフォ対応用はoption側のファイルに
    var $list_num_per_page = array(
        "index_index"   => 10,
        "article_index" => 10,
        "media_index"   => 20,
        "media_list"    => 20
    );

    //パスワード変更を定期的にアナウンスするか
    //0 : アナウンスしない
    //1以上の数値（X) : Xヶ月ごとにアナウンスする
    var $pass_change_alert = 3;

    /**
     * Lib_admin_view constructor.
     * @param $LACNE
     * @param $db
     */
    function Lib_admin_view($LACNE , $db) {
        parent::__construct($LACNE, $db);
    }
    
    
    /*
     * デバイス判別（スマートフォンかどうか）
     * @param void
     * @return boolean
    */
    function is_smph () {

        return false;
    }
    function device_type() {
        return "Pc";
    }
    function is_browser_IE()
    {
        $agent = getenv("HTTP_USER_AGENT");
        if(mb_ereg("MSIE", $agent))
        {
            return true;
        }
        return false;
    }
    
    /**
     * 各デバイス（pc or smp）用に作成されたjsファイルをロード
     * pc : temp_pc.php  smp : temp_smp.php としてファイル設置
     * Templateのrenderを利用しているため、変数を渡しそれを埋め込むことも可能
     *  
     * @param string $path
     * @param string $pagename
     * @param object $vars
     * @return string 
     */
    function load_js_opt_device($path , $pagename , $vars = array())
    {
        $view_dir = $this->LACNE->template->getViewDir();
        $this->LACNE->template->setViewDir($path."/".$pagename);
        $vars = array_merge($vars , array("device"=>"pc"));
        
        $js_code .= $this->LACNE->render("pc" , $vars , true , false); //temp_pc.phpはPC用jsコードのみでなく共有コードも含むため必ずロードさせる　※第４引数をfalseに エスケープをしない
        
        $this->LACNE->template->setViewDir($view_dir);
        return $js_code;
    }
    
    /**
     * 一覧ページの1Pあたりの表示件数
     * @param string $page 
     * @return number
     */
    function get_num_per_page($pagename , $device = "")
    {
        if(isset($this->list_num_per_page[$pagename]))
        {
            return $this->list_num_per_page[$pagename];
        }
        
        return NUM_LIST;
    }
    
    
    /**
     * login_id値からアカウント名を取得する
     * @param string $login_id 
     */
    function get_login_name($login_id){ return with(new Admin())->get_account_name($login_id); }
        
    
    /**
     * 管理画面中のモーダルウインドウ生成用
     * @param string $id
     * @param object $vars
     * @return string 
     */
    function html_modal_open($id ,$vars=array())
    {
        
        $vars = array_merge($vars , array("id"=>$id));
        
        $view_dir = $this->LACNE->template->getViewDir();
        $this->LACNE->template->setViewDir(LACNE_SHARE_TEMPLATE_DIR."/modal");
        $modal_code = $this->LACNE->render("modal_open" , $vars , true , false);
        
        $this->LACNE->template->setViewDir($view_dir);
        return $modal_code;
    }
    function html_modal_close($vars=array())
    {
        
        $view_dir = $this->LACNE->template->getViewDir();
        $this->LACNE->template->setViewDir(LACNE_SHARE_TEMPLATE_DIR."/modal");
        $modal_code = $this->LACNE->render("modal_close" , $vars , true , false);
        
        $this->LACNE->template->setViewDir($view_dir);
        return $modal_code;
    }
    

    /**
     * パスワードの変更アナウンスをしなければならないか
     * @param string $login_id 
     */
    function is_passchange_alert($login_id) {

        $alert_flag = false;
        $update_flag = false;
        if(!empty($login_id) && $this->pass_change_alert > 0) {
            $rs_data = with(new Admin())->fetchOneByLoginID($login_id);
            if($rs_data["passchange_alert"] == "0000-00-00 00:00:00") {
                //前回のパスワード変更日時が記録なしであれば、現在の日時を入れる
                $update_flag = true;
                
            } else {
                //前回のアナウンスから指定した期間経過しているか
                if(strtotime($rs_data["passchange_alert"]." +".$this->pass_change_alert." month") < strtotime("now")) {
                    //アナウンス日時を更新
                    $update_flag = true;
                    $alert_flag = true;
                }
            }
        }

        if($update_flag === TRUE) {
            //アナウンス日時を更新
            $updata = array(
                "login_id" => $login_id,
                "passchange_alert" => fn_get_date()
            );
            $result = with(new Admin())->replace($updata , "login_id");
        }

        return $alert_flag;
    }

}

?>