<?php
use lacne\core\Library;
use lacne\core\model\Post;

/**
// ------------------------------------------------------------------------
 * Lib_effect.php
 * 
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class Lib_effect_csv extends Library
{
    const TARGET_VIEW = 'OWNED';

    private $SOCIALS_EVENT_LABEL = array(
        'FBシェア',
        'FBいいね',
        'Twitterシェア',
        'google+',
        'hatenabookmark',
        'pocket',
    );

    private $EVENT_LABEL = array(
        '50%',
        '100%',
    );

	function Lib_effect($LACNE , $db) {
        parent::__construct($LACNE, $db);
        $this->LACNE->load_library(array('post', 'gapi'));
        $this->LACNE->load_model(array('post'));
        $this->event_target_filters = ';ga:eventLabel=='.implode(',ga:eventLabel==', $this->EVENT_LABEL);
	}


    public function getPageData($search_date_from, $search_date_to, $template_flag = false)
    {
        try {
            $analytics = $this->LACNE->library['gapi']->get_service_multi(self::TARGET_VIEW);
            $results = $analytics->data_ga->get(
                'ga:' . constant('VIEW_ID_'.self::TARGET_VIEW),
                $search_date_from,
                $search_date_to,
                implode(",", array(
                    'ga:pageviews', // PV
                    'ga:sessions', //セッション
                    'ga:visitors', // UU
                    'ga:bounces', // 直帰数
                    'ga:exits', //離脱数
                    'ga:entrances', //閲覧開始数
                )),
                array(
                    'dimensions' => implode(",", 
                        $template_flag ? array('ga:dimension3') : array('ga:pagePath', 'ga:pageTitle')
                    ),
                    'filters' => DETAIL_FILTERS
                )
            );
            $analytics_data = $results->getRows();
        } catch (Exception $e) {
            $err['get'] = 'データ取得に失敗しました。';
        }

        $post_data = array();
        if ($analytics_data) {
            foreach ($analytics_data as $data) {
                $row_id = $data[0];
                if (!$template_flag) {
                    if (!preg_match('/\/detail\/(\d+)/', $data[0], $match)) {
                        continue;
                    }
                    $row_id = $match[1];
                }
                if (!isset($post_data[$row_id])) {
                    $post_data[$row_id] = array(
                        'pageviews' => 0,
                        'sessions' => 0,
                        'visitors' => 0,
                        'bounces' => 0,
                        'exits' => 0,
                        'entrances' => 0,
                    );
                }
                if ($template_flag) {
                    $post_data[$row_id]['pageviews'] += $data[1];
                    $post_data[$row_id]['sessions'] += $data[2];
                    $post_data[$row_id]['visitors'] += $data[3];
                    $post_data[$row_id]['bounces'] += $data[4];
                    $post_data[$row_id]['exits'] += $data[5];
                } else {
                    $post_data[$row_id]['pageTitle'] = $data[1];
                    $post_data[$row_id]['pageviews'] += $data[2];
                    $post_data[$row_id]['sessions'] += $data[3];
                    $post_data[$row_id]['visitors'] += $data[4];
                    $post_data[$row_id]['bounces'] += $data[5];
                    $post_data[$row_id]['exits'] += $data[6];
                    $post_data[$row_id]['entrances'] += $data[7];
                }

            }
        }
        return $post_data;
    }

    public function getEventData($search_date_from, $search_date_to, $template_flag = false)
    {
        try{
            $analytics = $this->LACNE->library['gapi']->get_service_multi(self::TARGET_VIEW);
            $results = $analytics->data_ga->get(
                'ga:' . constant('VIEW_ID_'.self::TARGET_VIEW),
                $search_date_from,
                $search_date_to,
                'ga:totalEvents',
                array(
                    'dimensions' => implode(",",array(
                        $template_flag ? 'ga:dimension3' : 'ga:pagePath',
                        'ga:eventLabel',
                    )),
                    'max-results' => DISPLAY_COUNT*count($this->EVENT_LABEL),
                    'filters' => READ_EVENT_FILTERS.$this->event_target_filters
                )
            );
            $analytics_data = $results->getRows();
        } catch (Exception $e) {
            $err['get'] = 'データ取得に失敗しました。';
        }

        $post_data = array();
        if ($analytics_data) {
            foreach ($analytics_data as $data) {
                $row_id = $data[0];
                if (!$template_flag) {
                    if (!preg_match('/\/detail\/(\d+)/', $data[0], $match)) {
                        continue;
                    }
                    $row_id = $match[1];
                }
                if (!isset($post_data[$row_id])) {
                    $post_data[$row_id] = array(
                        'finishReadingRate50' => 0,
                        'finishReadingRate100' => 0
                    );
                }
                if ($data[1] == '50%') {
                    $post_data[$row_id]['finishReadingRate50'] += $data[2];
                }
                if ($data[1] == '100%') {
                    $post_data[$row_id]['finishReadingRate100'] += $data[2];
                }
            }
        }

        return $post_data;
    }

    function getCompletionData($search_date_from, $search_date_to, $template_flag = false){
        try {
            $analytics = $this->LACNE->library['gapi']->get_service_multi(self::TARGET_VIEW);
            $results = $analytics->data_ga->get(
                'ga:' . constant('VIEW_ID_'.self::TARGET_VIEW),
                $search_date_from,
                $search_date_to,
                'ga:goal1Completions,ga:goal2Completions',
                array(
                    'dimensions' => $template_flag ? 'ga:dimension3' : 'ga:goalCompletionLocation',
                    'filters' => 'ga:goalCompletionLocation=~/detail/(\d)'
                )
            );
            $analytics_data = $results->getRows();
        } catch (Exception $e) {
            $err['get'] = 'データ取得に失敗しました。ERR-01';
        }

        $post_data = array();
        if ($analytics_data) {
            foreach ($analytics_data as $data) {
                $row_id = $data[0];
                if (!$template_flag) {
                    if (!preg_match('/\/detail\/(\d+)/', $data[0], $match)) {
                        continue;
                    }
                    $row_id = $match[1];
                }
                if (!isset($post_data[$row_id])) {
                    $post_data[$row_id] = array(
                        "download" => 0, // DL数
                        "customerTransfer" => 0, // 送客数
                    );
                }
                $post_data[$row_id]["customerTransfer"] += $data[1];
                $post_data[$row_id]["download"] += $data[2];
            }
        }

        return $post_data;
    }


    function getSocialData($search_date_from, $search_date_to, $template_flag = false)
    {
        try {
            $analytics = $this->LACNE->library['gapi']->get_service_multi(self::TARGET_VIEW);
            $results = $analytics->data_ga->get(
                'ga:' . constant('VIEW_ID_'.self::TARGET_VIEW),
                $search_date_from,
                $search_date_to,
                'ga:totalEvents',
                array(
                    'dimensions' => implode(",",array(
                        $template_flag ? 'ga:dimension3' : 'ga:pagePath',
                        'ga:eventAction', //　イベント
                    )), // 副指標ごと
                    //'sort' => $sort_target, // - を付けると降順ソート
                    'max-results' => DISPLAY_COUNT, // 取得件数
                    'filters' => 'ga:eventCategory=~share|side|list'
                )
            );
            $analytics_data = $results->getRows();
        } catch (Exception $e) {
            $err['get'] = 'データ取得に失敗しました。';
        }

        $post_data = array();
        if ($analytics_data) {
            foreach ($analytics_data as $data) {
                $row_id = $data[0];
                if (!$template_flag) {
                    if (!preg_match('/\/detail\/(\d+)/', $data[0], $match)) {
                        continue;
                    }
                    $row_id = $match[1];
                }
                if (!isset($post_data[$row_id])) {
                    $post_data[$row_id] = array(
                        'fb_share' => 0,
                        'fb_like' => 0,
                        'twitter' => 0,
                        'google' => 0,
                        'hatena' => 0,
                        'pocket' => 0,
                    );
                }

                $event_key = array_search($data[1], $this->SOCIALS_EVENT_LABEL);
                if ($event_key !== false) {
                    switch ($event_key) {
                        case 0 :
                            $post_data[$row_id]['fb_share'] += $data[2];
                            break;
                        case 1 :
                            $post_data[$row_id]['fb_like'] += $data[2];
                            break;
                        case 2 :
                            $post_data[$row_id]['twitter'] += $data[2];
                            break;
                        case 3 :
                            $post_data[$row_id]['google'] += $data[2];
                            break;
                        case 4 :
                            $post_data[$row_id]['hatena'] += $data[2];
                            break;
                        case 5 :
                            $post_data[$row_id]['pocket'] += $data[2];
                            break;
                    }
                }
            }
        }

        return $post_data;
    }

    function getPostData()
    {
        $postModel = new Post();
        $postData = $postModel->fetchAll();
        $data = array();
        foreach($postData as $v) {
            $data[$v['id']] = array(
                'title' => $v['title'],
                'output_date' => $v['output_date'],
            );
        }
        return $data;
    }


    function integral($search_date_from, $search_date_to, $template_flag = false)
    {
        $page_data = $this->getPageData($search_date_from, $search_date_to, $template_flag);
        $event_data = $this->getEventData($search_date_from, $search_date_to, $template_flag );
        $completion_data = $this->getCompletionData($search_date_from, $search_date_to, $template_flag );
        $social_data = $this->getSocialData($search_date_from, $search_date_to, $template_flag );
        $post_data = $this->getPostData();

        $data = array();
        $sort_key = array();
        foreach($page_data as $row_id => $page) {
            if (isset($event_data[$row_id])) {
                $page = array_merge($page, $event_data[$row_id]);
            } else {

            }
            $page = array_merge($page,
                isset($event_data[$row_id])
                    ? $event_data[$row_id]
                    : array('finishReadingRate50' => 0, 'finishReadingRate100' => 0),
                isset($completion_data[$row_id])
                    ? $completion_data[$row_id]
                    : array('download' => 0, 'customerTransfer' => 0),
                isset($social_data[$row_id])
                    ? $social_data[$row_id]
                    : array('fb_share' => 0, 'fb_like' => 0, 'twitter' => 0, 'google' => 0, 'hatena' => 0, 'pocket' => 0)
            );
            if (!$template_flag) {
                $page['pageTitle'] = isset($post_data[$row_id]) ? $post_data[$row_id]['title'] : $page['pageTitle'];
                $page['pagePath'] = '/detail/'.$row_id;
                $page['output_date'] = isset($post_data[$row_id]) ? $post_data[$row_id]['output_date'] : '';
            } else {
                $page['template'] = $row_id;
            }
            $page['bounceRate'] = $this->rating($page['bounces'], $page['sessions']);
            $page['exitRate'] = $this->rating($page['exits'], $page['pageviews']);
            $page['downloadRate'] = $this->rating($page['download'], $page['pageviews']);
            $page['customerTransferRate'] = $this->rating($page['customerTransfer'], $page['pageviews']);
            $data[$row_id] = $page;
            $sort_key[] = $page['pageviews'];
        }

        array_multisort($sort_key, SORT_DESC, $data);

        return $data;
    }

    function downloadCsvByContents($search_date_from, $search_date_to){

        $header = array(
            'pageTitle' => 'タイトル',
            'pagePath' => 'URL',
            'output_date' => '公開日',
            'pageviews' => 'PV',
            'visitors' => 'UU',
            'entrances' => '閲覧開始数',
            'bounceRate' => '直帰率',
            'exitRate' => '離脱率',
            'finishReadingRate50' => '50%読了',
            'finishReadingRate100' => '100%読了',
            'download' => 'DL数',
            'downloadRate' => 'DL率',
            'customerTransfer' => '送客数',
            'customerTransferRate' => '送客率',
            'fb_like' => 'FBいいね',
            'fb_share' => 'FBシェア',
            'twitter' => 'twitter',
            'google' => 'google+',
            'hatena' => 'はてブ',
            'pocket' => 'pocket',
        );

        $body = $this->integral($search_date_from, $search_date_to, false);

        $this->download($header, $body, "owned_".$search_date_from."-".$search_date_to.".csv");
    }

    function downloadCsvByTemplate($search_date_from, $search_date_to){

        $header = array(
            'template' => 'テンプレート',
            'pageviews' => 'PV',
            'visitors' => 'UU',
            'bounceRate' => '直帰率',
            'exitRate' => '離脱率',
            'finishReadingRate50' => '50%読了',
            'finishReadingRate100' => '100%読了',
            'download' => 'DL数',
            'downloadRate' => 'DL率',
            'customerTransfer' => '送客数',
            'customerTransferRate' => '送客率',
            'fb_share' => 'FBシェア',
            'fb_like' => 'FBいいね',
            'twitter' => 'twitter',
            'google' => 'google+',
            'hatena' => 'はてブ',
            'pocket' => 'pocket',
        );

        $body = $this->integral($search_date_from, $search_date_to, true);

        $this->download($header, $body, "owned_".$search_date_from."-".$search_date_to.".csv");
    }

    function download($header, $body, $file_name){

        $data_list = array();
        $data_list[] = array_values($header);
        foreach ($body as $key => $value) {
            $export_list = array();
            foreach ($header as $key1 => $value1) {
                $export_list[] = $value[$key1]; //mb_convert_encoding ( $value[$key1] , "SJIS" , 'UTF-8' );
            }
            $data_list[] = $export_list;
        }
        $fp = fopen('php://temp/maxmemory:10485760','w');
        foreach($data_list as $datum){
            //mb_convert_variables("SJIS","UTF-8", $datum);
            fputcsv($fp, $datum, ',','"');
        }
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=".$file_name);
        rewind($fp);
        print stream_get_contents($fp);
        fclose($fp);
        exit();
    }

    function rating($child, $parent)
    {
        $rate =  $parent > 0 ? $child / $parent : 0;
        $rate = round($rate * 100, 2) . '%';
        return $rate;
    }
}

?>