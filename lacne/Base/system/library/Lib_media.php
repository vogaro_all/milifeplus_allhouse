<?php
use lacne\core\Library;
use lacne\core\model\Media;
use Intervention\Image\ImageManager;
use Intervention\Image\Image as Image;
/**
// ------------------------------------------------------------------------
 * Lib_media
 *
 * ファイルアップロード処理用
 * @package		LACNE
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

define("MEDIA_ICONFILE_PDF" , LACNE_SHAREDATA_PATH."/images/common/thumb_pdf.jpg");

class Lib_media extends Library
{

    /** @var string $upload_dir  アップロード先*/
    var $upload_dir;

    /** @var string $upload_path  アップロード先のパス（絶対パス)*/
    var $upload_path;

    /** @var array  $filetype  アップロード可能拡張子*/
    var $filetype = array("jpg" , "gif" , "png" , "pdf");

    /** @var number  $maxfilesize  アップロード可能なサイズ*/
    var $maxfilesize = IMG_SAVE_MAX_SIZE;

    //画像以外のファイルを一覧表示させる際のファイルタイプ別アイコン画像
    var $etcfile_thumb_arr = array(
        "pdf" => MEDIA_ICONFILE_PDF
    );

    /** @var string $upload_error */
    var $upload_error = "";

    /**
     * Lib_media constructor.
     * @param $LACNE
     * @param $db
     */
    function Lib_media($LACNE , $db) {
        parent::__construct($LACNE, $db);

        $this->upload_dir = realpath(LACNE_APP_UPLOAD_DIR);
        $this->upload_path = LACNE::get_path_docroot($this->upload_dir);

        $this->LACNE->load_library(array('validation'));
    }

	/**
	 * ファイルのアップロード処理を実行
	 * @param object $param
	 *	|- post : $_POSTデータ
	 *  |- media_file
	 *  |- csrf_check
	 * @return object
	 */
	function upload_process($param)
	{

        // リネームする場合はファイルを置換
        if($param["post"]["rename"] && $param["media_file"]["name"]){
            $ext = $this->get_file_extension($param["media_file"]["name"]);
            $param['media_file']["name"] = $param["post"]["rename"] . "." . $ext;
        }

		//アップロードチェック
		$err =  $this->check($param['media_file']);

		//CSRF TOKENチェック
		if(!empty($param['csrf_check']) && $param['csrf_check'])
		{
			$csrf_check = false;
			if(!empty($param['post']["token"]))
			{
				$csrf_check =  $this->LACNE->request->csrf_check($param['post']["token"]);
			}
			if(!$csrf_check) $err = "データの受け渡しで問題が発生しました。もう一度操作をやり直して下さい。";
		}
        if(!$err)
        {
            //タグテキストの入力チェック
            if(empty($param['post']["tag"])) $param['post']["tag"] = "";
            $err_check_arr = array(
                "tag"=>array("name"=>"タグ","type"=>array("len"),"length"=>200)
            );
            //バリデーション実行
            $err_tag = $this->LACNE->library["validation"]->check($param['post'],$err_check_arr);
            if($err_tag) {
                $err = $err_tag["tag"];
            }
        }

		if(!$err && !empty($param['media_file'])){

			$result =  $this->do_upload($param['media_file']);

			if($result && isset($result["filename"]))
			{
				$this->LACNE->load_library(array('output'));

				if(!empty($result['file_ext']) && $this->get_media_type($result["file_ext"]) == "image")
				{
					//ファイルタイプがimageなら、リサイズしてサムネイル用に(縦サイズ指定）
					$this->LACNE->library["output"]->_img_resize("thumb" , $this->upload_path."/".$result["filename"] , '' , $this->get_thumb_height());
					//Mobile_outputオプションが有効なら、携帯用とスマフォ用のリサイズ画像も書き出し
					if(defined('LACNE_OPTION_MOBILE_OUTPUT') && LACNE_OPTION_MOBILE_OUTPUT)
					{
						$this->LACNE->library["output"]->_img_resize("smph" ,  $this->upload_path."/".$result["filename"]);
						$this->LACNE->library["output"]->_img_resize("mobi" ,  $this->upload_path."/".$result["filename"]);

                        // 2017/11/27 画像圧縮処理追加
                        global $_OWNED_DETAIL_IMAGE_WIDTH;
                        $prefix = "dtil";
                        try{
                            $manager = new ImageManager(array('driver' => 'imagick'));
                            $targetImage = $manager->make($result["upload_path"]."/".$result["filename"]);

                            $resizedfilename = $result["upload_path"]."/".$targetImage->filename."_".$prefix.".".$targetImage->extension;

                            if(file_exists($resizedfilename)) throw new Exception();

                            $width = $targetImage->width();
                            if($width >= $_OWNED_DETAIL_IMAGE_WIDTH)
                            {
                                $image = $targetImage->resize($_OWNED_DETAIL_IMAGE_WIDTH, null, function($constraint){
                                    $constraint->aspectRatio();
                                });
                                $image->save($resizedfilename);
                            }
                            else
                            {
                                $targetImage->save($resizedfilename);
                            }

                        }catch(\Exception $e){
                            // 画像を作成しない
                        }
					}
				}
				//アップロードしたファイルをDBに挿入
				$insert_id =  $this->insert_media_data($result , fn_esc($param['post']["tag"]));
			}
			else
			{
				//アップロード失敗
				$err =  $this->get_upload_report();
			}
		}

		if(!$err && $insert_id){
			//登録完了フラグ
			return array(
				'status' => 'success',
				'message' => "ファイルをアップロードしました。"
			);
		}
		else
		{
			return array(
				'status' => 'error',
				'err' => $err
			);
		}
	}

    /**
     * ファイルのチェック
     * @param object $fileobj アップロードファイル $_FILE[XXX]
     *
     * @return object
     */
    function check($fileobj)
    {

        $err = "";

        if(!isset($fileobj["name"]) || !$fileobj["name"])
        {
            $err = "ファイルを選択して下さい。";
        }

	if(!$err){
            if (!preg_match("/^[a-zA-Z0-9_\-\.]+$/",$fileobj["name"])) {
                $err = "ファイル名は半角英数字記号（ハイフン、アンダーバーのみ）をご利用下さい。";
            }
        }

	//拡張子のチェック
	if(!$err){

            //拡張子
            $file_ext = $this->get_file_extension($fileobj["name"]);

            $match = 0;
            foreach($this->filetype as $type_val)
            {
                if($file_ext == $type_val)
                {
                    $match = 1;
                    break;
                }
            }

            if(!$match)
            {
                $errstr = "アップロードできるファイルは";
                $errstr .= implode("," , $this->filetype);
                $errstr .= "のみです。";

                $err = $errstr;
            }
	}

	//サイズチェック
	if(!$err){
            $filesize = ceil($fileobj['size']/1024);
            if($filesize > $this->maxfilesize){
                    $err = "ファイルサイズが大きすぎます。(容量が".$this->maxfilesize."キロバイトまでとなります。)";
            }
	}

        return $err;
    }

    /**
     *　ファイルのアップロード処理
     * @param object $fileobj  アップロードファイルオブジェクト $_FILE[XXX]
     * @param sstring $filename  アップロード設置する際、ファイル名を書き換えたい場合に指定
     * @param  boolean $overwrite (同名のファイル名がある場合、上書きするか　true , false)
     * @return boolean
     */
    function do_upload($fileobj , $filename = "" , $overwrite = false){

        $this->upload_error = "";

        if(!$this->upload_dir)
        {
            $this->upload_error = "アップロード先が指定されていません。";
        }
        else
        {
            if(!$filename) $filename = $fileobj["name"];
            //ファイル名に「.ファイル拡張子」と同じ文字列が混ざっている場合、その部分をリネームする
			$file_ext = substr($filename,-3);
			$filename = str_replace(".".$file_ext , "" , $filename);
			$filename .= ".".$file_ext;

            $upload_file = $this->upload_dir."/".$filename;

            if($overwrite && file_exists($upload_file))
            {
                if(is_file($upload_file))
                {
                    unlink($upload_file);
                }
                else
                {
                    $this->upload_error = "同名のファイルが存在しており、それを削除することができません。";
                }
            }

            if(!file_exists($upload_file)){

                if($fileobj['binary']) {
                    $result = file_put_contents($upload_file, $fileobj['binary']);
                } else {
                    $result = move_uploaded_file($fileobj["tmp_name"], $upload_file);
                }

                if($result){

                    //パーミッション変更
                    chmod($upload_file, 0644);

                    //拡張子
                    $file_ext = $this->get_file_extension($upload_file);

                    return array(
                        "filename" => $filename,
                        "upload_path" => $this->upload_dir,
                        "file_ext" => $file_ext
                    );
                } else {
                    $this->upload_error = "アップロードに失敗しました。再度お試し下さい。";
                    return false;
                }
            } else {
                $this->upload_error = "同名のファイルが存在しており、アップロードすることができません。";
                return false;
            }
        }
    }

    /**
     * アップロード処理の結果を得る
     * エラーがあればエラー文を返し、成功時は空文字が返る
     */
    function get_upload_report()
    {
        return $this->upload_error;
    }

    /**
     * アップロードしたメディアデータ（ファイル名）をDBに保存する
     * @param string $filename
     */
    function insert_media_data($fileinfo , $tag_str = "")
    {

        if($fileinfo && isset($fileinfo["filename"]) && isset($fileinfo["file_ext"]))
        {
            $insert_data = array(
                "filename"  => $fileinfo["filename"],
                "type"      => $this->get_media_type($fileinfo["file_ext"]),
                "tag"       => $tag_str,
                "created"   => fn_get_date()
            );

            return with(new Media())->replace($insert_data , "id");
        }

        return false;
    }


    /**
     * アップロードファイルの削除
     * @param object $db
     * @param array $delete_arr  削除対象ファイル名
     *
     * @return boolean
     */
    function delete_file($delete_arr)
    {
        $del_pict_cnt = 0;
/*
        //CSRF TOKENチェック
        if(!empty($_POST["token"]))
        {
            $csrf_check =  $this->LACNE->request->csrf_check($_POST["token"]);
        }
        if(!$csrf_check) die("データの受け渡しで問題が発生しました。もう一度操作をやり直して下さい。");
*/
        if(is_array($delete_arr) && count($delete_arr) > 0)
        {
            foreach($delete_arr as $value){

                $value = fn_sanitize($value);
                //DBから削除
                with(new Media())->delete("filename = ?" , array($value));

                if(file_exists($this->upload_dir."/".$value)){

                    unlink($this->upload_dir."/".$value);

                    //他デバイス向けにリサイズした画像があればそれも削除
                    $file_ext = substr($value,-3);
                    $filename_smph = str_replace(".".$file_ext , "_smph".".".$file_ext , $value);
                    $filename_mobi = str_replace(".".$file_ext , "_mobi".".".$file_ext , $value);
                    $filename_thumb = str_replace(".".$file_ext , "_thumb".".".$file_ext , $value);
                    //2017/11/27追加
                    $filename_detail = str_replace(".".$file_ext , "_dtil".".".$file_ext , $value);

                    if(file_exists($this->upload_dir."/".$filename_smph)){
                        unlink($this->upload_dir."/".$filename_smph);
                    }
                    if(file_exists($this->upload_dir."/".$filename_mobi)){
                        unlink($this->upload_dir."/".$filename_mobi);
                    }
                    if(file_exists($this->upload_dir."/".$filename_thumb)){
                        unlink($this->upload_dir."/".$filename_thumb);
                    }
                    if(file_exists($this->upload_dir."/".$filename_detail)){
                        unlink($this->upload_dir."/".$filename_detail);
                    }

                    $del_pict_cnt++;

                }
            }
        }

        return $del_pict_cnt;
    }


    /**
     *  アップロードされているファイルリスト取得(DB取得型)
     *
     *  @param  object $db
     *  @param  number $page
     *  @param  number $num
     *  @param  string $filter(image:gif,jpg,pngファイルのみ、movie:動画ファイルのみ）
     *  @param  string $tag (検索タグ)
     *  @return object
     */
    function get_ImageList($page , $num=NUM_LIST , $filter = "" , $tag = "") {

        $filelist = array();

        if($this->upload_dir && is_dir($this->upload_dir)){

            //画像ファイルリスト取得
            $media_list = with(new Media())->get_list($page , $num , $filter , $tag);

            if(count($media_list))
            {
                foreach($media_list as $media)
                {
                    $lfl = LACNE::get_filepath($this->upload_dir,$media["filename"]);

                    if(file_exists($lfl)){
                        //拡張子
                        $file_ext = $this->get_file_extension($lfl);

                        //許可されている拡張子のファイルのみ追加
                        $match = 0;
                        foreach($this->filetype as $type_val)
                        {
                            if($file_ext && $file_ext == $type_val)
                            {
                                $match = 1;
                                break;
                            }
                        }
                        if($match)
                        {
                            $filesize = ceil(filesize($lfl)/1024);
                            $type_str = $this->get_media_type($file_ext);
                            $filelist[] = array(
                                "id"=>$media["id"],
                                "type"=>$type_str,
                                "name"=>$media["filename"],
                                "filepath" => LACNE::get_filepath($this->upload_path, $media["filename"]),
                                "ext" => $file_ext,
                                "tag" => $media["tag"],
                                "size"=>$filesize
                            );
                        }
                    }
                }
            }

            if(count($filelist)){
                    return $filelist;
            }
        }

        return array();

    }

    /**
     * 登録されているタグの一覧とそのファイル数を返す
     * @param void
     * @return object
     */
    function get_tagList()
    {
        $taglist = array();
        $result = with(new Media())->get_tag_cnt();
        if(!empty($result))
        {
            foreach($result as $data)
            {
                $taglist[] = array(
                    "name" => $data["tag"],
                    "cnt" => $data["cnt"]
                );
            }
        }
        return $taglist;
    }

    /**
     * アップロードされたファイルの一覧表示サムネイル出力
     * @param string $file
     * @param number $thumb_height //サムネイルの高さ
     * @param string $title
     * @param string $css_class
     * @param boolean $modal_info //クリック時にモーダルウィンドウで画像拡大、情報表示させるか
     */
    function set_thumbnail($file , $thumb_height , $title ="" , $css_class="" , $modal_info = false)
    {
        $thumbimg_path = $this->get_thumbpath($file);
        $return = "";
        if($modal_info)
        {
            $filepath = LACNE::get_filepath($this->upload_path,$file["name"]);
            if($file["type"] == "image")
            {
                $modal_file_path = $filepath;
            }
            else
            {
                $modal_file_path = $thumbimg_path;
            }
            $return .= '<a href="'.$modal_file_path.'" class="modalinfo"><span style="display:none">'.$filepath.'</span>';
        }
        $return .= '<img src="'.$thumbimg_path.'" height="'.$thumb_height.'" title="'.(!empty($title)?$title:'').'" class="'.$css_class.'" />';
        if($modal_info)
        {
            $return .= '</a>';
        }

        return $return;
    }

    /**
     * ファイルのパスを返す
     * @param object $file
     * @return string
     */
    function get_filepath($file)
    {
        return LACNE::get_filepath($this->upload_path,$file["name"]);
    }

    /**
     * サムネイル表示用画像ファイルのパスを返す
     * @param object $file
     * @return string
     */
    function get_thumbpath($file)
    {
        if($file["type"] == "image")
        {
    		//サムネイル用にリサイズした画像があればそれを使う
    		$file_ext = substr($file["name"],-3);
    		$filename_thumb = str_replace(".".$file_ext , "_thumb".".".$file_ext , $file["name"]);
            if(file_exists($this->upload_dir."/".$filename_thumb)){
    			$path = LACNE::get_filepath($this->upload_path,$filename_thumb);
    		}
    		else
    		{
    			$path = LACNE::get_filepath($this->upload_path,$file["name"]);
    		}
            return $path;
        }
        else
        {
            if(isset($this->etcfile_thumb_arr[$file["ext"]]))
            {
                return $this->etcfile_thumb_arr[$file["ext"]];
            }
        }

        return "";
    }

    /**
     * アップロードファイル容量取得
     */
    function get_maxfilesize()
    {
        return $this->maxfilesize;
    }

    function get_file_extension($filepath)
    {
        $file_info = pathinfo($filepath);
        if(isset($file_info["extension"]))
        {
            return strtolower($file_info["extension"]);
        }

        return "";
    }

    /**
     * メディアデータのID指定でファイル名を取得
     * @param number $id
     */
    function get_mediafile($id)
    {
        if(is_numeric($id))
        {
            $filename = with(new Media())->fetchOne($id);
            if($filename && isset($filename["filename"]))
            {
                return LACNE::get_filepath($this->upload_path,$filename["filename"]);
            }
        }

        return false;
    }

    /**
     * アップロード許可するファイル拡張子を書き換え
     * @param array $filetype
     * @return void
     */
    function set_arrow_filetype($filetype = array())
    {
        if(is_array($filetype))
        {
            $this->filetype = $filetype;
        }

        return;
    }

    /**
     * ファイル拡張子の種類からメディアタイプを返す（動画対応オプションが有効でないならすべてimageを返す。）
     * @param string $extension
     * @return string
     */
    function get_media_type($extension)
    {
        if($extension && ($extension == "pdf"))
        {
            return "pdf";
        }
        return "image";
    }

    /**
     * 一覧画面でのサムネイル高さ
     * @return number
     */
    function get_thumb_height()
    {
        if(method_exists($this->LACNE->library["admin_view"], 'is_smph'))
        {
            if($this->LACNE->library["admin_view"]->is_smph())
            {
                return 70;
            }
        }
        return 80;
    }
    /**
     * 一覧画面での画像クリック時に表示されるモーダル画面の幅
     * @return number
     */
    function get_modal_width()
    {
        if(method_exists($this->LACNE->library["admin_view"], 'is_smph'))
        {
            if($this->LACNE->library["admin_view"]->is_smph())
            {
                return 250;
            }
        }
        return 350;
    }

}