<?php

/**
 * 拡張機能オプションファイル
 *
 * @package  Lacne
 * @author  InVogue Inc.
 * @link  http://lacne.jp
 * @copyright  Copyright 2008- InVogue Inc. All rights reserved.
 */

class Option_setting_base
{

    var $option = array(
        "library" => array(
            //"media"     => array("src" => "upload_movie/upload_movie.php" , "class_name" => "Lib_media_movie"),
            "login"     => array("src" => "account/Lib_account.php" , "class_name" => "Lib_account"),
            "setting"   => array("src" => "social_connect/social_connect.php" , "class_name" => "Lib_social_connect"),
//             "post"      => array("src" => "account/Lib_flow.php" , "class_name" => "Lib_flow"),
            "output"    => array("src" => "mobile_output/mobile_output.php" , "class_name" => "Lib_Mobile_output"),
            "social_btn"=> array("src" => "social_btn/social_btn.php" , "class_name" => "Lib_social_btn_func"),
            //"lpo_light" => array("src" => "lpo_light/lpo_light.php" , "class_name" => "Lib_lpo_light_func"),
            //"admin_view"=> array("src" => "admin_smph/Lib_admin_view_smph.php" , "class_name" => "Lib_admin_view_smph"),
        ),
        "model" => array(
//             "post"      => array("src" => "account/Model_flow.php" , "class_name" => "Model_flow"),
        ),
        "class" => array(
        )
    );


    function Option_setting(){}

    function get_option_library($library_name)
    {
        if(isset($this->option["library"][$library_name]))
        {
            return $this->option["library"][$library_name];
        }

        return false;
    }


    function get_option_model($model_name)
    {
        if(isset($this->option["model"][$model_name]))
        {
            return $this->option["model"][$model_name];
        }

        return false;
    }

    function get_option_class($class_name)
    {
        if(isset($this->option["class"][$class_name]))
        {
            return $this->option["class"][$class_name];
        }

        return false;
    }

    /**
     * 指定したオプション指定を削除する
     * @param object $option_params
     */
    function unset_option($option_params)
    {
        if(is_array($option_params) && isset($option_params["library"]))
        {
            foreach($option_params["library"] as $option)
            {
                if(isset($this->option["library"][$option])) unset($this->option["library"][$option]);
            }
        }
        if(is_array($option_params) && isset($option_params["model"]))
        {
            foreach($option_params["model"] as $option)
            {
                if(isset($this->option["model"][$option])) unset($this->option["model"][$option]);
            }
        }

        return;
    }

}
