<?php
/**
 * システムの基本情報
 *
 * @package  Lacne
 * @author  InVogue Inc.
 * @link  http://lacne.jp
 * @copyright  Copyright 2008- InVogue Inc. All rights reserved.
 */
date_default_timezone_set('Asia/Tokyo');
/*----------------------------------------------
 *  基本情報
 *  [_CHECK_] サイト名を入れて下さい（RSS FEEDなどで利用）
 *---------------------------------------------*/
define ("SITE_NAME" , "CMMS（シームズ）");

/*----------------------------------------------
 *  URL , PATH設定
 *  [_CHECK_] 導入するサイトのホスト名を入れて下さい
 *---------------------------------------------*/
define ("SITE_URL" , "https://".$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT']);

/*----------------------------------------------
 *  LACNE設置パス (etc : "/lacne")
 *  [_CHECK_] ドキュメントルートからのLACNEを設置したフォルダのパス
 *---------------------------------------------*/
define ("LACNE_PATH" , "/lacne");

/*----------------------------------------------
 *  コンテンツページ設置パス (etc : "/contents")
 *  [_CHECK_] コンテンツページの基本設置パス
 *---------------------------------------------*/
define ("SITE_BASE_PATH" , "");

/*----------------------------------------------
 *  LACNE設置フルパス (etc : "/var/www/htdocs/lacne")
 *  フルパスでLACNEを設置したフォルダのパスを記載
 *---------------------------------------------*/
define ("LACNE_DIR" , realpath(dirname(__FILE__).'/../../../'));

/*----------------------------------------------
 *  LACNE設置先URL
 *---------------------------------------------*/
define ("LACNE_URL" , SITE_URL.LACNE_PATH); //http://domain.com/lacne_dir

/*----------------------------------------------
 *  LACNE管理画面ログインURL
 *  [_CHECK_] ログインURLを記述
 *---------------------------------------------*/
define ("LACNE_LOGIN_URL" , LACNE_URL."/login.php");
/*----------------------------------------------
 *  LACNE管理画面ログイン成功後の遷移先URL
 *  [_CHECK_] ログイン成功後の遷移先URLを記述
 *---------------------------------------------*/
define ("LACNE_MAINPAGE_URL" , LACNE_URL."/news/admin/article/index.php");

/*----------------------------------------------
 *  HELPファイルの保存先
 *  [_CHECK_] URLを記載。ない場合は空で
 *---------------------------------------------*/
define ("LACNE_HELP_URL" , ""); //http://から

/*----------------------------------------------
 *  認証 (ランダムな文字列を入れる)
 *---------------------------------------------*/
/** 管理画面ログインパスワード暗号KEY (0:FALSE) */
define ("CERT_LOGIN_KEY","ZebPOVmLdCFFoh8z");
/** 認証文字列(認証成功の判定に使用) */
define ("CERT_STRING", "L3OThG16X54mnBvv");
/** CSRFで利用するtoken暗号用 */
define ("TOKEN_SEED", "CXpvrTsHIEOBI6Wz");

/*----------------------------------------------
 *  メール送信元
 *  [_CHECK_] パスワードリマインダや承認・差戻し処理時に送信されるメール送信元
 *---------------------------------------------*/
define ("FROM_MAINADDRESS" , "admin@lacne.jp");

/*----------------------------------------------
 *  セッション
 *---------------------------------------------*/
/** セッション管理タイプ (1:DB,2:FILE) */
define ("SESSION_TYPE",2);
/** セッション管理用テーブル(DB利用の場合のみ)  */
define ("TABLE_SESSION","sessiondata");

/*----------------------------------------------
 *  文字コード
 *---------------------------------------------*/
/** 文字コード変換処理  */
define("STRINGCODE_CHANGE",false);
/** 出力文字コード  */
define("STRINGCODE_OUTPUT","UTF-8");
/** PHP文字コード  */
define("STRINGCODE_PHP","UTF-8");
/** DB文字コード  */
define("STRINGCODE_DB","UTF-8");

/*----------------------------------------------
 *  表示数設定
 *---------------------------------------------*/
/** 表示件数(通常はlib_admin_viewに設定した値を利用) */
define("NUM_LIST",10);

/** アップロードサイズ上限（kb） */
define("IMG_SAVE_MAX_SIZE",3000);

/** 認証エラー時の各種リンク先 */
define ("LINK_LOGIN_ERR01" , LACNE_LOGIN_URL."?message=1"); //ログイン失敗時1
define ("LINK_LOGIN_ERR02" , LACNE_LOGIN_URL."?message=2"); //ログイン失敗時2

define("CRON_LOG_PATH", LACNE_DIR . '/cron/log');

/*----------------------------------------------
 *  DEBUG_MODE
 *---------------------------------------------*/
define ("DEBUG_MODE_SYS",0);
?>
