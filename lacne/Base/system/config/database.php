<?php

/**
 * データベース接続設定
 *
 * @package  Lacne
 * @author  InVogue Inc.
 * @link  http://lacne.jp
 * @copyright  Copyright 2008- InVogue Inc. All rights reserved.
 */


/*----------------------------------------------
 *  データベース
 *  [_CHECK_] DB接続情報を入れる
 *---------------------------------------------*/
/** データベースの種類  */
define("DB_TYPE","mysqli");
/** データベース_ホスト名  */
define("DB_HOST","mysql106.xbiz.ne.jp");
/** データベース名  */
define("DB_NAME","milifeplus0_lacnecms");
/** データベース_ユーザ  */
define("DB_USER","milifeplus0_user");
/** データベース_パスワード  */
define("DB_PASS","i37gvv01");
/** PDO用 */
define("DNS",DB_TYPE.":host=".DB_HOST."; dbname=".DB_NAME);


/** TABLE設定 */
//---------共通利用テーブル------------//
//管理者情報格納
define("TABLE_ADMIN","admin");

//--------コンテンツごとに用意してPREFIX名を付けて利用するテーブル------------//
//記事データ格納
define("TABLE_POSTS","posts");
//拡張フィールド
define("TABLE_POSTMETA","postmeta");
//カテゴリ設定用
define("TABLE_CATEGORY","category");
//画像管理用
define("TABLE_IMAGES","media");
//タグ管理用
define("TABLE_TAG","tag");
//記事タグ紐付け用
define("TABLE_POST_TAG","post_tag");

/*----------------------------------------------
 *  DEBUG_MODE
 *---------------------------------------------*/
define ("DEBUG_MODE_DB", 0);
