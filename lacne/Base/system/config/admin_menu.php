<?php

/**
 * 管理画面メニュー設定
 *
 */

$_SETTING_ADMIN_MENU = array(
    //Menu 2
    "owned" => array(
        "menu_id"	=> "Extend01",
        "label"     => "お知らせ管理",
        "link"		=> LACNE_PATH."/news/admin/index.php",
        "comment"	=> "お知らせの情報を管理・編集することができます",
        "submenu"   =>array(
            //Menu 2_2
            array(
                "label" => "お知らせ一覧",
                "link"  => LACNE_PATH."/news/admin/article/index.php",
                "icon"  => "list",
                "comment" => "登録されている%sの一覧を確認できます。",
            ),
            array(
                "label" => "お知らせ作成",
                "link"  => LACNE_PATH."/news/admin/article/edit.php",
                "icon"  => "news article",
                "comment" => "%sの作成、編集を行います。",
            ),
/**
            array(
                "terms" => "manage_tag",
                "label" => "KEYWORD管理",
                "link"  => LACNE_PATH."/news/admin/tag/index.php",
                "icon"  => "tag",
                "comment" => "KEYWORDの新規登録、削除を行います。",
            ),
            //Menu 2_5
            array(
                "terms" => "manage_ranking",
                "label" => "WEEKLYランキング管理",
                "link"  => LACNE_PATH."/news/admin/ranking/index.php",
                "icon"  => "ranking",
                "comment" => "WEEKLYランキングの順位を設定します",
            ),
            //Menu 2_5
             array(
             "terms" => "manage_total_ranking",
             "label" => "TOTALランキング管理",
             "link"  => LACNE_PATH."/news/admin/total_ranking/index.php",
             "icon"  => "total_ranking",
             "comment" => "TOTALランキングの順位を設定します",
             ),
            //Menu 2_5
            array(
                "terms" => "manage_keyword",
                "label" => "KEYWORDランキング管理",
                "link"  => LACNE_PATH."/news/admin/trend_keyword/index.php",
                "icon"  => "keyword",
                "comment" => "KEYWORDランキングの順位を設定します",
            ),
 */
            array(
                "label" => "メディアアップロード",
                "link"  => LACNE_PATH."/news/admin/media/index.php",
                "icon"  => "media",
                "comment" => "%s内に掲載するメディアファイルのアップロード、削除を行います。",
            ),
        )
    ),
);


/*
 * 以下、indexページと連動させる用の
 * 大きめのアイコン画像設定
 */

$_SETTING_INDEXMENU_IMAGES = array(
    "list" => "main_menu_img01.gif",
    "news" => "main_menu_img02.gif",
    "category" => "main_menu_img03.gif",
    "account" => "main_menu_img04.gif",
    "media" => "main_menu_img05.gif",
);
