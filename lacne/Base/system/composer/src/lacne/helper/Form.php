<?php namespace lacne\core\helper;

/**
 * 簡易的なフォームヘルパー
 * Class Bag
 * @package lacne\core\model
 */
class Form
{
    public static function CHECKED($val)
    {
        return isset($val) && $val == 1? "checked=\"checked\"": '';
    }
}