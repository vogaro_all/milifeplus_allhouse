<?php namespace lacne\core\helper;

/**
 * 簡易的なバッグクラス
 * Class Bag
 * @package lacne\core\model
 */
class Bag
{
    protected $data = array();

    public function __construct($data)
    {
        $this->data = empty($data)? array(): $data;
    }

    public function get($key, $default = '')
    {
        return $this->has($key)? $this->data[$key]: $default;
    }

    public function has($key)
    {
        return array_key_exists($key, $this->data)? true: false;
    }

    public function delete($key)
    {
        if($this->has($key)){
            unset($this->data[$key]);
            return true;
        }

        return false;
    }

    public function put($key, $val)
    {
        $this->data[$key] = $val;
    }
}