<?php if( $pager->hasPages() == true ) : ?>
        <div class="sec-pagination">
          <ul class="sec-pagination__list">

        <?php $pager->onePrev(function($pager, $prevID, $prevTxt){ ?>
            <li class="sec-pagination__list-item">
              <a href="?page=<?php echo($prevID); ?>" class="sec-pagination__list-prev">
                <img src="/assets/images/pages/news/arrow_icon.svg" alt="">
              </a>
            </li>
        <?php }
    ,'',''); ?>

    <?php $pager->getPages(function($i, $active, $pager, $isPrevEllipsis, $isNextEllipsis, $add_pager_link){ ?>

            <?php if($isNextEllipsis && $i == $pager->getLastPage()): ?>
            <li class="sec-pagination__list-item">
              <span class="sec-pagination__list-dots">...</span>
            </li>
            <?php endif; ?>

            <?php if( $active == $i ): ?>
            <li class="sec-pagination__list-item">
                  <a href="?page=<?php echo($i.$add_pager_link); ?>" class="sec-pagination__list-link is-current"><?php echo($i); ?></a>
            </li>
            <?php else: ?>
            <li class="sec-pagination__list-item">
              <a href="?page=<?php echo($i.$add_pager_link); ?>" class="sec-pagination__list-link"><?php echo($i); ?></a>
            </li>
            <?php endif; ?>

            <?php if($isPrevEllipsis && $i == 1): ?>
            <li class="sec-pagination__list-item">
              <span class="sec-pagination__list-dots">...</span>
            </li>
            <?php endif; ?>

    <?php }, 3, $add_pager_link); ?>

    <?php $pager->oneNext(function($pager, $nextID, $nextTxt){ ?>
            <li class="sec-pagination__list-item">
              <a href="?page=<?php echo($nextID); ?>" class="sec-pagination__list-next">
                <img src="/assets/images/pages/news/arrow_icon.svg" alt="">
              </a>
            </li>
    <?php }, '', ''); ?>

          </ul>
        </div>

<?php endif; ?>
