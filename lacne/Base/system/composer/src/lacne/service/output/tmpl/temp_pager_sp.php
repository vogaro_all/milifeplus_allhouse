<?php 
if (isset($_GET['category']) && is_numeric($_GET['category'])) {
	$add_pager_link = '&category='.$_GET['category'];
} elseif (isset($_GET['id']) && is_numeric($_GET['id'])) {
	$add_pager_link = '&id='.$_GET['id'];
} elseif (isset($_GET['search']) && !empty($_GET['search'])) {
	$add_pager_link = '&search='.fn_esc($_GET['search']);
} elseif (isset($_GET['doc']) && is_numeric($_GET['doc'])) {
	$add_pager_link = '&doc='.$_GET['doc'];
}
?>
<?php if( $pager->hasPages() == true ) : ?>
<div class="m-pagenation u-d-md-none">

	<?php $pager->onePrev(function($pager, $prevID, $prevTxt, $add_pager_link){ ?>
	<button type="button" class="m-pagenation__prev"><a href="?page=<?php echo($prevID); ?><?php echo($add_pager_link);?>">前へ</a></button>
	<?php }, function($pager){ ?>
		<button type="button" class="m-pagenation__prev" disabled>前へ</button>
	<?php }
	, $add_pager_link); ?>

    <select id="js-select" class="m-pagenation__current" name="number">	
	<?php if( $pager->hasPages() == true ) : ?>
		<?php $pager->getPages(function($i, $active, $pager, $isPrevEllipsis, $isNextEllipsis, $add_pager_link){ ?>
      <option value="/archive/archive/?page=<?php echo($i); ?><?php echo($add_pager_link);?>" <?php echo( $active == $i ) ? 'selected' : ''; ?>><?php echo($i); ?>/<?php echo $pager->getLastPage();?></option>
		<?php }, 3, $add_pager_link); ?>
	<?php endif; ?>
    </select>
    	
	<?php $pager->oneNext(function($pager, $nextID, $nextTxt, $add_pager_link){ ?>
	<button type="button" class="m-pagenation__next"><a href="?page=<?php echo($nextID); ?><?php echo($add_pager_link);?>">次へ</a></button>
	<?php },function($pager){ ?>
	<button type="button" class="m-pagenation__next" disabled>次へ</button>
	<?php }, $add_pager_link); ?>

			
	
	
</div><!-- .m-pagenation -->
<?php endif; ?>
