<!-- メイン画像 -->
            <div class="sec-images">
              <div class="figure figure--big">
                <div class="figure__image" style="background-image: url('<?php echo( $entity->getImg() ); ?>');"></div>
                <p class="figure__caption"><?php echo( $entity->caption_name ); ?></p>
              </div>
            </div>
<!-- end -->