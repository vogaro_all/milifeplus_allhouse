<!-- 画像 3カラム -->
            <div class="sec-images">
              <div class="figure figure--big">
                <div class="figure__image" style="background-image: url('<?php echo( $entity->getImg() ); ?>');"></div>
                <p class="figure__caption"><?php echo( $entity->caption_name ); ?></p>
              </div>
              <div class="sec-images__column">
                <div class="figure figure--small">
                  <div class="figure__image" style="background-image: url('<?php echo( $entity->getImg2() ); ?>');"></div>
                  <p class="figure__caption"><?php echo( $entity->caption2_name ); ?></p>
                </div>
                <div class="figure figure--small">
                  <div class="figure__image" style="background-image: url('<?php echo( $entity->getImg3() ); ?>');"></div>
                  <p class="figure__caption"><?php echo( $entity->caption3_name ); ?></p>
                </div>
              </div>
            </div>