<?php namespace lacne\service\output;

use lacne\domain\OwnedMedia\MediaEntity;
use lacne\service\output\OwnedMediaHelper;
use lacne\core\model\Tag;
use lacne\core\model\PostTag;
use lacne\core\model\Post;
use lacne\domain\OwnedMedia\specification\OwnedMediaPreviewSpec;
use lacne\domain\OwnedMedia\specification\OwnedMediaShowConditionSpec;

/**
 * 記事情報のトップ・一覧それぞれ掲載するために扱うAPIクラス
 * Class OwnedMediaOtherAPI
 * @package lacne\service\output
 */
class OwnedMediaOtherAPI extends OwnedMediaBaseAPI
{

    public function details()
    {
        $data = parent::details();

        if(!( $this->preview_mode && isset($_POST["title"])) ){
            return $data;
        }

        $ownedMediaPreviewSpec = new OwnedMediaPreviewSpec($data);
        return $ownedMediaPreviewSpec->build()->getPreviewItem();
    }

    /**
     * タグ情報を取得
     * @return array
     */
    public function tag_list()
    {
    	global $LACNE;
    	$LACNE->load_library(array('post'));
    	
    	$tag_list = $LACNE->library["post"]->get_tag_list();
    	return fn_esc($tag_list);
    }
    
    /**
     * タグ情報を取得
     * @return array
     */
    public function tag_exist_list()
    {
    	$tag_list = with(new Post())->get_tag_list_exist();
    	return fn_esc($tag_list);
    }
    
    /**
     * カテゴリ情報を取得
     * @return array
     */
    public function category_list()
    {
    
    	global $LACNE;
    	$LACNE->load_library(array('post'));
    	
    	return $LACNE->library["post"]->get_category_list();
    }
    
    /**
    * タグ情報を配列リストに追加（キーワード一覧ページ用）
    * @return array
    */
    protected function mergeTags($items)
    {
    	if(empty($items)) return array();
    
    	// 1度のクエリで取得が難しかったため記事に紐づくタグを再問い合わせ
    	foreach ($items as $index => $item)
    	{
    		$items[$index]['tags'] = with(new PostTag())->fetchByPostID( $item['id'] );
    	}
    	return $items;
    }
    
    /**
     * 記事一覧(一覧エリア)の情報を取得する
     * @return array|string
     */
    public function loadArticleList($params)
    {
    	$param = array();
    	if ($params['category']) {
    		$param["post.category = "] = $params['category'];
    	}
    	
    	if(isset($params["mv_flag"]) && is_numeric($params["mv_flag"]))
    	{
    	    $param["post.mv_flag ="] = $params["mv_flag"];
    	}
    	
    	if ($params['not_id']) {
    	    $param["post.id <> "] = $params['not_id'];
    	}
    	
    	if ($params['key']) {
    	   if (is_numeric($params['key'])) {
    		  $param["tag_post.tag_id = "] = $params['key'];
    		}
    	}
    	
    	if ($params['keywords']) {
    	    $search_word = fn_esc($_GET['keywords']);
    	        
    	    $tag = "tag.tag_name LIKE "."'%".$search_word."%'";
    	    $title = "post.title LIKE "."'%".$search_word."%'";
    	    $keyword = "post.keyword LIKE "."'%".$search_word."%'";
    	    $description = "post.description LIKE "."'%".$search_word."%'";
    	    
    	    $param["($tag OR $title OR $keyword OR $description)"] = '';
    	}
    	
    	return $this->lists( $this->getParam($params['limit']), $this->getSearchParam($param));
    }
    
    /**
     * 右サイド共通(ランキングエリア)の情報を取得する。
     * @return array|string
     */
    public function loadRankingWeekly($params)
    {
        $ret = $this->ranking_list( $this->getParam($params['limit']), $this->getSearchParam( array() ) );
        return $ret;
    }
    
    /**
     * 右サイド共通(ランキングエリア)の情報を取得する。
     * @return array|string
     */
    public function loadRankingTotal($params)
    {
        $ret = $this->total_ranking_list( $this->getParam($params['limit']), $this->getSearchParam( array() ) );
        return $ret;
    }
    
    /**
     * キーワードの情報を取得する。
     * @return array|string
     */
    public function loadKeywordList($params)
    {
        return $this->keyword_list();
    }
}