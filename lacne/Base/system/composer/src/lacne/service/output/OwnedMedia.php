<?php namespace lacne\service\output;

use lacne\service\output\View;
use lacne\core\model\Post;


class OwnedMedia extends View
{
	public function __construct(){ parent::__construct('ownedmedia'); }

	function lists($params, $search_param, $search_or_param=array(),$sql_order=array()) {

		$list_data = parent::lists($params, $search_param, $search_or_param,$sql_order);

		return $list_data;
	}


	function details() {
		$post_data = parent::details();

		if(!$post_data)
		{
			fn_redirect('/');
		}

		// ページ構成表記作成
		$contents_tmpl = '/';
		if(empty($post_data['section']))
		{
			$post_data['section'] = array();
		}
		foreach ($post_data['section'] as $sectionKey => $section)
		{
			$contents_tmpl .= $section['abbr'] . '/';
			unset($post_data['section'][$sectionKey]['abbr']);
		}
		$post_data['contents_tmpl'] = $contents_tmpl;

		// おすすめ記事は公開できるものだけにする
		// 公開状態か、また公開日時が過ぎているかをチェック
		if($post_data['recommend_post'])
		{
			foreach ($post_data['recommend_post'] as $key => $recommend)
			{
				if(!isset($recommend["output_flag"])
					|| !$recommend["output_flag"]
					|| !isset($recommend["output_date"])
					|| $recommend["output_date"] > fn_get_date())
				{
					unset($post_data['recommend_post'][$key]);
				}
			}
		}else{
			$post_data['recommend_post'] = array();
		}

		// PV数更新
//		if(!$post_data['isPreview'])
//		{
//			$rs = with(new Post())->fetchOne($_GET["id"]);
//			$updata = array(
//					"id" => $rs["id"],
//					"pv_num" => $rs['pv_num']+1,
//			);
//			$updataResult = with(new Post())->replace($updata , "id");
//		}

		return $post_data;
	}


	/*
	 * 記事と同じ年齢区分且つおすすめ記事以外の最新10件をランダムで指定数分取得する
	 * @param array 抽出源となる配列
	 * @param integer 抽出したい記事数
	 * @return array 抽出された配列
	 */
	function array_rand_fixed($target, $num = 1){

		if(empty($target)){
			return array();
		}

		if( !($num >= 1) )
		{
			return array();
		}

		$num = count($target) <= $num? count($target): $num;
		$rand_arr = array_rand($target, $num);
		if(!is_array($rand_arr) && !empty($rand_arr))
		{
			$rand_arr = array($rand_arr);
		}

		if(!is_array($rand_arr))
		{
			if ($rand_arr == 0) {
				$rand_arr = array(0);
			} else {
				$rand_arr = array();
			}
		}

		return array_map(function($item) use($target){
			return $target[$item];
		}, $rand_arr);
	}

}