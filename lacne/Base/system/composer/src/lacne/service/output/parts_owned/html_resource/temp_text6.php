<?php 
$content = str_replace(array("\r\n", "\r", "\n"), "\n", $entity->content_name);
$contents = explode("\n", $content);
?>
            <div class="block block--02">
              <p class="block__text"><?php echo( $entity->title_name ); ?></p>
              <ol class="block-list">
                <?php foreach ($contents as $key => $value) {?>
                <li>
                  <span><?php echo( $key+1 ); ?>.</span><?php echo( $value ); ?>
                </li>
                <?php } ?>
              </ol>
            </div>