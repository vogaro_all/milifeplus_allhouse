<?php namespace lacne\service\output;

use lacne\core\Template;
use lacne\core\model\Category;
use lacne\core\model\Post;
use lacne\core\model\Parts;
use lacne\core\model\Tag;
use lacne\core\model\Ranking;
use lacne\core\model\TotalRanking;
use lacne\core\model\Keyword;

class View
{
	protected $conditions = array(
			"output_flag =" => "1"
	);

	private $dirName; // ディレクトリ名

	private $post_data; // 記事データ

	protected $preview_mode; // プレビューモード

	public function __construct($dir){
		$this->c = new Template('');
		$this->dirName= $dir;
	}

	public function lists($params, $search_param_arr, $search_or_param = array() ,$sql_order=array()){

		global $LACNE;

    	// 公開されているもののみ
    	$search_param_arr["post.output_flag ="] = 1;
    	// 表示開始日が過ぎているもの
//     	$search_param_arr["post.output_date <= NOW()"] = '';


		if(isset($params["page_limit"]) && is_numeric($params["page_limit"])) $params["pager"] = 1; //page_limit指定があれば pager対応

		$post_data = $this->get_post_list($params , $search_param_arr, false, $search_or_param, $sql_order);

		$cnt = count($post_data);

		fn_getPages(count($post_data),$params["page_limit"]);

		return $this->c->_Escape($post_data);
	}

	public function details(){

		global $LACNE;
		$LACNE->load_library(array('post'));

		$this->preview_chk();

		//previewモードの場合(POSTデータがある場合）、そのデータを利用する
		if($this->preview_mode && isset($_POST["title"]) )
		{
			$post_data = fn_get_form_param($_POST);
			header('X-XSS-Protection: 0');
			
			// Preview用のカテゴリ名設定
			$category_list = $this->category_list();
			$post_data['category_name'] = $category_list[$post_data['category']];
		}
		else
		{

			if(!$post_id && isset($_GET["id"]) && is_numeric($_GET["id"]))
			{
				$post_id = $_GET["id"];
			}
			$post_data = $this->get_post_data($post_id);

			//パーツデータを読み込み
			$parts_data_list = with(new Parts())->findByPostID($post_id , '' , true);
			$post_data['section'] = array();
			if(!empty($parts_data_list)) {
				$post_data['section'] = with(new Parts())->convert_parts_data($parts_data_list,true);
			}
		}
		
	
		$search_param_arr = array(
				"post.output_flag =" => 1,
				"post.output_date <=" => fn_get_date(),
				'post.category = ' => 3,
				'post.id <>' => $post_data['id']
		);
		
		$other_data_list_sql = with(new Post())->get_list_sql($search_param_arr);
		$other_data_list = with(new Post())->_fetchAll($other_data_list_sql , array());
		if ($other_data_list) {
			$post_data['other_data_list'] = $other_data_list;
		}

		// プレビューであることを画面に通知
		if($this->preview_mode)
		{
			$post_data['isPreview'] = true;
		}

		//TODO ソーシャルボタン設置用のOGP情報をセット（social_btn拡張オプションが有効の場合のみ）
		if(method_exists($LACNE->library["social_btn"], "set_pageinfo"))
		{
			//$LACNE->library["social_btn"]->set_pageinfo($Output);
		}

		//取得したデータが公開状態か、また公開日時が過ぎているかをチェック　見れない状態であればTOPにリダイレクト
		//ただしpreviewモードなら非公開もしくは公開日時が過ぎていなくても見れる
		if(!$this->preview_mode && (!isset($post_data["output_flag"])
										|| !$post_data["output_flag"]
										|| !isset($post_data["output_date"])
										|| $post_data["output_date"] > fn_get_date()))
		{
			$post_data = array();
		}

		if (!$post_data['title'])
		{
			fn_redirect('/404.html');
		}

		$post_data['page_data'] = $this->getNextAndPrevData($post_id, array());
		
		return $post_data;
	}

	/**
	 * 詳細（個別）記事モード
	 */
	function get_post_data($post_id)
	{
		global $LACNE;
		$LACNE->load_library(array('post'));

		if(is_numeric($post_id))
		{
			$this->post_data = $LACNE->library["post"]->get_postdata($post_id);
			if($this->post_data)
			{
				return $this->post_data;
			}
		}
		return false;
	}

// 	/**
// 	 * 詳細（個別）記事モード
// 	 */
// 	function get_post_data_by_original_filename($original_filename, $category)
// 	{
// 		global $LACNE;
// 		$LACNE->load_library(array('post'));
	
// 		if(is_numeric($category))
// 		{
// 			$this->post_data = $LACNE->library["post"]->get_postdata_by_original_filename($original_filename, $category);
// 			if($this->post_data)
// 			{
// 				return $this->post_data;
// 			}
// 		}
// 		return false;
// 	}
	
	/**
	 * 表示対象となる記事データを取得する
	 * @param object $param //データ所得時の各種指定パラメータ
	 *    |-number $num //表示件数(all : 全て）※page_limitを指定する場合は空に
	 *    |-number $category //カテゴリ指定(ID)
	 *    |-number $page_limit //1ページ単位の表示数
	 *    |-string $date_target //日付指定しての絞込み(yyyy-mm-dd形式 絞りこみする場合、yyyyは必須 yyyy%やyyyy-mm% などが可能)
	 * @param object $search_param
	 * @param boolean $getCnt //記事データ数のみ取得する場合はtrueに
	 * @return mixed
	 */
	function get_post_list($param , $search_param = array() , $getCnt = false, $search_or_param = array(),$sql_order=array())
	{

		$result = array();

		$search_param_arr = array(
				"post.output_flag =" => 1,
				"post.output_date <=" => fn_get_date()
		);
		if($this->preview_mode) $search_param_arr = array(); //previewモードなら表示対象をすべてとする

		if(isset($param["category"]) && is_numeric($param["category"]))
		{
			$search_param_arr["post.category ="] = $param["category"];
		}
		if(isset($param["date_target"]) && $param["date_target"])
		{
			$search_param_arr["post.output_date LIKE"] = "%".$param["date_target"]."%";
		}

		//category , date_target以外でも他のデータ絞り込み条件があればそれをマージ
		if($search_param)
		{
			$search_param_arr = array_merge($search_param_arr , $search_param);
		}

		$cnt = with(new Post())->data_cnt($search_param_arr, $search_or_param);

		//データのカウントだけなら
		if($getCnt)
		{
			return $cnt;
		}

		if($cnt){
			if(empty($sql_order))
			{
				$sql_order = array("key"=>"post.output_date","by"=>"DESC","val"=>"sort_date");
			}
			//データ取得（ページャー非対応）
			//================================
			if(!isset($param["pager"]) || !$param["pager"]){

				$sql = with(new Post())->get_list_sql($search_param_arr , $sql_order, $search_or_param);

				if($param["num"] && is_numeric($param["num"]) && $param["num"] != "all"){
					$sql .= " LIMIT ".$param["num"];
				}

				$this->post_data = with(new Post())->_fetchAll($sql , array());

				//データ取得（ページャー対応）
				//================================
			}else{

				//現在のページ
				$page = 1;

				if($_GET['page']){
					$page = fn_now_page_set($_GET['page']);
				}

				$page_limit = NUM_LIST;
				if(isset($param["page_limit"]) && is_numeric($param["page_limit"])) $page_limit = $param["page_limit"];

				//総ページ数取得
				$page_num = fn_getPages($cnt,$page_limit);

				if($page > $page_num){
					$page = $page_num;
				}

				$this->post_data = with(new Post())->get_list_top($page , $page_limit , $search_param_arr , $sql_order, $search_or_param);
			}
			
			foreach ($this->post_data as $key => $value) {
    			//パーツデータを読み込み
			    $parts_data_list = with(new Parts())->findByPostID($value['id'] , '' , true);
			    $this->post_data[$key]['section'] = array();
    			if(!empty($parts_data_list)) {
    			    $this->post_data[$key]['section'] = with(new Parts())->convert_parts_data($parts_data_list,true);
    			}
    			
    			$new_period = 30;
    			$daydiff = (strtotime(date("Y/m/d"))-strtotime(date("Y/m/d" , strtotime($value['output_date']))))/(3600*24);
    			if($daydiff >= 0 && $daydiff <= $new_period) {
    			    $this->post_data[$key]["newicon"] = true;
    			} else {
    			    $this->post_data[$key]["newicon"] = false;
    			}
			}

			$result['data'] = $this->post_data;
			$result['cnt'] = $cnt;
			$result['pager'] = $this->getPager($_GET['page'], $cnt, $param["page_limit"]);

			return $result;
		}

		return false;
	}

	/*
	 * ランキング一覧取得
	 */
	public function ranking_list($params, $search_param_arr){
	    global $LACNE;
	    
	    // 公開されているもののみ
	    $search_param_arr["post.output_flag ="] = 1;
	    
	    if(isset($params["page_limit"]) && is_numeric($params["page_limit"])) $params["pager"] = 1; //page_limit指定があれば pager対応
	    
	    $post_data = $this->get_ranking_list($params , $search_param_arr);
	    
	    $cnt = count($post_data);
	    
	    fn_getPages(count($post_data),$params["page_limit"]);
	    if(!isset($params["escape"]) || $params["escape"])
	    {
	        $post_data = fn_esc($post_data);
	    }
	    if(empty($post_data)) $post_data = array();
	    
	    return $this->c->_Escape($post_data);
	}
	
	/*
	 * ランキング一覧取得
	 */
	public function total_ranking_list($params, $search_param_arr){
	    global $LACNE;
	    
	    // 公開されているもののみ
	    $search_param_arr["post.output_flag ="] = 1;
	    
	    if(isset($params["page_limit"]) && is_numeric($params["page_limit"])) $params["pager"] = 1; //page_limit指定があれば pager対応
	    
	    $post_data = $this->get_total_ranking_list($params , $search_param_arr);
	    
	    $cnt = count($post_data);
	    
	    fn_getPages(count($post_data),$params["page_limit"]);
	    if(!isset($params["escape"]) || $params["escape"])
	    {
	        $post_data = fn_esc($post_data);
	    }
	    if(empty($post_data)) $post_data = array();
	    
	    return $this->c->_Escape($post_data);
	}
	
	/**
	 * get_ranking_list
	 * 表示対象となる記事データを取得する
	 * @param object $param //データ所得時の各種指定パラメータ
	 *    |-number $num //表示件数(all : 全て）※page_limitを指定する場合は空に
	 *    |-number $category //カテゴリ指定(ID)
	 *    |-number $page_limit //1ページ単位の表示数
	 *    |-string $date_target //日付指定しての絞込み(yyyy-mm-dd形式 絞りこみする場合、yyyyは必須 yyyy%やyyyy-mm% などが可能)
	 * @param object $search_param
	 * @param boolean $getCnt //記事データ数のみ取得する場合はtrueに
	 * @return mixed
	 */
	function get_ranking_list($param , $search_param = array())
	{
	    
	    $result = array();
	    
	    $search_param_arr = array(
	        "post.output_flag =" => 1,
	        "post.output_date <=" => fn_get_date()
	    );
	    //category , date_target以外でも他のデータ絞り込み条件があればそれをマージ
	    if($search_param)
	    {
	        $search_param_arr = array_merge($search_param_arr , $search_param);
	    }
	    
	    $cnt = with(new Ranking())->data_cnt($search_param_arr);
	    
	    //データのカウントだけなら
	    if($getCnt)
	    {
	        return $cnt;
	    }
	    
	    if($cnt){
	        
	        
	        //現在のページ
	        $page = 1;
	        
	        if($_GET['page']){
	            $page = fn_now_page_set($_GET['page']);
	        }
	        
	        $page_limit = NUM_LIST;
	        if(isset($param["page_limit"]) && is_numeric($param["page_limit"])) $page_limit = $param["page_limit"];
	        
	        //総ページ数取得
	        $page_num = fn_getPages($cnt,$page_limit);
	        
	        if($page > $page_num){
	            $page = $page_num;
	        }
	        
	        $this->post_data = with(new Ranking())->get_list($page , $page_limit , $search_param_arr);
	        
	        $result['data'] = $this->post_data;
	        $result['cnt'] = $cnt;
	        $result['page'] = $page;
	        
	        return $result;
	    }
	    
	    return false;
	}
	
	/**
	 * get_ranking_list
	 * 表示対象となる記事データを取得する
	 * @param object $param //データ所得時の各種指定パラメータ
	 *    |-number $num //表示件数(all : 全て）※page_limitを指定する場合は空に
	 *    |-number $category //カテゴリ指定(ID)
	 *    |-number $page_limit //1ページ単位の表示数
	 *    |-string $date_target //日付指定しての絞込み(yyyy-mm-dd形式 絞りこみする場合、yyyyは必須 yyyy%やyyyy-mm% などが可能)
	 * @param object $search_param
	 * @param boolean $getCnt //記事データ数のみ取得する場合はtrueに
	 * @return mixed
	 */
	function get_total_ranking_list($param , $search_param = array())
	{
	    
	    $result = array();
	    
	    $search_param_arr = array(
	        "post.output_flag =" => 1,
	        "post.output_date <=" => fn_get_date()
	    );
	    //category , date_target以外でも他のデータ絞り込み条件があればそれをマージ
	    if($search_param)
	    {
	        $search_param_arr = array_merge($search_param_arr , $search_param);
	    }
	    
	    $cnt = with(new TotalRanking())->data_cnt($search_param_arr);
	    
	    //データのカウントだけなら
	    if($getCnt)
	    {
	        return $cnt;
	    }
	    
	    if($cnt){
	        
	        
	        //現在のページ
	        $page = 1;
	        
	        if($_GET['page']){
	            $page = fn_now_page_set($_GET['page']);
	        }
	        
	        $page_limit = NUM_LIST;
	        if(isset($param["page_limit"]) && is_numeric($param["page_limit"])) $page_limit = $param["page_limit"];
	        
	        //総ページ数取得
	        $page_num = fn_getPages($cnt,$page_limit);
	        
	        if($page > $page_num){
	            $page = $page_num;
	        }
	        
	        $this->post_data = with(new TotalRanking())->get_list($page , $page_limit , $search_param_arr);
	        
	        $result['data'] = $this->post_data;
	        $result['cnt'] = $cnt;
	        $result['page'] = $page;
	        
	        return $result;
	    }
	    
	    return false;
	}
	
	/*
	 * keyword一覧取得
	 *
	 */
	function keyword_list()
	{
	    if(isset($params["page_limit"]) && is_numeric($params["page_limit"])) $params["pager"] = 1; //page_limit指定があれば pager対応
	    $rs = with(new Keyword())->fetchKeyword();
	    
	    $list = array();
	    if(!empty($rs)) {
	        foreach($rs as $value) {
	            $list[$value['id']] = fn_esc($value);
	        }
	    }
	    if(!isset($params["escape"]) || $params["escape"])
	    {
	        $list = fn_esc($list);
	    }
	    return $this->c->_Escape($list);
	}
	
	/**
	 * プレビューリクエストかどうかをチェック
	 * （POSTでtitleデータが送信されている、もしくはGETでpreview値が送信されてきている場合）
	 * @return void
	 */
	function preview_chk()
	{
		global $LACNE;
		if((isset($_POST["title"]) && $_POST["title"]) || (isset($_GET["preview"]) && $_GET["preview"]))
		{
			$this->preview_mode = true;
		}
		return;
	}

	/**
	 * ページャ
	 * @return void
	 */
	public function getPager($page, $cnt=0, $limit, $prev_page_txt = "前の30件", $next_page_txt = "次の30件" )
	{
		//現在のページ
		$page = 1;
		if(isset($_GET['page']) && is_numeric($_GET['page'])){
			$page = fn_now_page_set($_GET['page']);
		}

		//総ページ数取得
		$page_num = fn_getPages($cnt,$limit);
		if($page > $page_num){ $page = $page_num; }

		$pager = $this->pagenavi($page , $page_num , fn_set_urlparam($_GET, array(),false),true,$prev_page_txt,$next_page_txt); //ページャまわりの表示

		return $pager;
	}

	/**
	 *  ページング出力
	 *
	 *  @param  int $page  現在のページ数
	 *  @param  int $page_num  総ページ数
	 *  @param  boolean $return
	 *  @return void
	 */
	function pagenavi($page,$page_num,$param="",$return=false, $prev_page_txt, $next_page_txt){

		$show_nav = 9;
		//総ページの半分
		$show_navh = floor($show_nav / 2);
		//現在のページをナビゲーションの中心にする
		$loop_start = $page - $show_navh;
		$loop_end = $page + $show_navh;
		//現在のページが両端だったら端にくるようにする
		if ($loop_start <= 0) {
			$loop_start  = 1;
			$loop_end = $show_nav;
		}
		if ($loop_end > $page_num) {
			$loop_start  = $page_num - $show_nav +1;
			$loop_end =  $page_num;
		}

		if($param){
			$linkparam = '?'.$param.'&page=';
		}else{
			$linkparam = '?page=';
		}

		if($page_num > 1){

			$output_txt = '';


			// ボタンリスト
			$output_txt .= '<div class="l-pager__prev">';

			if($page > 1){
				$output_txt .= '<div class="more c-btn c-btn--size01 c-btn--color01"><a href="' . $linkparam.($page-1).'">' . $prev_page_txt . '<span class="c-arrow c-arrow--size01 c-arrow--color01 c-arrow--left01"></span></a></div>';
			}

			$output_txt .= '</div>';
			$output_txt .= '<div class="l-pager__next">';

			if($page != $page_num){
				$output_txt .= '<div class="more c-btn c-btn--size01 c-btn--color01"><a href="' .$linkparam.($page+1).'">' . $next_page_txt . '<span class="c-arrow c-arrow--size01 c-arrow--color01 c-arrow--right02"></span></a></div>';
			}
			$output_txt .= '</div>';

			$output_txt .= '';

			// ページ番号リスト
			$output_txt .= '<div class="l-pager__numbers">';
			if($loop_start > 1) {
				$output_txt .= '<div class="item"><a href="'.$linkparam.'1">1</a></div>';
				if($loop_start > 2) {
					$output_txt .= '<div class="item"><span class="ellipsis">…</span></div>';
				}
			}

			for ($i=$loop_start; $i<=$loop_end; $i++) {
				if ($i > 0 && $page_num >= $i) {
					if($i == $page){
						$output_txt .= "<div class=\"item\"><span  class=\"is-active\">".$i."</span></div>";
					}else{
						$output_txt .= "<div class=\"item\"><a href=\"".$linkparam.$i."\">".$i."</a></div>";
					}
				}
			}

			if($loop_end < $page_num) {
				if($loop_end < ($page_num - 1)) {
					$output_txt .= '<div class="item"><span class="ellipsis">…</span></div>';
				}
				$output_txt .= '<div><a href="'.$linkparam.$page_num.'">'.$page_num.'</a></div>';
			}

			$output_txt .= '</div>';

			if($return)
			{
				return $output_txt;
			}

			echo $output_txt;
		}

		return "";
	}
	
	public function getTagName($id){

		$tagData= with(new Tag())->fetchOne($id);

		return $tagData;
	}

	/**
	 * 指定されたIDの前後にくる記事データを取得
	 */
	function getNextAndPrevData($id , $search_param = array())
	{
	    global $LACNE;
	    
	    $param = array(
	        'num' => 'all'
	    );
	    $search_param = array_merge($search_param , array(
	        "post.output_flag =" => 1,
	        "post.output_date <=" => fn_get_date()
	    ));
	    
	    $_post_data = $this->get_post_list($param ,$search_param);
	    
	    $prev_data = array();
	    $next_data = array();
	    if(!empty($_post_data['data'])) {
	        $post_data = $_post_data['data'];
	        foreach($post_data as $key => $data) {
	            if($data['id'] == $id) {
	                
	                //前のデータ
	                if(isset($post_data[($key+1)])) {
	                    $prev_data = $post_data[($key+1)];
	                }
	                
	                //次のデータ
	                if(isset($post_data[($key-1)])) {
	                    $next_data = $post_data[($key-1)];
	                }
	            }
	        }
	    }
	    
	    
	    return array(
	        'prev' => fn_esc($prev_data),
	        'next' => fn_esc($next_data)
	    );
	}

}