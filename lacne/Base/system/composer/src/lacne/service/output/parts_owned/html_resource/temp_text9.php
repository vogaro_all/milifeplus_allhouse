<ul class="movie-list">
	<?php array_unshift($composeItem, $entity)?>
	<?php foreach($composeItem as $entityItem): ?>
              <li class="movie-list__item">
                <a href="<?php echo( $entityItem->url_name ); ?>" target=”_blank” onclick="gtag('event', location.pathname, {'event_category': 'KUMApedia_Youtube','event_label': '<?php echo( $entityItem->title_name ); ?>'});">
                  <article class="card">
                    <div class="movie">
                      <img src="<?php echo( $entityItem->thumbnail_name ); ?>" alt="">
                    </div>
                    <div class="information">
                      <p class="information__title"><?php echo( $entityItem->title_name ); ?></p>
                      <p class="information__description"><?php echo( $entityItem->caption_name ); ?></p>
                    </div>
                  </article>
                </a>
              </li>
	<?php endforeach; ?>
</ul>