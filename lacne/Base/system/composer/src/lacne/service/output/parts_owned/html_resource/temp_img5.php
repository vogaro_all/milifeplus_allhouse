          <div class="sec-content__block sec-content__block--col">
            <div class="sec-content__group01">
              <?php if (!empty($entity->title_name)) {?>
              <h6><?php echo( $entity->title_name ); ?></h6>
              <?php }?>
              <p><?php echo(htmlspecialchars_decode($entity->content));?></p>
            </div>
            <img src="<?php echo( $entity->getImg() ); ?>" alt="">
          </div>