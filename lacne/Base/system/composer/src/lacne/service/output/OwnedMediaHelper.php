<?php namespace lacne\service\output;

use lacne\core\domain\MediaEntity;
use lacne\domain\Banner\entity\BannerEntity;
use lacne\core\Template;

/**
 * フロント用のヘルパークラス
 * Class Helper
 * @package lacne\service\output
 */
class OwnedMediaHelper
{

    /**
     * 汎用ページャ
     * @param  Pager $pager Pagerインスタンス
     * @return string pager string 
     */
    public static function pager($pager)
    {
        $template = new Template(dirname(__FILE__) . '/tmpl');
        return $template->render('pager', array("pager"=>$pager), true);
    }

    /**
     * 汎用SPページャ
     * @param  Pager $pager Pagerインスタンス
     * @return string pager string
     */
    public static function pager_sp($pager)
    {
    	$template = new Template(dirname(__FILE__) . '/tmpl');
    	return $template->render('pager_sp', array("pager"=>$pager), true);
    }
}

