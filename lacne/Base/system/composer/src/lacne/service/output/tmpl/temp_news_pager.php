<?php 
if (isset($_GET['category']) && is_numeric($_GET['category'])) {
	$add_pager_link = '&category='.$_GET['category'];
}

if (isset($_GET['key']) && !empty($_GET['key']) && is_numeric($_GET['key'])) {
    $add_pager_link .= '&key='.$_GET['key'];
}

if (isset($_GET['keywords']) && !empty($_GET['keywords'])) {
    $add_pager_link .= '&keywords='.$_GET['keywords'];
}

?>
<?php if( $pager->hasPages() == true ) : ?>
	<div class="c-pager">
		<?php $pager->onePrev(function($pager, $prevID, $prevTxt, $add_pager_link){ ?>
		<div class="c-pager__prev"><a href="?page=<?php echo($prevID.$add_pager_link); ?>">前へ</a></div>
		<?php }, function($pager){ ?>
		<div class="c-pager__prev"><a>前へ</a></div>
	<?php }, $add_pager_link); ?>
		
		<ul class="c-pager__pagination">
		<?php $pager->getPages(function($i, $active, $pager, $isPrevEllipsis, $isNextEllipsis, $add_pager_link){ ?>
	
			<?php if($isNextEllipsis && $i == $pager->getLastPage()): ?>
			<li class="c-pager__page">…</li>
			<?php endif; ?>

			<?php if( $active == $i ): ?>
			<li class="c-pager__page is-active"><a><?php echo($i);?></a></li>
			<?php else: ?>
			<li class="c-pager__page"><a href="?page=<?php echo($i.$add_pager_link); ?>"><?php echo($i); ?></a></li>
			<?php endif; ?>

			<?php if($isPrevEllipsis && $i == 1): ?>
			<li class="c-pager__page"><span>…</span></li>
			<?php endif; ?>
		<?php }, 2, $add_pager_link); ?>
		</ul>

	<?php $pager->oneNext(function($pager, $nextID, $nextTxt, $add_pager_link){ ?>
	<div class="c-pager__next"><a href="?page=<?php echo($nextID.$add_pager_link); ?>">次へ</a></div>
	<?php },function($pager){ ?>
	<div class="c-pager__next"><a>次へ</a></div>
	<?php }, $add_pager_link); ?>
	
	</div>
<?php endif; ?>