<?php namespace lacne\service\output;

use lacne\domain\OwnedMedia\entity\MediaEntity;
use lacne\domain\OwnedMedia\entity\TagEntity;
use lacne\service\output\OwnedMediaHelper;

/**
 * 記事情報の掲載するために扱うAPIクラスの親クラス
 * Class OwnedMediaBaseAPI
 * @package lacne\service\output
 */
class OwnedMediaBaseAPI extends OwnedMedia
{
    /**
     * 親クラスより取得後、フック処理を挟みたい場合に用意
     * @param $params
     * @param $search_param
     * @param array $search_or_param
     * @param array $sql_order
     * @return array
     */
    public function lists($params, $search_param, $search_or_param = array(), $sql_order = array())
    {
        $ret = parent::lists($params, $search_param, $search_or_param, $sql_order);
        return empty( $ret )? array() : $ret;
    }

    /**
     * @param $fnName
     * @param $fn
     * @param bool $d
     */
    public function load($fnName, $params = array())
    {
        $index = 0;
        if(!empty($params)){
            $ret = $this->$fnName($params);
        }else{
            $ret = $this->$fnName();    
        }
        
        // Warning対応
        if (!isset($ret) || empty($ret)) {
        	$ret = array();
        } 
        $self = $this;
        $pager = !empty($ret['pager'])? $ret['pager']: '';

        $debugFunc = function( $ret, $item, $d ) {
            if( $d == true ){
                var_dump("<pre>", $ret , "</pre>");
                var_dump("<pre>", $item , "</pre>");
            }
        };

        return array(
            "has" => function() use ($ret){
                $ret = array_filter($ret);
                return empty($ret) ? false : true;
            },
            "done" => function($fn, $d = false) use ($ret, $debugFunc){
                $index = 0;

                foreach($ret["data"] as $item){
                    $item = new MediaEntity($item);
                    $debugFunc($ret, $item, $d);
                    call_user_func($fn, $item, $ret["cnt"], $ret["page"], new OwnedMediaHelper(), ++$index );
                }
            },
            "tagdone" => function($fn, $d = false) use ($ret, $debugFunc){
                $index = 0;

                foreach($ret as $item){
                    $item = new TagEntity($item);
                    $debugFunc($ret, $item, $d);
                    call_user_func($fn, $item, $ret["cnt"], $ret["page"], new OwnedMediaHelper(), ++$index );
                }
            },
            "pager" => function() use($pager){
                return htmlspecialchars_decode( htmlspecialchars_decode( $pager ) );
            },
            "ret" => $ret
        );
    }

    /**
     * @param int $limit 1ページあたりのアイテムリミット数
     * @return array
     */
    protected function getParam($limit = 10)
    {
        return array(
            "page_limit" => $limit
        );
    }

    /**
     * 除外したいID
     * @param $key
     * @return array
     */
    protected function getSearchParam($searchParam, $ids = array())
    {
        if(!empty($ids))
        {
            $searchParam += array("post.id NOT IN (" . implode(',', $ids) .  ") " => '');
        }
        return $searchParam;
    }

}