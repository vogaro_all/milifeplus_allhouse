<?php namespace lacne\service\command;

/**
 * 簡易コマンド管理クラス
 * Class CommandList
 * @package lacne\service\command
 */
class CommandList
{
    protected static $list = array();

    /**
     * コマンドを追加する
     * @param Command $c
     */
    public static function add(Command $c){ array_push(self::$list, $c); }

    /**
     * 先頭のコマンドを実行し内容をキューから削除する。
     * @return bool true | false
     */
    public static function execute(){ return array_shift(self::$list)->execute(); }

    /**
     * 先頭からコマンドを全て随時実行し内容をキューから削除する。
     */
    public static function executes()
    {
        while( ( $target = array_shift( self::$list ) ) !== NULL ){
            $target->execute();
        }
    }
}

/**
 * Class Command
 * @package lacne\service\command
 */
abstract class Command
{
    /**
     * @var string 開始メッセージ(あれば)
     */
    protected $startMessage = '';

    /**
     * @var string 終了メッセージ(あれば)
     */
    protected $endMessage = '';

    /**
     * 特になし
     * Command constructor.
     */
    public function __construct(){}

    /**
     * 設定系
     */
    protected function config(){ return array(); }

    /**
     * 処理の組み立て
     * @param array $config
     * @return mixed
     */
    abstract protected function body($config = array());

    /**
     * コマンドの実行
     */
    public function execute()
    {
        $config = $this->config();
        $config = empty($config)? array(): $config;

        $log = $this->body($config);
        $this->writeLog($log);
    }

    /**
     * ログファイル名を取得します。
     * ファイル名の形式はapi_log_Y-m-d.txtになります。
     * @return string
     */
    public function fileName(){ return 'cron_log_'. date('Y-m-d') . '.log'; }

    /**
     * ログの書き込み
     * @param $log
     */
    protected function writeLog($log)
    {
        $convert = function($time){
            $_time = $time;
            if($_time) {
                list($sec, $usec) = explode('.', $_time);
                return date('H:i:s', $sec);
            }

            return 0;
        };

        $start_time = $convert($log['start_time']);
        $end_time = $convert($log['end_time']);

        $str = print_r( array(
            'start_time'        => $start_time,
            'end_time'          => $end_time,
            'created'           => date('Y-m-d H:i:s') . ' ' . microtime(true),
            'message'           => $this->startMessage . ' ' . $log['message'] . ' ' . $this->endMessage
        ), true);

        file_put_contents(CRON_LOG_PATH . '/' . $this->fileName(), $str, FILE_APPEND);
    }
}