<?php namespace lacne\core\model;
use lacne\core\Model;

/**
// ------------------------------------------------------------------------
 * model_category.php
 * カテゴリデータ用モデル
 * @package		Lacne
 * @author		In Vogue Inc.
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class Category extends Model
{

    /**
     * Category constructor.
     */
    public function __construct(){ parent::__construct(); }

    function fetchAll()
    {
        $sql = "SELECT * FROM ".$this->getTableName(TABLE_CATEGORY)." ORDER BY sort_no ASC, id ASC";
        return $this->_fetchAll($sql, array());
    }

    // カテゴリを抽出
    function fetchAllCategory($search_param = array())
    {
    	$sql = "SELECT category.*,
    			FROM ".$this->getTableName(TABLE_CATEGORY)." as category";

    	if(count($search_param) >= 1)
    	{
    		foreach($search_param as $key => $val)
    		{
    			$sql .= " AND " . $key . " ".$this->esc($val);
    		}
    	}

		$sql .= " ORDER BY category.sort_no ASC, category.id ASC";

    	return $this->_fetchAll($sql, array());
    }


    function fetchOne($id)
    {
        if(is_numeric($id))
        {
            $sql = "SELECT * FROM ".$this->getTableName(TABLE_CATEGORY)." WHERE id = ?";
            return $this->_fetchOne($sql, array($id));
        }

        return;
    }

    function replace($data , $key)
    {
        return $this->adodb_replace($this->getTableName(TABLE_CATEGORY) , $data , $key);
    }

    function delete($id)
    {

        if(is_numeric($id))
        {

            $sql = "DELETE FROM ".$this->getTableName(TABLE_CATEGORY)." WHERE id = ?";
            return $this->_execute($sql, array($id));

        }

        return;
    }


    function cnt($where , $param)
    {
        return $this->_cnt($this->getTableName(TABLE_CATEGORY) , $where , $param);
    }

    function fetchOneDoctor($id)
    {
    	if(is_numeric($id))
    	{
    		$sql = "SELECT * FROM doctor_category WHERE id = ?";
    		return $this->_fetchOne($sql, array($id));
    	}
    
    	return;
    }
    
}

?>