<?php namespace lacne\core\model;
use lacne\core\Model;

/**
// ------------------------------------------------------------------------
 * model_media.php
 * Rankingデータ用モデル
 * @package		Lacne
 * @author		In Vogue Inc.
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class Ranking extends Model
{

    /**
     * Media constructor.
     */
    public function __construct() { parent::__construct(); }

    /**
     *
     */
    function fetchAll()
    {
        $sql = "SELECT * FROM ".$this->getTableName(TABLE_RANKING)." ORDER BY id DESC";
        return $this->_fetchAll($sql, array());
    }

    function fetchOne($id)
    {
        if(is_numeric($id))
        {
            $sql = "SELECT * FROM ".$this->getTableName(TABLE_RANKING)." WHERE id = ?";
            return $this->_fetchOne($sql, array($id));
        }

        return;
    }

    /**
     * PVの多い順にすべてのデータを取得
     */
    function fetchRanking()
    {
        $sql = "SELECT * FROM ".$this->getTableName(TABLE_RANKING)." ORDER BY pv DESC";
        return $this->_fetchAll($sql, array());
    }

    function fetchRankingDownload()
    {
    	$sql = "SELECT title,url FROM ".$this->getTableName(TABLE_RANKING);
    	return $this->_fetchAll($sql, array());
    }
    
    function replace($data , $key)
    {
        foreach($data as $key => $val){
            if(!$this->adodb_replace($this->getTableName(TABLE_RANKING) , $val , $key)){
                return false;
            }
        }
        return true;
    }

    function delete($where = "" , $param = array())
    {

        $sql = "DELETE FROM ".$this->getTableName(TABLE_RANKING)." WHERE ".$where;
        return $this->_execute($sql, $param);

    }

    function cnt($where = "" , $param = array())
    {
        return $this->_cnt($this->getTableName(TABLE_RANKING) , $where , $param);
    }

    function get_tag_cnt()
    {
        $sql = "SELECT tag , count(id) as cnt FROM ".$this->getTableName(TABLE_RANKING)." WHERE tag != '' GROUP BY tag ORDER BY created DESC";
        return $this->_fetchAll($sql , array());
    }

    /**
     * 公開されているranking上の記事データの総件数
     * @param object $search_param
     * @return number
     */
    function data_cnt($search_param)
    {
    	$sql = $this->get_list_sql($search_param);
    	if($rs = $this->conn->Execute($sql)){
    		return $rs->RecordCount();
    	}

    	return 0;
    }

    /**
     * 一覧取得用のSQL生成
     * @param object $search_param
     * @param object $order
     * @return string
     */
    function get_list_sql($search_param)
    {

        $sql = "SELECT post.*, category.category_name as category_name
    			 FROM ".$this->getTableName(TABLE_RANKING)." as ranking
    				LEFT JOIN  ".$this->getTableName(TABLE_POSTS)." as post  ON  post.id = REPLACE(ranking.url,'https://orulab.allhouse.co.jp/contents/','')
                    LEFT JOIN ".$this->getTableName(TABLE_CATEGORY)." as category ON post.category = category.id
    				WHERE 1 = 1";
        
//     	$sql = "SELECT post.*, category.category_name as category_name,
//                     GROUP_CONCAT(concat(tag.id, ':', tag.tag_name) separator ',') as tags
//     			    FROM ".$this->getTableName(TABLE_RANKING)." as ranking
// 		    		LEFT JOIN  ".$this->getTableName(TABLE_POSTS)." as post ON post.id = REPLACE(ranking.url,'/archive/archive/detail/?id=','')
//                     LEFT JOIN ".$this->getTableName(TABLE_CATEGORY)." as category ON post.category = category.id
//                     LEFT JOIN ".$this->getTableName(TABLE_POST_TAG)." as tag_post ON post.id = tag_post.ownedmedia_posts_id
//                     LEFT JOIN ".$this->getTableName(TABLE_TAG)." as tag ON tag.id = tag_post.tag_id
//     				WHERE 1 = 1";

//     	$sql = "SELECT post.*, category.category_name as category_name, total_ranking.id as total_ranking_id,
//                     GROUP_CONCAT(concat(tag.id, ':', tag.tag_name) separator ',') as tags
//     			    FROM ".$this->getTableName(TABLE_RANKING)." as ranking
//      			   LEFT JOIN  ".$this->getTableName(TABLE_POSTS)." as post ON post.original_filename = ranking.url
//                     LEFT JOIN ".$this->getTableName(TABLE_CATEGORY)." as category ON post.category = category.id
//                     LEFT JOIN ".$this->getTableName(TABLE_POST_TAG)." as tag_post ON post.id = tag_post.ownedmedia_posts_id
//                     LEFT JOIN ".$this->getTableName(TABLE_TAG)." as tag ON tag.id = tag_post.tag_id
//                     LEFT JOIN ".$this->getTableName(TABLE_TOTAL_RANKING)." as total_ranking ON post.original_filename = total_ranking.url
//     				WHERE 1 = 1";
    	
    	// AND句
    	if(is_array($search_param) && count($search_param))
    	{
    		foreach($search_param as $key => $val)
    		{
    			$sql .= " AND ";
    			$sql .= $key . " ".$this->esc($val);
    		}
    	}

        $sql .= " GROUP BY post.id ";
    	$sql .= " order by ranking.pv DESC";

    	return $sql;
    }

    /**
     * 一覧データ取得
     * @param number $page
     * @param number $limit
     * @param object $search_param
     * @param object $order
     * @return object
     */
    function get_list($page , $limit , $search_param = array())
    {
    	$sql = $this->get_list_sql($search_param);

    	return $this->_fetchPage($sql, $page, $limit);
    }

}

?>