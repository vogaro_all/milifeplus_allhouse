<?php namespace lacne\core\model;
use lacne\core\Model;

/**
// ------------------------------------------------------------------------
 * model_tag.php
 * taguデータ用モデル
 * @package		Lacne
 * @author		In Vogue Inc.
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class Tag extends Model
{

    /**
     * Tag constructor.
     */
    public function __construct(){ parent::__construct(); }

    function fetchAll()
    {
        $sql = "SELECT * FROM ".$this->getTableName(TABLE_TAG)." ORDER BY id ASC";
        return $this->_fetchAll($sql, array());
    }

    function fetchOne($id)
    {
        if(is_numeric($id))
        {
            $sql = "SELECT * FROM ".$this->getTableName(TABLE_TAG)." WHERE id = ?";
            return $this->_fetchOne($sql, array($id));
        }

        return;
    }

    function fetchOneByName($name)
    {
        $sql = "SELECT id FROM ".$this->getTableName(TABLE_TAG)." WHERE tag_name = ?";
        return $this->_fetchOne($sql, array($name));
    }

    function replace($data , $key)
    {
        return $this->adodb_replace($this->getTableName(TABLE_TAG) , $data , $key);
    }

    function delete($id)
    {

        if(is_numeric($id))
        {

            $sql = "DELETE FROM ".$this->getTableName(TABLE_TAG)." WHERE id = ?";
            return $this->_execute($sql, array($id));

        }

        return;
    }


    function cnt($where , $param)
    {
        return $this->_cnt($this->getTableName(TABLE_TAG) , $where , $param);
    }

    /**
     * 一覧取得用のSQL生成
     * @param object $search_param
     * @param object $order
     * @return string
     */
    function get_list_sql($search_param)
    {

        $sql = "SELECT post.*, tag.*
                FROM ".$this->getTableName(TABLE_POST_TAG)." as post_tag
                    LEFT JOIN ".$this->getTableName(TABLE_TAG)." as tag ON post_tag.tag_id = tag.id
                    LEFT JOIN ".$this->getTableName(TABLE_TOPIC)." as topic ON topic.tag_id = tag.id
                    LEFT JOIN ".$this->getTableName(TABLE_POSTS)." as post ON post.id = post_tag.ownedmedia_posts_id
                    WHERE 1 = 1";

        // AND句
        if(is_array($search_param) && count($search_param))
        {
            foreach($search_param as $key => $val)
            {
                $sql .= " AND ";
                $sql .= $key . " ".$this->esc($val);
            }
        }

        $sql .= " order by post_tag.pv DESC";

        return $sql;
    }

    /**
     * 一覧データ取得
     * @param number $page
     * @param number $limit
     * @param object $search_param
     * @param object $order
     * @return object
     */
    // function get_list($page , $limit , $search_param = array())
    // {
    //     $sql = $this->get_list_sql($search_param);

    //     return $this->_fetchPage($sql, $page, $limit);
    // }

    // /**
    //  * 公開されているランキング上の記事データの総件数
    //  * @param object $search_param
    //  * @return number
    //  */
    // function data_cnt($search_param)
    // {
    //     $sql = $this->get_list_sql($search_param);
    //     if($rs = $this->conn->Execute($sql)){
    //         return $rs->RecordCount();
    //     }

    //     return 0;
    // }

}

?>