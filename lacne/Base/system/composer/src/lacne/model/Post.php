<?php namespace lacne\core\model;
use lacne\core\Model;
use lacne\core\model\Tag;
use lacne\core\model\PostTag;

/**
// ------------------------------------------------------------------------
 * model_post.php
 * 記事データ用モデル
 * @package		Lacne
 * @author		In Vogue Inc.
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class Post extends Model
{

    /**
     * Post constructor.
     */
    public function __construct() { parent::__construct(); }

    /**
     * 全データ取得
     */
    function fetchAll()
    {
        $sql = "SELECT * FROM ".$this->getTableName(TABLE_POSTS);
        return $this->_fetchAll($sql, array());
    }

    /**
     * 指定したidのデータ1件取得
     * @param number $id
     * @return object
     */
    function fetchOne($id)
    {
        if(is_numeric($id))
        {
            $sql = "SELECT post.* , category.category_name as category_name
            		 FROM ".$this->getTableName(TABLE_POSTS)." as post
                        LEFT JOIN ".$this->getTableName(TABLE_CATEGORY)." as category ON post.category = category.id
                        WHERE post.id = ? ";

            return $this->_fetchOne($sql, array($id));
        }

        return;
    }

    /**
     * 指定したoriginal_filenameのデータ1件取得
     * @param number $original_filename
     * @return object
     */
    function fetchOne_by_original_filename($original_filename, $category)
    {
    	if(is_numeric($category))
    	{
    		$sql = "SELECT post.* , category.category_name as category_name
            		 FROM ".$this->getTableName(TABLE_POSTS)." as post
                        LEFT JOIN ".$this->getTableName(TABLE_CATEGORY)." as category ON post.category = category.id
                        WHERE post.original_filename = ? AND post.category = ? ";
    
    		return $this->_fetchOne($sql, array($original_filename, $category));
    	}
    
    	return;
    }
    
    /**
     * 指定したカテゴリに紐付く記事データ取得
     * @param number $id
     * @return object
     */
    function fetchByCategoryID($cate_id)
    {
    	if(is_numeric($cate_id))
    	{
    		$sql = "SELECT post.* , category.category_name FROM ".$this->getTableName(TABLE_POSTS)." as post
                        LEFT JOIN ".$this->getTableName(TABLE_CATEGORY)." as category ON post.category = category.id
                        WHERE category.id = ?";

    		return $this->_fetchAll($sql, array($cate_id));
    	}

    	return;
    }

    /**
     * 指定したタグに紐付く記事データ取得
     * @param number $id
     * @return object
     */
    function fetchByTagID($tag_id,$cate_id)
    {

    	if(is_numeric($tag_id))
    	{
    		$arr_val = array();
    		$sql = "SELECT post.* FROM ".$this->getTableName(TABLE_POSTS)." as post
                        LEFT JOIN ".$this->getTableName(TABLE_POST_TAG)." as posttag ON posttag.ownedmedia_posts_id = post.id
                        WHERE 1 = 1";

    		if(!empty($tag_id))
    		{
    			$sql .= " AND posttag.tag_id = ? ";
    			$arr_val[] = $tag_id;
    		}

    		if(!empty($cate_id))
    		{
    			$sql .= " AND post.category = ? ";
    			$arr_val[] = $cate_id;
    		}

    		$sql .= " ORDER BY post.id DESC ";

    		return $this->_fetchAll($sql, $arr_val);
    	}

    	return;
    }


    /**
     *
     * データのインサートもしくはアップデート処理
     *
     * @param object $data
     * @param string $key
     * @param boolean $auto_quote
     */
    function replace($data , $key , $auto_quote = true)
    {
        $tid = $this->adodb_replace($this->getTableName(TABLE_POSTS) , $data , $key);

        return $tid;
    }

    /**
     *
     * データのインサートもしくはアップデート処理
     *
     * @param object $data
     * @param string $key
     * @param boolean $auto_quote
     */
    function replace_ownedmedia($data , $tag_names, $recommend, $key , $auto_quote = true)
    {
    	try
    	{
    		$this->conn->StartTrans();
    		//記事更新
    		$post_id = $this->adodb_replace($this->getTableName(TABLE_POSTS) , $data , $key);
            
		    //タグと記事の関連追加
			$ret = with(new PostTag())->replace($post_id, $tag_names, 'id');
    		
    		$this->conn->CompleteTrans();

    	}
    	catch(Exception $e)
    	{
    		var_dump($e->getMessage());
    		exit;
    	}

    	return $post_id;
    }

    function delete($id)
    {
        if(is_numeric($id))
        {
            $sql = "DELETE FROM ".$this->getTableName(TABLE_POSTS)." WHERE id = ?";
            $this->_execute($sql, array($id));
        }

        return;
    }

    /**
     * 該当するデータのカウント
     *
     * @param string $where
     * @param array  $param
     */
    function cnt($where , $param)
    {
        return $this->_cnt($this->getTableName(TABLE_POSTS) , $where , $param);
    }

    /**
     * 一覧取得用のSQL生成（1対多テーブルの複数join対応バージョン)
     * or検索はサブクエリ内のテーブルに対してしかかけれない（postテーブル）
     * @param object $search_param
     * @param object $order
     * @return string
     */
    function get_list_sql($search_param, $order = array(), $search_or_param=array())
    {

        $sub = "SELECT post.*
        		FROM ".$this->getTableName(TABLE_POSTS)." AS post
                WHERE 1 = 1
                ";
        
        // OR句
        if(is_array($search_or_param) && count($search_or_param))
        {
            $cnt = 0;
            $sub .= " AND ( ";
            foreach($search_or_param as $key => $val)
            {
                if($cnt) $sub .= " OR ";
                $sub .= $key . " "  . $this->esc($val);
                $cnt++;
            }
            $sub .= " ) ";
        }

        $sub .= " GROUP BY post.id ";

        $sql = "SELECT post.*, category.category_name, tag.id as tag_id, tag.tag_name,
                GROUP_CONCAT(DISTINCT concat(tag.id, ':', tag.tag_name) separator ',') as tags
                FROM
                (". $sub .") post
                LEFT JOIN ".$this->getTableName(TABLE_CATEGORY)." AS category ON post.category = category.id
                LEFT JOIN ".$this->getTableName(TABLE_POST_TAG)." AS tag_post ON post.id = tag_post.ownedmedia_posts_id
                LEFT JOIN ".$this->getTableName(TABLE_TAG)." AS tag ON tag.id = tag_post.tag_id
                WHERE 1 = 1
                ";
        
        // AND句
        if(is_array($search_param) && count($search_param))
        {
            foreach($search_param as $key => $val)
            {
                $sql .= " AND ";
                $sql .= $key . " ".$this->esc($val);
            }
        }

        $sql .= " GROUP BY post.id ";

        //ソート調整
        if(isset($order["key"]) && $order["key"]){
            $sql .= " ORDER BY ".$order["key"];
            if(isset($order["by"]) && $order["by"]){
                $sql .= " ".$order["by"];
            }
            else
            {
                $sql .= " DESC ";
            }
            $sql .= ",";
            if($order["val"] != "sort_date")
            {
                $sql .= "post.output_date DESC ,";
            }

            $sql .= "post.id DESC";
        }
        
        return $sql;
    }

    /**
     * 一覧取得用のSQL生成（1対多テーブルの複数join対応バージョン)
     * or検索はサブクエリ内のテーブルに対してしかかけれない（postテーブル）
     * @param object $search_param
     * @param object $order
     * @return string
     */
    function get_list_sql_top($search_param, $order = array(), $search_or_param=array())
    {
    
    	$sub = "SELECT post.*
        		FROM ".$this->getTableName(TABLE_POSTS)." AS post
                WHERE 1 = 1
                ";
    
    	// OR句
    	if(is_array($search_or_param) && count($search_or_param))
    	{
    		$cnt = 0;
    		$sub .= " AND ( ";
    		foreach($search_or_param as $key => $val)
    		{
    			if($cnt) $sub .= " OR ";
    			$sub .= $key . " "  . $this->esc($val);
    			$cnt++;
    		}
    		$sub .= " ) ";
    	}
    
    	$sub .= " GROUP BY post.id ";

    	$sql = "SELECT post.*, category.category_name, tag.id as tag_id, tag.tag_name,
                GROUP_CONCAT(DISTINCT concat(tag.id, ':', tag.tag_name) separator ',') as tags
                FROM
                (". $sub .") post
                LEFT JOIN ".$this->getTableName(TABLE_CATEGORY)." AS category ON post.category = category.id
                LEFT JOIN ".$this->getTableName(TABLE_POST_TAG)." AS tag_post ON post.id = tag_post.ownedmedia_posts_id
                LEFT JOIN ".$this->getTableName(TABLE_TAG)." AS tag ON tag.id = tag_post.tag_id
                WHERE 1 = 1
                ";
    
    	// AND句
    	if(is_array($search_param) && count($search_param))
    	{
    		foreach($search_param as $key => $val)
    		{
    			$sql .= " AND ";
    			$sql .= $key . " ".$this->esc($val);
    		}
    	}
    
    	$sql .= " GROUP BY post.id ";
    
    	//ソート調整
    	if(isset($order["key"]) && $order["key"]){
    		$sql .= " ORDER BY ".$order["key"];
    		if(isset($order["by"]) && $order["by"]){
    			$sql .= " ".$order["by"];
    		}
    		else
    		{
    			$sql .= " DESC ";
    		}
    		$sql .= ",";
    		if($order["val"] != "sort_date")
    		{
    			$sql .= "post.output_date DESC ,";
    		}
    
    		$sql .= "post.id DESC";
    	}
    
    	return $sql;
    }
    
    /**
     * 一覧データ取得
     * @param number $page
     * @param number $limit
     * @param object $search_param
     * @param object $order
     * @return object
     */
    function get_list($page , $limit , $search_param = array() , $order = array(), $search_or_param = array())
    {
        $sql = $this->get_list_sql($search_param , $order, $search_or_param);

        return $this->_fetchPage($sql, $page, $limit);
    }
    
    /**
     * 一覧データ取得
     * @param number $page
     * @param number $limit
     * @param object $search_param
     * @param object $order
     * @return object
     */
    function get_list_top($page , $limit , $search_param = array() , $order = array(), $search_or_param = array())
    {
    	$sql = $this->get_list_sql_top($search_param , $order, $search_or_param);
    
    	return $this->_fetchPage($sql, $page, $limit);
    }
    
    /**
     * 一覧データ取得（1ページあたり$limit件数とさらに$plus件数分余分にデータ取得する）
     * @param number $page
     * @param number $limit
     * @param number $plus
     * @param object $search_param
     * @param object $order
     * @return object
     */
    function get_list_plus($page , $limit , $plus , $search_param = array() , $order = array())
    {
        $sql = $this->get_list_sql($search_param , $order);
        $sql .= " LIMIT ".($limit+$plus)." OFFSET ".(($page-1)*$limit);
        return $this->_fetchAll($sql,array());
    }


    /**
     * 登録されている記事データの総件数
     * @param object $search_param
     * @return number
     */
    function data_cnt($search_param, $search_or_param=array())
    {
        $sql = $this->get_list_sql($search_param,array(),$search_or_param);
        if($rs = $this->conn->Execute($sql)){
            return $rs->RecordCount();
        }

        return 0;
    }

    
    
    
    
    /**
     * 指定パーマリンクが既に登録されている場合にtrue
     * @param object $search_param
     * @return number
     */
    function original_filename_exist($id = "", $original_filename)
    {
    	$sql = "SELECT *
				FROM ".$this->getTableName(TABLE_POSTS) . " as post 
				WHERE `original_filename` = ? ";
    	
    	$param = array($original_filename);
    	
    	if (isset($id) && !empty($id)) {
    		$sql .= " AND post.id <> ?";
    		array_push($param, $id);
    	}
    	
    	$rs = $this->_fetchOne($sql , $param);
    
    	return (isset($rs) && !empty($rs)) ? $rs : false;
    }
    
    /**
     * カテゴリごとに何件のデータがあるかを取得
     * @param string $where
     * @param array $param
     * @return array
     */
    function getnum_category_by($where = "" , $param = array())
    {

        $result = array();
        $sql = "SELECT post.category as category , count(post.id) as cnt FROM ".$this->getTableName(TABLE_POSTS) . " as post " .
        		"LEFT JOIN ".$this->getTableName(TABLE_CATEGORY)." as category_tbl ON post.category = category_tbl.id";
        if($where)
        {
            $sql .= " WHERE ".$where;
        }

        $sql .= " GROUP BY category ORDER BY category ASC";

        $rs = $this->_fetchAll($sql , $param);
        if($rs)
        {
            foreach($rs as $data)
            {
                $result[$data["category"]] = $data["cnt"];
            }
        }

        return $result;
    }

    /**
     * サブカテゴリごとに何件のデータがあるかを取得
     * @param string $where
     * @param array $param
     * @return array
     */
    function getnum_sub_category_by($where = "" , $param = array())
    {
        
        $result = array();
        $sql = "SELECT post.sub_category as sub_category , count(post.id) as cnt FROM ".$this->getTableName(TABLE_POSTS) . " as post " .
            "LEFT JOIN ".$this->getTableName(TABLE_SUB_CATEGORY)." as category_tbl ON post.sub_category = category_tbl.id";
        if($where)
        {
            $sql .= " WHERE ".$where;
        }
        
        $sql .= " GROUP BY sub_category ORDER BY category_tbl.id ASC";
        
        $rs = $this->_fetchAll($sql , $param);
        if($rs)
        {
            foreach($rs as $data)
            {
                $result[$data["sub_category"]] = $data["cnt"];
            }
        }
        
        return $result;
    }
    
    /**
     * タグごとに何件のデータがあるかを取得
     * @param string $where
     * @param array $param
     * @return array
     */
    function getnum_tag_by($where = "" , $param = array())
    {

    	$result = array();
    	$sql = "SELECT tag_id , count(id) as cnt FROM ".$this->getTableName(TABLE_POST_TAG);
    	if($where)
    	{
    		$sql .= " WHERE ".$where;
    	}

    	$sql .= " GROUP BY tag_id ORDER BY tag_id ASC";

    	$rs = $this->_fetchAll($sql , $param);
    	if($rs)
    	{
    		foreach($rs as $data)
    		{
    			$result[$data["tag_id"]] = $data["cnt"];
    		}
    	}

    	return $result;
    }

    /**
     * 表示中の記事に登録済みのタグを取得
     * @param string $where
     * @param array $param
     * @return array
     */
    function get_tag_list_exist()
    {
    
    	$result = array();
    	$sql = "SELECT
				    tag.id,tag.tag_name
				FROM
				    ".$this->getTableName(TABLE_POSTS)." post right join 
					".$this->getTableName(TABLE_POST_TAG)." post_tag on post.id = post_tag.ownedmedia_posts_id left join
					".$this->getTableName(TABLE_TAG)." tag on  post_tag.tag_id = tag.id
				WHERE
				    post.output_flag = 1 AND
				    post.output_date < now()
    	"; 
    
    	$rs = $this->_fetchAll($sql , $param);
    	if($rs)
    	{
    		foreach($rs as $data)
    		{
    			$result[$data["id"]] = $data["tag_name"];
    		}
    	}
    
    	return $result;
    }
    
    /**
     * 指定したアカウントIDを持つ記事をすべて任意のアカウントIDに変更する
     * （アカウント管理画面でアカウント削除を行った場合にそのアカウントで記事が作成されていた場合の対処）
     * @param number $target_id
     * @param number $change_id
     * @return number
     */
    function change_user_id($target_id , $change_id = 1)
    {
        $sql = "UPDATE ".$this->getTableName(TABLE_POSTS)." SET user_id = ? WHERE user_id = ?";
        return $this->_execute($sql , array($change_id , $target_id));
    }

    /**
     * POSTデータのカテゴリを一括変更させる（カテゴリ削除処理の際、すべてcategory=1に変更する処理で使う）
     * @param number $target_category 変更対象カテゴリID
     * @param number $change_category 変更後のカテゴリID
     * @return boolean
     */
    function post_category_change($target_category , $change_category)
    {
        $sql = "UPDATE ".$this->getTableName(TABLE_POSTS)." SET category = ? WHERE category = ?";
        return $this->_execute($sql , array($change_category , $target_category));
    }

    /**
     * 指定したアカウントIDで投稿データを絞り込んで取得
     * iphoneアプリ用 (さらにアプリから投稿されたデータのみに絞り込む必要がある※app_modifie値があるデータのみ取得）
     * @param type $user_id
     */
    function get_appdata_list($user_id)
    {
        //app_modifiedがmodifiedよりも過去ならアプリ投稿された内容がPC側で編集された可能性あり
        $sql = "SELECT * , IF(app_modified < modified , 1 , 0) as edit_pc ,
                    CASE
                        WHEN output_flag = 1 THEN 1
                        WHEN status = 'wait' THEN 3
                        WHEN output_flag = 0 THEN 2
                    END as status_val
                    FROM ".$this->getTableName(TABLE_POSTS)." WHERE user_id = ? AND app_modified != '0000-00-00 00:00:00' ORDER BY output_date DESC";

        return $this->_fetchAll($sql , array($user_id));

    }
}

?>