<?php namespace lacne\core\model;
use lacne\core\Model;
/**
// ------------------------------------------------------------------------
 * model_post.php
 * 記事データ用モデル
 * @package		Lacne
 * @author		In Vogue Inc.
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class PostTag extends Model
{

    /**
     * Post constructor.
     */
    public function __construct() { parent::__construct(); }

    /**
     * 全データ取得
     */
    function fetchAll()
    {
        $sql = "SELECT * FROM ".$this->getTableName(TABLE_POST_TAG);
        return $this->_fetchAll($sql, array());
    }

    function fetchAllwithTagInfo()
    {
    	$sql = "SELECT * FROM ".$this->getTableName(TABLE_POST_TAG) . " as posttag
                LEFT JOIN ".$this->getTableName(TABLE_TAG)." as tag ON tag.id = posttag.tag_id";

    	return $this->_fetchAll($sql, array());
    }

	/**
	* post_idで一旦削除してから、タグのインサート
	*/
	public function replace($post_id, $tag_ids, $auto_quote=true)
	{
        $ret = array();
		$this->conn->StartTrans();
		$this->_execute("DELETE FROM ". $this->getTableName(TABLE_POST_TAG) . " WHERE ownedmedia_posts_id = ?", array($post_id));
        if(!empty($tag_ids)){
            foreach ($tag_ids as $key => $value) {
                $ret[] = $this->adodb_replace($this->getTableName(TABLE_POST_TAG), array("ownedmedia_posts_id"=>$post_id, "tag_id"=>$value, "created"=>fn_get_date()), "");
            }
        }

		$this->conn->CompleteTrans();
	}

    function delete($id)
    {
        if(is_numeric($id))
        {
            $sql = "DELETE FROM ".$this->getTableName(TABLE_POST_TAG)." WHERE tag_id = ?";
            $this->_execute($sql, array($id));
        }

        return;
    }

    /**
     * 記事Idに紐づくタグ情報を返す
     *
     * @param number $post_id 記事ID
     */
    public function fetchByPostID($post_id)
    {
    	$sql = "SELECT tag_id FROM ".$this->getTableName(TABLE_POST_TAG)." WHERE ownedmedia_posts_id = ?";
    	$ret = $this->_fetchAll($sql, array($post_id));
    	if($ret)
    	{
    		$sql = "SELECT id, tag_name FROM ".$this->getTableName(TABLE_TAG)." WHERE ";
    		$tags_id = array();
    		for($i=0; $i<count($ret); $i++)
    		{
	    		$tags_id[] = $ret[$i]['tag_id'];
	    		if(count($ret) != $i+1)
	    		{
	    			$sql .= " id = ? OR ";
				}
	    		else
	    		{
	    			//名前順にソート
	    			$sql .= "id = ? ORDER BY tag_name;";
				}
    		}
            
    		return $this->_fetchAll($sql, $tags_id);
    	}
    	else
    	{
    		return false;
    	}
    }




}

?>