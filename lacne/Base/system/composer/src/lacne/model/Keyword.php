<?php namespace lacne\core\model;
use lacne\core\Model;

/**
// ------------------------------------------------------------------------
 * model_media.php
 * ランキングデータ用モデル
 * @package		Lacne
 * @author		In Vogue Inc.
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class Keyword extends Model
{

    /**
     * Media constructor.
     */
    public function __construct() { parent::__construct(); }

    /**
     *
     */
    function fetchAll()
    {
        $sql = "SELECT * FROM ".$this->getTableName(TABLE_KEYWORD)." ORDER BY id DESC";
        return $this->_fetchAll($sql, array());
    }

    function fetchOne($id)
    {
        if(is_numeric($id))
        {
            $sql = "SELECT * FROM ".$this->getTableName(TABLE_KEYWORD)." WHERE id = ?";
            return $this->_fetchOne($sql, array($id));
        }

        return;
    }

    /**
     * PVの多い順にすべてのデータを取得(idからタグ名を検索した結果を返す)
     */
    function fetchKeyword()
    {
        $sql = "SELECT tag.id, tag.tag_name, keyword.pv FROM ".$this->getTableName(TABLE_KEYWORD)." as keyword INNER JOIN ".$this->getTableName(TABLE_TAG)." as tag ON keyword.tag_id = tag.id ORDER BY pv DESC";
        
        return $this->_fetchAll($sql, array());
    }

    /**
     * 一覧データ取得
     * @param number $page
     * @param number $limit
     * @param string $type(image or movie)
     * @param string $tag
     * @return object
     */
    function get_list($page , $limit , $type="" , $tag = "")
    {

        $sql = "SELECT id,filename,tag FROM ".$this->getTableName(TABLE_KEYWORD);
        if($type)
        {
            $sql_where .= " WHERE type = ".$this->esc($type);
        }
        if($tag)
        {
            if(empty($sql_where)) $sql_where .= " WHERE ";
            else $sql_where .= " AND ";
            $sql_where .= " tag LIKE ".$this->esc("%".$tag."%");
        }
        $sql .= $sql_where;
        $sql .= " ORDER BY created DESC";

        return $this->_fetchPage($sql, $page, $limit);

    }

    function replace($data , $key)
    {
        foreach($data as $key => $val){
            if(!$this->adodb_replace($this->getTableName(TABLE_KEYWORD) , $val , $key)){
                return false;
            }
        }
        return true;
    }

    function delete($where = "" , $param = array())
    {

        $sql = "DELETE FROM ".$this->getTableName(TABLE_KEYWORD)." WHERE ".$where;
        return $this->_execute($sql, $param);

    }

    function cnt($where = "" , $param = array())
    {
        return $this->_cnt($this->getTableName(TABLE_KEYWORD) , $where , $param);
    }

    function get_tag_cnt()
    {
        $sql = "SELECT tag , count(id) as cnt FROM ".$this->getTableName(TABLE_KEYWORD)." WHERE tag != '' GROUP BY tag ORDER BY created DESC";
        return $this->_fetchAll($sql , array());
    }
}

?>