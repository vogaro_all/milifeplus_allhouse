<?php namespace lacne\core\model;

use lacne\core\Model;

/**
// ------------------------------------------------------------------------
 * admin.php
 * ADMIN用モデル
 * @package		Lacne
 * @author		In Vogue Inc.
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class Admin extends Model
{

    /**
     * Admin constructor.
     * @param object $LACNE
     * @param object $db
     */
    public function __construct(){ parent::__construct(); }

    /**
     * ログインアカウントデータ全件取得
     * @return object
     */
    public function fetchAll($limit = 0 , $order = "")
    {
        $sql = "SELECT * FROM ".TABLE_ADMIN;

        if($order) $sql .= " ".$order;
        if($limit) $sql .= " LIMIT ".$limit;

        return $this->_fetchAll($sql, array());
    }

    public function fetchAllbyAuthority($authority)
    {
        $sql = "SELECT * FROM ".TABLE_ADMIN." WHERE authority = ?";
        return $this->_fetchAll($sql, array($authority));
    }

    /**
     * ログインアカウントデータ1件取得
     * @param number $id
     * @return object
     */
    public function fetchOne($id)
    {
        if(is_numeric($id))
        {
            $sql = "SELECT * FROM ".TABLE_ADMIN." WHERE id = ?";
            return $this->_fetchOne($sql, array($id));
        }

        return array();
    }

    /**
     * ログインアカウントデータ1件取得
     * @param string $login_id
     * @return object
     */
    public function fetchOneByLoginID($login_id)
    {
        $sql = "SELECT * FROM ".TABLE_ADMIN." WHERE login_id = ?";
        return $this->_fetchOne($sql, array($login_id));
    }

    /**
     * ログインアカウントデータ1件取得（login_account値から）
     * @param $login_account
     * @return array
     */
    public function fetchOneByLoginAccount($login_account)
    {
        $sql = "SELECT * FROM ".TABLE_ADMIN." WHERE login_account = ?";
        return $this->_fetchOne($sql, array($login_account));
    }

    /**
     * データのインサートもしくはアップデート処理
     *
     * @param object $data
     * @param string $key
     * @return number
     */
    public function replace($data , $key)
    {
        return $this->adodb_replace(TABLE_ADMIN , $data , $key);
    }

    /**
     * DELETE
     * @param number $id
     * @return boolean
     */
    public function delete($id)
    {
        if(is_numeric($id))
        {
            $sql = "DELETE FROM ".TABLE_ADMIN." WHERE id = ?";
            return $this->_execute($sql, array($id));
        }

        return 0;
    }


    /**
     * @param $where
     * @param $param
     * @return bool
     */
    public function cnt($where , $param){ return $this->_cnt(TABLE_ADMIN , $where , $param); }


    /**
     * パスワードリマインダ用
     * login_accountとemailで照合し、存在すればデータIDを返す
     *
     * @param string $login_account
     * @param string $email
     * @return mixed
     */
    public function checkAccount($login_account , $email)
    {

        $sql = "SELECT * FROM ".TABLE_ADMIN." WHERE login_account = ? AND email = ?";
        $rs = $this->_fetchOne($sql, array($login_account , $email));

        if($rs && isset($rs["id"]))
        {
            return $rs["id"];
        }

        return false;
    }

    /**
     * login_id値からをアカウント名を得る
     * @param $login_id
     * @return string
     */
    public function get_account_name($login_id)
    {
        $result = $this->fetchOneByLoginID($login_id);
        if($result && isset($result["login_account"]))
        {
            return $result["login_account"];
        }

        return "";
    }

}