<?php namespace lacne\core\model;
use lacne\core\Model;

/**
 * ソーシャル管理モデル
 * Class Model
 * @package lacne\core\model
 */
class SnsReserved extends Model
{
    // /**
    //  * ID
    //  */
    // const KEY_ID                    = 'id';

    // /**
    //  * オウンドメディアの記事ID
    //  */
    // const KEY_OWNEDMEDIA_POSTS_ID   = 'ownedmedia_posts_id';

    // /**
    //  * 掲載URL
    //  */
    // const KEY_URL                   = 'url';

    /**
     * 特になし
     * Model constructor.
     */
    public function __construct() { parent::__construct(); }

    public static function ABSOLUTE_PATH_POST_URL($id, $category, $original_filename)
    {
          global $_CATEGORY_LINK_LIST;
          return "http://".$_SERVER['SERVER_NAME']. $_CATEGORY_LINK_LIST[$category] .$original_filename;
    }

    public static function ABSOLUTE_PATH_POST_PREVIEW_URL($id, $category, $original_filename)
    {
          global $_CATEGORY_LINK_LIST;
          return "http://".$_SERVER['SERVER_NAME']."/preview". $_CATEGORY_LINK_LIST[$category] .$original_filename;
    }

    public static function ABSOLUTE_PATH_IMG($img)
    {
          return "http://".$_SERVER['SERVER_NAME'].$img;
    }

}
