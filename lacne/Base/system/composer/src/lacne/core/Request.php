<?php namespace lacne\core;

/**
// ------------------------------------------------------------------------
 *  Request.php
 *
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class Request
{

    /**
     * Request constructor.
     */
    public function __construct(){}

    function csrf_token_generate() {
        if(session_id())
        {
            return sha1(TOKEN_SEED.session_id());
        }

        return "";
    }

    function csrf_check($token) {

        $token0 = $this->csrf_token_generate();
        if(preg_match("/^([a-zA-Z0-9])+$/i", $token) && $token0 && $token0 === $token)
        {
            return true;
        }

        return false;
    }


    function url_routing()
    {
        $params = split('[/\.]', $_SERVER['PATH_INFO']);
        if(count($params) > 1) {
            array_shift($params);
        }
        return $params;
    }


    /**
     * POSTパラメータ中に指定したKEY($action)を持つ値があるか
     * @param string $action
     * @param string $match //$match指定があり、そのKEYを持つ値が$matchと一致すればTRUE
     * @return boolean
     */
    function post_action($action , $match = "")
    {
        if(substr($action , 0 ,1) == "/") $action = substr($action , 1);
        if(isset($_POST[$action]) && !empty($_POST[$action]))
        {
            if($match)
            {
                if($_POST[$action] == $match)
                {
                    return TRUE;
                }
            }
            else
            {
                return TRUE;
            }
        }

        return FALSE;
    }
}
