<?php namespace lacne\core;
use LACNE;

/**
// ------------------------------------------------------------------------
 * Base_Model
 * Modelのベース
 * @package     Lacne
 * @author      In Vogue Inc.
 * @link        http://lacne.jp
 */
// ------------------------------------------------------------------------

class Model
{

    /**  @var object  $db  DBオブジェクト*/
    var $db;
    /**  @var object  $LACNE */
    var $LACNE;
    /** @var object  $conn  DB接続オブジェクト*/
    var $conn;

    var $TABLE_PREFIX = '';

    /**
     *  コンストラクタ
     *
     *  @param  object $LACNE
     *  @param  object $db
     *  @return void
     */
    public function __construct()
    {
        global $LACNE;
        $this->db       = $LACNE->db;
        $this->conn     = $this->db->conn;
    }

    /**
     *  エスケープ
     *
     *  @access public
     *  @param  string $str
     *  @return string
     */
    function esc($str){

        if($str) {
            return $this->conn->qstr($str);
        }

        return $str;
    }


    /**
     *  インサート、アップデート時のチェックとエスケープ処理(文字コード変換も)
     *
     *  @access public
     *  @param  array $list  登録データ配列
     *  @return array  変換済みデータ
     *  @return boolean  自動クォート（trueならクォートを付けない）
     */
    function insert_check($list,$autoQuote=true){

        foreach($list as $key => $value){

            if(STRINGCODE_CHANGE == true && STRINGCODE_DB != STRINGCODE_PHP){
                $value = mb_convert_encoding($value,STRINGCODE_DB,STRINGCODE_PHP);
            }
            if(!$autoQuote){
                $list2[$key] = $this->esc($value);
            }else{
                $list2[$key] = $value;
            }
        }

        return $list2;
    }

    /**
     *  アウトプット時のチェックと文字コード変換処理
     *
     *  @access public
     *  @param  array $list  DBデータ配列
     *  @return array  変換済みデータ
     */
    function output_check($list){

        foreach($list as $key => $value){

            if(STRINGCODE_CHANGE == true && STRINGCODE_PHP != STRINGCODE_DB){
                $value = mb_convert_encoding($value,STRINGCODE_PHP,STRINGCODE_DB);
            }
            $list2[$key] = $value;
        }

        return $list2;
    }

    function _prepare($sql)
    {
        return $this->conn->Prepare($sql);
    }

    function _fetchOne($sql , $param)
    {
        $stmt = $this->_prepare($sql);
        $ret = $this->conn->GetRow($stmt , $param);
        if($ret && is_array($ret))
        {
            $ret = $this->output_check($ret);
        }
        return $ret;
    }

    function _fetchAll($sql , $param)
    {
        $stmt = $this->_prepare($sql);
        $ret = $this->conn->GetAll($stmt , $param);

        if($ret && is_array($ret))
        {
            foreach($ret as $key => $val)
            {
                $ret[$key] = $this->output_check($val);
            }
        }
        return $ret;
    }

    function _fetchPage($sql, $page , $limit)
    {
        $ret = $this->conn->PageExecute($sql,$limit,$page);
        $result = array();

        if($ret){
            while($rs_data = $ret->fetchRow()){

                //出力チェック処理
                $rs_data = $this->output_check($rs_data);

                $result[] = $rs_data;

            }
        }
        return $result;
    }

    function _execute($sql , $param)
    {
        $stmt = $this->_prepare($sql);
        $ret = $this->conn->Execute($stmt , $param);

        return $ret;
    }

    function _cnt($table , $where , $param)
    {
        $sql = "SELECT count(*) as cnt FROM ".$table." WHERE ".$where;
        $stmt = $this->_prepare($sql);
        $ret = $this->conn->getRow($stmt , $param);

        if($ret && isset($ret["cnt"]))
        {
            return $ret["cnt"];
        }

        return FALSE;
    }

    /**
     * adodbのreplaceでインサートもしくはアップデートの処理実行
     *
     * @param string $table
     * @param object $data
     * @param object $key
     * @param boolean $auto_quote
     * @param boolean $insert_check
     */
    function adodb_replace($table , $data , $key , $auto_quote = true , $insert_check = true)
    {
        if($insert_check)
        {
            $data = $this->insert_check($data);
        }

        $rs = $this->conn->replace($table , $data , $key ,$auto_quote);


        if(!$rs)
        {
            die($this->conn->ErrorMsg());
            //@TODO DB Error
        }
        else if($rs == 1) //UPDATE
        {
            if(!is_array($key) && isset($data[$key]))
            {
                return $data[$key];
            }

            return 0;
        }
        else if($rs == 2) //INSERT
        {
            return $this->conn->insert_id();
        }

    }

    /**
     *  エラー処理
     *
     */
    function dispError($err){

        require_once(DIR_TEMP."temp_error.php");
        exit;
    }

    function setTablePrefix($prefix) {
        $this->TABLE_PREFIX = $prefix;
    }

    function getTablePrefix() {
        if(!empty($this->TABLE_PREFIX)) {
            return $this->TABLE_PREFIX."_";
        }else if(defined("TABLE_PREFIX") && TABLE_PREFIX) {
            return TABLE_PREFIX."_";
        }

        return "";
    }

    function getTableName($TABLE_NAME) {
        return $this->getTablePrefix().$TABLE_NAME;
    }
}

?>