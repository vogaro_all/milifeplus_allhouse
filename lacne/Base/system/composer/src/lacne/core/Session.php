<?php namespace lacne\core;

/**
// ------------------------------------------------------------------------
 *  Session
 *
 *  セッション管理クラス(ADOdb利用)
 *  セキュア化対応 参考：http://bba-ltom.blogspot.com/2008/07/php.html
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

define('STRICT_SESSION_UA_NAME' , 'strict_session_user_agent');
define('STRICT_SESSION_RA_NAME' , 'strict_session_remote_addr');
define('STRICT_SESSION_ENCRYPT_NAME' , 'strict_session_serialized');
define('STRICT_SESSION_ENCRYPT_KEY' , CERT_LOGIN_KEY);

class Session
{

    /**
     * Session constructor.
     */
    function __construct(){}

    /**
     *  セッションスタート
     *
     *  @access public
     *  @param  void
     *  @return void
     */
    function sessionStart($session_type = 0){

        if(!$session_type)
        {
            $session_type = SESSION_TYPE;
        }

        //if($this->session_type == 1){
        if($session_type == 1){

            require_once("adodb/session/adodb-session2.php");
            $options['table'] = TABLE_SESSION;
            ADOdb_Session::config(DB_TYPE,DB_HOST,DB_USER,DB_PASS,DB_NAME,$options);
            session_start();
            //adodb_session_regenerate_id();
            //adodb_session_regenerate_id();
        }else if($session_type == 3) { //
            session_start();
        }else{

            //$this->STRICT_SESSION_ENCRYPT_KEY = CERT_LOGIN_KEY;

            ini_set( 'session.use_only_cookies', 1 );
            // md5よりさらに予測困難なsha1でセッションIDを生成
            ini_set( 'session.hash_function', 1 );
            // セッションIDをURLパラメータに書かない
            ini_set( 'session.use_trans_sid', 0 );

            // HTTP <-> HTTPS 間でセッションの受渡しやCSRF対策でフォームに
            // 自動付与されるoutput_add_rewrite_varでURLパラメータに書かれ
            // ないようにする
            ini_set( 'url_rewriter.tags', 'form=' );

            if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] = 'on') {
                ini_set('session.cookie_secure', 1);
                session_name('SECURE_'.session_name());
            }
            session_start();

            $invalid=FALSE;

            // sha1でセッションIDのハッシュ値を生成した場合の書式チェック
            if (!preg_match("/^[0-9a-z]*$/i", session_id())){
                $invalid="1";
            }
            // IPとUAが同時に一致しなかった時のみエラーとする
            $ip = fn_getIP();
            if(($_SERVER['HTTP_USER_AGENT'] && $_SESSION[STRICT_SESSION_UA_NAME] && ($_SERVER['HTTP_USER_AGENT'] != $_SESSION[STRICT_SESSION_UA_NAME]))
                &&
                ($ip && $_SESSION[STRICT_SESSION_RA_NAME] && ($ip != $_SESSION[STRICT_SESSION_RA_NAME]))
            ){
                $invalid="2";
            }

            $_SESSION[STRICT_SESSION_UA_NAME]=$_SERVER['HTTP_USER_AGENT'];
            $_SESSION[STRICT_SESSION_RA_NAME]=fn_getIP();

            if($invalid){
                // セッションを破棄して新しいセッションを開始
                $this->strict_session_destroy();
            }else{

                if($_SESSION) {
                    $tmp = $_SESSION; $_SESSION = array();
                    session_destroy();
                    //session_id(md5(uniqid(rand(), 1)));
                    /*
                    if (!function_exists("session_regenerate_id")) {
                       $this->session_regenerate_id();
                    } else {
                       session_regenerate_id();
                    }
                    */
                    session_start();
                    $_SESSION = $tmp;
                }
            }
        }

    }
    /*
    * セッションを完全に削除
    */
    function strict_session_destroy(){
        // セッション変数を全て解除する
        $_SESSION = array();

        // セッションを切断するにはセッションクッキーも削除する。
        // Note: セッション情報だけでなくセッションを破壊する。
        if (isset($_COOKIE[session_name()])) {
            setcookie(session_name(), '', time()-42000, '/');
        }

        // HTTP <-> HTTPS 間でセッションの受渡し用Cookieも削除
        if (isset($_COOKIE[STRICT_SESSION_ENCRYPT_NAME])) {
            setcookie(STRICT_SESSION_ENCRYPT_NAME, '', time()-42000, '/');
        }

        // 最終的に、セッションを破壊する
        session_destroy();
    }

    /*
     * HTTP <-> HTTPS 間でセッションの受渡し用関数
     * 与えられた変数をシリアライズして、STRICT_SESSION_ENCRYPT_KEYで定義した
     * キーを元に暗号化。
     */
    function strict_session_serialize($d=null) {
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_3DES, MCRYPT_MODE_CFB), MCRYPT_RAND);
        setcookie(STRICT_SESSION_ENCRYPT_NAME, base64_encode($iv.mcrypt_encrypt(MCRYPT_3DES, STRICT_SESSION_ENCRYPT_KEY, serialize($d), MCRYPT_MODE_CFB, $iv)), 0, '/');
    }

    /*
     * HTTP <-> HTTPS 間でセッションの受渡し用関数
     * strict_session_serializeによって暗号化され変数を復号化する。
     */
    function strict_session_unserialize() {

        if(isset($_COOKIE[STRICT_SESSION_ENCRYPT_NAME])){
            $is = mcrypt_get_iv_size(MCRYPT_3DES, MCRYPT_MODE_CFB);
            $et = base64_decode($_COOKIE[STRICT_SESSION_ENCRYPT_NAME]);
            return unserialize(mcrypt_decrypt(MCRYPT_3DES, STRICT_SESSION_ENCRYPT_KEY, substr($et,$is), MCRYPT_MODE_CFB, substr($et, 0, $is)));
        }

        return FALSE;
    }

    function session_regenerate_id() {

        $random = $this->create_randomkey();
        // use md5 value for id or remove capitals from string $randval
        // $random = md5($random);
        if (session_id($random)) {
            return true;
        } else {
            return false;
        }
    }

    function create_randomkey()
    {
        $randlen = 32;
        $randval = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $random = "";
        for ($i = 1; $i <= $randlen; $i++) {
            $random .= substr($randval, rand(0,(strlen($randval) - 1)), 1);
        }

        return $random;

    }

    /**
     * session idを新しくする
     */
    function regenerate($old_delete = true)
    {

        if($_SESSION) {
            $tmp = $_SESSION; $_SESSION = array();
            session_destroy();

            session_start();
            $_SESSION = $tmp;

            if (!function_exists("session_regenerate_id")) {
                $this->session_regenerate_id();
            } else {
                session_regenerate_id($old_delete);
            }
        }
        return;
    }
}

?>