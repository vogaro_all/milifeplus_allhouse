<?php namespace lacne\core;

/**
// ------------------------------------------------------------------------
 *  emplate.php
 *  テンプレート
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class Template
{

    var $post;
    var $get;
    var $view_dir;
    var $default_vars = array();

    public function __construct($dir)
    {
        $this->view_dir = $dir;
        $this->post     = $_POST;
        $this->get      = $_GET;
    }

    function setVars($vars)
    {
        if(is_array($vars))
        {
            $this->default_vars = array_merge($this->default_vars , $vars);
        }

        return;
    }

    function getViewDir()
    {
        return $this->view_dir;
    }
    function setViewDir($dir)
    {
        $this->view_dir = $dir;
        return;
    }

    /**
     * ビューファイルをレンダリング
     *
     * @param string $tpl
     * @param array $vars
     * @param boolean $return //true : contentデータを返却
     * @param boolean $escape //エスケープするか
     * @return string
     */
    function render($tpl,  $vars = array() , $return = false , $escape = true)
    {
        if(!is_array($vars)) $vars = array();

        $vars = array_merge($vars,$this->default_vars);

        if($escape)
        {
            $esc_vars = $vars;
            $esc_vars = $this->_Escape($esc_vars);

            extract($vars , EXTR_PREFIX_ALL , ""); //エスケープしない変数は$_変数名（例：$a -> $_a）で取得できるように
            extract($esc_vars , EXTR_SKIP); //通常view側に渡される変数はすべてエスケープされる

            $esc_POST = $this->_Escape($this->post);
            $esc_GET  = $this->_Escape($this->get);
        }
        else
        {
            extract($vars , EXTR_SKIP);
        }
        $template_filename = $this->view_dir."/temp_".$tpl.'.php';
        //Todo エラーイベント実装
        if(!file_exists($template_filename)) exit("File Not Found / Template File");
        ob_start();
        ob_implicit_flush(0);

        include $template_filename;

        $content = ob_get_clean();

        return $content;
    }

    function _Escape($d)
    {
        if (is_array($d)) {
            return array_map(array($this , '_Escape') , $d);
        } else {
            if(gettype($d) == "string")
            {
                return htmlspecialchars($d, ENT_QUOTES, STRINGCODE_PHP);
            }
            else
            {
                return $d;
            }
        }
    }

}