<?php namespace lacne\core;


/**
 * URLまわりの処理用クラス
 * Class UrlSetting
 * @package lacne\core
 */
class UrlSetting
{

    /**  @var object  $setting  詳細ページURL設定 */
    var $setting;

    /**
     * UrlSetting constructor.
     */
    public function __construct(){}

    /**
     * 詳細ページのURL定義を取得（local_dir/include/config/page.phpで定義したルールを参照し、
     * 条件に合致するURL定義を返却）
     * @param number $id
     * @param number $category
     * @param string $device(smph , mobi)
     * @return string
     */
    function get_pageurl($id , $category , $device = "", $original_filename)
    {
        if($this->setting && is_array($this->setting))
        {
            $url_setting = "";
            $replace_pattern = array('/\[id\]/','/\[category\]/','/\[device\]/','/\[original_filename\]/');

            foreach($this->setting as $key => $setting)
            {
                //ID値をKEYとしたURLルールがあるか
                if($key == "id" && isset($setting[$id]))
                {
                    $url_setting = $setting[$id];
                }
                else if($key == "category" && isset($setting[$category]))
                {
                    $url_setting = $setting[$category];
                }
                else if($key == "url")
                {
                    $url_setting = $setting;
                }
                if($url_setting) break;
            }

            if($url_setting)
            {
                return preg_replace($replace_pattern,array($id , $category , $device, $original_filename),$url_setting);
            }
        }

        return false;
    }


    /**
     * 設定された詳細ページURLが http://～でないパスであれば、
     * そのパスの先頭に SITE_BASE_PATH($BASE_PATH) のパスを付け足す
     * @param string $BASE_PATH
     * @return void
     */
    function _add_base_path($BASE_PATH)
    {
        if($BASE_PATH && !empty($this->setting) && is_array($this->setting))
        {
            foreach($this->setting as $key => $val)
            {
                if($key == "url" && !preg_match('/^https?:\/\//',$val))
                {
                    $this->setting[$key] = $BASE_PATH.$val;
                }
                else if($key == "id" || $key == "category")
                {
                    foreach($val as $id => $val2)
                    {
                        if( !preg_match('/^https?:\/\//',$val) )
                        {
                            $this->setting[$key][$id] = $BASE_PAT.$val2;
                        }
                    }
                }
            }
        }

        return;
    }

}