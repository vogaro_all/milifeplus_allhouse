<?php namespace lacne\core;

/**
 * 簡易ページャ
 *
 * @author Vogaro.Inc
 */
class Pager
{
    /**
     * @var integer ページ番号
     */
    protected $active = 1;

    /**
     * @var integer 全表示データ件数
     */
    protected $length = 0;

    /**
     * @var integer ページ単位の表示件数
     */
    protected $limit = 30;

    /**
     * @var integer 最終ページ
     */
    protected $lastPage = 0;

    /**
     * @var int 現在の開始番号
     */
    protected $current_start_number = 0;

    /**
     * @var int 現在の終了番号
     */
    protected $current_end_number = 0;

    /**
     * @var var 前ページの文言
     */
    protected $prev_txt = "";

    /**
     * @var var 次ページの文言
     */
    protected $next_txt = "";

    /**
     * ページ番号<br>
     * 全データ件数<br>
     * ページ単位の表示件数<br>
     * を元にページャの機能を初期化
     *
     * @param integer $active ページ番号
     * @param integer $length 全件表示データ
     * @param integer $limit ページ単位の表示件数
     */
    public function __construct($active, $length, $limit, $prev_txt = "前の30件", $next_txt = "次の30件")
    {
        $this->active   = $active ? $active: 1;
        $this->length   = $length;
        $this->limit    = $limit;
        $this->lastPage = ceil($length / $limit);
        //TODO ロジック調整
        $this->current_end_number =  $length >= $active * $limit? $length: $active * $length;
        $this->current_start_number = $this->current_end_number - $length -1;

        $this->prev_txt  = $prev_txt;
        $this->next_txt  = $next_txt;
    }

    /**
     * 前のページがあるか確認
     *
     * @return boolean true 前のページがある | false 前のページがない
     */
    public function isPrev(){ return $this->active <= 1? false: true; }

    /**
     * 次のページがあるか確認
     *
     * @return boolean true 次のページがある | false 次のページがない
     */
    public function isNext(){ return $this->active >= $this->lastPage? false: true; }

    /**
     * 次のページがある場合・ない場合のλ式を指定
     *
     * @param Closure $done(Pager $pager, integer $next) 次のページがある場合のλ式を指定
     * @param Closure $fail(Pager $pager) 前のページがある場合のλ式を指定
     */
    public function oneNext($done, $fail = '', $add_pager_link)
    {
        if($this->isNext() == true){
            if(is_callable($done)){
                $done($this, $this->nextActiveID(), $this->getNextTxt(), $add_pager_link);
            }
        }else{
            if(is_callable($fail)){
                $fail($this);
            }
        }
    }

    /**
     * 前のページがある場合・ない場合のλ式を指定
     *
     * @param $done(Pager $pager, integer $prev) 次のページがある場合のλ式を指定
     * @param $fail(Pager $pager) 前のページがある場合のλ式を指定
     */
    public function onePrev($done, $fail = '', $add_pager_link)
    {
        if($this->isPrev() == true){
            if(is_callable($done)){
                $done($this, $this->prevNextID(), $this->getPrevTxt(), $add_pager_link);
            }
        }else{
            if(is_callable($fail)){
                $fail($this);
            }
        }
    }

    /**
     * @return int
     */
    public function getLimit(){ return $this->limit; }

    /**
     * @return int
     */
    public function getLength(){ return $this->length; }

    /**
     * @return int
     */
    public function getCurrentStartNumber(){ return $this->current_start_number; }

    /**
     * @return int
     */
    public function getCurrentEndNumber(){ return $this->current_end_number; }

    /**
     * @return int
     */
    public function getLastPage(){ return $this->lastPage; }

    /**
     * @return int
     */
    public function getactivePage(){ return $this->active; }
    
    /**
     * @return int
     */
    public function hasPages(){ return ($this->lastPage > 1)? true : false; }

    /**
     * 表示可能なページング数λ式関数が実行されます。
     *　ページングは現在のページから前後2件の情報を表示します。
     *
     * @param Closure $fn(integer $index, integer $active, Pager $pager) ページング用λ関数を指定
     * @param integer ページの前後の数
     */
    public function getPages($fn, $around = 3, $add_pager_link="")
    {
        if(!is_callable($fn)) return;
        if(empty($this->length)) return;

        $isPrevEllipsis = $isNextEllipsis = false;

        if($this->active < 3 && 6 < $this->lastPage)
        {
            $start = 1;
            $end = 3;
            $isNextEllipsis = true;
        }

        if($this->lastPage <= $this->active){
            $this->active = $this->lastPage;
        }


        if($this->active >= 3 && $this->lastPage - $around > $this->active && 6 < $this->lastPage)
        {
            $start = $this->active;
            $end = $this->active + 1;
            $isPrevEllipsis = $isNextEllipsis = true;
        }

        if($this->lastPage - $around <= $this->active && 6 < $this->lastPage)
        {
            $start = $this->active;
            $end = $this->active + $around;
            $isPrevEllipsis = true;
        }

        if($this->lastPage <= $end)
        {
            $start = $this->lastPage - $around;
            $end = $this->lastPage;
        }

        if(6 >= $this->lastPage)
        {
            $start = 1;
            $end = $this->lastPage;
        }

        $range = range($start, $end);

        array_unshift($range, 1);
        array_push($range, $this->lastPage);

        $range = array_unique($range, SORT_NUMERIC);

        foreach ($range as $index => $pageNum) {
            $fn($pageNum, $this->active, $this, $isPrevEllipsis, $isNextEllipsis, $add_pager_link);
        }

    }

    /**
     * @return int
     */
    public function nextActiveID(){ return $this->active+1; }

    /**
     * @return int
     */
    public function prevNextID(){ return $this->active-1; }

    /**
     * @return var
     */
    public function getPrevTxt(){ return $this->prev_txt; }

    /**
     * @return var
     */
    public function getNextTxt(){ return $this->next_txt; }

    /**
     * ページング用のクエリを生成
     * TODO 処理修正
     *
     * @param integer ページ番号
     * @param string ページキー
     * @return string ページング用のクエリ文字列
     */
    public function getQuery($page, $page_key = 'page')
    {
        unset($_GET[$page_key]);
        $query = array("$page_key=$page");
        foreach($_GET as $key => $value){
            $query[] = $key . '=' . $value;
        }
        return implode('&', $query);
    }

    /**
     *  rel="next" rel="prev"のSEOタグを生成する
     *  @param  array $query_keys    引き継ぎたいURLパラメータのキー
     *  @return string
     */
    public function getMultiPageTag($query_keys=array(), $page_key = 'page')
    {
        $parse_url = parse_url($_SERVER['REQUEST_URI']);
        $url = $parse_url["path"];

        $next_prev_tag = "";
        if($this->isPrev() == true)
        {
            $next_prev_tag .= '<link rel="prev" href="'.$url."?".$this->getQuery($this->active-1).'" />'.PHP_EOL;
        }

        if($this->isNext() == true)
        {
            $next_prev_tag .= '<link rel="next" href="'.$url."?".$this->getQuery($this->active+1).'" />'.PHP_EOL;
        }

        return $next_prev_tag;

    }




}