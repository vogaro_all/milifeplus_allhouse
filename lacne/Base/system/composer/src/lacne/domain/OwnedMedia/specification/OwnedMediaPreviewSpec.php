<?php namespace lacne\domain\OwnedMedia\specification;

use lacne\core\model\Post;

/**
 * 
 * Class OwnedMediaPreviewSpec
 * @package lacne\domain\OwnedMedia\specification
 */
class OwnedMediaPreviewSpec
{
    protected $original_item = array();

    protected $item = array();

    public function __construct($original_item)
    {
        $this->item = $this->original_item = $original_item;
    }

    public function getPreviewItem()
    {
        return $this->item;
    }

    public function build()
    {
        $this->category();
        $this->tags();
        $this->section();
        $this->recommend();

        return $this;
    }

    protected function category($key = "category")
    {
        $this->item[$key] = $this->original_item[$key];
    }


    protected function tags($key = "tags", $replace_key = "p_tags")
    {
        $this->item[$key] = $this->original_item[$replace_key];
    }

    protected function ages($key = "ages", $replace_key = "p_ages")
    {
        $this->item[$key] = $this->original_item[$replace_key];
    }

    protected function duplicate_categories($key = "duplicate_categories", $replace_key = "p_duplicate_categories")
    {
        $this->item[$key] = $this->original_item[$replace_key];
    }

    protected function section($key = "section")
    {
        $this->item[$key] = array();

        $index = 1;
        foreach( $this->original_item[$key] as $item ){
            $this->item[$key][($index++)] = $item;
        }
        //$this->item[$key] = array_values($this->item);
    }

    public function recommend($key = "recommend_post")
    {
        $recommend_post = array_filter($_POST['recommend_post']);

        $ret = array();
        foreach ($recommend_post as $id) {
            $item = with(new Post())->fetchOneNotAdvertise($id);
            if(!is_array($item)) $item = array();
            $item = array_filter($item);
            if(!empty($item))
            {
                array_push($ret, $item);
            }
        }
        $this->item[$key] = $ret;
    }

}