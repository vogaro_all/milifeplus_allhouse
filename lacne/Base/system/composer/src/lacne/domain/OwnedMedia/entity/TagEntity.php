<?php namespace lacne\domain\OwnedMedia\entity;

use lacne\domain\OwnedMedia\Entity;

/**
 * Class TagEntity
 * @package lacne\domain\OwnedMedia\entity
 */
class TagEntity extends Entity
{

    /**
     * TagEntity constructor.
     * @param array|null|object $input
     */
    public function __construct($input)
    {
        parent::__construct($input);
    }

    /**
     * タグID
     */
    public function getID()
    {
        return !empty($this->id) ? $this->id : "";
    }

    /**
     * タグ名
     */
    public function getName()
    {
        return !empty($this->tag_name) ? $this->tag_name : "";
    }


    /**
     * リンク先
     */
    public function getLink()
    {
        $link = "/search/?key=".$this->getID();
        return $link;

    }
}