<?php namespace lacne\domain\OwnedMedia\entity;

use lacne\domain\OwnedMedia\Entity;
use lacne\service\output\OwnedMediaHelper;
use lacne\domain\OwnedMedia\entity\TagEntity;


/**
 * Class MediaEntity
 * @package lacne\domain\OwnedMedia\entity
 */
class MediaEntity extends Entity
{
    const DATE_FORMAT = "Y.m.d";

    /**
     * MediaEntity constructor.
     * @param array|null|object $input
     */
    public function __construct($input)
    {
        parent::__construct($input);

//         $this->setOutputDate();

        // meta・ogp関連
//         $this->setKeyword();
        $this->setDescription();
//         $this->setOgpImage();

    }

    /**
     * IDの取得
     */
    public function getId()
    {
        return !empty($this->id) ? $this->id : "";
    }

//     /**
//      * 公開日付の設定
//      */
//     public function setOutputDate()
//     {
//         $this->output_date = date(self::DATE_FORMAT ,strtotime($this->output_date));
//     }

//     /**
//      * キーワード：メタ情報の設定
//      */
//     public function setKeyword()
//     {
//         $this->keyword = !empty($this->keyword) ? $this->keyword : ',';
//     }

    /**
     * 説明：メタ情報の設定
     */
    public function setDescription()
    {
        $this->description = !empty($this->description) ? $this->description : '';
    }

//     /**
//      * og:imageの設定
//      */
//     public function setOgpImage()
//     {
//         $this->ogp_image = !empty($this->thumb_image) ? "http://".$_SERVER['SERVER_NAME'].$this->thumb_image : "";
//     }

    /**
     * 公開日付の設定
     */
    public function getOutputDate($format = 'Y.m.d')
    {
        return date($format ,strtotime(date($this->output_date)));
    }

    /**
     * 新着のタグ取得
     */
    public function getNewTag()
    {
        if (fn_dateComparison(date($this->output_date), date(). '-30 day')) {
            return ' card--new';
        } else {
            return '';
        }
    }

    /**
     * カテゴリを取得する
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * カテゴリ名を取得する
     */
    public function getCategoryName()
    {
        return $this->category_name;
    }

    public function getCategoryClass()
    {
        if ($this->category == 1) {
            return "info";
        } elseif ($this->category == 2) {
            return "event";
        } elseif ($this->category == 3) {
            return "seminar";
        }
        return '';
    }

    public function getCategoryPath()
    {
        if ($this->category == 1) {
            return "notice";
        } elseif ($this->category == 2) {
            return "event";
        } elseif ($this->category == 3) {
            return "seminar";
        }
        return '';
    }


    /**
     * タイトルの取得
     */
    public function getTitle()
    {
        return !empty($this->title) ? $this->title : "";
    }

    /**
     * 短く省略したタイトルの取得
     */
    public function getShortTitle($num = 60)
    {
        return !empty($this->title) ? mb_strimwidth($this->title, 0, $num, "...", utf8) : "";
    }

    /**
     * サイト側に表示するフルフルのタイトルの取得
     */
    public function getFullTitle()
    {
        return !empty($this->title) ? $this->title : "";
    }

//     /**
//      * リンク先(絶対パス)の取得
//      * @return string
//      */
//     public function getCategoryLink()
//     {
//     	return "http://".$_SERVER['SERVER_NAME'].'/?category='.$this->category;
//     }

    /**
     * リンク先paramの取得
     * @return string
     */
    public function getLinkParam()
    {
//         return " href='/contents/".$this->id."'";
        return "/news/detail/?id=$this->id";
    }

    /**
     * 外部先リンク
     * @return string
     */
    public function getBlank()
    {
        if( $this->hasLinkWindow() ){
            return "target='blank'";
        }

        return "";
    }

    /**
     * リンク先ｳｨﾝﾄﾞｳが有効か確認する。
     * @return bool
     */
    public function hasLinkWindow()
    {
        return $this->link_window == 1 ? true: false;
    }

    /**
     * 記事に紐づくすべてのタグを取得する
     * @return array
     */
    public function getAllTags()
    {
        if($this->hasTags() == true)
        {
            if(!is_array($this->tags))
            {
                $this->tags = $this->getTagsArray();
            }

            // タグの表示順を統一
            foreach ($this->tags as $key => $v) {
                $tag_name[$key] = $v['tag_name'];
            }
            array_multisort($tag_name, SORT_ASC, $this->tags);

            return array_map(function($item){
                return new TagEntity($item);
            }, $this->tags);
        }
        return array();
    }

    /**
     * 記事に紐づくすべてのタグをmeta keyword用に取得する
     * @return string
     */
    public function getAllTagsForMetaKeyword()
    {
        $tag_names = array_map(function($tagEntity){
            return $tagEntity->getName();
        }, $this->getAllTags());

        return implode(",", $tag_names);
    }

    /**
     * 記事に紐づくタグが存在するか確認する
     * @return array
     */
    public function hasTags()
    {
        if( !empty($this->tags) )
        {
            return true;
        }
        return false;
    }

    /**
     * タグ情報配列を返す
     * @return array
     */
    public function getTagsArray()
    {
        $key_template = array(
            "id",
            "tag_name"
        );

        return array_map(function($item) use ($key_template){
            return array_combine( $key_template, explode(":", $item) );
        }, explode(",", $this->tags));
    }

    /**
     * プレビューモードかどうか
     * @return bool
     */
    public function isPreviewMode()
    {
        return $this->isPreview == 1 ? true : false;
    }

   /**
     * すべての検索エンジンで検索結果として表示されないタグを取得
     * @return mixed
     */
    public function getNoIndexMetaTag()
    {
        if($this->isPreviewMode() == true)
        {
            return "<meta name='robots' content='noindex' />";
        }
        return "";
    }

    public function getThumbImage()
    {
        return !empty($this->thumb_image) ? $this->thumb_image : '';
    }

    public function getOtherDataList()
    {
        $entities = array();
        foreach ((array) $this->other_data_list as $value) {
            if (!empty($value)) {
                $entities[] = new MediaEntity($value);
            }
        }
        return $entities;
    }

    /**
     * ==============================================
     * 以下、拡張
     * ==============================================
     * */
    /**
     * 開催日程の設定
     */
    public function getHeldputDate($format = 'Y.m.d')
    {
        if (empty(date($this->held_date)) || $this->held_date == '0000-00-00 00:00:00') {
            return '';
        }
        return date($format ,strtotime(date($this->held_date)));
    }
}