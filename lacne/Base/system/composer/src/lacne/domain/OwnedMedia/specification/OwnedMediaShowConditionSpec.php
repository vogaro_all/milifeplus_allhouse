<?php namespace lacne\domain\OwnedMedia\specification;

use lacne\domain\OwnedMedia\entity\MvEntity;
use lacne\domain\OwnedMedia\entity\MediaEntity;

/**
 * 記事情報のフロント表示条件仕様クラス
 * Class OwnedMediaShowConditionSpec
 * @package lacne\domain\OwnedMedia\specification
 */
class OwnedMediaShowConditionSpec
{
    protected $notIds = array();

    public function setNotInId($lists)
    {
        if( empty($lists["data"]) ){
            $lists["data"] = array();
        }

        $notIds = $this->notIds;
        $this->notIds = array_map(function($item) use (&$notIds){
            return array_push( $notIds , with(new MediaEntity($item))->getId() );
        }, $lists["data"]);
        $this->notIds = $notIds;
        return $this;
    }

    public function getNotIds()
    {
        return $this->notIds;
    }

    public function setNotId($id)
    {
        if(!empty($id))
        {
            array_push( $this->notIds, $id );
        }
        return $this;
    }
}