<?php
use lacne\core\model\Post;
use lacne\core\model\PostMeta;
return function($request, $response, $service, $app) {

	$app->lacne->load_library(array('login' , 'post')); //library load
	$login_id = $app->login_id;

	//Templateファイルのフォルダ指定(通常読み込むtemplateフォルダの中のさらにarticle以下にあるため）
	$app->lacne->template->setViewDir($app->lacne->template->getViewDir()."/article");
	//render_data : Template側に渡すデータ変数
	$render_data = array(
	    "login_id" => $login_id
	);

	//1ページあたりのデータ表示数
	$output_num = $app->lacne->library["admin_view"]->get_num_per_page("article_index");

	// ------------------------------------------------------------------------
	// 削除処理
	// ------------------------------------------------------------------------
	if($app->lacne->post("/delete"))
	{
/*
	    //CSRF TOKENチェック
	    $csrf_check = false;
	    if(isset($_POST["token"]) && $_POST["token"])
	    {
	        $csrf_check = $app->lacne->request->csrf_check($_POST["token"]);
	    }
	    if(!$csrf_check)
	    {
	        //CSRFエラー
	        echo "0"; exit;
	    }
*/
	    $delete_ids = array();
	    if(isset($_POST["id"]) && is_numeric($_POST["id"]))
	    {
	        //権限チェック(削除権限があるか、もしくは自分自身が作成した記事で公開されていないかどうかチェックする OKなら削除実行）
	        $delete_data = with(new Post())->fetchOne($_POST["id"]);
	        if($app->lacne->library["login"]->chk_controll_limit("delete_post") || (!$delete_data["output_flag"] && isset($delete_data["user_id"]) && $login_id == $delete_data["user_id"]))
	        {
	            $delete_ids[] = $_POST["id"];
	        }
	    }
	    else if(isset($_POST["del_posts"]) && count($_POST["del_posts"]) > 0){

	        foreach($_POST["del_posts"] as $post_id)
	        {
	            if(is_numeric($post_id))
	            {
	                //権限チェック(削除権限があるか、もしくは自分自身が作成した記事で公開されていないかどうかチェックする OKなら削除実行）
	                $delete_data = with(new Post())->fetchOne($post_id);
	                if($app->lacne->library["login"]->chk_controll_limit("delete_post") || (!$delete_data["output_flag"] && isset($delete_data["user_id"]) && $login_id == $delete_data["user_id"]))
	                {
	                    $delete_ids[] = $post_id;
	                }
	            }
	        }
	    }
	    if(count($delete_ids))
	    {
	        foreach($delete_ids as $tid)
	        {
	            //データ削除
	            with(new Post())->delete($tid);
	            //$app->lacne->model["postmeta"]->delete($tid);
	        }
	    }
	    if(isset($_POST["ajax"]))
	    {
	        echo "1";
	        exit;
	    }
	    else
	    {
	        //複数削除リクエストなら削除後リダイレクト
	        fn_redirect(LACNE_APP_ADMIN_PATH."/article/index.php".fn_set_urlparam($_GET, array("category","page","sort","wait")));

	    }
	}

	// ------------------------------------------------------------------------
	// 記事タイトルの問い合わせ（削除時にajaxで問い合わせされる）
	// ------------------------------------------------------------------------
	if($app->lacne->post('/post_title'))
	{
	    $return = "";
	    if(isset($_POST["post_title"]) && is_numeric($_POST["post_title"]))
	    {
	        $result = with(new Post())->fetchOne($_POST["post_title"]);
	        if($result && isset($result["title"]))
	        {
	            $return = $result["title"];
	        }
	    }
	    echo $return;
	    exit;
	}


	//Todo 以下の処理もっときれいにまとめたい
	//検索条件(カテゴリー検索)
	$search_param_arr = array();
	if(isset($_GET["category"]) && is_numeric($_GET["category"]))
	{
	    $category_id = $_GET["category"];
	    $search_param_arr = array(
	        "post.category =" => $category_id
	    );
	}
	//承認待ちのデータのみ（承認フローオプションが有効な場合のみ利用）
	if(isset($_GET["wait"]) && is_numeric($_GET["wait"]))
	{
	    $search_param_arr["post.status ="] = "wait";
	}

	//ソート指定（デフォルトは表示日時）
	$order = array(
	    "val" => "sort_date",
	    "key" => "post.output_date",
	    "by" => "desc"
	);
	if(isset($_GET["sort"]) && $_GET["sort"])
	{
	    //SORTは、GETパラメータで sort=sort_cat-desc , sort=sort_user-asc
	    //のようなかたちでリクエストされる
	    list($sort_key , $sort_by) = explode("-", $_GET["sort"]);
	    if($sort_key == "sort_cat")
	    {
	        //カテゴリでソート
	        $order["val"] = "sort_cat";
	        $order["key"] = "post.category";
	    }
	    else if($sort_key == "sort_user")
	    {
	        //アカウントIDでソート
	        $order["val"] = "sort_user";
	        $order["key"] = "post.user_id";
	    }

	    if(isset($sort_by) && $sort_by == "desc") $order["by"] = "desc";
	    else if(isset($sort_by) && $sort_by == "asc") $order["by"] = "asc";
	}

	//現在のページ
	$page = 1;
	if(isset($_GET['page']) && is_numeric($_GET['page'])){
	    $page = fn_now_page_set($_GET['page']);
	}
	//データ件数取得
	$cnt = with(new Post())->data_cnt($search_param_arr);
	//総ページ数取得
	$page_num = fn_getPages($cnt,$output_num);
	if($page > $page_num){ $page = $page_num; }

	//承認待ちデータ数をカウント（オプションが有効な場合のみ利用）
	$cnt_wait = 0;
	if(method_exists(with(new Post()), "data_cnt_waiting"))
	{
	    $cnt_wait = with(new Post())->data_cnt_waiting();
	}

	//記事データがカテゴリごとに何件あるかを取得
	$cat_cnt_where = "";
	$cat_cnt_param = array();
	if(isset($_GET["wait"]) && is_numeric($_GET["wait"]))
	{
	    $cat_cnt_where = "status = ?";
	    $cat_cnt_param = array("wait");
	}
	$cnt_category_by =  with(new Post())->getnum_category_by($cat_cnt_where , $cat_cnt_param);

	//カテゴリ一覧データ取得
	$category_list = $app->lacne->library['post']->get_category_list();


	//-----------------------------------------------------
	// Ajaxリクエスト データ件数まわりの表示のみ取得
	//-----------------------------------------------------
	if($app->lacne->post('/post_cnt'))
	{
	    echo $app->lacne->library["post"]->html_post_list_pager_head($cnt , $page , $page_num , $output_num);
	    exit;
	}
	//-----------------------------------------------------
	// Ajaxリクエスト ページャまわりの表示のみ取得
	//-----------------------------------------------------
	if($app->lacne->post('/post_pager'))
	{
	    echo fn_pagenavi($page , $page_num , fn_set_urlparam($_GET, array('category','sort','wait'),false),true);
	    exit;
	}

	//-----------------------------------------------------
	// 記事一覧データを取得し、一覧表示部分のhtmlをまず生成させるために
	// データを整理
	//-----------------------------------------------------
	$data_list = array();
	$post_data = array();

	//データ取得
	//$data_list = with(new Post())->get_list($page, $output_num+3, $search_param_arr , $order
	$data_list = with(new Post())->get_list_plus($page, $output_num , 3, $search_param_arr , $order);
	$list_data_param = array(
	    //削除用URL
	    "form_delete_url" => LACNE_APP_ADMIN_PATH."/article/index.php".fn_set_urlparam($_GET, array('category','page','sort','wait')),
	    "option_account" => (method_exists($app->lacne->library["login"] , "get_account_name"))?true:false,
	    //CSRF TOKEN
	    "csrf_token" => $app->lacne->request->csrf_token_generate()
	);
	if($cnt > 0)
	{
	    $i = 0;
	    foreach($data_list as $data)
	    {

	        //preview URLを生成（GETでpreview=1を追記する）
	        $preview_url = $app->lacne->url_setting->get_pageurl($data["id"],$data["category"], "", $data["original_filename"]);
	        $parse_preview_url = parse_url($app->lacne->get_urlpath().$preview_url);
	        if(isset($parse_preview_url["query"]) && $parse_preview_url["query"]){
	            $preview_url .= "&preview=1";
	        }
	        else
	        {
	            $preview_url .= "?preview=1";
	        }

	        //データを表示用に整形
	        $post_data[] = array(
	            "id"            => $data["id"],
	            "sub_data"      => ($i >= $output_num)?' class="subdata" style="display:none"':'', //さらに10件分取得したデータか。その場合classを付ける
	            "title"         => $data["title"],
	            //権限チェック(削除権限があるか、もしくは自分自身が作成した記事で公開されていないかどうかのチェック）
	            "delete"        => ($app->lacne->library["login"]->chk_controll_limit("delete_post") || (!$data["output_flag"] && isset($data["user_id"]) && $data["user_id"] == $login_id))?true:false,
	            "output_date"   => $app->lacne->library["post"]->view_output_date($data["output_date"]),
	            "app_icon_class"=> (isset($data["app_modified"]) && $data["app_modified"] != "0000-00-00 00:00:00") ? " class='smp-icon'":"", //iphone appからの投稿の場合に付けるアイコン
	            "app_device_txt"=> (isset($data["app_modified"]) && $data["app_modified"] != "0000-00-00 00:00:00") ? "【iphoneアプリからの投稿】":"", //iphoneアプリからの投稿の場合、タイトル横に付ける（スマフォのみ）
	            "category"      => (isset($category_list[$data["category"]]))?$category_list[$data["category"]]:"", //カテゴリ名
	            "edit_link"     => LACNE_APP_ADMIN_PATH."/article/edit.php?id=".$data["id"], //編集画面へのリンク先
	            "account_name"  => (method_exists($app->lacne->library["login"] , "get_account_name"))?$app->lacne->library["login"]->get_account_name($data["user_id"]):"",
	            "publish_menu"  => $app->lacne->library["post"]->publish_menu($data), //公開、非公開ボタンを表示するためのhtmlタグ
	            "publish_menu_smp"  => $app->lacne->library["post"]->publish_menu($data , true), //公開、非公開ボタンを表示するためのhtmlタグ
	            "preview_url"   => $preview_url, //プレビュー先URL
	        	"memo"          => $data["memo"] //プレビュー先URL
	        );
	        $i++;
	    }
	}
	$list_data_param["post_data"] = $post_data;
	//ここで一覧表示部分のhtmlを取得
	$list_html = $app->lacne->render("index_list" , $list_data_param , true);

	//view側に渡すデータさんたち
	$render_data = array_merge($render_data , array(
	    "category_list"     => $category_list,
	    "category_id"       => (!isset($category_id) || !$category_id) ? 0:$category_id,
	    "cnt_category_by"   => $cnt_category_by, //記事データがカテゴリごとに何件あるか
	    "data_list"         => $data_list,
	    "list_html"         => (isset($list_html))?$list_html:"", //一覧表示部分のhtml
	    "page"              => $page,
	    "cnt"               => $cnt,
	    "cnt_wait"          => $cnt_wait,
	    "page_num"          => $page_num,
	    "order"             => $order,
	    "html_pager_head"   => $app->lacne->library["post"]->html_post_list_pager_head($cnt , $page , $page_num , $output_num), //データ件数まわりの表示
	    "html_pager_str"    => fn_pagenavi($page , $page_num , fn_set_urlparam($_GET, array('category','sort','wait'),false),true), //ページャまわりの表示
	    //CSRF TOKEN
	    "csrf_token"        => $app->lacne->request->csrf_token_generate()
	));

	//Render
	return $app->lacne->render("index" , $render_data, true);
}
?>