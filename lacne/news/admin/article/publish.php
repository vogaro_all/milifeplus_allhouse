<?php

use lacne\core\model\Post;
use lacne\core\model\SnsReserved;

return function($request, $response, $service, $app) {
	$app->lacne->load_library(array('login' , 'post' , 'setting')); //library load
	$app->lacne->session->sessionStart(); //Session start
	$login_id = $app->lacne->library["login"]->IsSuccess(); //認証チェック

	//Templateファイルのフォルダ指定(通常読み込むtemplateフォルダの中のさらにarticle以下にあるため）
	$app->lacne->template->setViewDir($app->lacne->template->getViewDir()."/article");

	//操作対象となる記事の情報取得
	$data = array();
	if(isset($_GET["id"]) && is_numeric($_GET["id"]))
	{
	    $data = with(new Post())->fetchOne($_GET["id"]);
	}
	if(!$data)
	{
	    die("Error(Not Found)");
	}

	$render_data = array(
	    "data" => $data,
	    "cancel_page" => LACNE_APP_ADMIN_PATH."/article/index.php".fn_set_urlparam($_GET, array("page","category","sort","wait"))
	);

	// ------------------------------------------------------------------------
	// 承認or差戻し画面
	// ------------------------------------------------------------------------
	if($app->lacne->action("/app")){

	    //権限チェック(公開権限があるか）
	    if($app->lacne->library["login"]->chk_controll_limit("publish_post"))
	    {

	        //この記事がstatus情報を持つ（承認待ち状態か）かどうか
	        if(!$data["status"])
	        {
	            $err = "この".KEYWORD_KIJI."は承認待ちの状態ではないか、権限がない可能性があります。";
	        }
	        else
	        {
	            if(isset($_POST["message"]) && (isset($_POST["app"]) || isset($_POST["back"])))
	            {
/*
	                //CSRFチェック
	                $csrf_check = false;
	                if(isset($_POST["token"]) && $_POST["token"])
	                {
	                    $csrf_check = $app->lacne->request->csrf_check($_POST["token"]);
	                }
	                if(!$csrf_check) $err = "データの受け渡しで問題が発生しました。もう一度操作をやり直して下さい。";
*/
	                // -------------------
	                // 承認実行
	                // -------------------
	                if($csrf_check && $_POST["app"])
	                {
	                    //承認オプションが有効で、さらに
	                    if(method_exists($app->lacne->library["post"], "app_publish"))
	                    {
	                        $app->lacne->library["post"]->app_publish($data["id"] , $login_id);

	                        //SNS連携が有効かつSNS認証済みならそっちに飛ばす
	                        if(method_exists($app->lacne->library["setting"], "get_connect_info"))
	                        {
	                            //SNS認証データ取得
	                            $connect_info = $app->lacne->library["setting"]->get_connect_info();
	                            //TwitterまたはFacebookの認証ができているか
	                            if($connect_info["twitter"]["account"] || $connect_info["facebook"]["account"])
	                            {
	                                fn_redirect(LACNE_APP_ADMIN_PATH."/article/publish.php?action=social&id=".$data["id"]."&".fn_set_urlparam($_GET, array("page","category","sort","wait"),false));

	                            }
	                        }

	                        //SNSの遷移がなければ、そのままコンプリート画面へ
	                        fn_redirect(LACNE_APP_ADMIN_PATH."/article/publish.php?action=complete&id=".$data["id"]."&".fn_set_urlparam($_GET, array("page","category","sort","wait"),false));

	                    }
	                }
	                // -------------------
	                // 差戻し実行
	                // -------------------
	                else if($csrf_check && $_POST["back"])
	                {
	                    //承認オプションが有効で、さらにこの記事がstatus情報を持つ（承認待ち状態か）かどうか
	                    if(method_exists($app->lacne->library["post"], "app_publish") && $data["status"])
	                    {
	                        $message = "";
	                        if(isset($_POST["message"]) && $_POST["message"])
	                        {
	                            $message = $_POST["message"];
	                        }
	                        $app->lacne->library["post"]->app_back($data["id"] , $login_id , $message);

	                        //コンプリート画面へ
	                        fn_redirect(LACNE_APP_ADMIN_PATH."/article/publish.php?action=complete&id=".$data["id"]."&".fn_set_urlparam($_GET, array("page","category","sort","wait"),false));
	                    }
	                }
	            }
	        }

	        if(!$err)
	        {
	            $render_data["submit_link"] = LACNE_APP_ADMIN_PATH."/article/publish.php?action=app&id=".$data["id"]."&".fn_set_urlparam($_GET, array("page","category","sort","wait"),false);
	            $render_data["csrf_token"] = $app->lacne->request->csrf_token_generate(); //CSRF_TOKEN
	            $app->lacne->render("approval" , $render_data);
	        }
	    }
	    else
	    {
	        //権限なし エラー表示
	        $err = "この操作をおこなう権限がありません。";
	    }

	    if(isset($err) && $err)
	    {
	        $render_data["err"] = $err;
	        $app->lacne->render("error" , $render_data);
	    }
	}


	// ------------------------------------------------------------------------
	// 公開or非公開実行
	// ------------------------------------------------------------------------
	if($app->lacne->action("/publish")) {
/*
	    //CSRFチェック
	    $csrf_check = false;
	    if(isset($_POST["token"]) && $_POST["token"])
	    {
	        $csrf_check = $app->lacne->request->csrf_check($_POST["token"]);
	    }
*/
	    //権限チェック(公開権限があるか）
//	    if($csrf_check && $app->lacne->library["login"]->chk_controll_limit("publish_post"))
	    if($app->lacne->library["login"]->chk_controll_limit("publish_post"))
	    {
	        //さらにこの記事がstatus情報を持たない（承認待ち状態でない）状態か
	        if(!$data["status"])
	        {
	            $app->lacne->library["post"]->change_publish($data["id"] , $login_id);
	            //SNS連携が有効かつSNS認証済みならそっちに飛ばす
	            if($data["output_flag"] != 1 && method_exists($app->lacne->library["setting"], "get_connect_info"))
	            {
	                //SNS認証データ取得
	                $connect_info = $app->lacne->library["setting"]->get_connect_info();
	                //TwitterまたはFacebookの認証ができているか
	                if($connect_info["twitter"]["account"] || $connect_info["facebook"]["account"])
	                {
	                    fn_redirect(LACNE_APP_ADMIN_PATH."/article/publish.php?action=social&id=".$data["id"]."&".fn_set_urlparam($_GET, array("page","category","sort","wait"),false));
	                }
	            }
	            //SNSの遷移がなければ、そのままコンプリート画面へ
	            fn_redirect(LACNE_APP_ADMIN_PATH."/article/publish.php?action=complete&id=".$data["id"]."&".fn_set_urlparam($_GET, array("page","category","sort","wait"),false));

	        }
	        else
	        {
	            $err = "この".KEYWORD_KIJI."は承認待ち状態の可能性があります。";
	        }
	    }
	    else
	    {
	        //権限なし エラー表示
	        $err = "この操作をおこなう権限がありません。";
	    }

	    if(isset($err) && $err)
	    {
	        $render_data["err"] = $err;
	        $app->lacne->render("error" , $render_data);
	    }
	}

	// ------------------------------------------------------------------------
	// 完了
	// ------------------------------------------------------------------------
	if($app->lacne->action("/complete")){
	    $app->lacne->render("complete" , $render_data);
	}

	// ------------------------------------------------------------------------
	// 公開・非非公開の処理確認画面
	// ------------------------------------------------------------------------
	if($app->lacne->action("/")){

	    //公開、非公開の処理実行用URL
	    $render_data["publish_link"] = LACNE_APP_ADMIN_PATH."/article/publish.php?action=publish&id=".$data["id"]."&".fn_set_urlparam($_GET, array("page","category","sort","wait"),false);
	    $render_data["csrf_token"] = $app->lacne->request->csrf_token_generate(); //CSRF_TOKEN
	    return $app->lacne->render("confirm" , $render_data, true);
	}
}
?>
