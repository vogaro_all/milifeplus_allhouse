<?php

return function($request, $response, $service, $app) {
	$app->lacne->load_library(array('login', 'post', 'media', 'validation')); //library load
	$app->lacne->session->sessionStart(); //Session start
	$app->lacne->library["login"]->IsSuccess(); //認証チェック

	$return = "";

	// ------------------------------------------------------------------------
	// カテゴリに紐付くタグ取得
	// ------------------------------------------------------------------------
	if(isset($_GET["recmmd_cate"]) && is_numeric($_GET["recmmd_cate"]) && $_GET["getdata"] == "tag" && !isset($_GET["recmmd_tag"]))
	{
		$tag = $app->lacne->library['post']->get_categoy_tag_list($_GET["recmmd_cate"]);
		$return = fn_output_html_select_str($tag,'');
	}

	// ------------------------------------------------------------------------
	// カテゴリに紐付く記事取得
	// ------------------------------------------------------------------------
	if(isset($_GET["recmmd_cate"]) && is_numeric($_GET["recmmd_cate"]) && $_GET["getdata"] == "article" && !isset($_GET["recmmd_tag"]))
	{
		$article = $app->lacne->library['post']->get_category_article_list($_GET["recmmd_cate"]);
		$return = fn_output_html_select_str($article,'');
	}

	// ------------------------------------------------------------------------
	// タグに紐付く記事取得
	// ------------------------------------------------------------------------
	if(isset($_GET["recmmd_tag"]) && is_numeric($_GET["recmmd_tag"]) && isset($_GET["recmmd_cate"]) && is_numeric($_GET["recmmd_cate"]) )
	{
		$article = $app->lacne->library['post']->get_tag_article_list($_GET["recmmd_tag"], $_GET["recmmd_cate"]);
		$return = fn_output_html_select_str($article,'');
	}

	// ------------------------------------------------------------------------
	// 選択された記事情報を取得
	// ------------------------------------------------------------------------
	if(isset($_GET["recmmd_post"]) && is_numeric($_GET["recmmd_post"]) )
	{
		$ret = $app->lacne->library['post']->get_the_tag_post($_GET["recmmd_post"]);
		$return = json_encode($ret);
	}


	$response->append($return);
	$response->send();
}
?>