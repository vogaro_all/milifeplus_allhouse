<?php

use lacne\core\model\Post;
use lacne\core\model\Parts;
use lacne\core\model\Tag;
// use lacne\core\model\Doctor;

return function($request, $response, $service, $app) {
    global $_OWNEDMEDIA_PARTS_CONFIG;
// 	global $_RECOMMEND_CNT;

    // ------------------------------------------------------------------------
    // セットアップ
    // ------------------------------------------------------------------------
    $app->lacne->load_library(array('login', 'post', 'media', 'validation')); //library load
    $app->lacne->session->sessionStart(); //Session start
    $login_id = $app->lacne->library["login"]->IsSuccess(); //認証チェック

    //Templateファイルのフォルダ指定(通常読み込むtemplateフォルダの中のさらにarticle以下にあるため）
    $app->lacne->template->setViewDir($app->lacne->template->getViewDir()."/article");
    //render_data : Template側に渡すデータ変数
    $render_data = array(
        "login_id" => $login_id
    );

// 	$recommend_list = array();

    //記事の作成・編集が可能かどうかをチェック
    $post_authority = 1;
    //編集モードの場合、公開権限があるか、またはまだ公開されていない自身作成の記事であるかをチェック
    if(isset($_GET["id"]) && is_numeric($_GET["id"]))
    {
        $rs_data = with(new Post())->fetchOne($_GET["id"]);
        if(empty($rs_data)) $post_authority = 0;
        if(!($app->lacne->library["login"]->chk_controll_limit("publish_post") || (!$rs_data["output_flag"] && isset($rs_data["user_id"]) && $login_id == $rs_data["user_id"])))
        {
            $post_authority = 0; //編集不可
        }

        // 関連記事
// 	    $recommend_list = $app->lacne->library['post']->get_recommend_list($_GET["id"]);
    }

    // ------------------------------------------------------------------------
    // ここからPOST 登録処理
    // ------------------------------------------------------------------------
    if($app->lacne->action('/submit'))
    {

        //直接アクセスはリダイレクト
        if(!isset($_POST["title"]))
        {
            fn_redirect(LACNE_APP_ADMIN_PATH."/article/edit.php");
        }

        //送信されてきたパラメータ取得
        $data_list = fn_get_form_param($_POST);

        if(isset($data_list["_meta_"]) && $data_list["_meta_"])
        {
            $_meta_data = $data_list["_meta_"];
        }

        //タグデータの整形
        if(!is_array($data_list['tags'])) $data_list['tags'] = array();
        $data_tags =array_filter(array_unique($data_list['tags']));
        unset($data_list['tags']);
        unset($data_list['search_tag']);

        //関連記事の整形
// 	    $recommend_arr = (!empty($data_list['recommend_post'])) ? $data_list['recommend_post'] : array();
// 	    $data_recommend = array_filter(array_unique($recommend_arr));

        unset($data_list['p_tags']);

        //POSTで送信されるsection_radioXXX のデータを削除
        if(!empty($data_list)) {
            foreach($data_list as $key => $value) {
                if(preg_match('/^section_radio/' , $key)) {
                    unset($data_list[$key]);
                }
            }
        }

        //入力チェック TODO
        $err_check_arr = array(
            "title"		 	=>array("name"=>"タイトル","type"=>array("require","len"),"length"=>200),
            "output_date"	=>array("name"=>"公開日時","type"=>array("require","len","output_date"),"length"=>19),
            "category"	 	=>array("name"=>"カテゴリ","type"=>array("require","len","numeric"),"length"=>2),
            "thumb_image" 	=>array("name"=>"画像","type"=>array("null", "len"),"length"=>255),
//             "keyword" 		=>array("name"=>"メタ設定 キーワード","type"=>array("len"),"length"=>255),
            "description" 	=>array("name"=>"メタ設定 説明","type"=>array("len"),"length"=>255),
//             "ogp_image" 	=>array("name"=>"メタ設定 og:image","type"=>array("len"),"length"=>255),
        );

        //バリデーション実行
        $err = $app->lacne->library["validation"]->check($data_list,$err_check_arr);


        $parts_data_list = array();
        $parts_validation_rule = array();
        if(!empty($data_list['section']) && is_array($data_list['section'])) {
            $post_section_data = array();
            $cnt = 0;
            foreach($data_list['section'] as $section_no => $section_data) {
                $section_no = ($cnt+1);
                $key = key($section_data);
                if(!empty($_OWNEDMEDIA_PARTS_CONFIG[$key])) {
                    $label = $_OWNEDMEDIA_PARTS_CONFIG[$key]['label'];
                    foreach($_OWNEDMEDIA_PARTS_CONFIG[$key]['items'] as $item_key => $item) {
                        $parts_data_list['section__'.$section_no.'__'.$item_key] = (isset($section_data[$key][$item_key]) ? $section_data[$key][$item_key] : '');
                        $parts_validation_rule['section__'.$section_no.'__'.$item_key] = array_merge($item['validation_rule'] , array('name' => '（セクション '.$section_no.'）'.$item['label']));
                    }
                }

                $post_section_data[$cnt] = $section_data;
                $cnt++;
            }

            if(!empty($parts_data_list) && !empty($parts_validation_rule)) {

                //バリデーション実行
                $err_parts = $app->lacne->library["validation"]->check($parts_data_list,$parts_validation_rule);
                if(!empty($err_parts)) {
                    $err = array_merge($err , $err_parts);
                }
            }

            $data_list['section'] = $post_section_data;

        }

        //編集モード
        if(isset($_GET["id"]) && is_numeric($_GET["id"]))
        {
            $rs_data = with(new Post())->fetchOne($_GET["id"]);

            //編集モードの場合、公開権限があるか、またはまだ公開されていない自身作成sの記事であるかをチェック
            if($post_authority)
            {
                //最終更新日時をチェックし、間に別アカウントで同じ記事を編集されていないか
                //チェック（複数アカウント対応用）
                if($rs_data && $rs_data["modified"] != $data_list["modified"])
                {
                    $err["update_err"] = "別のユーザーによってこの".KEYWORD_KIJI."の編集が行われた可能性があるため、この編集内容を保存することができません。<br />".KEYWORD_KIJI."一覧画面からもう一度編集作業をお試しください。";
                }
            }
            else
            {
                $err["update_err"] = "公開権限がないため、この".KEYWORD_KIJI."を編集することができない状態になっていない可能性があります。";
            }

            if(!$err)
            {
                $data_list["id"] = $_GET["id"];
                $data_list["modified"] = fn_get_date();
            }
        }
        else
        {
            //新規登録モードなら
            $data_list["output_flag"] = 0;
            $data_list["created"] = fn_get_date();
            $data_list["modified"] = fn_get_date();
            $data_list['status'] = '';
// 	        $recommend_list = array();
        }
/*
        //CSRF TOKENチェック
        if(!$err)
        {
            $csrf_check = false;
            if(isset($data_list["token"]) && $data_list["token"])
            {
                $csrf_check = $app->lacne->request->csrf_check($data_list["token"]);
            }
            if(!$csrf_check) $err["csrf_check"] = "データの受け渡しで問題が発生しました。もう一度操作をやり直して下さい。";
        }
*/
        //エラーがあれば、エラー出力 なければ確認画面へ
        if(!$err)
        {
            //記事作成（編集）者
            $data_list["user_id"] = $login_id;
            //unset data
            if(isset($data_list["_meta_"])) unset($data_list["_meta_"]);
            if(isset($data_list["token"])) unset($data_list["token"]);

            //パーツデータは別変数へ一旦移動
            if(!empty($data_list['section'])) {
                $section_data = $data_list['section'];
                unset($data_list['section']);
            }

            /** 予約フラグ */
            if (isset($data_list["reservation_flag"]) && $data_list["reservation_flag"]) {
                $data_list["reservation_flag"] = 1;
            } else {
                $data_list["reservation_flag"] = 0;
            }

            //DB登録
            $post_id = $app->lacne->library['post']->replace_post($data_list , $data_tags , $data_recommend, "id");

            //=============================
            //パーツデータを登録
            //=============================
            if(!empty($post_id))
            {
                with(new Parts())->save($_OWNEDMEDIA_PARTS_CONFIG , $post_id , $section_data);
                $data_list["section"] = $section_data;
            }

            //=============================
            //カスタムフィールド内容を登録
            //=============================
            if(isset($post_id) && $post_id && isset($_meta_data) && $_meta_data)
            {
                $meta_rs = $app->lacne->library["post"]->replace_meta($post_id , $_meta_data);
                $data_list["_meta_"] = $_meta_data;
            }

            if($post_id)
            {
                //登録完了
                //リダイレクトさせて、完了メッセージを表示させる
                //complete : 登録完了 ,  completewait : 承認待ち状態で登録
                //まず登録した記事が承認待ちかどうかチェック
                $post_info = with(new Post())->fetchOne($post_id);
                $message_type = "complete";
                if($post_info["status"] == "wait")
                {
                    $message_type = "completewait";
                }
                fn_redirect(LACNE_APP_ADMIN_PATH."/article/edit.php?id=".$post_id."&".$message_type);
            }
            else
            {
                //返却された$post_idが0 or falseならエラー発生の可能性
                $render_data["err"] = "データ登録時に問題が発生した可能性があります";
            }

        }
        else
        {
            //エラー
            $render_data["err"] = $err;

        }
    }

    //postされた$data_listが空で記事idがある場合、編集モードの表示内容　取得生成
    if(!isset($data_list) && isset($_GET["id"]) && fn_check_int($_GET["id"]))
    {
        $data_list = $app->lacne->library['post']->get_postdata2($_GET["id"]);

        //partsデータを読み込み
        $parts_data_list = with(new Parts())->findByPostID($data_list['id'] , '' , true);
        $data_list['section'] = array();
        if(!empty($parts_data_list)) {
            $data_list['section'] = with(new Parts())->convert_parts_data($parts_data_list);
        }

        //表示日時をY-m-d H:i形式に補正
        if(isset($data_list["output_date"]) && $data_list["output_date"])
        {
            $data_list["output_date"] = date("Y-m-d H:i" , strtotime($data_list["output_date"]));
        }

        /** 予約フラグ */
//         //開催日程をY-m-d形式に補正
//         if(isset($data_list["held_date"]) && $data_list["held_date"])
//         {
//             $data_list["held_date"] = date("Y-m-d" , strtotime($data_list["held_date"]));
//         }

        //登録完了後にリダイレクトしてきた場合、表示メッセージを用意する
        if(isset($_GET["complete"]))
        {
            $render_data["message"] = KEYWORD_KIJI."の登録・編集が完了しました。<br />新規作成された場合、".KEYWORD_KIJI."一覧画面において公開切り替えの操作を行う必要があります。";
        }
        else if(isset($_GET["completewait"]))
        {
            $render_data["message"] = KEYWORD_KIJI."の登録・編集が完了しました。<br />この".KEYWORD_KIJI."は承認待ちの状態として保存されました。管理者による承認が行われるまで公開されません。";
        }
    }


    $render_data = array_merge($render_data ,array(

        "post_authority"		=> $post_authority,
        "data_list"				=> $data_list,
        "category_list" 		=> $app->lacne->library['post']->get_category_list(), //登録されているカテゴリデータを取得
// 	    "sub_category_list" 		=> $app->lacne->library['post']->get_sub_category_list(), //登録されているカテゴリデータを取得
        "tag_list"				=> $app->lacne->library['post']->get_tag_list(), //登録されているタグデータを取得
        "csrf_token" 			=> $app->lacne->request->csrf_token_generate(),
    ));

    //Render
    return $app->lacne->render("edit" , $render_data, true);


};