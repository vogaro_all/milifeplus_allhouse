<?php
use lacne\core\model\Media;
return function($request, $response, $service, $app) {
	$app->lacne->load_library(array('login' , 'media')); //library load
	$app->lacne->session->sessionStart(); //Session start
	$login_id = $app->lacne->library["login"]->IsSuccess(); //認証チェック

	//Templateファイルのフォルダ指定(通常読み込むtemplateフォルダの中のさらにmedia以下にあるため）
	$app->lacne->template->setViewDir($app->lacne->template->getViewDir()."/media");
	//render_data : Template側に渡すデータ変数
	$render_data = array(
	    "login_id" => $login_id
	);


	$type = "";
	$field_name = "";
	$type_image = 0;
	$type_movie = 0;
	$search_tag = !empty($_GET["search_tag"]) ? fn_esc($_GET["search_tag"]):'';
	$search_param = "";
	//カスタムフィールドタイプの場合
	if(isset($_GET["type"]) && $_GET["type"] == "meta") {
		$type = "meta";
	}

	//カスタムフィールドタイプの画像指定ボタン判別用に画像フィールド名を受け渡し
	if(isset($_GET["field_name"]) && $_GET["field_name"]) {
		$field_name = fn_esc($_GET["field_name"]);
	}

	//画像ファイル選択タイプか
	if(isset($_GET["image"]) && $_GET["image"])
	{
		$type_image = 1;
	}

	//動画ファイル選択タイプか
	if(isset($_GET["movie"]) && $_GET["movie"])
	{
		$type_movie = 1;
	}

	//ページャーリンク用の付加パラメータ
	if(isset($type) && $type)
	{
		if($search_param)
		{
			$search_param .= "&";
		}
		$search_param .= "type=".$type;
	}
	if(isset($field_name) && $field_name)
	{
		if($search_param)
		{
			$search_param .= "&";
		}
		$search_param .= "field_name=".$field_name;
	}
	if(isset($type_image) && $type_image)
	{
		if($search_param)
		{
			$search_param .= "&";
		}
		$search_param .= "image=1";
	}
	if(isset($type_movie) && $type_movie)
	{
		if($search_param)
		{
			$search_param .= "&";
		}
		$search_param .= "movie=1";
	}
	if(isset($search_tag) && $search_tag)
	{
		if($search_param)
		{
			$search_param .= "&";
		}
		$search_param .= "search_tag=".$search_tag;
	}

	$render_data = array_merge($render_data , array(
		"type" => $type,
		"field_name" => $field_name,
		"type_movie" => $type_movie,
		"search_param" => $search_param
	));

	// ------------------------------------------------------------------------
	// ここからアップロードの処理
	// ------------------------------------------------------------------------
	if(!empty($_GET['upload']))
	{

		if($app->lacne->post('/upload')){

			//送信されてきたPOSTデータ取得
			$param = fn_get_form_param($_POST);

			$message = "";
			$errors = array();

			$data_list = array(
				"rename" => $param["rename"],
				"tag" => $param["tag"]
			);

			if(isset($param['file']) && isset($param['filename'])){
				$data_list['file'] = $param['file'];
				$data_list['filename'] = $param['filename'];
			}

			$file_num = count($data_list["rename"]);
			if($file_num != count($data_list["tag"])){ exit; }

			for($i=0; $i<$file_num; $i++)
			{
				//アップロードされたメディアファイル取得
				$media_file = array();
				if (isset($param['file']) && isset($param['file'][$i]) && preg_match('/^data:(.*?);base64,(.*)/', $param['file'][$i], $matches)) {
					$binary = base64_decode(strtr($matches[2], '-_,', '+/='));
					$media_file = array(
						'size' => strlen($binary),
						'name' => $data_list["filename"][$i],
						'binary' => $binary
					);
				} else {
					foreach($_FILES["file"] as $key=>$file){
						$media_file[$key] = $file[$i];
					}
				}

				// タグ
				$param["tag"] = $data_list["tag"][$i];
				// リネーム後のファイル名
				$param["rename"] = $data_list["rename"][$i];

				$result = $app->lacne->library["media"]->upload_process(array(
					'post' => $param,
					'media_file' => $media_file,
					'csrf_check' => false
				));

				if(!empty($result['status']) && $result['status'] == 'success' && !empty($result['message']))
				{
					//upload OK
					 $message = $result['message'];
				}
				else if(!empty($result['status']) && ($result['status'] == 'error'))
				{
					$errors[$i] = $result['err'];
				}
				else
				{
					$errors[$i] = 'アップロード時にエラーが発生しました。';
				}
			}
		}
		if(!$errors){
			$render_data["message"] = $message;
		}
		else
		{
			$render_data["err"] = $errors;
		}

	    $render_data['data_list'] = $data_list;
	    $render_data['file_num'] = $file_num;
		$render_data['csrf_token'] = $app->lacne->request->csrf_token_generate();
		//Render
		$app->lacne->render("list_upload" , $render_data);
	}

	// ------------------------------------------------------------------------
	// ここから一覧表示の処理
	// ------------------------------------------------------------------------
	else
	{

		//ページ単位の画像表示数
		$output_num = $app->lacne->library["admin_view"]->get_num_per_page("media_list");

		$filter = "";
		if($type_image) $filter = "image";
		if($type_movie) $filter = "movie";

		//現在のページ
		$page = 1;

		if($_GET['page']){
			$page = fn_now_page_set($_GET['page']);
		}
		//データ件数取得
		if($search_tag) {
			$cnt = with(new Media())->cnt("type = ? AND tag LIKE ?" , array($filter , '%'.$search_tag.'%'));
		} elseif($filter) {
			$cnt = with(new Media())->cnt("type = ?" , array($filter));
		} else {
			$cnt = with(new Media())->cnt("1=1");
		}

		//総ページ数取得
		$page_num = fn_getPages($cnt,$output_num);

		if($page > $page_num) {
			$page = $page_num;
		}

		$render_data = array_merge($render_data , array(
			"page" => $page,
			"page_num" => $page_num,
	    	"search_tag" => $search_tag,
		));

		//ファイルリスト取得
		$media_data = $app->lacne->library["media"]->get_ImageList($page , $output_num , $filter , $search_tag);
		$render_data["media_data"] = $media_data;

		//タグリスト取得
		$taglist_data = $app->lacne->library["media"]->get_tagList();
		$render_data["taglist_data"] = $taglist_data;
		//Render
		return $app->lacne->render("list" , $render_data, true);

	}
}
?>