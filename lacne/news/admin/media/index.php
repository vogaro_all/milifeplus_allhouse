<?php
use lacne\core\model\Media;
return function($request, $response, $service, $app) {
	$app->lacne->load_library(array('login' , 'media')); //library load
	$app->lacne->session->sessionStart(); //Session start
	$login_id = $app->lacne->library["login"]->IsSuccess(); //認証チェック

	//Templateファイルのフォルダ指定(通常読み込むtemplateフォルダの中のさらにmedia以下にあるため）
	$app->lacne->template->setViewDir($app->lacne->template->getViewDir()."/media");
	//render_data : Template側に渡すデータ変数
	$render_data = array(
	    "login_id" => $login_id
	);

	//アップロード済みファイル一覧ページ単位の画像表示数
	$output_num = $app->lacne->library["admin_view"]->get_num_per_page("media_index");

	// ------------------------------------------------------------------------
	// ここからアップロードの処理
	// ------------------------------------------------------------------------
	if($app->lacne->post('/upload')){

	    //送信されてきたPOSTデータ取得
	    $param = fn_get_form_param($_POST);

	    $message = "";
	    $errors = array();

	    $data_list = array(
	        "rename" => $param["rename"],
	        "tag" => $param["tag"]
	    );

	    if(isset($param['file']) && isset($param['filename'])){
	        $data_list['file'] = $param['file'];
	        $data_list['filename'] = $param['filename'];
	    }

	    $file_num = count($data_list["rename"]);
	    if($file_num != count($data_list["tag"])){ exit; }

	    for($i=0; $i<$file_num; $i++)
	    {
	    	//アップロードされたメディアファイル取得
	        $media_file = array();
	        if (isset($param['file']) && isset($param['file'][$i]) && preg_match('/^data:(.*?);base64,(.*)/', $param['file'][$i], $matches)) {
	            $binary = base64_decode(strtr($matches[2], '-_,', '+/='));
	            $media_file = array(
	                'size' => strlen($binary),
	                'name' => $data_list["filename"][$i],
	                'binary' => $binary
	            );

	        } else {
	            foreach($_FILES["file"] as $key=>$file){
	                $media_file[$key] = $file[$i];
	            }
	        }

	        // タグ
	        $param["tag"] = $data_list["tag"][$i];
	        // リネーム後のファイル名
	        $param["rename"] = $data_list["rename"][$i];

			$result = $app->lacne->library["media"]->upload_process(array(
				'post' => $param,
				'media_file' => $media_file,
				'csrf_check' => false
			));

			if(!empty($result['status']) && $result['status'] == 'success' && !empty($result['message']))
			{
				//upload OK
				$message = $result['message'];
			}
			else if(!empty($result['status']) && ($result['status'] == 'error'))
			{
				$errors[$i] = $result['err'];
			}
			else
			{
				$errors[$i] = 'アップロード時にエラーが発生しました。';
			}
		}

		if(!$errors){
			$render_data["message"] = $message;
		}
		else
		{
			$render_data["err"] = $errors;
		}
	}


	// ------------------------------------------------------------------------
	// ここからファイル削除の処理
	// ------------------------------------------------------------------------
	if($app->lacne->post('/del')){

	    $del_pict_cnt = 0;
	    if(isset($_POST["del_pict"]) && count($_POST["del_pict"]) > 0){
	        $del_pict_cnt = $app->lacne->library["media"]->delete_file($_POST["del_pict"]);
	    }

	    if($del_pict_cnt){
	        //登録完了フラグ
	        $render_data["message_delete"] = $del_pict_cnt."件のファイルを削除しました。";
	    }
	}


	// ------------------------------------------------------------------------
	// ここから画像ファイルリスト取得し、表示させる処理
	// ------------------------------------------------------------------------
	//絞り込みファイル・タイプ（image , movie）
	$filter = "";
	//現在のページ
	$page = 1;
	if(isset($_GET['page']) && $_GET['page']){
	    $page = fn_now_page_set($_GET['page']);
	}
	$search_tag = !empty($_GET["search_tag"]) ? fn_esc($_GET["search_tag"]):'';
	//データ件数取得
	if($search_tag) {
		$cnt = with(new Media())->cnt("tag LIKE ?" , array('%'.$search_tag.'%'));
	}else{
		$cnt = with(new Media())->cnt('1');
	}

	//総ページ数取得
	$page_num = fn_getPages($cnt,$output_num);

	if($page > $page_num){
	    $page = $page_num;
	}

	//アップ済みの画像リスト
	$media_data = $app->lacne->library["media"]->get_ImageList($page , $output_num , $filter , $search_tag);
	$taglist_data = $app->lacne->library["media"]->get_tagList();

	$render_data = array_merge($render_data , array(
		"upload_href" => "index.php",
	    "media_data" => $media_data,
	    "taglist_data" => $taglist_data,
	    "cnt" => $cnt,
	    "page" => $page,
	    "page_num" => $page_num,
	    "search_tag" => $search_tag,
	    "csrf_token" => $app->lacne->request->csrf_token_generate(),
	    "data_list" => $data_list,
	    "file_num" => $file_num,
	));

	//Render
	return $app->lacne->render("index" , $render_data, true);
}
?>