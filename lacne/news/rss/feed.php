<?php
use lacne\core\model\Post;
/**------------------------------------------------------------------------
 *
 *  RSS出力
 *
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------


/**
 *  RSS出力パラメータ
 *
 *  @param  number $category //カテゴリNo
 *  @param  number $limit //出力数
 */

require_once(dirname(__FILE__)."/../include/setup.php");

// ------------------------------------------------------------------------
// セットアップ
// ------------------------------------------------------------------------
$LACNE->load_library(array('output')); //library load

if(!defined("LACNE_RSS_ENABLED") || !LACNE_RSS_ENABLED) exit;

include_once(DIR_VENDORS."rhaco/Rhaco.php");
Rhaco::import("tag.feed.Rss20");

$option = array(
    "category"  => "",
    "num"       => (defined('LACNE_RSS_OUTPUT_NUM'))?LACNE_RSS_OUTPUT_NUM:10
);

//カテゴリ指定あり
if(isset($_GET["category"]) && $_GET["category"] && fn_check_int($_GET["category"]))
{
    $option["category"] = fn_sanitize($_GET["category"]);
}

//出力件数指定
if(isset($_GET["limit"]) && $_GET["limit"] && fn_check_int($_GET["limit"]))
{
    $option["limit"] = fn_sanitize($_GET["limit"]);
}

//データ取得
$search_param_arr = array(
    "post.output_flag =" => 1,
    "post.output_date <=" => fn_get_date()
);
if($option["category"])
{
    $search_param_arr["post.category ="] = $option["category"];
}
$post_data = with(new Post())->get_list(1 , $option["limit"] , $search_param_arr , array("key"=>"post.output_date","by"=>"DESC","val"=>"sort_date"));

if($post_data)
{
    $rss = new Rss20();
    $rss->setChannel(
        (defined('LACNE_RSS_TITLE'))?LACNE_RSS_TITLE:"",
        (defined('LACNE_RSS_SUBTITLE'))?LACNE_RSS_SUBTITLE:"",
        (defined('LACNE_RSS_SITEURL'))?LACNE_RSS_SITEURL:LACNE_URL,
        'ja'
    );
    foreach($post_data as $post)
    {
        $post_url = $post["link"];
        if(!$post["link"] && $post["title"])
        {
            $post_url = $LACNE->url_setting->get_pageurl($post["id"],$post["category"]);
            $post_url = $LACNE->get_urlpath($post_url);
        }
        $rssItem = new RssItem20();
        $rssItem->setTitle($post["title"]);
        $rssItem->setDescription($post["description"]);
        $rssItem->setLink($post_url);
        $rssItem->setPubdate($post["output_date"]);
        $rss->setItem($rssItem);
    }

    $rss->output();
}

exit;

?>


