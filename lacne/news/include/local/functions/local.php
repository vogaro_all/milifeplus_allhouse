<?php

/**
 * ローカル用共有関数
 *
 * @package  Lacne
 * @author  In Vogue Inc.
 * @link  http://lacne.jp
 * @copyright  Copyright 2008- In Vogue Inc. All rights reserved.
 */

/**
 *  リダイレクト
 *
 *  @param  string $url 移動先URL
 *  @param  bool $is301
 *  @return void
 */
/*
if( !function_exists('fn_redirect') )
{
    function fn_redirect( $url, $is301 = FALSE )
    {
            if( $is301 )
            {
                    header( "HTTP/1.1 301 Moved Permanently" );
            }
            header( "Location: " . $url );
            exit();
    }
}
*/

/**
 * タグ用のセレクトボックス作成
 *
 */
function createSelectBoxTag($allTag, $tag_name)
{
	$str = '<select name="tags[]" class="targetOptions"><option value="">選択してください</option>';
	foreach($allTag as $index => $item)
	{
		$str .= '<option value="'. $index .'"';

		if($item == $tag_name)
		{
			$str .= ' selected ';
		}

		$str .= '>'. $item .'</option>';
	}

	$str .= '</select>';

	return $str;
}