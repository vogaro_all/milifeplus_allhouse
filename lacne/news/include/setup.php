<?php
/**
 * セットアップファイル
 * コアファイルのロードとローカル用の設定ファイル、拡張するクラス等をinclude
 *
 * @package  Lacne
 * @author  In Vogue Inc.
 * @link  http://lacne.jp
 * @copyright  Copyright 2008- In Vogue Inc. All rights reserved.
 */


/**
 * LACNEコアファイルのinclude
 */
require_once(dirname(__FILE__)."/../../Base/system/loader.php");


/**
 * ローカルのパスやURLセット
 */
$APP_DIRNAME = LACNE::get_diff_path(realpath(dirname(__FILE__)."/../"));
define("LACNE_APP_PATH" , LACNE_PATH.$APP_DIRNAME); //lacne/local_dir
define("LACNE_APP_DIR" , LACNE_DIR.$APP_DIRNAME); //var/www/example.com/htdocs/lacne/local_dir
define("LACNE_APP_URL" , LACNE_URL.$APP_DIRNAME); //http://example.com/lacne/local_dir
define("LACNE_TEMPLATE_PATH" , LACNE_APP_DIR."/include/template"); //var/www/example.com/htdocs/lacne/local_dir/include/template/

define("LACNE_SHAREDATA_PATH", LACNE_PATH."/share"); //共通ファイル置き場へのパス /lacne/share
define("LACNE_SHAREDATA_DIR", LACNE_DIR."/share"); //共通ファイル置き場へのパス //var/www/example.com/htdocs/lacne/share
define("LACNE_APP_ADMIN_PATH" , LACNE_APP_PATH."/admin"); //管理画面のパス /lacne/local_dir/admin
define("LACNE_APP_UPLOAD_DIR" , LACNE_APP_DIR."/upload"); //メディアファイルのアップロード先

/**
 * ローカル用の設定ファイル、拡張するクラス等のinclude設定
 */
define("LACNE_APP_DIR_SYSTEM", LACNE_APP_DIR."/include");
define("LACNE_APP_DIR_CONFIG", LACNE_APP_DIR_SYSTEM."/config/");
define("LACNE_APP_DIR_FUNC", LACNE_APP_DIR_SYSTEM."/local/functions/");
define("LACNE_APP_DIR_MODEL", LACNE_APP_DIR_SYSTEM."/local/model/");
define("LACNE_APP_DIR_LIBRARY", LACNE_APP_DIR_SYSTEM."/local/library/");
define("LACNE_APP_DIR_VENDORS", LACNE_APP_DIR_SYSTEM."/local/vendors/");
define("LACNE_APP_DIR_OPTION", LACNE_APP_DIR_SYSTEM."/local/option/");
define("LACNE_APP_DIR_TEMPLATE", LACNE_APP_DIR_SYSTEM."template/");
define("LACNE_APP_DIR_OUTPUT_TEMPLATE", LACNE_APP_DIR_SYSTEM.'/../output/template/');

//テンプレートファイルでインクルードするheaderやfooter、サイドナビなどの共通パーツ用の置き場所
define("LACNE_SHARE_TEMPLATE_DIR", LACNE_SHAREDATA_DIR."/include/template/");


//ローカル用でincludeするファイル
$_LOCAL_FILE_REQUIRES = array(
    //CONFIG
    LACNE_APP_DIR_CONFIG.'config.php',
    LACNE_APP_DIR_CONFIG.'database.php',
    LACNE_APP_DIR_CONFIG.'option.php',
    LACNE_APP_DIR_CONFIG.'RSS.php',
    LACNE_APP_DIR_CONFIG.'pageurl.php',
    LACNE_APP_DIR_CONFIG.'iphone_app.php',
    //FUNCTIONS
    LACNE_APP_DIR_FUNC.'local.php'
    //VENDORS
    //LACNE_APP_DIR_VENDORS.'xxxxx.php',
);

/**
 * 各種ファイルのinclude実行
 */
if(isset($_LOCAL_FILE_REQUIRES) && is_array($_LOCAL_FILE_REQUIRES))
{
    foreach ($_LOCAL_FILE_REQUIRES as $file) {
        if(file_exists($file))
        {
            include_once($file);
        }
    }
}


$LACNE = new LACNE();

$LACNE->load_library(array('admin_view')); //admin_viewライブラリはデフォで入れる
