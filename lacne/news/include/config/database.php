<?php

/**
 * ローカル用のDB設定ファイル
 *
 * @package  Lacne
 * @author  InVogue Inc.
 * @link  http://lacne.jp
 * @copyright  Copyright 2008- InVogue Inc. All rights reserved.
 */


/*----------------------------------------------
 *  TABLE設定
 *  [_CHECK_] ローカルコンテンツ内で利用するテーブル名のプレフィックス名やテーブル名の定義など
 *---------------------------------------------*/

//例えば「news」と定義すると
//記事データテーブルは「news_posts」テーブルを参照するようになる
define("TABLE_PREFIX" , "news");

//その他、このコンテンツ内でのみ利用するテーブルがあればここで定義してもOK
//define("TABLE_DUMMY" ,"dummy");

?>
