<?php

/**------------------------------------------------------------------------
 * 
 * iPhoneアプリ用の設定
 *
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------


/*----------------------------------------------
 *  iPhoneアプリ用テンプレート設定
 *  [_CHECK_] アプリから投稿された場合の本文テンプレートファイルを指定
 *---------------------------------------------*/

$IAPP_TEMPLATE_LIST = array(
    1 => array("name" => "テンプレート1" , "path" => LACNE_TEMPLATE_PATH."/app" , "viewname" => "app1"), //template/app/app1.php
    2 => array("name" => "テンプレート2" , "path" => LACNE_TEMPLATE_PATH."/app" , "viewname" => "app2")  //template/app/app2.php
);
