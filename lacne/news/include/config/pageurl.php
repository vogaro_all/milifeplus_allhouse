<?php
use lacne\core\UrlSetting;
/**------------------------------------------------------------------------
 *
 * 詳細ページのリンク設定
 *
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class PageUrl_setting extends UrlSetting {

    /*----------------------------------------------
     *  詳細ページのURL設定
     *  [_CHECK_] 詳細ページURLを設計する
     *---------------------------------------------*/
//     var $setting = array(
//         "url"  => "/detail/?id=[id]"
//     );

    // var $setting = array(
    //     "url"  => "/[detail]/[id]"
    // );

    /*
    var $setting = array(
        "url"  => "/news/cat_[category]/[id].php", //url: すべての詳細に対してURLルールは一定($categoryを割り当てることもできる）
    );

    var $setting = array(
        //id値をKEYに詳細ページのURLを決定する
        "id" => array(    //指定したIDを持つ詳細ページは個別のURLを割り当てる
            1 => "/news/detail_one.php",
            2 => "/news/detail_two.php"
        ),
        "url" => "/news/detail_[id].php" //url : それ以外はURLルールは一定
    );
*/
//     var $setting = array(
//         //category値をKEYに詳細ページのURLを決定する
//         "url" => "/contents/[id]" //url : それ以外はURLルールは一定
//     );

    var $setting = array(
        "url"  => "/news/detail/?id=[id]"
    );

//     var $setting = array(
//         //category値をKEYに詳細ページのURLを決定する
//         "category" => array(    //指定したカテゴリ値を持つ詳細ページは個別のURLを割り当てる
//             1 => "/study/[original_filename]",
//             2 => "/entertainment/[original_filename]",
//             3 => "/trend/[original_filename]",
//             4 => "/hobby/[original_filename]"
//         ),
        // //合致する条件がなければ、次にid値をKEYに詳細ページのURLを決定する
        // "id" => array(    //指定したIDを持つ詳細ページは個別のURLを割り当てる
        //     1 => "/news/detail_one.php",
        //     2 => "/news/detail_two.php"
        // ),
        // //上記のどれにも合致する条件がなければ,下記のそれ以外のルールを適用する
        // "url" => "/news/cat_[category]/[id].php" //url : それ以外はURLルールは一定
//     );



    /**
     * 設定された詳細ページURLが http://～でないパスであれば、
     * そのパスの先頭に SITE_BASE_PATH のパスを付け足す
     * @param boolean $add_base_path パスを付け足すかどうか
     * @return void
     */
    function PageUrl_setting($add_base_path = true)
    {
        if($add_base_path && defined("SITE_BASE_PATH") && SITE_BASE_PATH)
        {
            parent::_add_base_path(SITE_BASE_PATH);

        }
    }

}




