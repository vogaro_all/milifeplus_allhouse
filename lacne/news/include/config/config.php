<?php

/**
 * ローカル用の設定ファイル core側にない設定を記述
 *
 * @package  Lacne
 * @author  InVogue Inc.
 * @link  http://lacne.jp
 * @copyright  Copyright 2008- InVogue Inc. All rights reserved.
 */


/*----------------------------------------------
 *  基本情報
 *  [_CHECK_] 管理するコンテンツ名（ニュース管理、アイテム管理など）と
 *  サイドナビゲーションのID値などを指定（/share/include/template/temp_side）
 *---------------------------------------------*/
define ("LACNE_APP_ADMIN_PAGENAME" , "記事管理");
//define ("LACNE_APP_ADMIN_NAVI_ID" , "#Usually");
define ("LACNE_APP_ADMIN_NAVI_ID" , "#Extend01"); //サイドナビゲーションID
//サイトを見るでジャンプするフロント側のURL
define ("LACNE_APP_FRONTPAGE_URL" , "/");
//このコンテンツのINDEX（もしくはMAIN）ページURL
define ("LACNE_APP_INDEXPAGE_URL" , LACNE_APP_ADMIN_PATH."/index.php");

/*----------------------------------------------
 *  本文内の画像にモーダルウインドウ表示対応させるためのクラスを割り当てる
 *---------------------------------------------*/
define("USE_SETTING_LIGHTBOX" , "fancy_img");

/*----------------------------------------------
 *  「記事」という表現を変えたい場合にここを編集する（ex:商品、実績など）
 *---------------------------------------------*/
define("KEYWORD_KIJI" , "お知らせ");
global $_OWNEDMEDIA_PARTS_CONFIG;
global $_RECOMMEND_CNT;

// 2017/11/27追加
global $_OWNED_DETAIL_IMAGE_WIDTH;
$_OWNED_DETAIL_IMAGE_WIDTH = 840;

// 3点リーダの文字数
define("DOTS_CHAR_NO", 100);

// パーツ追加・変更時編集点
// パーツセクションの入力をチェック
$_OWNEDMEDIA_PARTS_CONFIG = array(
            //--------------------------------------------
            // 見出し
            //--------------------------------------------
            'head1' => array(
                'label' => '見出し / 大見出し',
                'abbr' => '大見',
                'items' => array(
                    'content' => array(
                        'label' => '内容',
                        'validation_rule' => array("type" => array("require","len"),"length"=>255)
                    )
                )
            ),
            'head2' => array(
                    'label' => '見出し / 中見出し',
                    'abbr' => '中見',
                    'items' => array(
                            'content' => array(
                                    'label' => '内容',
                                    'validation_rule' => array("type" => array("require","len"),"length"=>255)
                            )
                    )
            ),
            'head3' => array(
                'label' => '見出し / 小見出し',
                'abbr' => '小見',
                'items' => array(
                    'content' => array(
                        'label' => '内容',
                        'validation_rule' => array("type" => array("require","len"),"length"=>255)
                    )
                )
            ),
            //--------------------------------------------
            // 画像
            //--------------------------------------------
            'img1' => array(
                'label' => '画像 / 大画像',
                'abbr' => '大画',
                'items' => array(
                    'img' => array(
                        'label' => '画像',
                        'validation_rule' => array("type" => array("require","len"),"length"=>255)
                    ),
                    'caption_name' => array(
                        'label' => 'タイトル',
                        'validation_rule' => array("type" => array("len"),"length"=>255)
                    ),
                )
            ),
            'img2' => array(
                'label' => '画像 / 中画像',
                'abbr' => '中画',
                'items' => array(
                    'img' => array(
                        'label' => '画像',
                        'validation_rule' => array("type" => array("require","len"),"length"=>255)
                    ),
                    'caption_name' => array(
                           'label' => 'タイトル',
                           'validation_rule' => array("type" => array("len"),"length"=>255)
                    ),
                    'img2' => array(
                        'label' => '画像',
                        'validation_rule' => array("type" => array("require","len"),"length"=>255)
                    ),
                    'caption2_name' => array(
                        'label' => 'タイトル',
                        'validation_rule' => array("type" => array("len"),"length"=>255)
                    ),
                )
            ),
                'img3' => array(
                    'label' => '画像 / 小画像',
                    'abbr' => '小画',
                    'items' => array(
                        'img' => array(
                            'label' => '画像',
                            'validation_rule' => array("type" => array("require","len"),"length"=>255)
                        ),
                        'caption_name' => array(
                               'label' => 'タイトル',
                               'validation_rule' => array("type" => array("len"),"length"=>255)
                        ),
                        'img2' => array(
                            'label' => '画像',
                            'validation_rule' => array("type" => array("require","len"),"length"=>255)
                        ),
                        'caption2_name' => array(
                            'label' => 'タイトル',
                            'validation_rule' => array("type" => array("len"),"length"=>255)
                        ),
                        'img3' => array(
                            'label' => '画像',
                            'validation_rule' => array("type" => array("require","len"),"length"=>255)
                        ),
                        'caption3_name' => array(
                            'label' => 'タイトル',
                            'validation_rule' => array("type" => array("len"),"length"=>255)
                        ),
                    )
                ),
                'img4' => array(
                    'label' => '画像 / 左画像',
                    'abbr' => '左画',
                    'items' => array(
                        'img' => array(
                            'label' => '画像',
                            'validation_rule' => array("type" => array("require","len"),"length"=>255)
                        ),
                        'title_name' => array(
                               'label' => '見出し',
                               'validation_rule' => array("type" => array("len"),"length"=>255)
                        ),
                       'content' => array(
                           'label' => 'テキスト',
                           'validation_rule' => array("type" => array("require"))
                        ),
                    )
                ),
                'img5' => array(
                    'label' => '画像 / 右画像',
                    'abbr' => '右画',
                    'items' => array(
                        'img' => array(
                            'label' => '画像',
                            'validation_rule' => array("type" => array("require","len"),"length"=>255)
                        ),
                        'title_name' => array(
                               'label' => '見出し',
                               'validation_rule' => array("type" => array("len"),"length"=>255)
                        ),
                       'content' => array(
                           'label' => 'テキスト',
                           'validation_rule' => array("type" => array("require"))
                        ),
                    )
                ),
        'img6' => array(
            'label' => '画像 / 大画像',
            'abbr' => '大画',
            'items' => array(
                'img' => array(
                    'label' => '画像',
                    'validation_rule' => array("type" => array("require","len"),"length"=>255)
                ),
            )
        ),
    //--------------------------------------------
    // テキスト
    //--------------------------------------------
    'text1' => array(
        'label' => 'テキスト / リード文',
        'abbr' => '文',
        'items' => array(
            'content' => array(
                'label' => '内容',
                'validation_rule' => array("type" => array("require"))
            )
        )
    ),
    'text2' => array(
        'label' => 'テキスト / テキスト',
        'abbr' => '文',
        'items' => array(
            'content' => array(
                'label' => '内容',
                'validation_rule' => array("type" => array("require"))
            )
        )
    ),
    'text3' => array(
        'label' => 'テキスト / 塗りつぶし',
        'abbr' => '文',
        'items' => array(
            'content' => array(
                'label' => '内容',
                'validation_rule' => array("type" => array("require"))
            )
        )
    ),
    'text4' => array(
        'label' => 'テキスト / 箇条書き',
        'abbr' => '文',
        'items' => array(
            'title_name' => array(
                'label' => 'タイトル',
                'validation_rule' => array("type" => array("require", "len"),"length"=>255)
            ),
            'content_name' => array(
                'label' => '内容',
                'validation_rule' => array("type" => array())
            )
        )
    ),
    'text5' => array(
        'label' => 'テキスト / 枠付き箇条書き',
        'abbr' => '文',
        'items' => array(
            'title_name' => array(
                'label' => 'タイトル',
                'validation_rule' => array("type" => array("require", "len"),"length"=>255)
            ),
            'content_name' => array(
                'label' => '内容',
                'validation_rule' => array("type" => array("require"))
            )
        )
    ),
    'text6' => array(
        'label' => 'テキスト / 塗りつぶし箇条書き',
        'abbr' => '文',
        'items' => array(
            'title_name' => array(
                'label' => 'タイトル',
                'validation_rule' => array("type" => array("require", "len"),"length"=>255)
            ),
            'content_name' => array(
                'label' => '内容',
                'validation_rule' => array("type" => array("require"))
            )
        )
    ),
    'text7' => array(
        'label' => 'テキスト / リンクテキスト',
        'abbr' => 'リンクテキスト',
        'items' => array(
            'title_name' => array(
                'label' => 'リンク先タイトル',
                'validation_rule' => array("type" => array("require"))
            ),
            'link_name' => array(
                'label' => 'リンク先',
                'validation_rule' => array("type" => array("require"))
            )
        )
    ),
    'text8' => array(
        'label' => 'テキスト / 注釈',
        'abbr' => '文',
        'items' => array(
            'content' => array(
                'label' => '内容',
                'validation_rule' => array("type" => array("require"))
            )
        )
    ),
    'text9' => array(
        'label' => 'テキスト / Youtube動画',
        'abbr' => 'Youtube動画',
        'items' => array(
            'url_name' => array(
                'label' => '動画URL',
                'validation_rule' => array("type" => array("require"))
            ),
            'thumbnail_name' => array(
                'label' => '動画サムネイルURL',
                'validation_rule' => array("type" => array("require"))
            ),
            'title_name' => array(
                'label' => 'タイトル',
                'validation_rule' => array("type" => array())
            ),
            'caption_name' => array(
                'label' => 'キャプション',
                'validation_rule' => array("type" => array())
            ),
        )
    ),
            //--------------------------------------------
            // プロフィール
            //--------------------------------------------
            'profile2' => array(
                'label' => 'プロフィール / プロフィール大',
                'abbr' => 'プロ大',
                'items' => array(
                    'img_name' => array(
                        'label' => '画像',
                        'validation_rule' => array("type" => array("require","len"),"length"=>255)
                    ),
                    'simei_name' => array(
                        'label' => '氏名',
                        'validation_rule' => array("type" => array("require","len"),"length"=>255)
                    ),
                    'soshiki_name' => array(
                        'label' => '組織・役職',
                        'validation_rule' => array("type" => array("len"),"length"=>30)
                    ),
                   'intoro_name' => array(
                       'label' => '紹介文',
                       'validation_rule' => array("type" => array())
                    ),
                )
            ),
        );