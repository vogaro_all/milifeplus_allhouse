<script type="text/javascript">
<?php
if($device == "pc") :
// -----------------------------------------------
// PCの場合のみロード
// -----------------------------------------------
?>
$(function(){
    
    $.library.tooltip({margin_x:-35});
    $.library.css3('#Pc #Main p.toolTip span.text','<?=LACNE_SHAREDATA_PATH?>/');
    $.library.pulldown();
    
    $('#Pc #Main input.master').click(function(){
            $('#Pc #Main .table-list tbody input[type=checkbox]').prop('checked', this.checked);
    });
    $('#Pc #Main .section .table-list tbody tr').hover(function(){
            $(this).addClass('highlight');
    },function(){
            $(this).removeClass('highlight');
    });

    //削除ボタン
    $('.btn_del').css("cursor","pointer").click(function(){
        var tid = $(this).attr("id");
        tid = tid.replace("del_" , "");
        //idから削除対象とする記事のタイトルをajaxで取得してモーダルアラートに表示させる
        $.ajax({
            type:'POST',
            url : '<?=LACNE_APP_ADMIN_PATH?>/article/index.php',
            data:'post_title='+tid,
            success : function(result)
            {
                if(result)
                {
                    $('#box-delete a#delete_link').attr('href',tid);
                    $('#delete_post').html(result);
                    $.library.modalOpen($("<a>").attr("href","#box-delete"),"#Modal");
                }
            }
        });
    });
    $('#box-delete a#delete_link').click(function(){
        //リストから削除対象データを消して、次のデータを出現させる（Ajax利用）
        var tid = $(this).attr("href");
        $.ajax({
            url:'<?=LACNE_APP_ADMIN_PATH?>/article/index.php',
            type:"POST",
            data:"delete=1&ajax=1&id="+$(this).attr("href")+"&token=<?=$csrf_token?>",
            success:function(result)
            {
                $.library.modalClose();
                if(result == "1")
                {
                    $("#post_"+tid).fadeOut(500, function(){
                        //削除して、一覧表示部分を切り替え
                        $(this).remove();

                        //待機して（隠して）いた次のデータを表示
                        if($('#Main .section .table-list tbody tr.subdata').size())
                        {
                            $('#Main .section .table-list tbody tr.subdata:first').removeClass("subdata").fadeIn();
                        
                        }
                        else if((PAGE_NUM != '1' && PAGE != PAGE_NUM) || !$('#Main .section .table-list tbody tr').size()) 
                        {
                            window.location.reload(); //待機データがなく、現在地が最終ページでないならリロード
                        }
                        
                        //件数まわりの表示を再描画
                        $.ajax({
                            type : 'POST',
                            url  : '<?=LACNE_APP_ADMIN_PATH?>/article/index.php<?=fn_set_urlparam($_GET, array('category','page','sort','wait'))?>',
                            data : 'post_cnt=1',
                            dataType : 'html',
                            success : function(data)
                            {
                                $("#pager_head_area").html(data);
                            }
                        });

                        //ページャまわりの表示を再描画
                        $.ajax({
                            type : 'POST',
                            url  : '<?=LACNE_APP_ADMIN_PATH?>/article/index.php<?=fn_set_urlparam($_GET, array('category','page','sort','wait'))?>',
                            data : 'post_pager=1',
                            dataType : 'html',
                            success : function(data)
                            {
                                $("#pager_area").html(data);
                            }
                        });
                        
                        
                        //しましまストライプ再描画
                        $('#Main .section .table-list tbody tr').removeClass('odd');
                        $('#Main .section .table-list tbody tr').removeClass('last');
                        stripe_list();
                    })
                }
            }
        })
        return false;
    });
    
    
    //複数削除
    $('#btn_dels').click(function(){
        //削除にチェックされているか
        if($("input.delete_check:checked")[0])
        {
            $.library.modalOpen($("<a>").attr("href","#box-delete02"),"#Modal");
        }
        else
        {
            $("#error_message").html("削除対象とする<?=KEYWORD_KIJI?>にチェックを入れて下さい。");
            $.library.modalOpen($("<a>").attr("href","#box-error"),"#Modal");
        }
    });
    $('#run-delete').click(function(){
        $('form[name=del_form]').submit();
    });

    //公開切り替え
    $('.link_publish').css("cursor" , "pointer").click(function(){
        var tid = $(this).attr("id");
        tid = tid.replace("publish_" , "");
        $('#box-public iframe').attr('src','<?=LACNE_APP_ADMIN_PATH?>/article/publish.php?id='+tid+'&<?=fn_set_urlparam($_GET, array('category','page','sort','wait'),false)?>');
        $.library.modalOpen($("<a>").attr("href","#box-public"),"#Modal");
    });

    //承認操作
    $('.link_waiting').css("cursor" , "pointer").click(function(){
        var tid = $(this).attr("id");
        tid = tid.replace("waiting_" , "");
        $('#box-public iframe').attr('src','<?=LACNE_APP_ADMIN_PATH?>/article/publish.php?action=app&id='+tid+'&<?=fn_set_urlparam($_GET, array('category','page','sort','wait'),false)?>');
        $.library.modalOpen($("<a>").attr("href","#box-public"),"#Modal");
    });


    //ソート切り替え
    var sort_val = '<?=$order["val"]?>';
    var sort_by = '<?=$order["by"]?>';
    $(".sort").click(function(){
        var sort_param = $(this).attr("id");
        if(sort_param == sort_val)
        {
            if(sort_by == "desc") sort_param += "-asc";
            else sort_param += "-desc";
        }

        window.location.href = "<?=LACNE_APP_ADMIN_PATH?>/article/index.php?sort="+sort_param+"&<?=fn_set_urlparam($_GET, array('category' , 'wait'),false)?>";
        return false;
    })

});
        
<?php
endif;
?>
    
// -----------------------------------------------
// 以下は共通ロード
// -----------------------------------------------
$(function(){
    
    $.library.css3('#Main .section .table-list thead th,#Main .section .table-list thead td,#Main .section .pager-foot p.btn a,#Main .section .pager-foot div.pager ul li.count a,#Main .section .pager-foot div.pager ul li.count span,#Main .section .pager-foot div.pager ul li.count','<?=LACNE_SHAREDATA_PATH?>/');
    //しましま表示
    stripe_list();

});


function stripe_list()
{
    //しましま表示
    $('#Pc #Main .section .table-list tbody tr:last').addClass('last');
    $('#Pc #Main .section .table-list tbody tr:nth-child(even)').addClass('odd');
    $('#Smp #Main .section .list li:last').addClass('last');
}
// -----------------------------------------------
</script>
