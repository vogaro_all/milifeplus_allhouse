var lacne_component = lacne_component || {};

(function($){

    function get_img(path){

        if(path){
            return '<img src="' + path + '" width="100px" />';
        }

        return '';
    }

    /**
     * テキスト
     * @constructor
     */
    lacne_component.Text1 = function(){
        this.editor = new lacne_component.WYSIWYG();
    };

    /**
     * 通常テキスト
     * @type {string}
     */
    lacne_component.Text1.STATE = 'text1';

    lacne_component.Text1.prototype = {
        vm: function(){
            var self = this;

            return Vue.extend({
                created: function(){
                    this.$data.key = 'img-text' + lacne_component.Base.getUnique();

                    var data = this.$parent.$data.init_data.text1;
                    this.$data.value = data.content;
                },
                attached: function(){
                    self.editor.replace(this.$data.key);
                },
                template: $('#select-text-component-text1').html(),
                data: function(){
                    var name = lacne_component.Base.getName( lacne_component.Text1.STATE );
                    return  {
                        'name': name + '[content]',
                        value: '',
                        key: ''
                    }
                },
                methods: {
                    clear: function (e) {
                        if(window.confirm('選択された記事詳細の内容をクリアします。よろしいですか？')){
                            self.editor.editor.setData('');
                        }
                        return false;
                    }
                }
            });
        }
    };

    /**
     * テキスト
     * @constructor
     */
    lacne_component.Text2 = function(){
        this.editor = new lacne_component.WYSIWYG();
    };
    
    /**
     * 通常テキスト
     * @type {string}
     */
    lacne_component.Text2.STATE = 'text2';

    lacne_component.Text2.prototype = {
        vm: function(){
            var self = this;

            return Vue.extend({
                created: function(){
                    this.$data.key = 'img-text' + lacne_component.Base.getUnique();

                    var data = this.$parent.$data.init_data.text2;
                    this.$data.value = data.content;
                },
                attached: function(){
                    self.editor.replace(this.$data.key);
                },
                template: $('#select-text-component-text2').html(),
                data: function(){
                    var name = lacne_component.Base.getName( lacne_component.Text2.STATE );
                    return  {
                        'name': name + '[content]',
                        value: '',
                        key: ''
                    }
                },
                methods: {
                    clear: function (e) {
                        if(window.confirm('選択された記事詳細の内容をクリアします。よろしいですか？')){
                            self.editor.editor.setData('');
                        }
                        return false;
                    }
                }
            });
        }
    };
    
    /**
     * テキスト
     * @constructor
     */
    lacne_component.Text3 = function(){
        this.editor = new lacne_component.WYSIWYG();
    };
    /**
     * 通常テキスト
     * @type {string}
     */
    lacne_component.Text3.STATE = 'text3';

    lacne_component.Text3.prototype = {
        vm: function(){
            var self = this;

            return Vue.extend({
                created: function(){
                    this.$data.key = 'img-text' + lacne_component.Base.getUnique();

                    var data = this.$parent.$data.init_data.text3;
                    this.$data.value = data.content;
                },
                attached: function(){
                    self.editor.replace(this.$data.key);
                },
                template: $('#select-text-component-text3').html(),
                data: function(){
                    var name = lacne_component.Base.getName( lacne_component.Text3.STATE );
                    return  {
                        'name': name + '[content]',
                        value: '',
                        key: ''
                    }
                },
                methods: {
                    clear: function (e) {
                        if(window.confirm('選択された記事詳細の内容をクリアします。よろしいですか？')){
                            self.editor.editor.setData('');
                        }
                        return false;
                    }
                }
            });
        }
    };

    /**
     * @constructor
     */
     lacne_component.Text4 = function(){};

    lacne_component.Text4.STATE = 'text4';

    lacne_component.Text4.prototype = {
        vm: function(){
            var self = this;

            return Vue.extend({
                created: function(){
                    this.$data.key = 'img-text' + lacne_component.Base.getUnique();

                    var data = this.$parent.$data.init_data.text4;
                    this.$data.title_value = data.title_name;
                    this.$data.content_value = data.content_name;
                },

                template: $('#select-text-component-text4').html(),
                data: function(){
                    var name = lacne_component.Base.getName( lacne_component.Text4.STATE );
                    return  {
                        title_name: name + '[title_name]',
                        content_name: name + '[content_name]',
                        title_value: '',
                        content_value: ''
                    }
                },
            });
        }
    };
    
    
    /**
     * @constructor
     */
     lacne_component.Text5 = function(){};

    lacne_component.Text5.STATE = 'text5';

    lacne_component.Text5.prototype = {
        vm: function(){
            var self = this;

            return Vue.extend({
                created: function(){
                    this.$data.key = 'img-text' + lacne_component.Base.getUnique();

                    var data = this.$parent.$data.init_data.text5;
                    this.$data.title_value = data.title_name;
                    this.$data.content_value = data.content_name;
                },

                template: $('#select-text-component-text5').html(),
                data: function(){
                    var name = lacne_component.Base.getName( lacne_component.Text5.STATE );
                    return  {
                        title_name: name + '[title_name]',
                        content_name: name + '[content_name]',
                        title_value: '',
                        content_value: ''
                    }
                },
            });
        }
    };
    
    /**
     * @constructor
     */
     lacne_component.Text6 = function(){};

    lacne_component.Text6.STATE = 'text6';

    lacne_component.Text6.prototype = {
        vm: function(){
            var self = this;

            return Vue.extend({
                created: function(){
                    this.$data.key = 'img-text' + lacne_component.Base.getUnique();

                    var data = this.$parent.$data.init_data.text6;
                    this.$data.title_value = data.title_name;
                    this.$data.content_value = data.content_name;
                },

                template: $('#select-text-component-text6').html(),
                data: function(){
                    var name = lacne_component.Base.getName( lacne_component.Text6.STATE );
                    return  {
                        title_name: name + '[title_name]',
                        content_name: name + '[content_name]',
                        title_value: '',
                        content_value: ''
                    }
                },
            });
        }
    };
    
    /**
     * リンクテキスト
     * @constructor
     */
    lacne_component.Text7 = function(){
    };

    lacne_component.Text7.STATE = 'text7';

    lacne_component.Text7.prototype = {
        vm: function(){
            var self = this;

            return Vue.extend({
                created: function(){
                    this.$data.key = 'img-text' + lacne_component.Base.getUnique();

                    var data = this.$parent.$data.init_data.text7;
                    this.$data.title_value = data.title_name;
                    this.$data.link_value = data.link_name;
                },

                template: $('#select-text-component-text7').html(),
                data: function(){
                    var name = lacne_component.Base.getName( lacne_component.Text7.STATE );
                    return  {
                        title_name: name + '[title_name]',
                        link_name: name + '[link_name]',
                        title_value: '',
                        content_value: '',
                        link_value: ''
                    }
                },
            });
        }
    };
    
    /**
     * テキスト
     * @constructor
     */
    lacne_component.Text8= function(){
        this.editor = new lacne_component.WYSIWYG();
    };
    
    /**
     * 通常テキスト
     * @type {string}
     */
    lacne_component.Text8.STATE = 'text8';

    lacne_component.Text8.prototype = {
        vm: function(){
            var self = this;

            return Vue.extend({
                created: function(){
                    this.$data.key = 'img-text' + lacne_component.Base.getUnique();

                    var data = this.$parent.$data.init_data.text8;
                    this.$data.value = data.content;
                },
                attached: function(){
                    self.editor.replace(this.$data.key);
                },
                template: $('#select-text-component-text8').html(),
                data: function(){
                    var name = lacne_component.Base.getName( lacne_component.Text8.STATE );
                    return  {
                        'name': name + '[content]',
                        value: '',
                        key: ''
                    }
                },
                methods: {
                    clear: function (e) {
                        if(window.confirm('選択された記事詳細の内容をクリアします。よろしいですか？')){
                            self.editor.editor.setData('');
                        }
                        return false;
                    }
                }
            });
        }
    };

    /**
     * リンクテキスト
     * @constructor
     */
    lacne_component.Text9 = function(){
    };

    lacne_component.Text9.STATE = 'text9';

    lacne_component.Text9.prototype = {
        vm: function(){
            var self = this;

            return Vue.extend({
                created: function(){
                    this.$data.key = 'img-text' + lacne_component.Base.getUnique();

                    var data = this.$parent.$data.init_data.text9;
                    this.$data.url_value = data.url_name;
                    this.$data.thumbnail_value = data.thumbnail_name;
                    this.$data.title_value = data.title_name;
                    this.$data.caption_value = data.caption_name;
                },

                template: $('#select-text-component-text9').html(),
                data: function(){
                    var name = lacne_component.Base.getName( lacne_component.Text9.STATE );
                    return  {
                    	url_name: name + '[url_name]',
                    	thumbnail_name: name + '[thumbnail_name]',
                        title_name: name + '[title_name]',
                        caption_name: name + '[caption_name]',
                        url_value: '',
                        thumbnail_value: '',
                        title_value: '',
                        caption_value: ''
                    }
                },
            });
        }
    };
}(jQuery));
