var lacne_component = lacne_component || {};

(function($){

    //エントリーポイント
    $(function(){

        if(Object.keys(section_items).length){
            $.each(section_items, function(i, item){
                (new lacne_component.Section(item)).active(-1);
            });
        }else{
            (new lacne_component.Section()).active(-1);
        }
            (new lacne_component.Section()).updateSectionNum();

    });

}(jQuery));
