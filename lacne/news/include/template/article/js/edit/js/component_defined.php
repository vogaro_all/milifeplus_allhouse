<?php
// パーツ追加・変更時編集点
/**==========================
 * コンポーネント
 *==========================*/
?>
<script type="x-template" id="section-template">
    <section class="section-item-container">
        <div class="component">
            <h3 class="head-line03">セクション</h3>
            <div class="format">
                <ul>
                    <li class="head"><a class="title" href="#" onclick="return false">見出し</a>
                        <ul v-on:click="click($event, 1)" class="item">
                            <li><a href="#" data-bind="head1" onclick="return false">大見出し</a></li>
                            <li><a href="#" data-bind="head2" onclick="return false">中見出し</a></li>
                            <li><a href="#" data-bind="head3" onclick="return false">小見出し</a></li>
                        </ul>
                    </li>

                    <li class="img"><a class="title" href="#" onclick="return false">画像</a>
                        <ul v-on:click="click($event, 2)" class="item">
                            <li><a href="#" data-bind="img6" onclick="return false">メイン画像</a></li>
                            <!--<li><a href="#" data-bind="img1" onclick="return false">1カラム画像</a></li>
                            <li><a href="#" data-bind="img2" onclick="return false">2カラム画像</a></li>
							<li><a href="#" data-bind="img3" onclick="return false">1:2カラム画像</a></li>-->
						    <li><a href="#" data-bind="img4" onclick="return false">左画像</a></li>
						    <li><a href="#" data-bind="img5" onclick="return false">右画像</a></li>
                        </ul>
                    </li>

                    <li class="text"><a class="title" href="#" onclick="return false">テキスト</a>
                        <ul v-on:click="click($event, 3)" class="item">
                            <!--<li><a href="#" data-bind="text1" onclick="return false">リード文 &emsp;&emsp;</a></li>-->
                            <li><a href="#" data-bind="text2" onclick="return false">テキスト　　　　　</a></li>
                            <!--<li><a href="#" data-bind="text3" onclick="return false">塗りつぶしテキスト</a></li>
                            <li><a href="#" data-bind="text4" onclick="return false">箇条書き</a></li>
                            <li><a href="#" data-bind="text5" onclick="return false">枠付き箇条書き</a></li>
                            <li><a href="#" data-bind="text6" onclick="return false">塗りつぶし番号付き箇条書き</a></li>
                            <li><a href="#" data-bind="text7" onclick="return false">リンク</a></li>
                            <li><a href="#" data-bind="text8" onclick="return false">注釈</a></li>
                            <li><a href="#" data-bind="text9" onclick="return false">Youtube動画</a></li>-->
                        </ul>
                    </li>
                </ul>
                <!-- .format --></div>
            <div class="area">
                <component :is="currentView"></component>
                <!-- .area --></div>
            <!-- .component--></div>

        <div class="btn">
            <p class="btn-type01 pie insert" v-on:click="insert"><span class="pie">挿入</span></p>
            <p class="btn-type02 pie pc delete" v-on:click="delete"><span class="pie">削除</span></p>
        </div>
    </section>
</script>

<?php
/**==========================
 * ラジオボタンパーツ系
 *==========================*/
?>
<script type="x-template" id="radio-group">
    <input type="radio" name="{{ name }}" id="{{ bind1 }}" v-on:change="onChange" data-bind="1" /><label for="{{ bind1 }}" >見出し</label>
    <input type="radio" name="{{ name }}" id="{{ bind2 }}" v-on:change="onChange" data-bind="2" /><label for="{{ bind2 }}" >画像</label>
    <input type="radio" name="{{ name }}" id="{{ bind3 }}" v-on:change="onChange" data-bind="3" /><label for="{{ bind3 }}" >テキスト</label>
    <input type="radio" name="{{ name }}" id="{{ bind4 }}" v-on:change="onChange" data-bind="4" /><label for="{{ bind4 }}" >CV導線</label>
    <input type="radio" name="{{ name }}" id="{{ bind5 }}" v-on:change="onChange" data-bind="5" /><label for="{{ bind5 }}" >プロフィール</label>
</script>

<?php
/**==========================
 * 見出し系パーツ一覧
 *==========================*/
?>
<?php //大見出し ?>
<script type="x-template" id="select-head-component-head1">
    <dl>
        <dt>フォーマット：</dt>
        <dd>大見出し</dd><dt>内容：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ name }}" value="{{ value }}" /></span>
        </dd>
    </dl>
</script>
<?php //中見出し ?>
<script type="x-template" id="select-head-component-head2">
    <dl>
        <dt>フォーマット：</dt>
        <dd>中見出し</dd><dt>内容：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ name }}" value="{{ value }}" /></span>
        </dd>
    </dl>
</script>
<?php //小見出し ?>
<script type="x-template" id="select-head-component-head3">
    <dl>
        <dt>フォーマット：</dt>
        <dd>小見出し</dd><dt>内容：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ name }}" value="{{ value }}" /></span>
        </dd>
    </dl>
</script>

<?php
/**==========================
 * 画像系パーツ
 *==========================*/
?>
<?php //大画像の設定 ?>
<script type="x-template" id="select-img-component-img1">
    <dl>
        <dt>フォーマット：</dt>
        <dd>1カラム画像</dd>
        <dt>画像：<span class="req">必須</span><?php //画像選択フィールド ?></dt>
        <dd>
            <div class="btn img_area">
                <p class="btn-type01 pie img_btn"><input type="button" value="画像選択" id="img_select_{{ key }}" /></p>
                <p class="btn-type02 pie img_delete_btn"><input type="button" value="削除" id="img_delete_{{ key }}" /></p>
            </div>
            <input type="hidden" name="{{ img_name }}" value="{{ img_value }}" class="img_hidden_value" id="img_hidden_{{ key }}" />
            <p id="img_prev_{{ key }}" class="img_preview">{{{ preview_img }}}</p>
        </dd>
        <dt>タイトル:</dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ caption_name }}" value="{{ caption_value }}" /></span>
        </dd>
    </dl>
</script>

<?php //中画像 ?>
<script type="x-template" id="select-img-component-img2">
    <dl>
        <dt>フォーマット：</dt>
        <dd>2カラム画像</dd>
        <dt>画像：<span class="req">必須</span><?php //画像選択フィールド ?></dt>
        <dd>
            <div class="btn img_area">
                <p class="btn-type01 pie img_btn"><input type="button" value="画像選択" id="img_select_{{ key }}" /></p>
                <p class="btn-type02 pie img_delete_btn"><input type="button" value="削除" id="img_delete_{{ key }}" /></p>
            </div>
            <input type="hidden" name="{{ img_name }}" value="{{ img_value }}" class="img_hidden_value" id="img_hidden_{{ key }}" />
            <p id="img_prev_{{ key }}" class="img_preview">{{{ preview_img }}}</p>
        </dd>
        <dt>タイトル:</dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ caption_name }}" value="{{ caption_value }}" /></span>
        </dd>

		<dt>画像：<span class="req">必須</span><?php //画像選択フィールド ?></dt>
        <dd>
            <div class="btn img_area">
                <p class="btn-type01 pie img_btn"><input type="button" value="画像選択" id="img_select_{{ key2 }}" /></p>
                <p class="btn-type02 pie img_delete_btn"><input type="button" value="削除" id="img_delete_{{ key2 }}" /></p>
            </div>
            <input type="hidden" name="{{ img2_name }}" value="{{ img2_value }}" class="img_hidden_value" id="img_hidden_{{ key2 }}" />
            <p id="img_prev_{{ key2 }}" class="img_preview">{{{ preview2_img }}}</p>
        </dd>
        <dt>タイトル:</dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ caption2_name }}" value="{{ caption2_value }}" /></span>
        </dd>
    </dl>
</script>

<?php //小画像 ?>
<script type="x-template" id="select-img-component-img3">
    <dl>
        <dt>フォーマット：</dt>
        <dd>1:2カラム画像</dd>
        <dt>画像：<span class="req">必須</span><?php //画像選択フィールド ?></dt>
        <dd>
            <div class="btn img_area">
                <p class="btn-type01 pie img_btn"><input type="button" value="画像選択" id="img_select_{{ key }}" /></p>
                <p class="btn-type02 pie img_delete_btn"><input type="button" value="削除" id="img_delete_{{ key }}" /></p>
            </div>
            <input type="hidden" name="{{ img_name }}" value="{{ img_value }}" class="img_hidden_value" id="img_hidden_{{ key }}" />
            <p id="img_prev_{{ key }}" class="img_preview">{{{ preview_img }}}</p>
        </dd>
        <dt>タイトル:</dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ caption_name }}" value="{{ caption_value }}" /></span>
        </dd>
		<dt>画像：<span class="req">必須</span><?php //画像選択フィールド ?></dt>
        <dd>
            <div class="btn img_area">
                <p class="btn-type01 pie img_btn"><input type="button" value="画像選択" id="img_select_{{ key2 }}" /></p>
                <p class="btn-type02 pie img_delete_btn"><input type="button" value="削除" id="img_delete_{{ key2 }}" /></p>
            </div>
            <input type="hidden" name="{{ img2_name }}" value="{{ img2_value }}" class="img_hidden_value" id="img_hidden_{{ key2 }}" />
            <p id="img_prev_{{ key2 }}" class="img_preview">{{{ preview2_img }}}</p>
        </dd>
        <dt>タイトル:</dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ caption2_name }}" value="{{ caption2_value }}" /></span>
        </dd>
		<dt>画像：<span class="req">必須</span><?php //画像選択フィールド ?></dt>
        <dd>
            <div class="btn img_area">
                <p class="btn-type01 pie img_btn"><input type="button" value="画像選択" id="img_select_{{ key3 }}" /></p>
                <p class="btn-type02 pie img_delete_btn"><input type="button" value="削除" id="img_delete_{{ key3 }}" /></p>
            </div>
            <input type="hidden" name="{{ img3_name }}" value="{{ img3_value }}" class="img_hidden_value" id="img_hidden_{{ key3 }}" />
            <p id="img_prev_{{ key3 }}" class="img_preview">{{{ preview3_img }}}</p>
        </dd>
        <dt>タイトル:</dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ caption3_name }}" value="{{ caption3_value }}" /></span>
        </dd>
    </dl>
</script>

<?php //左画像 ?>
<script type="x-template" id="select-img-component-img4">
    <dl>
        <dt>フォーマット：</dt>
        <dd>左画像</dd>
        <dt>画像：<span class="req">必須</span><?php //画像選択フィールド ?></dt>
        <dd>
            <div class="btn img_area">
                <p class="btn-type01 pie img_btn"><input type="button" value="画像選択" id="img_select_{{ key }}" /></p>
                <p class="btn-type02 pie img_delete_btn"><input type="button" value="削除" id="img_delete_{{ key }}" /></p>
            </div>
            <input type="hidden" name="{{ img_name }}" value="{{ img_value }}" class="img_hidden_value" id="img_hidden_{{ key }}" />
            <p id="img_prev_{{ key }}" class="img_preview">{{{ preview_img }}}</p>
        </dd>
		<dt>見出し:</dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ title_name }}" value="{{ title_value }}" /></span>
        </dd>
		<dt>テキスト：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><textarea cols="100" rows="7" class="textarea" name="{{ name }}" id="{{ key }}" >{{ value }}</textarea></span>
            <div class="reset" v-on:click="clear">
                <p class="btn-type03"><strong><a href="#" class="pie" onclick="return false">詳細クリア</a></strong></p>
            </div>
        </dd>
    </dl>
</script>

<?php //右画像 ?>
<script type="x-template" id="select-img-component-img5">
    <dl>
        <dt>フォーマット：</dt>
        <dd>右画像</dd>
        <dt>画像：<span class="req">必須</span><?php //画像選択フィールド ?></dt>
        <dd>
            <div class="btn img_area">
                <p class="btn-type01 pie img_btn"><input type="button" value="画像選択" id="img_select_{{ key }}" /></p>
                <p class="btn-type02 pie img_delete_btn"><input type="button" value="削除" id="img_delete_{{ key }}" /></p>
            </div>
            <input type="hidden" name="{{ img_name }}" value="{{ img_value }}" class="img_hidden_value" id="img_hidden_{{ key }}" />
            <p id="img_prev_{{ key }}" class="img_preview">{{{ preview_img }}}</p>
        </dd>
		<dt>見出し:</dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ title_name }}" value="{{ title_value }}" /></span>
        </dd>
		<dt>テキスト：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><textarea cols="100" rows="7" class="textarea" name="{{ name }}" id="{{ key }}" >{{ value }}</textarea></span>
            <div class="reset" v-on:click="clear">
                <p class="btn-type03"><strong><a href="#" class="pie" onclick="return false">詳細クリア</a></strong></p>
            </div>
        </dd>
    </dl>
</script>

<?php //メイン画像の設定 ?>
<script type="x-template" id="select-img-component-img6">
    <dl>
        <dt>フォーマット：</dt>
        <dd>メイン画像</dd>
        <dt>画像：<span class="req">必須</span><?php //画像選択フィールド ?></dt>
        <dd>
            <div class="btn img_area">
                <p class="btn-type01 pie img_btn"><input type="button" value="画像選択" id="img_select_{{ key }}" /></p>
                <p class="btn-type02 pie img_delete_btn"><input type="button" value="削除" id="img_delete_{{ key }}" /></p>
            </div>
            <input type="hidden" name="{{ img_name }}" value="{{ img_value }}" class="img_hidden_value" id="img_hidden_{{ key }}" />
            <p id="img_prev_{{ key }}" class="img_preview">{{{ preview_img }}}</p>
        </dd>
    </dl>
</script>

<?php
/**==========================
 * テキスト系パーツ
 *==========================*/
?>
<?php //リード文 ?>
<script type="x-template" id="select-text-component-text1">
    <dl>
        <dt>フォーマット：</dt>
        <dd>リード文</dd>
        <dt>内容：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><textarea cols="100" rows="7" class="textarea" name="{{ name }}" id="{{ key }}" >{{ value }}</textarea></span>
            <div class="reset" v-on:click="clear">
                <p class="btn-type03"><strong><a href="#" class="pie" onclick="return false">詳細クリア</a></strong></p>
            </div>
        </dd>
    </dl>
</script>

<?php //テキスト ?>
<script type="x-template" id="select-text-component-text2">
    <dl>
        <dt>フォーマット：</dt>
        <dd>テキスト</dd>
        <dt>内容：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><textarea cols="100" rows="7" class="textarea" name="{{ name }}" id="{{ key }}" >{{ value }}</textarea></span>
            <div class="reset" v-on:click="clear">
                <p class="btn-type03"><strong><a href="#" class="pie" onclick="return false">詳細クリア</a></strong></p>
            </div>
        </dd>
    </dl>
</script>

<?php //塗りつぶしテキスト ?>
<script type="x-template" id="select-text-component-text3">
    <dl>
        <dt>フォーマット：</dt>
        <dd>塗りつぶしテキスト</dd>
        <dt>内容：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><textarea cols="100" rows="7" class="textarea" name="{{ name }}" id="{{ key }}" >{{ value }}</textarea></span>
            <div class="reset" v-on:click="clear">
                <p class="btn-type03"><strong><a href="#" class="pie" onclick="return false">詳細クリア</a></strong></p>
            </div>
        </dd>
    </dl>
</script>

<?php //囲み帯付き枠 ?>
<script type="x-template" id="select-text-component-text4">
    <dl>
        <dt>フォーマット：</dt>
        <dd>箇条書き(※連続して同じ項目を選択することで、1つのリストグループとして登録可能です。)</dd>
        <dt>タイトル：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ title_name }}" value="{{ title_value }}" /></span>
        </dd>
        <dt>内容：</dt>
        <dd>
            <span class="input-usually"><textarea cols="100" rows="7" class="textarea" name="{{ content_name }}" id="{{ key }}" >{{ content_value }}</textarea></span>
        </dd>
    </dl>
</script>

<?php //枠付き箇条書き ?>
<script type="x-template" id="select-text-component-text5">
    <dl>
        <dt>フォーマット：</dt>
        <dd>枠付き箇条書き</dd>
        <dt>タイトル：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ title_name }}" value="{{ title_value }}" /></span>
        </dd>
        <dt>内容：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><textarea cols="100" rows="7" class="textarea" name="{{ content_name }}" id="{{ key }}" >{{ content_value }}</textarea></span>
            <p class="ex">※改行区切りで入力してください。</p>
        </dd>
    </dl>
</script>

<?php //塗りつぶし箇条書き ?>
<script type="x-template" id="select-text-component-text6">
    <dl>
        <dt>フォーマット：</dt>
        <dd>塗りつぶし番号付き箇条書き</dd>
        <dt>タイトル：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ title_name }}" value="{{ title_value }}" /></span>
        </dd>
        <dt>内容：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><textarea cols="100" rows="7" class="textarea" name="{{ content_name }}" id="{{ key }}" >{{ content_value }}</textarea></span>
            <p class="ex">※改行区切りで入力してください。</p>
        </dd>
    </dl>
</script>

<?php //リンクテキスト ?>
<script type="x-template" id="select-text-component-text7">
    <dl>
        <dt>フォーマット：</dt>
        <dd>リンクテキスト</dd>
        <dt>リンクタイトル：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ title_name }}" value="{{ title_value }}" /></span>
        </dd>
        <dt>リンク先：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ link_name }}" value="{{ link_value }}" /></span>
        </dd>
    </dl>
</script>

<?php //注釈 ?>
<script type="x-template" id="select-text-component-text8">
    <dl>
        <dt>フォーマット：</dt>
        <dd>注釈</dd>
        <dt>内容：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><textarea cols="100" rows="7" class="textarea" name="{{ name }}" id="{{ key }}" >{{ value }}</textarea></span>
            <div class="reset" v-on:click="clear">
                <p class="btn-type03"><strong><a href="#" class="pie" onclick="return false">詳細クリア</a></strong></p>
            </div>
        </dd>
    </dl>
</script>

<?php //Youtube動画 ?>
<script type="x-template" id="select-text-component-text9">
    <dl>
        <dt>フォーマット：</dt>
        <dd>Youtube動画(※連続して同じ項目を選択することで、1つのリストグループとして登録可能です。)</dd>
        <dt>動画URL：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ url_name }}" value="{{ url_value }}" /></span>
        </dd>
        <dt>動画サムネイルURL：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ thumbnail_name }}" value="{{ thumbnail_value }}" /></span>
        </dd>
        <dt>動画タイトル：</dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ title_name }}" value="{{ title_value }}" /></span>
        </dd>
        <dt>キャプション：</dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ caption_name }}" value="{{ caption_value }}" /></span>
        </dd>
    </dl>
</script>




<?php //引用 ?>
<script type="x-template" id="select-text-component-text12">
    <dl>
        <dt>フォーマット：</dt>
        <dd>引用</dd>
        <dt>内容：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><textarea cols="100" rows="7" class="textarea" name="{{ name }}" id="{{ key }}" >{{ value }}</textarea></span>
        </dd>
    </dl>
</script>
<?php //テキスト ?>
<script type="x-template" id="select-text-component-text16">
    <dl>
        <dt>フォーマット：</dt>
        <dd>塗りつぶしテキスト</dd>
        <dt>内容：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><textarea cols="100" rows="7" class="textarea" name="{{ name }}" id="{{ key }}" >{{ value }}</textarea></span>
            <div class="reset" v-on:click="clear">
                <p class="btn-type03"><strong><a href="#" class="pie" onclick="return false">詳細クリア</a></strong></p>
            </div>
        </dd>
    </dl>
</script>


<?php //目次 ?>
<script type="x-template" id="select-text-component-text13">
    <dl>
        <dt>フォーマット：</dt>
        <dd>目次</dd>
        <dt>目次タイトル：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ title_name }}" value="{{ title_value }}" /></span>
        </dd>
        <dt>セクション番号：</dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ section_name }}" value="{{ section_value }}" /></span>
        </dd>
    </dl>
</script>

<?php //対談 ?>
<script type="x-template" id="select-text-component-text14">
    <dl>
        <dt>フォーマット：</dt>
        <dd>対談</dd>
        <dt>対談者名<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ title_name }}" value="{{ title_value }}" /></span>
        </dd>
        <dt>内容：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><textarea cols="100" rows="7" class="textarea" name="{{ content_name }}" >{{ content_value }}</textarea></span>
        </dd>
    </dl>
</script>

<?php //リンクテキスト ?>
<script type="x-template" id="select-text-component-text15">
    <dl>
        <dt>フォーマット：</dt>
        <dd>リンクテキスト</dd>
        <dt>内容：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><textarea cols="100" rows="7" class="textarea" name="{{ content_name }}" >{{ content_value }}</textarea></span>
        </dd>
        <dt>リンク先タイトル：</dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ title_name }}" value="{{ title_value }}" /></span>
        </dd>
        <dt>リンク先：</dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ link_name }}" value="{{ link_value }}" /></span>
        </dd>
    </dl>
</script>

<?php //囲み帯付き枠 ?>
<script type="x-template" id="select-text-component-text16">
    <dl>
        <dt>フォーマット：</dt>
        <dd>囲み帯付き枠</dd>
        <dt>見出し:<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ title_name }}" value="{{ title_value }}" /></span>
        </dd>
        <dt>内容：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><textarea cols="100" rows="7" class="textarea" name="{{ content_name }}" id="{{ key }}" >{{ content_value }}</textarea></span>
        </dd>
        <dt>画像：<?php //画像選択フィールド ?></dt>
        <dd>
            <div class="btn img_area">
                <p class="btn-type01 pie img_btn"><input type="button" value="画像選択" id="img_select_{{ key }}" /></p>
                <p class="btn-type02 pie img_delete_btn"><input type="button" value="削除" id="img_delete_{{ key }}" /></p>
            </div>
            <input type="hidden" name="{{ img_name }}" value="{{ img_value }}" class="img_hidden_value" id="img_hidden_{{ key }}" />
            <p id="img_prev_{{ key }}" class="img_preview">{{{ preview_img }}}</p>
            <p class="ex recommd-size"><b>※推奨画像サイズ</b>　横678 x 縦398px以上　</p>
        </dd>
    </dl>
</script>

<?php //吹き出し ?>
<script type="x-template" id="select-text-component-text17">
    <dl>
        <dt>フォーマット：</dt>
        <dd>吹き出し</dd>
        <dt>内容：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><textarea cols="100" rows="7" class="textarea" name="{{ name }}" id="{{ key }}" >{{ value }}</textarea></span>
        </dd>
    </dl>
</script>

<?php //Q&A枠 ?>
<script type="x-template" id="select-text-component-text18">
    <dl>
        <dt>フォーマット：</dt>
        <dd>Q&A枠</dd>
        <dt>質問：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><textarea cols="100" rows="7" class="textarea" name="{{ q_name }}" >{{ q_value }}</textarea></span>
        </dd>
        <dt>解答：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><textarea cols="100" rows="7" class="textarea" name="{{ a_name }}" >{{ a_value }}</textarea></span>
        </dd>
    </dl>
</script>

<?php //ソーシャルメディア埋込 ?>
<?php $sns_list= array('facebook'=>'facebook','twitter'=>'twitter','Instagram'=>'Instagram','youtube'=>'youtube');?>
<script type="x-template" id="select-text-component-text19">
    <dl>
        <dt>フォーマット：</dt>
        <dd>ソーシャルメディア埋込</dd>
        <dt>メディア：<span class="req">必須</span></dt>
        <dd>
            <select v-model="selected_item" name="{{ sns_name }}">
                <option v-for="option in options">
                    {{ option.text }}
                </option>
            </select>
        </dd>
        <dt>内容：<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><textarea cols="100" rows="7" class="textarea" name="{{ text_name }}" id="{{ key }}" >{{ text_value }}</textarea></span>
        </dd>
    </dl>
</script>


<?php
/**==========================
 * プロフィール系のパーツ
 *==========================*/
?>
<?php //プロフィール大 ?>
<script type="x-template" id="select-profile-component-profile1">
    <dl>
        <dt>フォーマット</dt>
        <dd>プロフィール大</dd>
        <dt>ブログ画像：<span class="req">必須</span><?php //画像選択フィールド ?></dt>
        <dd>
            <div class="btn img_area">
                <p class="btn-type01 pie img_btn"><input type="button" value="画像選択" id="img_select_{{ key }}" /></p>
                <p class="btn-type02 pie img_delete_btn"><input type="button" value="削除" id="img_delete_{{ key }}" /></p>
            </div>
            <input type="hidden" name="{{ img_name }}" value="{{ img_value }}" class="img_hidden_value" id="img_hidden_{{ key }}" />
            <p id="img_prev_{{ key }}" class="img_preview">{{{ img_preview }}}</p>
            <p class="ex recommd-size"><b>※推奨画像サイズ</b>　横680 x 縦500px以上　</p>
        </dd>
        <dt>プロフィール画像：<?php //画像選択フィールド ?></dt>
        <dd>
            <div class="btn img_area">
                <p class="btn-type01 pie img_btn"><input type="button" value="画像選択" id="img_select_{{ profile_key }}" /></p>
                <p class="btn-type02 pie img_delete_btn"><input type="button" value="削除" id="img_delete_{{ profile_key }}" /></p>
            </div>
            <input type="hidden" name="{{ profile_img_name }}" value="{{ profile_img_value }}" class="img_hidden_value" id="img_hidden_{{ profile_key }}" />
            <p id="img_prev_{{ profile_key }}" class="img_preview">{{{ profile_img_preview }}}</p>
            <p class="ex recommd-size"><b>※推奨画像サイズ</b>　横160 x 縦160px以上　</p>
        </dd>

        <dt>氏名:<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ simei_name }}" value="{{ simei_value }}" /></span>
        </dd>
        <dt>組織・役職</dt>
        <dd>
            <span class="input-usually">
                 <textarea cols="100" rows="7" class="textarea" name="{{ soshiki_name }}" >{{ soshiki_value }}</textarea>
            </span>
        </dd>
        <dt>紹介文</dt>
        <dd>
            <span class="input-usually"><textarea cols="100" rows="7" class="textarea" name="{{ intoro_name }}" >{{ intoro_value }}</textarea></span>
        </dd>
        <dt>ブログリンク先文言</dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ blog_link_title_name }}" value="{{ blog_link_title_value }}" /></span>
        </dd>
        <dt>ブログリンク先</dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ blog_link_name }}" value="{{ blog_link_value }}" /></span>
        </dd>
    </dl>
</script>

<?php //プロフール大 ?>
<script type="x-template" id="select-profile-component-profile2">
    <dl>
        <dt>フォーマット</dt>
        <dd>プロフィール大</dd>
        <dt>画像：<span class="req">必須</span><?php //画像選択フィールド ?></dt>
        <dd>
            <div class="btn img_area">
                <p class="btn-type01 pie img_btn"><input type="button" value="画像選択" id="img_select_{{ key }}" /></p>
                <p class="btn-type02 pie img_delete_btn"><input type="button" value="削除" id="img_delete_{{ key }}" /></p>
            </div>
            <input type="hidden" name="{{ img_name }}" value="{{ img_value }}" class="img_hidden_value" id="img_hidden_{{ key }}" />
            <p id="img_prev_{{ key }}" class="img_preview">{{{ preview_img }}}</p>
            <p class="ex recommd-size"><b>※推奨画像サイズ</b>　横120 x 縦120px以上　</p>
        </dd>
        <dt>氏名:<span class="req">必須</span></dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ simei_name }}" value="{{ simei_value }}" /></span>
        </dd>
        <dt>組織・役職:</dt>
        <dd>
            <span class="input-usually"><input class="text" type="text" name="{{ soshiki_name }}" value="{{ soshiki_value }}" /></span>
        </dd>
        <dt>紹介文</dt>
        <dd>
            <span class="input-usually"><textarea cols="100" rows="7" class="textarea" name="{{ intoro_name }}" >{{ intoro_value }}</textarea></span>
        </dd>
    </dl>
</script>

