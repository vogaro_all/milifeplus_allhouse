var lacne_component = lacne_component || {};

/**================================
 * ckeditorの初期化変数箇所
 *================================*/

var CK_toolbar = [
    ['Source','-','Undo','Redo','-','Bold','Italic','Underline','Strike','RemoveFormat','-','NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','Link','Unlink','-','Image','Syntaxhighlight','-','CreateDiv','ShowBlocks'],'/',
    ['Styles','Format','FontSize','-','TextColor']
	];
var CK_toolbar_movie = [
    ['Source','-','Undo','Redo','-','Bold','Italic','Underline','Strike','RemoveFormat','-',
        'NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','Link','Unlink','-','Image','CreatePlaceholder','-','CreateDiv','ShowBlocks'],'/',
    ['Styles','Format','FontSize','-','TextColor']
];

var CK_extraplugins = 'justify,font,colorbutton,div,showblocks,placeholder,widget,lineutils';

Setting_toolbar = CK_toolbar;
if(INSERT_MOVIE) Setting_toolbar = CK_toolbar_movie; //動画パネル
CK_EDITOR_HEIGHT = 250; //エディタの高さ指定
CK_EDITOR_WIDTH = 700; //エディタのデフォルト幅指定

//[_CHECK_] CSS STYLE選択リストに下記のCLASS STYLEを追加する（無い場合は配列を空に）
var LACNE_PARTS_CSS =  [
];

//[_CHECK_] エディタ内で適用させるCSS FILEのパス指定（配列 elrte-inner.cssは必須）
var LACNE_PAGE_CSS_FILE = [
    '<?=LACNE_SHAREDATA_PATH?>/js/ckeditor/contents.css'
    //'/css/common/import.css'
]

//スタイル追加
if(LACNE_PARTS_CSS.length > 0)
{
    CKEDITOR.stylesSet.add( 'default',LACNE_PARTS_CSS );
}

(function($){

    /**
     * WYSIWYGラッパークラス
     * @constructor
     */
    lacne_component.WYSIWYG = function(){
        this.editor = null;
        this.id     = '';
    };

    /**
     *
     * @type {string}
     */
    lacne_component.WYSIWYG.STATE = 'wysiwyg';

    /**
     *
     * @type {{vm: lacne_component.WYSIWYG.vm, replace: lacne_component.WYSIWYG.replace, clearListener: lacne_component.WYSIWYG.clearListener}}
     */
    lacne_component.WYSIWYG.prototype = {
        //インターフェース合わせる為だけに存在。特に処理をしない
        vm: function(){ return ''; },

        replace: function(id){
            this.id = id;
            this.editor = CKEDITOR.replace( id,
                {
                    language : 'ja',
                    enterMode : CKEDITOR.ENTER_BR,
                    contentsCss : LACNE_PAGE_CSS_FILE,
                    width : CK_EDITOR_WIDTH,
                    resize_maxWidth : 700,
                    height : CK_EDITOR_HEIGHT,
                    extraPlugins : CK_extraplugins,
                    toolbar : Setting_toolbar,
                    filebrowserBrowseUrl: '../media/list.php',
                    allowedContent: true,
                    filebrowserImageBrowseUrl: '../media/list.php?image=1'
                });
        }
    };

}(jQuery));
