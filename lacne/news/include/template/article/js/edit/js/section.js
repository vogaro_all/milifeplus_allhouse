// パーツ追加・変更時編集点
var lacne_component = lacne_component || {};

(function($){

    /**
     * セクションコンポーネント
     * @param id
     * @constructor
     */
    lacne_component.Section = function(item){
        this.vm     = null;
        this.item   = this.convert(item) || {};
        this.key    = '';
        var self    = this;

        //TODO m
        this.list = {
            head1: 0,
            head2: 0,
            head3: 0,

            img1: 1,
            img2: 1,
            img3: 1,
            img4: 1,
            img5: 1,
            img6: 1,

            text1: 2,
            text2: 2,
            text3: 2,
            text4: 2,
            text5: 2,
            text6: 2,
            text7: 2,
            text8: 2,
            text9: 2,

            profile1: 4,
            profile2: 4

        };

        //TODO m
        this.list2 = {
            head1: 'head1',
            head2: 'head2',
            head3: 'head3',

            img1: 'img1',
            img2: 'img2',
            img3: 'img3',
            img4: 'img4',
            img5: 'img5',
            img6: 'img6',

            text1: 'text1',
            text2: 'text2',
            text3: 'text3',
            text4: 'text4',
            text5: 'text5',
            text6: 'text6',
            text7: 'text7',
            text8: 'text8',
            text9: 'text9',

            profile1: 'profile1',
            profile2: 'profile2',
        };

        //TODO m
        this.list3 = {

            head1: 1,
            head2: 2,
            head3: 3,

            img1: 1,
            img2: 2,
            img3: 3,
            img4: 4,
            img5: 5,
            img6: 6,

            text1: 1,
            text2: 2,
            text3: 3,
            text4: 4,
            text5: 5,
            text6: 6,
            text7: 7,
            text8: 8,
            text9: 9,

            profile1: 1,
            profile2: 2

        };

        $.each(this.item, function(k, item){
            if( item != '' ){
                self.key = k;
                return;
            }
        });

    };

    lacne_component.Section.prototype = {

        convert: function(item){
            return $.extend(true, {
                /**=========================
                 * 見出し系パーツ値一覧値
                 *=========================*/
                head1: '', //大見出し
                head2: '', //中見出し
                head3: '', //小見出し

                /**=========================
                 * 画像系パーツ一覧値
                 *=========================*/
                img1: '',  //大画像
                img2: '',  //中画像
                img3: '',  //小画像
                img4: '',  //左画像
                img5: '',  //右画像
                img6: '',  //画像

                /**=========================
                 * テキスト系パーツ一覧値
                 *=========================*/
                text1: '', //テキスト
                text2: '', //囲み1
                text3: '', //囲み2
                text4: '', //囲み3
                text5: '', //囲み引用
                text6: '', //囲み帯付き枠
                text7: '', //吹き出し
                text8: '', //Q&A枠
                text9: '', //ソーシャルメディア埋め込み

                /**=========================
                 *プロフィール系パーツ一覧値
                 *=========================*/
                profile1: '', //プロフィール大
                profile2: '' //プロフィール小
                
            }, item);
        },

        active: function(index){
            var self = this;

            //オブジェクトリテラルに擬似名前空間区切りで定義できなかったので関数を挟んで作成
            var components = function(){
                var list = {};
                $.each([
                    //見出し
                    'Head1', 'Head2', 'Head3',
                    //画像
                    'Img1', 'Img2', 'Img3', 'Img4', 'Img5', 'Img6',
                    //テキスト
                    'Text1', 'Text2', 'Text3', 'Text4', 'Text5', 'Text6', 'Text7','Text8','Text9',
                    //プロフィール
                    'Profile1', 'Profile2',
                ], function(index, key){
                    try{
                        list[lacne_component[key].STATE] = (new lacne_component[key]()).vm();
                    }catch(e){
                        console.error(e);
                    }
                });


                return list;
            };
            var self = this;

            var Section = Vue.extend({
                template: $('#section-template').html(),
                data: function() {
                    return {
                        currentView: '',
                        'init_data':self.item,
                        select_position: self.list3[self.key]?self.list3[self.key]:''
                    }
                },
                //すべての状態を定義
                components: components(),
                methods: {
                    insert: function (e) {
                        var section = new lacne_component.Section();
                        var index = $('#app .insert').index( $(e.target).parent() );
                        section.active(index);
                        section.updateSectionNum();
                    },
                    delete: function(e){
                        var self = this;
                        var section = new lacne_component.Section();
                        $(this.$el).fadeOut().queue( function() {
                            self.$remove();
                            section.updateSectionNum();
                        });
                    },
                    click: function(e, current){
                        var currentView     = $(e.target).attr('data-bind');
                        $(this.$el).find('li').removeClass('active');
                        var text = ' (' + $(e.target).text() + ')';

                        if(currentView){

                            //TODO m
                            this.currentView  = currentView;
                            switch (current){
                                case 1:
                                    $(this.$el).find('.head').addClass('active');
                                    break;
                                case 2:
                                    $(this.$el).find('.img').addClass('active');
                                    break;
                                case 3:
                                    $(this.$el).find('.text').addClass('active');
                                    break;
                                case 5:
                                    $(this.$el).find('.profile').addClass('active');
                                    break;
                            }
                        }
                    }
                }
            });

            this.vm = new Section();

            var el = this.vm.$mount().$el;
             if( index >= 0){
                $('#app section:eq(' + index  + ')').after(el);
                $(el).hide().fadeIn(1000);
            }else{
                $('#app').append(el).hide().fadeIn(1000, function(){

                    var text = '';
                    $(el).find('li').removeClass('active');
                    $.each($(el).find('.format li a'), function(){
                        if(self.key == $(this).attr('data-bind')){
                            text = ' (' + $(this).text() + ')';
                        }
                    });

                    //TODO m
                    switch (self.list[self.key]){
                        case 0:
                            $(el).find('.head').addClass('active');
                            break;
                        case 1:
                            $(el).find('.img').addClass('active');
                            break;
                        case 2:
                            $(el).find('.text').addClass('active');
                            break;
                        case 4:
                            $(el).find('.profile').addClass('active');
                            break;
                    }

                    self.vm.currentView = self.list2[self.key];

                });
            }
        },

        updateSectionNum : function() {
            var num = $('.section-item-container').length;
            if(num >= 200) {
                $('.btn > .insert').hide();
            } else {
                $('.btn > .insert').show();
            }

            $('.section-item-container').each(function(i , val) {
                $(this).find('h3').text('セクション'+(i+1));
            });
        }
    };

}(jQuery));
