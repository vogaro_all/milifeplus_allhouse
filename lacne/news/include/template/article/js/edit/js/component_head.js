var lacne_component = lacne_component || {};

(function($){

    /**
     * 大見出し
     * @constructor
     */
    lacne_component.Head1 = function(){};

    lacne_component.Head1.STATE = 'head1';

    lacne_component.Head1.prototype = {
        vm: function(){
            return Vue.extend({
                created:function(){
                    var data = this.$parent.$data.init_data;
                    this.$data.value = data.head1.content;
                },
                template: $('#select-head-component-head1').html(),
                data: function(){
                    var name = lacne_component.Base.getName( lacne_component.Head1.STATE );
                    return  {
                        'name': name + '[content]',
                        value: ''
                    }
                }
            });
        }
    };

    /**
     * 中見出し
     * @constructor
     */
    lacne_component.Head2 = function(){};

    lacne_component.Head2.STATE = 'head2';

    lacne_component.Head2.prototype = {
        vm: function(){
            return Vue.extend({
                created:function(){
                    var data = this.$parent.$data.init_data;
                    this.$data.value = data.head2.content;
                },
                template: $('#select-head-component-head2').html(),
                data: function(){
                    var name = lacne_component.Base.getName( lacne_component.Head2.STATE );
                    return  {
                        'name': name + '[content]',
                        value: ''
                    }
                }
            });
        }
    };

    /**
     * 小見出し
     * @constructor
     */
    lacne_component.Head3 = function(){};

    lacne_component.Head3.STATE = 'head3';

    lacne_component.Head3.prototype = {
        vm: function(){
            return Vue.extend({
                created:function(){
                    var data = this.$parent.$data.init_data;
                    this.$data.value = data.head3.content;
                },
                template: $('#select-head-component-head3').html(),
                data: function(){
                    var name = lacne_component.Base.getName( lacne_component.Head3.STATE );
                    return  {
                        'name': name + '[content]',
                        value: ''
                    }
                }
            });
        }
    };

}(jQuery));
