var lacne_component = lacne_component || {};

(function($){

    function get_img(path){

        if(path){
            return '<img src="' + path + '" width="100px" />';
        }

        return '';
    }

    /**
     * 大画像（1カラム）
     * @constructor
     */
    lacne_component.Img1 = function(){};

    lacne_component.Img1.STATE = 'img1';

    lacne_component.Img1.prototype = {
        vm: function(){
            return Vue.extend({
                created: function(){
                    var index = $('input').length + 1;
                    this.$data.key = 'section_img_img' + index;
                    
                    var data = this.$parent.$data.init_data.img1;
                    this.$data.img_value = data.img;
                    this.$data.preview_img = get_img(this.$data.img_value);
                    this.$data.caption_value   = data.caption_name;
                },
                template: $('#select-img-component-img1').html(),
                data: function(){
                    var name = lacne_component.Base.getName( lacne_component.Img1.STATE);
                    return  {
                        img_name:   name + '[img]',
                        img_value: '',
                        preview_img: '',
                        key: '',
                        
                        caption_name: name + '[caption_name]',
                        caption_value: '',
                    }
                }
            });
        }
    };

    /**
     * 中画像（2カラム）
     * @constructor
     */

    lacne_component.Img2 = function(){};

    lacne_component.Img2.STATE = 'img2';

    lacne_component.Img2.prototype = {
            vm: function(){
                var self = this;
                return Vue.extend({
                    created: function(){
                        var index = $('input').length + 1;
                        this.$data.key = 'section_img_img' + index;
                        this.$data.key2 = 'section_img_img2' + index;

                        var data                 = this.$parent.$data.init_data.img2;
                        this.$data.img_value     = data.img;
                        this.$data.preview_img   = get_img(this.$data.img_value);
                        this.$data.caption_value   = data.caption_name;
                        
                        this.$data.img2_value     = data.img2;
                        this.$data.preview2_img   = get_img(this.$data.img2_value);
                        this.$data.caption2_value   = data.caption2_name;
                        
                    },
                    template: $('#select-img-component-img2').html(),
                    data: function(){
                        var name = lacne_component.Base.getName( lacne_component.Img2.STATE );

                        return  {

                            img_name: name + '[img]',
                            img_value: '',
                            preview_img: '',
                            key: '',
                            
                            caption_name: name + '[caption_name]',
                            caption_value: '',
                            
                            img2_name: name + '[img2]',
                            img2_value: '',
                            preview2_img: '',
                            key2: '',
                            
                            caption2_name: name + '[caption2_name]',
                            caption2_value: '',
                        }
                    },
                    methods: {
                        clear: function (e) {
                            if(window.confirm('選択された記事詳細の内容をクリアします。よろしいですか？')){
                                self.editor.editor.setData('');
                            }
                            return false;
                        }
                    }
                });
            }
        };
    
    /**
     * 小画像（3カラム）
     * @constructor
     */

    lacne_component.Img3 = function(){};

    lacne_component.Img3.STATE = 'img3';

    lacne_component.Img3.prototype = {
            vm: function(){
                var self = this;
                return Vue.extend({
                    created: function(){
                        var index = $('input').length + 1;
                        this.$data.key = 'section_img_img' + index;
                        this.$data.key2 = 'section_img_img2' + index;
                        this.$data.key3 = 'section_img_img3' + index;

                        var data                 = this.$parent.$data.init_data.img3;
                        this.$data.img_value     = data.img;
                        this.$data.preview_img   = get_img(this.$data.img_value);
                        
                        this.$data.caption_value   = data.caption_name;
                        this.$data.url_value      = data.url_name;
                        this.$data.text_value      = data.text_name;
                        
                        this.$data.img2_value     = data.img2;
                        this.$data.preview2_img   = get_img(this.$data.img2_value);
                        this.$data.caption2_value   = data.caption2_name;
                        
                        this.$data.img3_value     = data.img3;
                        this.$data.preview3_img   = get_img(this.$data.img3_value);
                        this.$data.caption3_value   = data.caption3_name;
                        
                        
                    },
                    template: $('#select-img-component-img3').html(),
                    data: function(){
                        var name = lacne_component.Base.getName( lacne_component.Img3.STATE );

                        return  {

                            img_name: name + '[img]',
                            img_value: '',
                            preview_img: '',
                            key: '',
                            
                            caption_name: name + '[caption_name]',
                            caption_value: '',
                           


                            img2_name: name + '[img2]',
                            img2_value: '',
                            preview2_img: '',
                            key2: '',
                            
                            caption2_name: name + '[caption2_name]',
                            caption2_value: '',
                            
                            
                            img3_name: name + '[img3]',
                            img3_value: '',
                            preview3_img: '',
                            key3: '',
                            
                            caption3_name: name + '[caption3_name]',
                            caption3_value: '',
                            
                        }
                    },
                    methods: {
                        clear: function (e) {
                            if(window.confirm('選択された記事詳細の内容をクリアします。よろしいですか？')){
                                self.editor.editor.setData('');
                            }
                            return false;
                        }
                    }
                });
            }
        };

    /**
     * 左画像
     * @constructor
     */

    lacne_component.Img4 = function(){
        this.editor = new lacne_component.WYSIWYG();
    };
    
    lacne_component.Img4.STATE = 'img4';

    lacne_component.Img4.prototype = {
            vm: function(){
                var self = this;
                return Vue.extend({
                    created: function(){
                        var index = $('input').length + 1;
                        this.$data.key = 'section_img_img' + index;

                        var data                 = this.$parent.$data.init_data.img4;
                        this.$data.img_value     = data.img;
                        this.$data.preview_img   = get_img(this.$data.img_value);
                        
                        this.$data.title_value      = data.title_name;
                        this.$data.value = data.content;
                    },
                    attached: function(){
                        self.editor.replace(this.$data.key);
                    },
                    template: $('#select-img-component-img4').html(),
                    data: function(){
                        var name = lacne_component.Base.getName( lacne_component.Img4.STATE );

                        return  {

                            img_name: name + '[img]',
                            img_value: '',
                            preview_img: '',
                            key: '',
                            
                            title_name: name + '[title_name]',
                            title_value: '',
                            
                            'name': name + '[content]',
                            value: '',
                            key: '',
                        }
                    },
                    methods: {
                        clear: function (e) {
                            if(window.confirm('選択された記事詳細の内容をクリアします。よろしいですか？')){
                                self.editor.editor.setData('');
                            }
                            return false;
                        }
                    }
                });
            }
        };
    
    /**
     * 右画像
     * @constructor
     */

    lacne_component.Img5 = function(){
        this.editor = new lacne_component.WYSIWYG();
    };
    
    lacne_component.Img5.STATE = 'img5';

    lacne_component.Img5.prototype = {
            vm: function(){
                var self = this;
                return Vue.extend({
                    created: function(){
                        var index = $('input').length + 1;
                        this.$data.key = 'section_img_img' + index;

                        var data                 = this.$parent.$data.init_data.img5;
                        this.$data.img_value     = data.img;
                        this.$data.preview_img   = get_img(this.$data.img_value);
                        
                        this.$data.title_value      = data.title_name;
                        this.$data.value = data.content;
                    },
                    attached: function(){
                        self.editor.replace(this.$data.key);
                    },
                    template: $('#select-img-component-img5').html(),
                    data: function(){
                        var name = lacne_component.Base.getName( lacne_component.Img5.STATE );

                        return  {

                            img_name: name + '[img]',
                            img_value: '',
                            preview_img: '',
                            key: '',
                            
                            title_name: name + '[title_name]',
                            title_value: '',
                            'name': name + '[content]',
                            value: '',
                            key: '',
                        }
                    },
                    methods: {
                        clear: function (e) {
                            if(window.confirm('選択された記事詳細の内容をクリアします。よろしいですか？')){
                                self.editor.editor.setData('');
                            }
                            return false;
                        }
                    }
                });
            }
        };
    
    /**
     * 大画像（1カラム）
     * @constructor
     */
    lacne_component.Img6 = function(){};

    lacne_component.Img6.STATE = 'img6';

    lacne_component.Img6.prototype = {
        vm: function(){
            return Vue.extend({
                created: function(){
                    var index = $('input').length + 1;
                    this.$data.key = 'section_img_img' + index;
                    var data = this.$parent.$data.init_data.img6;
                    this.$data.img_value = data.img;
                    this.$data.preview_img = get_img(this.$data.img_value);
                },
                template: $('#select-img-component-img6').html(),
                data: function(){
                    var name = lacne_component.Base.getName( lacne_component.Img6.STATE);
                    return  {
                        img_name:   name + '[img]',
                        img_value: '',
                        preview_img: '',
                        key: '',
                    }
                }
            });
        }
    };
}(jQuery));
