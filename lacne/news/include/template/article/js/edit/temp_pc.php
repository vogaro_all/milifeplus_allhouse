<script type="text/javascript">
var section_items = <?php echo(json_encode($__data_list['section']? $__data_list['section']: array())); ?>;


<?php
if($device == "pc") :
// -----------------------------------------------
// PCの場合のみロード
// -----------------------------------------------
?>
$(function(){

    //timepicker
    $('#datetime').datetimepicker({
        dateFormat: 'yy-mm-dd',
        timeFormat: 'hh:mm',
        firstDay: 1,
        isRTL: false,
        monthNames: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
        showMonthAfterYear: false,
        yearSuffix: '',
        timeText:'時刻指定',
        hourText:'時',
        minuteText:'分',
        currentText:'現時刻',
        closeText:'確定'
    });
});
<?php
endif;
?>

// -----------------------------------------------
// 以下は共通ロード
// -----------------------------------------------
$(function(){

    <?php
        //wysiwygの設定値移動しています。
        require_once(dirname(__FILE__) . '/js/component_wysiwyg.js');
        if($device == "pc") :
        // -----------------------------------------------
        // PCの場合のみロード
        // -----------------------------------------------
    ?>
            Setting_toolbar = CK_toolbar;
            if(INSERT_MOVIE) Setting_toolbar = CK_toolbar_movie; //動画パネル
            CK_EDITOR_HEIGHT = 250; //エディタの高さ指定
            CK_EDITOR_WIDTH = 700; //エディタのデフォルト幅指定
    <?php
    endif;
    ?>


    if($("#edit-p").size()>0)
    {
        // create editor
        var editor = CKEDITOR.replace( 'edit-p',
        {
            language : 'ja',
            enterMode : CKEDITOR.ENTER_BR,
            contentsCss : LACNE_PAGE_CSS_FILE,
            width : CK_EDITOR_WIDTH,
            resize_maxWidth : 700,
            height : CK_EDITOR_HEIGHT,
            extraPlugins : CK_extraplugins,
            toolbar : Setting_toolbar,
            filebrowserBrowseUrl: '../media/list.php',
            filebrowserImageBrowseUrl: '../media/list.php?image=1'
        });
    }

    //click preview button
//    var preview_url = '<?=$LACNE->url_setting->get_pageurl($data_list["id"],$data_list["category"],"", $data_list["original_filename"])?>';
//    var preview_url = '/preview/<?php echo($data_list["id"]); ?>';
    var preview_url = '/news/detail/?id=<?php echo($data_list["id"]); ?>';

    var submit_url = '<?=LACNE_APP_ADMIN_PATH?>/article/edit.php?action=submit&<?=fn_set_urlparam($_GET , array('id') , false)?>';

    $("#preview").css("cursor","pointer").click(function(){

        var $form_preview_area = $("form #preview_area");
        $form_preview_area.empty();

        $('[name="tags[]"] option:selected').each(function(i){
            var self = this;
            if($(self).val() != ""){
                $form_preview_area
                .append(function(){
                    return $("<input>").attr("type", "hidden").attr("name", "p_tags["+i+"][id]").attr("value", $(self).val());
                }).append(function(){
                    return $("<input>").attr("type", "hidden").attr("name", "p_tags["+i+"][tag_name]").attr("value", $(self).text());
                });
            }
        });

        $('[name="ages[]"]:checked').each(function(i){
            var self = this;
            if($(self).val() != ""){
                $form_preview_area
                .append(function(){
                    return $("<input>").attr("type", "hidden").attr("name", "p_ages["+i+"][id]").attr("value", $(self).val());
                }).append(function(){
                    return $("<input>").attr("type", "hidden").attr("name", "p_ages["+i+"][age_name]").attr("value", $(self).attr("id"));
                });
            }
        });

        $("form").attr({"action":preview_url , 'target':'_blank'}).submit();
//         $("form").attr({"action":submit_url, 'target':'_self'});
    });

    //click regstered button
    $("#submit").on('click' , function(){
        $(this).off('click');
        $("form").attr({"action":submit_url, 'target':'_self'}).submit();
    });

    //click cancel button
    $("#cancel").click(function(){
        window.location.href = "<?=LACNE_APP_ADMIN_PATH?>/article/index.php";
    });

    //詳細クリア
    $("#clear").click(function(){
         $.library.modalOpen($("<a>").attr("href","#box-clear"),"#Modal");
         return false;
    })
    $("#delete_link").click(function(){
        editor.setData( '' );
        $.library.modalClose();
        return false;
    });

    //　おすすめ記事で選択された記事が広告連動用であれば初期値でアラートを表示
    $(".recommend_post").each(function() {
        toggleShowAlert($(this));
        var $selector = $('.recommend_title').get( $(this).data('num') );
        $($selector).val( $(this).find('option:selected').text() );
    });


    //Androidの場合はエディタが使えないため、ソース入力に切り替える
    //管理画面がスマフォ対応している場合のみ有効
    <?php
    if(method_exists($LACNE->library["admin_view"] , 'is_Android') && $LACNE->library["admin_view"]->is_Android()) :
    ?>
    if($('#edit-p').size() > 0)
    {
        //editor.setMode( 'source' ); //Andoridの場合、ckeditor側が自動で切り替えをしてくれるため、ここはコメントアウトする
    }
    <?php
    endif;
    ?>

    //message
    <?php
    if((isset($err) && $err) || (isset($message) && $message)) {
    ?>
        $.library.do_slideDown_message("#comp_message");
    <?php
    }
    ?>

    // パーツ追加・変更時編集点
    /**========================
     * セクション系のjsを読み込み
     *========================*/
    <?php require_once(dirname(__FILE__).'/js/component_base.js');?>
    <?php require_once(dirname(__FILE__).'/js/component_head.js');?>
    <?php require_once(dirname(__FILE__).'/js/component_img.js');?>
    <?php require_once(dirname(__FILE__).'/js/component_text.js');?>
    <?php require_once(dirname(__FILE__).'/js/component_profile.js');?>
    <?php require_once(dirname(__FILE__).'/js/section.js');?>
    <?php require_once(dirname(__FILE__).'/js/main.js');?>

    //タグ選択エリアの可視化
    $('.tag_list .delete').css({'visibility': 'visible'});
    $('.tag_list .delete:first').css({'visibility': 'hidden'});








    // まずは対象のセレクトボックスの内容一覧を取得する
    var dataList = {};
    selectObject = $('#tag_default').children();
    dataList[0] = '--';
    for(i = 0; i < selectObject.length; i++) {
        targetObject = selectObject.eq(i);
        dataList[targetObject.val()] = targetObject.text();
    }

    console.log(dataList);

 // テキストエリアが変更された時の動きを登録
  $(document).on('change', '.targetText', function(){
    var index = $('.targetText').index(this);

   console.log($(this).val());

   targetString = $(this).val();

   //optionを一旦削除
   $('.targetOptions').eq(index).children('option').remove();
   // 絞込開始
   if(targetString == '') {
       // 絞込文字列が空の時は全部表示する。
//        $('.targetOptions > option').remove();

       $('.targetOptions').eq(index).append($('<option>').html('--').val(''));
       for ( var key in dataList ) {
           text = dataList[key];
           $('.targetOptions').eq(index).append($('<option>').html(text).val(key));
       }
   } else {
       // 絞込文字列が設定されているときは部分一致するもののみを表示する
//        $('.targetOptions > option').remove();

       $('.targetOptions').eq(index).append($('<option>').html('--').val(''));
       for ( var key in dataList ) {
           text = dataList[key];
           if(text.indexOf(targetString) != -1) {
               $('.targetOptions').eq(index).append($('<option>').html(text).val(key));
           }
       }
   }
  });
});

//タグ選択エリアの追加
$(document).on('click', 'span#tag-add-button', function(){
    var $clone = $('.tag_list li:first').clone();
    $clone.find('.targetText').val('');

    //最大10件までに設定
    var fixed = 10;
    if($('.tag_list li').length != fixed) {
        $clone.find('.delete').css({'visibility': 'visible'});
        $('.tag_list').append($clone);
    }

});

//タグ選択エリア削除
$(document).on('click', '.tag_list .delete', function(e){
    $(this).parent().remove();
});


// -----------------------------------------------

//おすすめ記事　カテゴリ
$(document).on('change', '.recommend_select_category', function(e){
    var num = $(this).data("num");
    var tid = $(this).val();

    $.ajax({
        type:'GET',
        url : '<?=LACNE_APP_ADMIN_PATH?>/article/ajax.php',
        data:'getdata=tag&recmmd_cate='+tid,
        success : function(result)
        {
            $('.recommend_select_tag[data-num="'+ num + '"] > option').remove();
            $('.recommend_select_tag[data-num="'+ num + '"]').append('<option value="">選択してください</option>');
            $('.recommend_select_tag[data-num="'+ num + '"]').append(result);
        }
    });

    $.ajax({
        type:'GET',
        url : '<?=LACNE_APP_ADMIN_PATH?>/article/ajax.php',
        data:'getdata=article&recmmd_cate='+tid,
        success : function(result)
        {
            $('[name="recommend_post['+ num + ']"] > option').remove();
            $('[name="recommend_post['+ num + ']"]').append('<option value="">選択してください</option>');
            $('[name="recommend_post['+ num + ']"]').append(result);
        }
    });

});

//おすすめ記事 タグ
$(document).on('change', '.recommend_select_tag', function(e){
    var num = $(this).data("num");
    var tag_id = $(this).val();
    var cate_id = $('[name="recommend_category['+ num + ']"]').val();

    $.ajax({
        type:'GET',
        url : '<?=LACNE_APP_ADMIN_PATH?>/article/ajax.php',
        data:'recmmd_tag='+tag_id+'&recmmd_cate='+cate_id,
        success : function(result)
        {
            $('[name="recommend_post['+ num + ']"] > option').remove();
            $('[name="recommend_post['+ num + ']"]').append('<option value="">選択してください</option>');
            $('[name="recommend_post['+ num + ']"]').append(result);
        }
    });

});

//おすすめ記事 記事
$(document).on('change', '.recommend_post', function(e){
    toggleShowAlert($(this));

    // hidden値に選択された記事のタイトルをいれる
    var $selector = $('.recommend_title').get( $(this).data('num') );
    $($selector).val( $(this).find('option:selected').text() );
});

//選択された記事が広告連動用ならアラートを表示
function toggleShowAlert($self)
{
    var num = $self.data("num");
    var post_id = $self.find('option:selected').val();

    //広告連動用メッセージ要素があれば削除する
    if($self.next(".is_advertise_alert").length)
    {
        $self.next(".is_advertise_alert").remove();
    }

    $.ajax({
        type:'GET',
        url : '<?=LACNE_APP_ADMIN_PATH?>/article/ajax.php',
        dataType: 'json',
        data:'recmmd_post='+post_id,
    }).done(function(result) {
        if(result)
        {
            if(result.output_flag == 0)
            {
                $('[name="recommend_post['+ num + ']"]').after("<p class='is_advertise_alert'>※非公開の記事のため、おすすめエリアには表示されません。</p>");
            }
        }
    }).fail(function(data) {
    });



}


</script>
