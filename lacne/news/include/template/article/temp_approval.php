<?php

    $page_setting = array(
        "title" => "承認確認",
        "js" => array(),
        "css" => array(LACNE_SHAREDATA_PATH."/css/common/global_iframe.css" , LACNE_SHAREDATA_PATH."/css/article/confirm.css")
    );

    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");
    
?>
<script type="text/javascript">
$(document).ready(function(){
    //Navigation
    $.library.sideActive('<?=LACNE_APP_ADMIN_NAVI_ID?> .news');
});
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "approval");
?>


<section class="section">
<?php if(!isset($err) || !$err) : ?>
<h1 class="head-line01 smp">承認・差戻し処理</h1>
<div class="alert memo pie smp_alert"><span class="icon">情報</span><p class="fl">この<?=KEYWORD_KIJI?>を承認・公開、または、差戻しを行うことができます。<br />差戻しの場合、差戻し理由を記載して送信することができます。</p></div>
<?php else: ?>
<h1 class="head-line01 smp"><?=$err?></h1>
<div class="alert error pie pc"><span class="icon">情報</span><p class="fl"><?=$err?></p></div>
<?php endif; ?>
<form action="<?=$submit_link?>" method="post">
<div class="input">
<p><textarea name="message" id="app_message"><?=(isset($message))?$message:""?></textarea></p>
<!-- .input // --></div>
<div class="btn">
<p class="btn-type01 pie"><input type="submit" name="app" value="承認" class="pie" /></p>
<p class="btn-type02 pie"><input type="submit" name="back" value="差戻し" class="pie" /></p>
<!-- .btn // --></div>
<input type="hidden" name="token" value="<?=$csrf_token?>" />
</form>
<!-- .section // --></section>



<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>