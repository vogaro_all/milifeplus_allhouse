<?php

    $page_setting = array(
        "title" => "公開確認",
        "js" => array(),
        "css" => array(LACNE_SHAREDATA_PATH."/css/common/global_iframe.css" , LACNE_SHAREDATA_PATH."/css/article/confirm.css")
    );

    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");
    
?>
<script type="text/javascript">
$(document).ready(function(){
    //Mavigation
    $.library.sideActive('<?=LACNE_APP_ADMIN_NAVI_ID?> .news');
});
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "confirm" , array("cancel_page"=>$_cancel_page));
?>


<?php
$publish_str = "公開";
if($data["output_flag"] == 1)
{
    $publish_str = "非公開";
}
?>
<section class="section">
<div class="section-inside">
<div class="alert comp pie"><span class="icon">完了</span><p class="fl">「<?=$data["title"]?>」を<?=$publish_str?>にしますか？</p></div>
<form action="<?=$publish_link?>" method="post">
<div class="btn">
<p class="btn-type01 pie"><input type="submit" name="submit" value="<?=$publish_str?>" /></p>
<p class="btn-type02 pie"><a href="<?=$cancel_page?>" id="btn_cancel"><span class="pie">キャンセル</span></a></p>
<!-- .btn // --></div>
<input type="hidden" name="token" value="<?=$csrf_token?>" />
</form>
<!-- .section-inside // --></div>
<!-- .section // --></section>


<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>