<?php

    $page_setting = array(
        "title" => "ソーシャル連携",
        "js" => array(
            LACNE_SHAREDATA_PATH."/js/prettyLoader/js/jquery.prettyLoader.js",
            LACNE_SHAREDATA_PATH."/js/medialist.js",
        ),
        "css" => array(LACNE_SHAREDATA_PATH."/css/common/global_iframe.css" , LACNE_SHAREDATA_PATH."/css/article/confirm.css?20160325", LACNE_SHAREDATA_PATH."/js/prettyLoader/css/prettyLoader.css")
    );

    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");

?>
<script type="text/javascript">
$(document).ready(function(){
    //Navigation
    $.library.sideActive('<?=LACNE_APP_ADMIN_NAVI_ID?> .news');
});
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "social");
?>


<section class="section social">
<?php if(!isset($err) || !$err) : ?>
<h1 class="head-line01 smp">この<?=KEYWORD_KIJI?>をソーシャルメディアに投稿しますか？</h1>
<div class="alert memo pie pc"><span class="icon">情報</span><p class="fl">この<?=KEYWORD_KIJI?>をソーシャルメディアに投稿しますか？</p></div>
<?php else: ?>
<h1 class="head-line01 smp"><?=$err?></h1>
<div class="alert error pie pc"><span class="icon">情報</span><p class="fl"><?=$err?></p></div>
<?php endif; ?>
<form action="<?=$submit_link?>" method="post">
<div class="input">
<input type="file" name="file" />
<p id="img_prev" class="img_preview">
    <img src="<?php echo($ogp_image); ?>" width="100px" />
    <input type="hidden" name="ogp_image" value="<?php echo($ogp_image); ?>" />
</p>
<p><textarea name="description"><?=(isset($description))?$description:""?></textarea></p>
<!-- .input // --></div>
<div class="btn">
<p class="btn-type01 pie"><input type="submit" id="btn_sns_submit" name="send" value="投稿する" class="pie" /></p>
<p class="btn-type02 pie"><a href="<?=$complete_page?>"><span class="pie">投稿しない</span></a></p>
<!-- .btn // --></div>
<input type="hidden" name="token" value="<?=$csrf_token?>" />
</form>
<!-- .section // --></section>

<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>

<script>
    $(function(){
        $('input[name="file"]').change(function(evt) {


            jQuery.each(evt.target.files, function() {
                // サムネイル画像生成
                var reader = new FileReader();
                reader.readAsDataURL($(this)[0]);
                reader.onloadend=function(){
                    $('#img_prev img').attr('src', this.result);
                    $('#img_prev input[type=hidden]').attr('value', this.result);
                };
            });
        });
    });
</script>
