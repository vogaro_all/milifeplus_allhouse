<?php

    $page_setting = array(
        "title" => "メディアアップロード",
        "js" => array(
            LACNE_SHAREDATA_PATH."/js/jquery.powertip/jquery.powertip-1.1.0.min.js",
            LACNE_SHAREDATA_PATH."/js/fancybox/jquery.fancybox-1.3.4.js",
            LACNE_SHAREDATA_PATH."/js/mediadrop.js"
        ),
        "css" => array(
            LACNE_SHAREDATA_PATH."/css/media/index.css",
            LACNE_SHAREDATA_PATH."/css/media/drop_upload.css",
            LACNE_SHAREDATA_PATH."/css/common/pager.css",
            LACNE_SHAREDATA_PATH."/js/jquery.powertip/jquery.powertip.css",
            LACNE_SHAREDATA_PATH."/js/fancybox/jquery.fancybox-1.3.4.css",
        )
    );

    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");
    
?>
<script type="text/javascript">
$(document).ready(function(){
    //Navigation
    $.library.sideActive('<?=LACNE_APP_ADMIN_NAVI_ID?> .media');
    $(".modalinfo").click(function() {
        $.fancybox({
            'title': $('span',this).text(),
            'href' : this.href,
            'transitionIn'  : 'elastic',
            'transitionOut' : 'elastic'
        });
        return false;
    });
    
    $('.pager a').each(function(){
        var href = $(this).attr("href")+"#mediaList";
        $(this).attr("href",href);
    });
});
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "index" , array("err"=>$err , "message"=>$message , "message_delete"=>$message_delete));
?>


<section class="section">
<h1 class="head-line01"><a href="<?=LACNE_APP_INDEXPAGE_URL?>" class="btn-back smp">戻る</a>メディアアップロード</h1>
<p class="load"><?=KEYWORD_KIJI?>内に掲載する画像や動画ファイルのアップロード、削除を行います。</p>
<!-- .section // --></section>

<?php
//--------------------------------------------------------
//アップロード画面部分をインクルード
//--------------------------------------------------------
include_once(dirname(__FILE__)."/temp_common_upload.php");
?>

<section class="section" id="mediaList">
<h2 class="head-line02">メディアファイル一覧</h2>
<?php
if(!empty($media_data) || (empty($media_data) && !empty($search_tag))) :
?>
<p class="section-inside">※ファイルを削除するには、削除対象のチェックボックスにチェックを入れて削除ボタンを押して下さい。</p>

<?php 
//削除メッセージ
if(isset($message_delete) && $message_delete) : 
?>
<div class="alert comp pie" id="comp_message_delete" style="display:none;margin:5px 0 0 0"><span class="icon">完了</span><p class="fl"><?=$message_delete?></p></div>
<?php endif; ?>

<div>
<form action="index.php#mediaList" method="get" id="tagSearchForm">
<div class="mt10">
<label class="labelstr">タグ検索：</label><input type="text" name="search_tag" value="<?=!empty($search_tag)?$search_tag:''?>" maxlength="100" size="15" /><input type="submit" class="btn-tagsearch" value="検索" />
<?php if(!empty($_GET["search_tag"])) : ?>
<input type="button" class="btn-tagsearch" id="btnAllView" value="すべて表示" />
<?php endif; ?>
<?php if(!empty($taglist_data)) : ?>
<p class="taglist">
<?php foreach($taglist_data as $tag) : ?>
<span><a href="#" class="tag"><?=$tag["name"]?></a> (<?=$tag["cnt"]?>)</span>
<?php endforeach; ?>
</p>
<?php endif; ?>
</div>
</form>
</div>

<form name="del_form" action="<?=LACNE_APP_ADMIN_PATH?>/media/index.php<?=fn_set_urlparam($_GET , array('page','search_tag'))?>" method="post">
<ul class="list-media">
<?php
    if(is_array($media_data)) :
        foreach($media_data as $media) :
?>
<li>
<p class="img"><?=$LACNE->library["media"]->set_thumbnail($media , $LACNE->library["media"]->get_thumb_height(), $media["tag"] , !empty($media["tag"])?"tip":"" , true)?></p>
<span>
<?php if($media["type"] == "pdf") : ?>
<a href="<?php echo $media["filepath"];?>" target="_blank">
<?php endif; ?>
<?=$media["name"]?>
<?php if($media["type"] == "pdf") : ?>
</a>
<?php endif; ?>
</span>
<p><input type="checkbox" name="del_pict[]" class="delete_check" id="mediafile_<?=$media["id"]?>" value="<?=$media["name"]?>" /> <label for="mediafile_<?=$media["id"]?>">削除</label></p>
</li>
<?php
        endforeach;
    endif;
?>
</ul>
<input type="hidden" name="token" value="<?=$csrf_token?>" />
<input type="hidden" name="del" value="1" />
</form>

<div class="pager-foot">

<?php
//権限チェック
if($LACNE->library["login"]->chk_controll_limit("delete_files")) :
?>
<p class="btn-type03 pie"><strong><a href="javascript:void(0)" class="pie" id="btn-delete"><span>ファイルを削除</span></a></strong></p>
<?php
else:
?>
<p class="note">ファイルを削除することができません</p>
<?php
endif;
?>
<div class="pager">
<?=fn_pagenavi($page , $page_num ,"search_tag=".$search_tag)?>
<!-- .pager // --></div>
<!-- .pager-info // --></div>

<?php
else:
?>
<p class="section-inside">アップロードされたファイルはまだありません。</p>
<?php
endif;
?>
<!-- .section // --></section>


<section id="Modal" class="section">
<?php
//--------------------------------------------------------
//モーダル画面用
//--------------------------------------------------------
?>
<?=
//削除（個別削除）
//------------------------------
$LACNE->library["admin_view"]->html_modal_open("box-delete");
?>
<div class="alert note pie"><span class="icon">注意</span><p class="fl">選択したファイルを削除してもよろしいですか？</p></div>
<div class="btn">
<p class="btn-type01 pie"><a href="#" id="run-delete"><span class="pie">削除</span></a></p>
<p class="btn-type02 pie"><a href="#" class="modal-close"><span class="pie">キャンセル</span></a></p>
<!-- .btn // --></div>
<?=$LACNE->library["admin_view"]->html_modal_close();?>

<?=
//エラー表示用
//------------------------------
$LACNE->library["admin_view"]->html_modal_open("box-error");
?>
<div class="alert error pie"><span class="icon">注意</span><p class="fl" id="error_message"></p></div>
<div class="btn btn-one">
<p class="btn-type02 pie"><a href="#" class="modal-close"><span class="pie">閉じる</span></a></p>
<!-- .btn // --></div>
<?=$LACNE->library["admin_view"]->html_modal_close();?>

<!-- #Modal // --></section>

<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>