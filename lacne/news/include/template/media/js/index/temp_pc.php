<script type="text/javascript">
<?php
if($device == "pc") :
// -----------------------------------------------
// PCの場合のみロード
// -----------------------------------------------
?>
$(function(){
    
    $.library.css3('#Pc #Main p.toolTip span.text','<?=LACNE_SHAREDATA_PATH?>/');
    $('figcaption a.tip').hover(function(){
            $('#Main .toolTip .text').text($(this).closest('figure').find('img').attr('alt'));
    },function(){
            $('#Main .toolTip .text').text('');
    });

    $('.tip').powerTip({
        placement:'s',
        fadeInTime:100
    });

    MediaDrop.init({
        'filetype' : <?php echo "['".implode("','", $LACNE->library['media']->filetype)."']"; ?>,
        'maxfilesize' : <?php echo $LACNE->library['media']->maxfilesize; ?>,
    });
});
function template(){
    return "form.pc table.upload-ui tbody tr";
}
function template_delete(){
    return "tr";
}

<?php
endif;
?>
    
// -----------------------------------------------
// 以下は共通ロード
// -----------------------------------------------
$(function(){
    
    // アップロードするファイルの追加
    $("a#AddFile").click(function(){
        var $template = $(template());
        var file_num = $template.size();
        if(file_num < 10){
            $template = $template.filter(":last");
            $template
                .clone(true)
                .find('input').val("").end()
                .insertAfter($template);
            if(file_num == 9){
                $(this).hide();
            }
        }
        return false;
    });

    // アップロードするファイルの削除
    $("a.CancelFile").click(function(){
        if($(this).closest('#image-preview').length){
            $(this).closest(template_delete()).remove();
        }else{
            var file_num = $(template()).size();
            if(1 < file_num){
                $(this).closest(template_delete()).remove();
                $("a#AddFile").show();
            }
        }
        return false;
    });

    //削除処理
    $('#btn-delete').click(function(){
        //削除にチェックされているか
        if($("input.delete_check:checked")[0])
        {
            $.library.modalOpen($("<a>").attr("href","#box-delete"),"#Modal");
        }
        else
        {
            $("#error_message").html("削除するファイルにチェックを入れて下さい。");
            $.library.modalOpen($("<a>").attr("href","#box-error"),"#Modal");
        }
        return false;
    });
    $('#run-delete').click(function(){
        $('form[name=del_form]').submit();
        return false;
    });

    //タグをクリックすると検索フォームに入力される処理
    $('.taglist a.tag').click(function(){
        $('input[name=search_tag]').val($(this).html());
        return false;
    });

    //タグ検索 すべて表示ボタン
    $('#btnAllView').click(function(){
        $('input[name=search_tag]').val("");
        $('#tagSearchForm').submit();
    });

    //Output Error モーダルで表示
    <?php
    if((isset($err) && $err) || (isset($message) && $message)) {
    ?>
            $.library.do_slideDown_message("#comp_message");
    <?php
    }
    if(isset($message_delete) && $message_delete) {
    ?>
            $.library.do_slideDown_message("#comp_message_delete");
    <?php
    }
    ?>


});
// -----------------------------------------------
</script>
