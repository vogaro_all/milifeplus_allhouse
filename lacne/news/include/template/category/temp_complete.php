<?php

    $page_setting = array(
        "title" => "中カテゴリ管理",
        "js" => array(),
        "css" => array(LACNE_SHAREDATA_PATH."/css/common/global_iframe.css",LACNE_SHAREDATA_PATH."/css/category/complete.css")
    );
    
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");
?>

<script type="text/javascript">
$(document).ready(function(){
    //Navigation
    $.library.sideActive('<?=LACNE_APP_ADMIN_NAVI_ID?> .category');
});
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "complete");
?>

<section class="section">
<div class="section-inside">
<div class="alert comp pie"><span class="icon">完了</span><p class="fl">中カテゴリの登録が完了しました。</p></div>
<p class="btn-type02 pie pc"><a href="#"><span class="pie">閉じる</span></a></p>
<p class="btn-type02 pie smp"><a href="<?=LACNE_APP_ADMIN_PATH?>/category/index.php"><span class="pie">戻る</span></a></p>
<!-- .section-inside // --></div>
<!-- .section // --></section>


<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>