<?php

    $page_setting = array(
        "title" => "中カテゴリ管理",
        "js" => array(),
        "css" => array(LACNE_SHAREDATA_PATH."/css/category/index.css")
    );

    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");
    
?>
<script type="text/javascript">
$(document).ready(function(){
    //Navigation
    $.library.sideActive('<?=LACNE_APP_ADMIN_NAVI_ID?> .category');
        
});
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "index" , array("csrf_token" =>$csrf_token));
?>

<section class="section">
<!--
タイトル
-->
<h1 class="head-line01"><a href="<?=LACNE_APP_INDEXPAGE_URL?>" class="btn-back smp">戻る</a>中カテゴリ管理</h1>
<p class="load">中カテゴリの新規登録、削除を行います。</p>
<!-- .section // --></section>

<section class="section edit">
<!--
新規登録エリア
-->
<h2 class="head-line02">中カテゴリ新規登録</h2>
<div class="form section-inside">
<p class="btn-type01 pie section-inside pc"><a href="#box-regist" class="modal-open"><span class="pie">新規登録</span></a></p>
<p class="btn-type01 pie section-inside smp"><a href="<?=LACNE_APP_ADMIN_PATH?>/category/register.php"><span class="pie">新規登録</span></a></p>
<!-- .form // --></div>

<!-- .section // --></section>

<section class="section list">
<!--
一覧エリア
-->
<h2 class="head-line02">登録カテゴリ一覧</h2>
<ul id="cat_list">
<?php
//---------------------------------------------
//カテゴリ一覧表示部分
//---------------------------------------------
if(isset($category_list) && $category_list) :
    foreach($category_list as $category) :
//         $num = 0;
//         if(isset($category["id"]) && isset($cnt_category_by[$category["id"]]))
//         {
// //             $num = $cnt_category_by[$category["id"]];
//         }
?>
<li id="cat_<?=$category["id"]?>"><span class="name"><?=(strlen($category["sort_no"]) == 1) ? '&nbsp;&nbsp;'.$category["sort_no"] : $category["sort_no"] ?>&nbsp;&nbsp;<?=$category["category_name"]?></span>
    <span class="edit"><a href="javascript:void(0)" class="edit_link" id="edit_<?=$category["id"]?>"><img src="<?=LACNE_SHAREDATA_PATH?>/images/common/ico_edit.gif" alt="編集" class="tip" /></a>
    <?php //id=1 のカテゴリは消さない 
//         if($category["id"] != 1) :
    ?>
    <a href="javascript:void(0)" class="del" id="del_<?=$category["id"]?>"><img src="<?=LACNE_SHAREDATA_PATH?>/images/common/ico_delete.gif" alt="削除" class="tip" /></a>
    <?php //endif; ?>
    </span>
</li>
<?php
    endforeach;
else:
?>
<li class="no-data">登録がありません。</li>
<?php
endif;
?>
</ul>

<!-- .section // --></section>


<section id="Modal" class="section">
<?php
//--------------------------------------------------------
//モーダル画面用
//--------------------------------------------------------
?>
<?=
//登録画面（iframe）
//------------------------------
$LACNE->library["admin_view"]->html_modal_open("box-regist");
?>
<iframe src="<?=LACNE_APP_ADMIN_PATH?>/category/register.php" width="500" frameborder="0" scrolling="no"></iframe>
<?=$LACNE->library["admin_view"]->html_modal_close();?>

<?=
//カテゴリ削除（個別削除）
//------------------------------
$LACNE->library["admin_view"]->html_modal_open("box-delete");
?>
<div class="alert note pie"><span class="icon">注意</span><p class="fl">中カテゴリ「<span id="delete_cat_name"></span>」を削除してもよろしいですか？</p></div>
<div class="btn">
<p class="btn-type01 pie"><a href="javascript:void(0)" id="delete_link"><span class="pie">削除</span></a></p>
<p class="btn-type02 pie"><a href="javascript:void(0)" class="modal-close"><span class="pie">キャンセル</span></a></p>
<!-- .btn // --></div>
<?=$LACNE->library["admin_view"]->html_modal_close();?>


<!-- #Modal // --></section>

<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>