<?php

    $page_setting = array(
        "title" => "中カテゴリ管理",
        "js" => array(),
        "css" => array(LACNE_SHAREDATA_PATH."/css/common/global_iframe.css",LACNE_SHAREDATA_PATH."/css/category/regist.css")
    );
    
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");
?>
<script type="text/javascript">
$(document).ready(function(){
    //Navigation
    $.library.sideActive('<?=LACNE_APP_ADMIN_NAVI_ID?> .category');
});
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "regist");
?>

<section class="section">
<h1 class="head-line01 smp">中カテゴリ登録</h1>
<p class="load smp">中カテゴリの登録・編集を行います。</p>
<?php
if(!isset($err) || !$err) :
?>
<div class="alert memo pie pc"><span class="icon">情報</span><p class="fl">下記の項目を入力のうえ、確認ボタンを押してください。</p></div>
<?php
else:
?>
<div class="alert error pie"><span class="icon">情報</span><p class="fl"><?=fn_output_errtxt($err)?></p></div>
<?php
endif;
?>
<form action="<?=LACNE_APP_ADMIN_PATH?>/category/register.php?action=check&<?=fn_set_urlparam($_GET , array('id') , false)?>" method="POST">
<div class="input">
<p><span class="label">ソート番号：</span><input type="text" name="sort_no" maxlength="10" value="<?php echo (isset($data_list["sort_no"]))?$data_list["sort_no"]:""?>" style="width:50px;" /></p>
<p><span class="label">中カテゴリ名：</span><input type="text" name="category_name" maxlength="255" value="<?php echo (isset($data_list["category_name"]))?$data_list["category_name"]:""?>" /></p>
<p><span class="label">リード文：</span><textarea name="lead" style="width:300px; resize: none;" rows="3"><?=(isset($data_list["lead"]) && $data_list["lead"])?$data_list["lead"]:""?></textarea></p>


<!-- .input // --></div>
<div class="btn">
<p class="btn-type01 pie"><input type="submit" name="conf" value="確認" class="pie" /></p>
<p class="btn-type02 pie"><a href="javascript:void(0)"><span class="pie">キャンセル</span></a></p>
<!-- .btn // --></div>
<input type="hidden" name="token" value="<?=$csrf_token?>" />
</form>
<!-- .section // --></section>


<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>