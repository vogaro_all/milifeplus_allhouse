<?php
/**-------------------------------------------------------------
 *
 * 記事データ表示用
 *
 * @package     Lacne
 * @author      In Vogue Inc. 2008 -
 * @link        http://lacne.jp
 */
// -------------------------------------------------------------

include_once(dirname(__FILE__)."/../include/setup.php");
include_once(LACNE_SHAREDATA_DIR."/include/output/post.php");

use lacne\service\output\OwnedMedia;
// use lacne\core\model\Tag;


function getItem(){
  $search_param = searchParam();
  $search_or_param = searchOrParam();
  return with(new OwnedMedia('owned_media'))->lists(array(), $search_param, $search_or_param);
}


function getActiveTag(){
  $active_tag = 0;
  $tag_list = with(new OwnedMedia('owned_media'))->getTagLists();
  if(!empty($_GET['tagid']) && !empty($tag_list[$_GET['tagid']])) {
    $active_tag = $_GET['tagid'];
  }
  return $active_tag;
}

function getSearchTxt(){
  return fn_esc($_GET['search']);
}

function searchOrParam(){
  if(!empty($_GET['search'])){
    $search_or_param = array(
                          'post.title LIKE' => $_GET['search'],
                          'post.keyword LIKE' => $_GET['search'],
                          'post.description LIKE' => $_GET['search']
                         );
  }
  return array();
}

// function searchParam(){
//   $tagid = $_GET['tagid'];
//   if($tagid){
//     $tag_list = with(new Tag())->fetchAllIDKey();
//     if(!array_key_exists($tagid, $tag_list)){
//             fn_redirect('/');
//     }
//     $search_param = array('tag_post2.tag_id =' => $tagid);
//     return $search_param;
//   }
//   return array();
// }

function getImageFullPath($image_path){
  return SITE_URL. $image_path;
}
