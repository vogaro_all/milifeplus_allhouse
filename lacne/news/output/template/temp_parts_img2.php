<!-- ******フォトライブラリー******  -->

<div class="col02">
<?php
if(!empty($_data)) :
  foreach($_data as $key => $img2) :
?>

<div class="section sec-col">
<div class="img">
<p class="img"><a href="#modal_item<?=$section_no?>_<?=$key?>" class="modal-trigger"><img src="<?=$img2['img']?>" alt=""></a></p>
<?php if($img2['url']) : ?><p class="exhibit"><a href="<?=$img2['url']?>" target="_blank">出典 <span><?php if($img2['urlText']) : ?><?=$img2['urlText']?><?php else: ?><?=$img2['url']?><?php endif; ?></span></a></p><?php endif; ?>
<!-- .img // --></div>
<h2 class="article-ttl"><?=$img2['title']?></h2>
<p class="txt"><?=$img2['text']?></p>
<!-- .section // --></div>
<?php
  endforeach;
endif;
?>
<!-- .col02 // --></div>

