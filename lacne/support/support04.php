<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />

<title>インデックス画面 | LACNE CMSサポートガイド</title>

<script type="text/javascript" src="js/jquery1.7_pack.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/library.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<link rel="stylesheet" type="text/css" href="css/global/import.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/global/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/support04.css" media="all" />

<script type="text/javascript">
$(document).ready(function(){
	$.library.active('sn-04',{type:'text'});
	common.init();
});
</script>
</head>

<body>
<div id="Container">
<a id="Top" name="Top"></a>
<div class="nonvisual-menu">
<dl><dt>ページ内を移動するためのリンクです。</dt><dd><ul><li><a href="#main-Contents">メインコンテンツへ移動</a></li></ul></dd></dl>
<!-- .nonvisual-menu // --></div>

<?php
include_once("./template/header.inc");
?>


<div id="Main">
<h2><img src="images/support04/page_ttl.gif" width="660" height="52" alt="インデックス画面" /></h2>
<p class="lead">インデックス画面には、各種操作メニューと、最近作成された記事の一覧が表示されます。<br />
（ 承認・公開権限の機能オプションを導入されている場合、承認待ちとなっている記事の一覧もこの画面上に表示されます。 ）</p>

<div class="capture">
<p class="M-align-center"><img src="images/support04/capture_img.jpg" width="620" height="448" alt="" /></p>
<ul>
<li class="popup01"><img src="images/support04/capture_txt_01.jpg" width="134" height="51" alt="各種操作メニュー" /></li>
<li class="popup02"><img src="images/support04/capture_txt_02.jpg" width="134" height="90" alt="認証待ち通知 承認待ち記事の一覧画面へ遷移します。" /></li>
<li class="popup03"><img src="images/support04/capture_txt_03.jpg" width="177" height="52" alt="最近作成された記事一覧" /></li>
</ul>
<!-- .capture // --></div>

<!-- #Main // --></div>

<?php
include_once("./template/side.inc");
?>


<!-- .content-inline // --></div>

<?php
include_once("./template/footer.inc");
?>

<!-- #Container // --></div>
</body>
</html>
