<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />

<title>記事作成画面 | LACNE CMSサポートガイド</title>

<script type="text/javascript" src="js/jquery1.7_pack.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/library.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<link rel="stylesheet" type="text/css" href="css/global/import.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/global/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/support06.css" media="all" />

<script type="text/javascript">
$(document).ready(function(){
	$.library.active('sn-06',{type:'text'});
	common.init();
});
</script>
</head>

<body>
<div id="Container">
<p><a id="Top" name="Top"></a>
</p>
<div class="nonvisual-menu">
<dl><dt>ページ内を移動するためのリンクです。</dt><dd><ul><li><a href="#main-Contents">メインコンテンツへ移動</a></li></ul></dd></dl>
<!-- .nonvisual-menu // --></div>

<?php
include_once("./template/header.inc");
?>


<div id="Main">
<h2><img src="images/support06/page_ttl.gif" width="660" height="52" alt="記事作成画面" /></h2>
<p class="lead">記事作成を行うための画面です。必要な項目に入力・選択を行い、「登録」ボタンをクリックすると、記事が作成されます。「プレビュー」ボタンをクリックすると、実際にどのように表示されるかプレビューを行います。</p>

<div class="capture M-mb20">
<p class="M-align-center"><img src="images/support06/capture_img.jpg" width="620" height="997" alt="" /></p>
<ul>
<li class="popup01"><img src="images/support06/capture_txt_01.jpg" width="262" height="399" alt="表示日時は、日付のみ、または日付と時間を指定することが可能です。表日されるカレンダーから、日付を選択して下さい。（時間指定が必要であればカレンダー下の時刻指定のスライダーを操作し時間指定を行います）" /></li>
<li class="popup02"><img src="images/support06/capture_txt_02.jpg" width="75" height="49" alt="クリック！" /></li>
<li class="popup03"><img src="images/support06/capture_txt_03.jpg" width="178" height="70" alt="実際の表示画面が見れるプレビューボタン" /></li>
</ul>
<!-- .capture // --></div>

<p class="att"><span class="heighlight">※</span> 表示日時の項目について<br />
記事の表示日時を指定します。この日時を未来の日付で登録すると、その日付になるまで記事の表示は行われません（公開状態であっても、表示日時が未来であれば、その日付になるまで表示されません）</p>

<!-- #Main // --></div>

<?php
include_once("./template/side.inc");
?>


<!-- .content-inline // --></div>

<?php
include_once("./template/footer.inc");
?>


<!-- #Container // --></div>
</body>
</html>
