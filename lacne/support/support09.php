<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />

<title>アカウント画面 | LACNE CMSサポートガイド</title>

<script type="text/javascript" src="js/jquery1.7_pack.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/library.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<link rel="stylesheet" type="text/css" href="css/global/import.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/global/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/support09.css" media="all" />

<script type="text/javascript">
$(document).ready(function(){
	$.library.active('sn-09',{type:'text'});
	common.init();
});
</script>
</head>

<body>
<div id="Container">
<p><a id="Top" name="Top"></a>
</p>
<div class="nonvisual-menu">
<dl><dt>ページ内を移動するためのリンクです。</dt><dd><ul><li><a href="#main-Contents">メインコンテンツへ移動</a></li></ul></dd></dl>
<!-- .nonvisual-menu // --></div>

<?php
include_once("./template/header.inc");
?>


<div id="Main">
<h2><img src="images/support09/page_ttl.gif" width="660" height="52" alt="アカウント画面" /></h2>
<p class="lead M-pb00">管理画面にログインし、管理操作をおこなうアカウントの作成や編集を行います。<br />
各アカウントにそれぞれ「マスター」「管理者」「投稿者」の権限を割り当てることで、記事の承認・公開フローを
導入することができます。</p>
<p class="lead att M-size-txt"><span class="heighlight">※</span> 承認・公開権限の機能オプションを導入されている場合のみ、アカウント管理画面をご利用いただけます。</p>

<div class="capture">
<p class="M-align-center"><img src="images/support09/capture_img.jpg" width="600" height="477" alt="" /></p>
<ul>
<li class="popup01"><img src="images/support09/capture_txt_01.jpg" width="200" height="61" alt="アカウントの新規作成画面が表示されます。" /></li>
<li class="popup02"><img src="images/support09/capture_txt_02.jpg" width="300" height="116" alt="アカウントの削除を行います。※ 削除を行う管理者アカウントが過去に作成した記事は、すべて別アカウントに移動されます。（一覧の一番上にある管理者アカウントに移動します）" /></li>
</ul>
<!-- .capture // --></div>

<!-- #Main // --></div>

<?php
include_once("./template/side.inc");
?>


<!-- .content-inline // --></div>

<?php
include_once("./template/footer.inc");
?>


<!-- #Container // --></div>
</body>
</html>
