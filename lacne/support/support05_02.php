<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />

<title>公開・非公開操作について | 記事一覧画面 | LACNE CMSサポートガイド</title>

<script type="text/javascript" src="js/jquery1.7_pack.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/library.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<link rel="stylesheet" type="text/css" href="css/global/import.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/global/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/support05_02.css" media="all" />

<script type="text/javascript">
$(document).ready(function(){
	$.library.active('sn-05',{type:'text'});
	$.library.active('sn-05-02',{type:'text'});
	common.init();
});
</script>
</head>

<body>
<div id="Container">
<p><a id="Top" name="Top"></a>
</p>
<div class="nonvisual-menu">
<dl><dt>ページ内を移動するためのリンクです。</dt><dd><ul><li><a href="#main-Contents">メインコンテンツへ移動</a></li></ul></dd></dl>
<!-- .nonvisual-menu // --></div>

<?php
include_once("./template/header.inc");
?>


<div id="Main">
<h2><img src="images/support05_02/page_ttl.gif" width="660" height="52" alt="承認、差戻しの操作について" /></h2>

<div class="blueblock"><div class="blueblock-outline"><div class="blueblock-inline">

<p class="att M-mb25 M-size-txt"><span class="heighlight">※</span> 承認・公開権限の機能オプションを導⼊されている場合のみ、承認・公開、差戻しの操作が
    有効になります。</p>
<p class="M-align-center"><img src="images/support05_02/capture_img_01.jpg" width="562" height="102" alt="" /></p>

<div class="in-aside M-mb05"><div class="in-aside-outline"><div class="in-aside-inline">

<div class="in-aside-whblock M-mb10"><div class="in-aside-whblock-outline"><div class="in-aside-whblock-inline">
<ul class="circle">
<li class="M-mb05"><span>承認待ち</span>・・・「承認待ち」の状態です。</li>
<li><span>差戻し</span>・・・ 承認待ち状態の記事が「差戻し」された状態です。</li>
</ul>
<!-- .in-aside-whblock-inline // --></div><!-- .in-aside-whblock-outline // --></div><!-- .in-aside-whblock // --></div>
<p class="att"><span class="heighlight">※</span> 公開権限を持たない管理者が記事を作成すると、その記事は「承認待ち」状態となり、公開権限を持つ管理者によって、「承認」が行われることで、記事が公開化されます。</p>
<!-- .in-aside-inline // --></div><!-- .in-aside-outline // --></div><!-- .in-aside // --></div>

<p class="arw M-mb05"><span>公開権限を持つ管理者が、「承認待ち」または「差戻し」をクリックすると、<br />確認画面が表示されます。</span></p>

<div class="in-border M-mb20"><div class="in-border-outline"><div class="in-border-inline">
<p class="M-mb20"><img src="images/support05_02/capture_img_02.jpg" width="510" height="323" alt="" /></p>
<p class="M-mb05">「承認」をクリックすると、記事が承認され、「公開」状態に切り替わります。</p>
<p class="att"><span class="heighlight">※</span> この記事の公開日が未来の日付である場合、承認・公開を行なわれても、その公開日がくるまでは公開されません。</p>
<p class="line">「差戻し」をクリックすると、記事を承認せずに、差戻しを行います（メッセージ欄に差戻し理由等を記載し、記事作成者に通知することができます）</p>
<!-- .in-border-inline // --></div><!-- .in-border-outline // --></div><!-- .in-border // --></div>

<div class="in-aside M-mb05"><div class="in-aside-outline"><div class="in-aside-inline">
<p class="att"><span class="heighlight">※</span> 承認、または差戻しが⾏われたタイミングで、記事の作成をおこなった管理者宛てにメール通知されます。</p>
<!-- .in-aside-inline // --></div><!-- .in-aside-outline // --></div><!-- .in-aside // --></div>

<!-- .blueblock-inline // --></div><!-- .blueblock-outline // --></div><!-- .blueblock // --></div>

<!-- #Main // --></div>

<?php
include_once("./template/side.inc");
?>


<!-- .content-inline // --></div>

<?php
include_once("./template/footer.inc");
?>


<!-- #Container // --></div>
</body>
</html>
