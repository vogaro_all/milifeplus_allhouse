<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />

<title>パスワードの再発行（パスワードリマインダ） | LACNE CMSサポートガイド</title>


<script type="text/javascript" src="js/jquery1.7_pack.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/library.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<link rel="stylesheet" type="text/css" href="css/global/import.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/global/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/support02.css" media="all" />
<script type="text/javascript">
$(document).ready(function(){
	$.library.active('sn-02',{type:'text'});
	common.init();
});
</script>
</head>

<body>
<div id="Container">
<a id="Top" name="Top"></a>
<div class="nonvisual-menu">
<dl><dt>ページ内を移動するためのリンクです。</dt><dd><ul><li><a href="#main-Contents">メインコンテンツへ移動</a></li></ul></dd></dl>
<!-- .nonvisual-menu // --></div>

<?php
include_once("./template/header.inc");
?>


<div id="Main">
<h2><img src="images/support02/page_ttl.gif" width="660" height="52" alt="パスワードの再発行（パスワードリマインダ）" /></h2>
<p class="lead">発行されたパスワードをお忘れになられた場合は、「※パスワードを忘れた方はこちら」をクリックして下さい。<br />
パスワードの再発行画面が表示されますので、IDと登録されているメールアドレスを入力し、送信ボタンをクリックして下さい。</p>

<div class="capture capture01 M-mb20">
<p class="M-align-center"><img src="images/support02/capture_img_01.jpg" width="560" height="393" alt="" /></p>
<ul>
<li class="popup01"><img src="images/support02/capture_txt_01.jpg" width="75" height="49" alt="クリック！" /></li>
</ul>
<!-- .capture // --></div>

<div class="capture capture02 M-mb20">
<p class="M-align-center"><img src="images/support02/capture_img_02.jpg" width="560" height="432" alt="" /></p>
<ul>
<li class="popup01"><img src="images/support02/capture_txt_02.jpg" width="184" height="49" alt="IDとメールアドレスを入力" /></li>
</ul>
<!-- .capture // --></div>

<div class="aside"><div class="aside-outline"><div class="aside-inline">
<p class="M-mb10">パスワードの再発行が行われると、再発行されたパスワードが記載されたメールが登録されているメールアドレス宛てへ送信されます。</p>
<p class="att"><span class="heighlight">※</span> 再発行されたパスワードでログインを⾏われた後、設定画面にて任意のパスワードを再登録されることをおすすめ致します。</p>
<!-- .aside-inline // --></div><!-- .aside-outline // --></div><!-- .aside // --></div>
<!-- #Main // --></div>

<?php
include_once("./template/side.inc");
?>


<!-- .content-inline // --></div>

<?php
include_once("./template/footer.inc");
?>

<!-- #Container // --></div>
</body>
</html>
