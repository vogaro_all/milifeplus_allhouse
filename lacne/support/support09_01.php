<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />

<title>アカウントの作成画面について | アカウント画面 | LACNE CMSサポートガイド</title>

<script type="text/javascript" src="js/jquery1.7_pack.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/library.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<link rel="stylesheet" type="text/css" href="css/global/import.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/global/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/support09_01.css" media="all" />

<script type="text/javascript">
$(document).ready(function(){
	$.library.active('sn-09',{type:'text'});
	$.library.active('sn-09-01',{type:'text'});
	common.init();
});
</script>
</head>

<body>
<div id="Container">
<p><a id="Top" name="Top"></a>
</p>
<div class="nonvisual-menu">
<dl><dt>ページ内を移動するためのリンクです。</dt><dd><ul><li><a href="#main-Contents">メインコンテンツへ移動</a></li></ul></dd></dl>
<!-- .nonvisual-menu // --></div>

<?php
include_once("./template/header.inc");
?>


<div id="Main">
<h2><img src="images/support09_01/page_ttl.gif" width="660" height="52" alt="アカウントの作成画面について" /></h2>
<p class="lead">「新規作成」ボタン、または、アカウント一覧上の編集アイコンをクリックすると、アカウントの作成（編集）画面が表示され、アカウントの新規作成、または編集を行うことができます。</p>

<div class="blueblockarw M-mb20"><div class="blueblockarw-outline"><div class="blueblockarw-inline">
<div class="capture capture01">
<p class="M-align-center"><img src="images/support09_01/capture_img_01.jpg" width="520" height="294" alt="" /></p>
<ul>
<li class="popup01"><img src="images/support09_01/capture_txt_01.jpg" width="76" height="45" alt="クリック！" /></li>
</ul>
<!-- .capture // --></div>
<!-- .blueblockarw-inline // --></div><!-- .blueblockarw-outline // --></div><!-- .blueblockarw // --></div>

<div class="blueblockarw"><div class="blueblockarw-outline"><div class="blueblockarw-inline2">
<div class="capture capture02">
<p class="M-align-center"><img src="images/support09_01/capture_img_02.jpg" width="520" height="414" alt="" /></p>
<!-- .capture // --></div>
<!-- .blueblockarw-inline2 // --></div><!-- .blueblockarw-outline // --></div><!-- .blueblockarw // --></div>

<!-- #Main // --></div>

<?php
include_once("./template/side.inc");
?>


<!-- .content-inline // --></div>

<?php
include_once("./template/footer.inc");
?>


<!-- #Container // --></div>
</body>
</html>
