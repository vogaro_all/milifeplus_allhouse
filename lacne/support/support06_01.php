<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />

<title>リンク先の入力について | 記事作成画面 | LACNE CMSサポートガイド</title>

<script type="text/javascript" src="js/jquery1.7_pack.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/library.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<link rel="stylesheet" type="text/css" href="css/global/import.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/global/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/support06_01.css" media="all" />

<script type="text/javascript">
$(document).ready(function(){
	$.library.active('sn-06',{type:'text'});
	$.library.active('sn-06-01',{type:'text'});
	common.init();
});
</script>
</head>

<body>
<div id="Container">
<p><a id="Top" name="Top"></a>
</p>
<div class="nonvisual-menu">
<dl><dt>ページ内を移動するためのリンクです。</dt><dd><ul><li><a href="#main-Contents">メインコンテンツへ移動</a></li></ul></dd></dl>
<!-- .nonvisual-menu // --></div>

<?php
include_once("./template/header.inc");
?>


<div id="Main">

<h2><img src="images/support06_01/page_ttl01.gif" width="660" height="52" alt="リンク先の入力について" /></h2>
<p class="lead">記事タイトルのみを登録し、その記事を別ページヘ直接リンクさせたい場合には、このリンク先の項目にリンク先URLを登録して下さい。</p>
<div class="capture capture01 M-mb50">
<p class="M-align-center M-mb20"><img src="images/support06_01/capture_img_01.jpg" width="620" height="281" alt="" /></p>
<ul>
<li class="popup01"><img src="images/support06_01/capture_txt_01.jpg" width="75" height="49" alt="チェック！" /></li>
</ul>
<p class="M-align-left">「リンク先ウインドウ」 にチェックを⼊れると、このリンク先を別ウインドウで開きます。</p>
<!-- .capture // --></div>




<h2><img src="images/support06_01/page_ttl02.gif" width="660" height="52" alt="メタ情報（キーワード、説明）の入力について" /></h2>
<p class="lead">メタ情報を登録されると、その記事の詳細ページ中のmetaタグ情報として埋め込まれます。</p>
<div class="capture capture02">
<p class="M-align-center M-mb20"><img src="images/support06_01/capture_img_02.jpg" width="620" height="201" alt="" /></p>
<ul>
<li class="popup01"><img src="images/support06_01/capture_txt_02.jpg" width="120" height="49" alt="メタ情報を入力" /></li>
</ul>
<p class="att M-size-txt M-align-left"><span class="heighlight">※</span> キーワード：&lt;meta name="keywords" /&gt; の値として登録されます。<br />説明 ：&lt;meta name="description" /&gt; の値として登録されます。</p>
<!-- .capture // --></div>

<!-- #Main // --></div>

<?php
include_once("./template/side.inc");
?>


<!-- .content-inline // --></div>

<?php
include_once("./template/footer.inc");
?>


<!-- #Container // --></div>
</body>
</html>
