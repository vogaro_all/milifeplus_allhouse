<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />

<title>公開・非公開操作について | 記事一覧画面 | LACNE CMSサポートガイド</title>


<script type="text/javascript" src="js/jquery1.7_pack.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/library.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<link rel="stylesheet" type="text/css" href="css/global/import.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/global/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="css/common.css" media="all" />

<script type="text/javascript">
$(document).ready(function(){
	$.library.active('sn-05',{type:'text'});
	$.library.active('sn-05-01',{type:'text'});
	common.init();
});
</script>
</head>

<body>
<div id="Container">
<a id="Top" name="Top"></a>
<div class="nonvisual-menu">
<dl><dt>ページ内を移動するためのリンクです。</dt><dd><ul><li><a href="#main-Contents">メインコンテンツへ移動</a></li></ul></dd></dl>
<!-- .nonvisual-menu // --></div>

<?php
include_once("./template/header.inc");
?>


<div id="Main">
<h2><img src="images/support05_01/page_ttl.gif" width="660" height="52" alt="公開・非公開操作について" /></h2>

<div class="blueblock"><div class="blueblock-outline"><div class="blueblock-inline">

<p class="M-align-center"><img src="images/support05_01/capture_img_01.jpg" width="562" height="87" alt="" /></p>
<p class="M-align-center"><img src="images/support05_01/capture_txt_01.gif" width="562" height="141" alt="記事が「非公開」の状態です。クリックすると、記事が公開化されます。" /></p>
<div class="in-border"><div class="in-border-outline"><div class="in-border-inline">
<p class="M-mb20"><img src="images/support05_01/capture_img_02.jpg" width="510" height="203" alt="" /></p>
<p class="M-mb05">記事を公開するかどうかの確認画面が表示されます。「公開」をクリックすると、記事が公開されます。</p>
<p class="att"><span class="heighlight">※</span> 記事の公開日が未来の日付である場合、「公開化」を行なわれても、その公開日がくるまでは公開されません。</p>
<!-- .in-border-inline // --></div><!-- .in-border-outline // --></div><!-- .in-border // --></div>
<p class="M-align-center"><img src="images/support05_01/capture_txt_02.gif" width="562" height="141" alt="記事が「公開」状態になります。" /></p>
<p class="M-align-center M-mb25">さらに、ON状態のボタンをクリックすると、その記事を「非公開」状態に変更します。<br />（確認画面が表示されます）</p>
<div class="in-aside"><div class="in-aside-outline"><div class="in-aside-inline">
<p class="att M-align-center"><span class="heighlight">※</span> 承認・公開権限の機能オプションを導入されている場合、ログインしている管理者アカウントが<br />「公開権限」を持たなければ記事の公開・非公開化の操作を行うことはできません。</p>
<!-- .in-aside-inline // --></div><!-- .in-aside-outline // --></div><!-- .in-aside // --></div>
<!-- .blueblock-inline // --></div><!-- .blueblock-outline // --></div><!-- .blueblock // --></div>

<!-- #Main // --></div>

<?php
include_once("./template/side.inc");
?>


<!-- .content-inline // --></div>

<?php
include_once("./template/footer.inc");
?>


<!-- #Container // --></div>
</body>
</html>
