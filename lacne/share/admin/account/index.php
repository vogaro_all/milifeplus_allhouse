<?php
use lacne\core\model\Admin;
use lacne\core\model\Post;
/**-------------------------------------------------------------
 *  
 *  ユーザー管理
 *
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// -------------------------------------------------------------

require_once(dirname(__FILE__)."/../../include/setup.php");

// ------------------------------------------------------------------------
// セットアップ
// ------------------------------------------------------------------------
$LACNE->load_library(array('login')); //library load
$LACNE->session->sessionStart(); //Session start
$login_id = $LACNE->library["login"]->IsSuccess(true , "manage_account"); //認証チェック

$admin = new Admin();

//登録されている管理者アカウントリストを取得
$account_list = $LACNE->library['login']->get_AccountList();
//権限タイプリストを取得
$authority_list = $LACNE->library['login']->get_AuthorityData();


//Templateファイルのフォルダ指定(通常読み込むtemplateフォルダの中のさらにaccount以下にあるため）
$LACNE->template->setViewDir($LACNE->template->getViewDir()."/account");
//render_data : Template側に渡すデータ変数
$render_data = array(
    "login_id" => $login_id
);


// ------------------------------------------------------------------------
// ここから削除処理
// ------------------------------------------------------------------------
if($LACNE->post('/delete')){
    
    //CSRF TOKENチェック
    $csrf_check = false;
    if(isset($_POST["token"]) && $_POST["token"])
    {
        $csrf_check = $LACNE->request->csrf_check($_POST["token"]);
    }
    if(!$csrf_check)
    {
        //CSRFエラー
        echo "0"; exit;
    }
    
    $delete_ids = array();
    //送信されてきたパラメータ取得(カテゴリ1は削除不可)
    if(isset($_POST["id"]) && is_numeric($_POST["id"]))
    {
        $delete_ids[] = $_POST["id"];
    }
    else if(isset($_POST["del_account"]) && count($_POST["del_account"]) > 0){
        
        foreach($_POST["del_account"] as $account_id)
        {
            if(is_numeric($account_id))
            {
                $delete_ids[] = $account_id;
            }
        }
    }
    if(count($delete_ids))
    {
        
        //アカウントID = 1のユーザーのlogin_idを得る
        $user_info1 = $admin->fetchOne(1);
        $login_id1 = 0;
        if(isset($user_info1["login_id"])) $login_id1 = $user_info1["login_id"];
        
        foreach($delete_ids as $tid)
        {
            if($tid != 1)
            {
                //そのアカウントIDが作成した記事があれば、すべて1にする
                //$tidのユーザーのlogin_id値を得る
                $t_login_id = "";
                $t_user_info = $admin->fetchOne($tid);
                if(isset($t_user_info["login_id"])) $t_login_id = $t_user_info["login_id"];
                if(with(new Post())->cnt("user_id = ?" , array($t_login_id)))
                {
                    //指定したアカウントIDを持つ記事をすべてアカウントID=1にする
                    with(new Post())->change_user_id($t_login_id , $login_id1); 
                }

                $admin->delete($tid);
            }
        }
    }
    //削除後リダイレクト
    fn_redirect(LACNE_APP_ADMIN_PATH."/account/index.php");
}

// ------------------------------------------------------------------------
// アカウント名の問い合わせ（削除時にajaxで問い合わせされる）
// ------------------------------------------------------------------------
if($LACNE->post('/account_name'))
{
    $return = "";
    if(isset($_POST["account_name"]) && is_numeric($_POST["account_name"]))
    {
        $result = $admin->fetchOne($_POST["account_name"]);
        if($result && isset($result["login_account"]))
        {
            $return = $result["login_account"];
        }
    }
    echo $return;
    exit;
}



$render_data = array_merge($render_data , array(
    "account_list" => $account_list,
    "authority_list" => $authority_list,
    //CSRF TOKEN
    "csrf_token" => $LACNE->request->csrf_token_generate()
));

//Render
$LACNE->render("index" , $render_data);

?>