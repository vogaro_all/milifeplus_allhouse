<?php
use lacne\core\model\Admin;
/**-------------------------------------------------------------
 *  
 * ユーザー管理
 *
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// -------------------------------------------------------------


require_once(dirname(__FILE__)."/../../include/setup.php");

// ------------------------------------------------------------------------
// セットアップ
// ------------------------------------------------------------------------
$LACNE->load_library(array('login' , 'category' , 'validation')); //library load
$LACNE->session->sessionStart(); //Session start
$login_id = $LACNE->library["login"]->IsSuccess(true , "manage_account"); //認証チェック

//登録されている管理者アカウントリストを取得
$account_list = $LACNE->library['login']->get_AccountList();
//権限タイプリストを取得
$authority_list = $LACNE->library['login']->get_AuthorityData();

//Templateファイルのフォルダ指定(通常読み込むtemplateフォルダの中のさらにaccount以下にあるため）
$LACNE->template->setViewDir($LACNE->template->getViewDir()."/account");
//render_data : Template側に渡すデータ変数
$render_data = array(
    "login_id" => $login_id,
    "account_list" => $account_list,
    "authority_list" => $authority_list,
    //CSRF TOKEN
    "csrf_token" => $LACNE->request->csrf_token_generate()
);


// ------------------------------------------------------------------------
// ここから登録の確認画面の処理
// ------------------------------------------------------------------------
if($LACNE->post('/conf'))
{

    //画面遷移のチェック（確認画面を経由しているか）
    $_SESSION["route_check"] = "";

    //送信されてきたパラメータ取得
    $data_list = fn_get_form_param($_POST);

    $err = account_validation($LACNE , $data_list);

    $render_data = array_merge($render_data , array(
        "data_list" => $data_list,
        "hidden_data" => serialize_base64_encode($data_list)
    ));
    
    if(!$err)
    {
        //パスワードの文字の長さ分 * にする
        $password_view = "";
        if(isset($data_list["password"]) && $data_list["password"])
        {
            $password_view = str_pad($password_view, strlen($data_list["password"]) , "*");
        }
        else if(isset($pass_check) && !$pass_check)
        {
            $password_view = "変更なし";
        }
        
        $render_data["password_view"] = $password_view;
        
        $_SESSION["route_check"] = "confirm";
        //確認画面 Render
        $LACNE->render("confirm" , $render_data);
    }
    else
    {
        $render_data["err"] = $err;
        //エラー画面Render
        $LACNE->render("regist" , $render_data);
    }
    
}

// ------------------------------------------------------------------------
// ここから完了画面、DB登録の処理
// ------------------------------------------------------------------------
if($LACNE->post("/complete")){

    //画面遷移チェック
    if(!isset($_SESSION["route_check"]) || $_SESSION["route_check"] != "confirm") exit;
    unset($_SESSION["route_check"]);
	
    //送信されてきたパラメータ取得
    $param = fn_get_form_param($_POST);
    //CSRF TOKENチェック
    $csrf_check = false;
    if(isset($param["token"]) && $param["token"])
    {
        $csrf_check = $LACNE->request->csrf_check($param["token"]);
    }
    
    if($csrf_check)
    {
        //デコード
        //Todo シリアライズ/デコード処理まわりをきちんとクラス化してまとめたい
        //Requestあたり？
        $data_list = unserialize_base64_decode($param["hidden"]);

        //再度チェック
        $err = account_validation($LACNE , $data_list);
        if(!empty($err)) exit;

        unset($data_list["password_chk"]);

        //編集モードならば渡されたIDをセット
        if(isset($_GET["id"]) && is_numeric($_GET["id"])) {

            //GETで指定されたidが存在しない値だったらexit
            $tid = $_GET["id"];
            $edit_data = with(new Admin())->fetchOne($tid);
            if(empty($edit_data)) exit;

                $data_list["id"] = $_GET["id"];
                $data_list["modified"] = fn_get_date();
        }

        //新規登録モードならば、IDと作成日と
        //login_id値（不変なユニーク値）を生成してセット
        else{
            
                $data_list["login_id"] = uniq();
                $data_list["created"] = fn_get_date();
                $data_list["modified"] = fn_get_date();
        }

        //パスワードはハッシュ暗号化
        //これは新規登録時、またはパスワード変更時のみ
        //編集モードの場合password値は空が送られる
        if(isset($data_list["password"]) && $data_list["password"])
        {
            $data_list["password"] = sha1(CERT_LOGIN_KEY.$data_list["password"]);
        }
        else
        {
            unset($data_list["password"]);
        }

        //unset data
        if(isset($data_list["token"])) unset($data_list["token"]);

        //DBへインサート
        $rs = with(new Admin())->replace($data_list , "id");
        
        //完了画面へ
        fn_redirect(LACNE_APP_ADMIN_PATH."/account/register.php?action=comp");
        
    }
    else
    {
        $render_data["err"] = array(
            "csrf_error" => "データの受け渡しで問題が発生しました。もう一度操作をやり直して下さい。"
        );
        //エラー画面Render
        $LACNE->render("regist" , $render_data);
    }
}

// ------------------------------------------------------------------------
// 完了画面描画
// ------------------------------------------------------------------------
if($LACNE->action("/comp")){
    
    $_SESSION["route_check"] = "";
    //Render
    $LACNE->render("complete" , $render_data);
        
}

// ------------------------------------------------------------------------
// ここから最初の画面の処理
// ------------------------------------------------------------------------
if($LACNE->action("/")){
    
    $_SESSION["route_check"] = "";
    
    //確認画面からの修正もどり
    if($LACNE->post("/back") && $LACNE->post("/hidden"))
    {
        $param = fn_get_form_param($_POST);
        //CSRF TOKENチェック
        $csrf_check = false;
        if(isset($param["token"]) && $param["token"])
        {
            $csrf_check = $LACNE->request->csrf_check($param["token"]);
        }
        if(!$csrf_check)
        {
            $render_data["err"]["csrf_error"] = "データの受け渡しで問題が発生しました。もう一度操作をやり直して下さい。";
            $data_list = array();
        }
        else
        {
        //デコード
        	$data_list = unserialize_base64_decode($param["hidden"]);
        }
        
    //ID指定がある編集モードの場合
    }
    else if(isset($_GET["id"]) && is_numeric($_GET["id"]))
    {
        $tid = $_GET["id"];
        $data_list = with(new Admin())->fetchOne($tid);
        if(isset($data_list["password"])) unset($data_list["password"]);
    }
    
    $render_data = array_merge($render_data ,array(
        "data_list" => $data_list
    ));
    
    //Render
    $LACNE->render("regist" , $render_data);
    
}

/**
 * アカウント登録処理のバリデーション
 */
function account_validation($LACNE , $data_list)
{
    //入力チェック
    $err_check_arr = array(
            "user_name"=>array("name"=>"担当者名","type"=>array("null","len"),"length"=>100),
            "login_account"=>array("name"=>"ログインID","type"=>array("null","len","alpha_numeric"),"length"=>100),
            "password"=>array("name"=>"パスワード","type"=>array("null","len","alpha_numeric"),"length"=>100),
            "email"=>array("name"=>"メールアドレス","type"=>array("null","email","len"),"length"=>100),
            "authority"=>array("name"=>"権限タイプ","type"=>array("null","alpha_numeric")),
    );

    //編集モードの場合パスワードが空ならチェックしない
    $pass_check = 1;
    if(isset($_GET["id"]) && fn_check_int($_GET["id"])){
        if(empty($data_list["password"]) && empty($data_list["password_chk"]))
        {
            unset($err_check_arr["password"]);
            $pass_check = 0;
        }
    }
    //バリデーション実行
    $err = $LACNE->library["validation"]->check($data_list,$err_check_arr);

    //編集モードでlogin_idを変更しているかのチェック
    $old_data = array();
    if(isset($_GET["id"]) && is_numeric($_GET["id"]))
    {
        $old_data = with(new Admin())->fetchOne($_GET["id"]);
        if(isset($old_data["login_account"]) && $old_data["login_account"] == $data_list["login_account"])
        {
            $old_data = array();
        }
    }
    //新規登録の場合かもしくは編集モードでlogin_idを変更している場合、ログインIDの重複チェック
    if(!isset($_GET["id"]) || isset($old_data["login_account"])){
        if (with(new Admin())->cnt("login_account = ?" , array($data_list["login_account"]))){
            $err["duplication"] = "入力されたアカウント名は既に使用されています";
        }
    }

    //パスワードチェック
    if ($pass_check && $data_list["password"] !== $data_list["password_chk"]){
        $err["password_chk"] = "パスワードが一致しません";
    }
    //新しいパスワードが半角英数字であるか
    elseif ($pass_check && !preg_match("/^[a-zA-Z0-9]{8,20}$/" , $data_list["password"]))
    {
        $err["password_chk"] = "パスワードは英数字8文字以上20文字以下で入力して下さい。";
    }

    //id=1(初期マスター)の場合は、権限タイプを変更できない
    if(isset($_GET["id"]) && is_numeric($_GET["id"]))
    {
        $tid = $_GET["id"];
        $edit_data = with(new Admin())->fetchOne($tid);
        if(empty($edit_data)) exit;
        if($edit_data["id"] == "1" && $edit_data["authority"] != $data_list["authority"])
        {
            $err["authority"] = "このアカウントは権現を変えることができません。";
        }
    }

    //IPアドレス制限値のチェック
    $data_list["ipaddress"] = str_replace(" ","",$data_list["ipaddress"]);
    if(!empty($data_list["ipaddress"]) && !preg_match('/^[0-9\.,]+$/' , $data_list["ipaddress"]))
    {
        $err["ipaddress"] = "「IP制限」の入力値に不正な文字があります（数値とドット、カンマのみ）。";
    }

    //CSRF TOKENチェック
    $csrf_check = false;
    if(isset($data_list["token"]) && $data_list["token"])
    {
        $csrf_check = $LACNE->request->csrf_check($data_list["token"]);
    }
    if(!$csrf_check) $err["csrf_error"] = "データの受け渡しで問題が発生しました。もう一度操作をやり直して下さい。";

    return $err;
}
?>