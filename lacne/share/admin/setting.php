<?php
use lacne\core\model\Admin;
/**------------------------------------------------------------------------
 *  
 * 各種設定
 *
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

require_once(dirname(__FILE__)."/../include/setup.php");

// ------------------------------------------------------------------------
// セットアップ
// ------------------------------------------------------------------------
$LACNE->load_library(array('login' , 'setting')); //library load
$LACNE->session->sessionStart(); //Session start
$login_id = $LACNE->library["login"]->IsSuccess(); //認証チェック

//Templateファイルのフォルダ指定(通常読み込むtemplateフォルダの中のさらにsetting以下にあるため）
$LACNE->template->setViewDir($LACNE->template->getViewDir()."/setting");
//render_data : Template側に渡すデータ変数
$render_data = array(
    "login_id" => $login_id,
    "csrf_token" => $LACNE->request->csrf_token_generate()
);

//IPアドレス制限の登録値を取得
$account_data = with(new Admin())->fetchOneByLoginID($login_id);
if(isset($account_data['ipaddress'])) {
    $render_data['ipaddress'] = (!empty($account_data['ipaddress']) ? $account_data['ipaddress'] : '');
}
// ------------------------------------------------------------------------
// パスワード変更送信
// ------------------------------------------------------------------------
if($LACNE->post('/password_change'))
{

    //送信されてきたパラメータ取得
    $data_list = fn_get_form_param($_POST);
    
    $err = "";
    $message = "";
    //パスワード変更処理
    if($data_list["password"] && $data_list["new_password"] && $data_list["new_password_conf"])
    {
        if($data_list["new_password"] != $data_list["new_password_conf"])
        {
            $err = "入力された新しいパスワードと確認用のパスワードが一致していません。";
        }
        
        if(!$err)
        {
            //現在のパスワードが正確かどうか
            $account_data = with(new Admin())->fetchOneByLoginID($login_id);
            $password = "";
            if($account_data && isset($account_data['password']))
            {
                $password = $account_data["password"];
            }
            if($password != sha1(CERT_LOGIN_KEY.$data_list["password"]))
            {
                $err = "入力された古いパスワードの値が間違っています。";
            }
        }
        
        if(!$err)
        {
            //新しいパスワードが半角英数字であるか
            if (!preg_match("/^[a-zA-Z0-9]{8,20}$/" , $data_list['new_password']))
            {
                $err = "新しいパスワードは英数字8文字以上20文字以下で入力して下さい。";
            }
        }
/*
        if(!$err)
        {
            //CSRF TOKENチェック
            $csrf_check = false;
            if(isset($data_list["token"]) && $data_list["token"])
            {
                $csrf_check = $LACNE->request->csrf_check($data_list["token"]);
            }
            if(!$csrf_check) $err =  "データの受け渡しで問題が発生しました。もう一度操作をやり直して下さい。";
        }
*/
        
        if(!$err)
        {
            $passwordstring = sha1(CERT_LOGIN_KEY.$data_list['new_password']);
            $updata = array(
                "login_id" => $login_id,
                "password" => $passwordstring,
                "passchange_alert" => fn_get_date() //パスワード変更アラートを最後に出した日時を更新
            );
            $ret = with(new Admin())->replace($updata , "login_id");
            
            if($ret)
            {
                $render_data = array_merge($render_data , array(
                    "message" => "パスワードを更新しました。"
                ));
            }
        }
    }
        else
        {
        $err = "現在のパスワードまたは新しいパスワードの入力がありません。";
    }

    if(!empty($err))
    {
            $render_data = array_merge($render_data , array(
                "err" => $err
            ));
        }
}

// ------------------------------------------------------------------------
// IPアドレス設定の送信
// ------------------------------------------------------------------------
elseif($LACNE->post('/ipaddress_lock'))
{

    //送信されてきたパラメータ取得
    $data_list = fn_get_form_param($_POST);

    $err = "";
    $message = "";

    //パスワード変更処理
    if(!empty($data_list["ipaddress"]))
    {
        $data_list["ipaddress"] = str_replace(" ","",$data_list["ipaddress"]);
        if(!preg_match('/^[0-9\.,]+$/' , $data_list["ipaddress"]))
        {
            $err = "IPアドレス制限の入力データ中に不正な文字があります。IPアドレス（半角数値とドット）とカンマのみ有効です。";
        }
        
        if(!$err)
        {
            //CSRF TOKENチェック
            $csrf_check = false;
            if(isset($data_list["token"]) && $data_list["token"])
            {
                $csrf_check = $LACNE->request->csrf_check($data_list["token"]);
            }
            if(!$csrf_check) $err =  "データの受け渡しで問題が発生しました。もう一度操作をやり直して下さい。";
        }
    }

    if((!empty($data_list["ipaddress"]) && empty($err)) || isset($data_list["ipaddress"])) {

        $updata = array(
            "login_id" => $login_id,
            "ipaddress" => $data_list["ipaddress"]
        );
        $ret = with(new Admin())->replace($updata , "login_id");
        
        if($ret)
        {
            $render_data = array_merge($render_data , array(
                "message" => "接続元IPアドレスを更新しました。",
                "warning" => $warning,
                "ipaddress" => $data_list["ipaddress"]
            ));
        }
    }

    if(!empty($err))
    {
        $render_data = array_merge($render_data , array(
            "err" => $err,
            "ipaddress" => $data_list["ipaddress"]
        ));
    }
}

// ------------------------------------------------------------------------
// 
// 以下、その他設定処理
// 
// ------------------------------------------------------------------------

//権限チェック（以下の処理は権限が必要）
if($LACNE->library["login"]->chk_controll_limit("manage_setting")){ }


//Render
$LACNE->render("index" , $render_data);

?>