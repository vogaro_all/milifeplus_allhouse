<?php

/**------------------------------------------------------------------------
 *  
 *  ログイン
 *
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

require_once(dirname(__FILE__)."/../include/setup.php");

// ------------------------------------------------------------------------
// セットアップ
// ------------------------------------------------------------------------
$LACNE->load_library(array('login'));
$LACNE->session->sessionStart(); //Session start

//ログアウト
$LACNE->library["login"]->logout();

fn_redirect(LACNE_LOGIN_URL);

?>