<?php

/**------------------------------------------------------------------------
 *  
 *  総合メイン
 * 
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

require_once(dirname(__FILE__)."/../include/setup.php");

// ------------------------------------------------------------------------
// セットアップ
// ------------------------------------------------------------------------
$LACNE->load_library(array('login')); //library load
$LACNE->session->sessionStart(); //Session start
$login_id = $LACNE->library["login"]->IsSuccess(); //認証チェック

$render_data = array(
    "login_id" => $login_id
);

//メイン画面にリダイレクト
fn_redirect(LACNE_MAINPAGE_URL);

?>