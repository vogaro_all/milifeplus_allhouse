<?php

    $page_setting = array(
        "title" => "アカウント管理",
        "js" => array(),
        "css" => array(LACNE_SHAREDATA_PATH."/css/common/global_iframe.css",LACNE_SHAREDATA_PATH."/css/account/confirm.css")
    );

    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");
?>
<script type="text/javascript">
$(document).ready(function(){
    //Navigation
    $.library.sideActive('<?=LACNE_APP_ADMIN_NAVI_ID?> .account');
});
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "confirm");
?>


<section class="section">
<h1 class="head-line01 smp">アカウント新規登録</h1>
<p class="load smp"></p>
<div class="alert memo pie"><span class="icon">情報</span><p class="fl">下記内容で登録します。よろしければ登録ボタンを押してください。</p></div>
<form action="<?=LACNE_APP_ADMIN_PATH?>/account/register.php<?=fn_set_urlparam($_GET , array('id'))?>" method="POST">
<div class="input">
<p><span class="label">担当者名：</span><?=$data_list["user_name"]?></p>
<p><span class="label">ログインID：</span><?=$data_list["login_account"]?></p>
<p><span class="label">メールアドレス：</span><?=$data_list["email"]?></p>
<p><span class="label">パスワード：</span><?=$password_view?></p>
<?php
if(isset($data_list["ipaddress"])) {
?>
<p><span class="label">IP制限：</span><?=$data_list["ipaddress"]?>
<?php
    //現在アクセスしているIPアドレスは含まれているかチェック
    if(!empty($data_list["ipaddress"])) {
        $this_ip = fn_getIP();
        $input_ip = explode("," , $data_list["ipaddress"]);
        if(!in_array($this_is , $input_ip)) {
?>
<br>
<strong style="color:#FF3300">※現在アクセスされているIPアドレスが、入力されたIP制限値に含まれていませんのでご注意下さい。</strong><br>
<?php
        }
    }
?>
</p>
<?php
}
?>
<p><span class="label">権限タイプ：</span><?=(isset($authority_list[$data_list["authority"]]))?$authority_list[$data_list["authority"]]:""?></p>
<!-- .input // --></div>
<div class="btn">
<p class="btn-type01 pie"><input type="submit" name="complete" value="登録" class="pie" /></p>
<p class="btn-type02 pie"><input type="submit" name="back" value="修正" class="pie" /></p>
<!-- .btn // --></div>

<input type="hidden" name="hidden" value="<?=$hidden_data?>" />
<input type="hidden" name="token" value="<?=$csrf_token?>" />
</form>
<!-- .section // --></section>



<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>