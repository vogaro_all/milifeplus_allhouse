<?php

    $page_setting = array(
        "title" => "アカウント管理",
        "js" => array(),
        "css" => array(LACNE_SHAREDATA_PATH."/css/account/index.css")
    );

    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");

?>

<script type="text/javascript">
$(document).ready(function(){
    //Navigation
    $.library.sideActive('#MainMenu .account');
});
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "index" , array("csrf_token"=>$csrf_token));
?>

<section class="section">
<h1 class="head-line01"><a href="<?=LACNE_APP_INDEXPAGE_URL?>" class="btn-back smp">戻る</a>アカウント管理</h1>
<p class="load">管理者アカウントの管理・作成、編集を行います。</p>
<!-- .section // --></section>

<section class="section">
<h2 class="head-line02">アカウント新規登録</h2>
<p class="btn-type01 pie section-inside pc"><a href="#box-regist" class="modal-open"><span class="pie">新規登録</span></a></p>
<p class="btn-type01 pie section-inside smp"><a href="<?=LACNE_APP_ADMIN_PATH?>/account/register.php"><span class="pie">新規登録</span></a></p>
<!-- .section // --></section>

<section class="section">
<h2 class="head-line02">登録アカウント一覧</h2>

<div id="account_list">

<?php
//---------------------------------------------
//アカウント一覧表示部分
//---------------------------------------------

if(isset($account_list) && count($account_list)) :

//-------------------------------------------
//PC表示用のリストここから
//-------------------------------------------
?>
<form action="<?=LACNE_APP_ADMIN_PATH?>/account/index.php?action=delete" name="del_form" method="POST">
<table cellpadding="0" cellspacing="0" border="0" summary="登録アカウント一覧" class="table-list pc pie">
<thead>
<tr>
<td width="3%" class="lt"><input type="checkbox" name="" value="" class="master" /></td>
<th width="15%">担当者名</th>
<th width="25%">ログインID</th>
<th width="29%">メールアドレス</th>
<th width="15%">権限タイプ</th>
<th width="13%" class="rt">編集</th>
</tr>
</thead>
<tbody>
<?php
    foreach($account_list as $account) :
?>
<tr id="acc_<?=$account["id"]?>">
<td>
<?php if($account["id"] != 1) : ?>
<input type="checkbox" name="del_account[]" class="delete_check" value="<?=$account["id"]?>" />
<?php endif; ?>
</td>
<td class="name"><?=$account["user_name"]?></td>
<td class="account"><?=$account["login_account"]?></td>
<td class="mail"><?=$account["email"]?></td>
<td class="authority"><?=(isset($authority_list[$account["authority"]]))?$authority_list[$account["authority"]]:""?></td>
<td class="edit">
    <a href="javascript:void(0)" class="btn_edit" id="edit_<?=$account["id"]?>"><img src="<?=LACNE_SHAREDATA_PATH?>/images/common/ico_edit.gif" alt="編集" class="tip" /></a>
    <?php if($account["id"] != 1) : ?>
    <a href="javascript:void(0)" class="btn_del" id="del_<?=$account["id"]?>"><img src="<?=LACNE_SHAREDATA_PATH?>/images/common/ico_delete.gif" alt="削除" class="tip" /></a>
    <?php endif; ?>
</td>
</tr>
<?php
    endforeach;
?>
</tbody>
</table>

<?php
//-------------------------------------------
//スマフォ表示用のリストここから
//-------------------------------------------
?>
<ul class="list smp">
 <?php
    foreach($account_list as $account) :
?>
<li id="acc_<?=$account["id"]?>">
<p class="check">
<?php if($account["id"] != 1) : ?>
<input type="checkbox" name="del_account[]" class="delete_check" value="<?=$account["id"]?>" />
<?php endif; ?>
</p>
<div class="text">
<a href="<?=LACNE_APP_ADMIN_PATH?>/account/register.php?id=<?=$account["id"]?>" class="location">編集</a>
<p><?=$account["user_name"]?></p>
<p><?=$account["login_account"]?></p>
<p><?=$account["email"]?></p>
<p class="long"><?=(isset($authority_list[$account["authority"]]))?$authority_list[$account["authority"]]:""?></p>
<?php if(!empty($account["ipaddress"])) { ?>
<p class="long">（IP制限あり）</p>
<?php } ?>
</div>
</li>
<?php
    endforeach;
?>
</ul>

<p class="btn-type03 pie"><strong><a href="#box-delete02" class="modal-open pie" id="btn_dels"><span>選択アカウントを削除</span></a></strong></p>
<input type="hidden" name="delete" value="1" />
<input type="hidden" name="token" value="<?=$csrf_token?>" />
</form>
<?php
endif;
?>


</div>

<!-- .section // --></section>


<section id="Modal" class="section">
<?php
//--------------------------------------------------------
//モーダル画面用
//--------------------------------------------------------
?>
<?=
//アカウント削除（個別削除）
//------------------------------
$LACNE->library["admin_view"]->html_modal_open("box-delete");
?>
<div class="alert note pie"><span class="icon">注意</span><p class="fl">アカウント「<span id="delete_account"></span>」を削除してもよろしいですか？</p></div>
<div class="btn">
<p class="btn-type01 pie"><a href="javascript:void(0)" id="delete_link"><span class="pie">削除</span></a></p>
<p class="btn-type02 pie"><a href="javascript:void(0)" class="modal-close"><span class="pie">キャンセル</span></a></p>
<!-- .btn // --></div>
<?=$LACNE->library["admin_view"]->html_modal_close();?>

<?=
//アカウント削除（複数選択削除）
//------------------------------
$LACNE->library["admin_view"]->html_modal_open("box-delete02");
?>
<div class="alert note pie"><span class="icon">注意</span><p class="fl">選択したアカウントを削除してもよろしいですか？</p></div>
<div class="btn">
<p class="btn-type01 pie"><a href="javascript:void(0)" id="run-delete"><span class="pie">削除</span></a></p>
<p class="btn-type02 pie"><a href="javascript:void(0)" class="modal-close"><span class="pie">キャンセル</span></a></p>
<!-- .btn // --></div>
<?=$LACNE->library["admin_view"]->html_modal_close();?>

<?=
//登録画面（iframe）
//------------------------------
$LACNE->library["admin_view"]->html_modal_open("box-regist");
?>
<iframe src="<?=LACNE_PATH?>/index.html" width="500" frameborder="0" scrolling="no"></iframe>
<?=$LACNE->library["admin_view"]->html_modal_close();?>

<?=
//エラー表示用
//------------------------------
$LACNE->library["admin_view"]->html_modal_open("box-error");
?>
<div class="alert error pie"><span class="icon">注意</span><p class="fl" id="error_message"></p></div>
<div class="btn btn-one">
<p class="btn-type02 pie"><a href="#" class="modal-close"><span class="pie">閉じる</span></a></p>
<!-- .btn // --></div>
<?=$LACNE->library["admin_view"]->html_modal_close();?>

<!-- #Modal // --></section>


<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>