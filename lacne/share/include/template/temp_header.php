<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />

<title><?=$page_setting["title"]?> | CMMS</title>

<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="copyright" content="Copyright (c) CMMS All rights reserved." />
<meta name="robots" content="noindex,nofollow" />
<meta name="robots" content="noarchive" />
<meta name="googlebot" content="noarchive" />
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />

<link rel="shortcut icon" href="/favicon.ico" />
<link rel="index" href="/" />

<link rel="stylesheet" type="text/css" href="<?=LACNE_SHAREDATA_PATH?>/css/common/import.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?=LACNE_SHAREDATA_PATH?>/css/common/print.css" media="print" />
<?php
	if(isset($page_setting["css"]) && is_array($page_setting["css"])){
            foreach($page_setting["css"] as $value){
		if($value){
			echo '<link rel="stylesheet" type="text/css" href="'.$value.'" media="all" />'."\n";
		}
            }
        }
?>

<!--[if IE 6]><script type="text/javascript" src="<?=LACNE_SHAREDATA_PATH?>/js/DD_belatedPNG_0.0.8a-min.js"></script><![endif]-->
<!--[if lt IE 9]><script src="<?=LACNE_SHAREDATA_PATH?>/js/html5.js"></script><![endif]-->
<script type="text/javascript" src="<?=LACNE_SHAREDATA_PATH?>/js/jquery1.7_pack.js"></script>
<script type="text/javascript" src="<?=LACNE_SHAREDATA_PATH?>/js/library.js"></script>
<?php
	if(isset($page_setting["js"]) && is_array($page_setting["js"])){
            foreach($page_setting["js"] as $value){
		if($value){
			echo '<script type="text/javascript" src="'.$value.'"></script>'."\n";
		}
            }
        }
?>

<script type="text/javascript">
var LACNE_APP_ADMIN_PATH = "<?=LACNE_APP_ADMIN_PATH?>";
var LACNE_SHAREDATA_PATH = "<?=LACNE_SHAREDATA_PATH?>";
var LACNE_ADMIN_ENABLE_SMP = "<?=($LACNE->library["admin_view"]->device_type() == "Smp")?"TRUE":""?>";
$(document).ready(function(){
	$.library.css3('#Content,#Main,#GlobalNav li a,#GlobalNav dt a,#GlobalNav dd a,.input-usually,.input-usually input,.input-usually textarea,.pie','<?=LACNE_SHAREDATA_PATH?>/');
});
</script>


</head>

<body id="<?=$LACNE->library["admin_view"]->device_type()?>">
<div id="Container">
<a id="Top" name="Top"></a>
<div class="nonvisual-menu">
<dl><dt>ページ内を移動するためのリンクです。</dt><dd><ul><li><a href="#main-Contents">メインコンテンツへ移動</a></li></ul></dd></dl>
<!-- .nonvisual-menu // --></div>

<!-- 
//////////////////////////////////////////////////////////////////////////////
Header
//////////////////////////////////////////////////////////////////////////////
--> 
<header id="GlobalHeader">
<p class="logo"><a href="<?=LACNE_MAINPAGE_URL?>"><img src="<?=LACNE_SHAREDATA_PATH?>/images/common/header_logo.png" alt="CMMS" /></a></p>

<nav class="FunctionNav"><ul>
<?php
if(isset($login_id) && $login_id)
{
?>
<?php 
if(defined("LACNE_APP_FRONTPAGE_URL") && LACNE_APP_FRONTPAGE_URL) :
?>
<li class="site pc"><a href="<?=LACNE_APP_FRONTPAGE_URL?>" target="_blank">サイトを見る</a></li>
<?php
endif;
?>
<li class="set"><a href="<?=LACNE_PATH?>/setting.php">各種設定</a></li>
<?php if(defined("LACNE_HELP_URL") && LACNE_HELP_URL) :?>
<li class="help"><a href="<?=LACNE_HELP_URL?>">ヘルプ</a></li>
<?php endif; ?>
<li class="logout"><a href="<?=LACNE_PATH?>/logout.php">ログアウト</a></li>
<?php
}
?>
</ul></nav>
<!-- #GlobalHeader // --></header>

<!-- 
//////////////////////////////////////////////////////////////////////////////
Content
//////////////////////////////////////////////////////////////////////////////
--> 
<div class="content-outline"><div id="Content">
<a id="main-Contents" name="main-Contents"><img src="<?=LACNE_SHAREDATA_PATH?>/images/common/spacer.gif" alt="ここからメインコンテンツです" /></a>

<?php
//サイドメニュー表示PC用（$hidden_sidemenu=1としておくと表示されない）
if(isset($login_id) && $login_id && (!isset($hidden_sidemenu) || !$hidden_sidemenu) && $LACNE->library["admin_view"]->device_type() == "Pc") :
    require_once(dirname(__FILE__)."/temp_side_type_b.php");
endif;
?>

<div id="Main">
