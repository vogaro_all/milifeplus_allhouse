<?php
    
    $page_setting = array(
        "title" => "ログイン",
        "js" => array(),
        "css" => array(LACNE_SHAREDATA_PATH."/css/login.css")
    );

    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");
?>

<script type="text/javascript">
$(document).ready(function(){
});
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "login" , array("err"=>$err));
?>


<section class="section login-box pie">
<h1 class="head-line03 pie">ログイン</h1>
<form action="" method="post">
<div class="login-input">
<p>ログインIDとパスワードを入力し、『ログイン』ボタンを押して下さい。</p>
<p class="input"><span class="label"><strong>ログインID：</strong></span><input type="text" name="login_account" tabindex="1" value="<?php echo (isset($data["login_account"]))?$data["login_account"]:""?>" /></p>
<p class="input"><span class="label"><strong>パスワード：</strong></span><input type="password" name="password" tabindex="2" value="" /></p>
<!-- .login-input // --></div>
<div class="login-exec pie">
<p class="btn-type01 pie"><input type="submit" tabindex="3" name="" id="btn_login" value="ログイン" class="pie" /></p>
<?php
if(defined("FROM_MAINADDRESS") && FROM_MAINADDRESS) :
?>
<?php
/*
<p><a href="#box-forget" class="modal-open pc">※パスワードを忘れた方はこちら</a></p>
<p><a href="<?=LACNE_APP_ADMIN_PATH?>/forget.php" class="smp">※パスワードを忘れた方はこちら</a></p>
*/
?>
<?php
endif;
?>
<!-- .login-exec // --></div>
</form>
<!-- .section // --></section>

<section class="section info">
<p class="pc">管理画面ご利用時の推薦環境<br />Windows Intemet Explorer9以降(11推奨）、もしくはGoogle Chrome , Firefox最新版でのご利用を推奨しております。</p>
</section>


<section id="Modal" class="section">
<?php
//--------------------------------------------------------
//モーダル画面用
//--------------------------------------------------------
?>
<?=
//リマインダ画面（iframe）
//------------------------------
$LACNE->library["admin_view"]->html_modal_open("box-forget");
?>
<iframe src="<?=LACNE_APP_ADMIN_PATH?>/forget.php" width="500" frameborder="0" scrolling="no"></iframe>
<?=$LACNE->library["admin_view"]->html_modal_close();?>

<?php
//エラーメッセージ
//------------------------------
echo $LACNE->library["admin_view"]->html_modal_open("box-error");
?>
<div class="alert error pie"><span class="icon">認証エラー</span><p class="fl" id="error_message"></p></div>
<div class="btn">
<p class="btn-type02 pie" style="margin:30px auto 0 auto"><a href="#" class="modal-close"><span class="pie">閉じる</span></a></p>
<!-- .btn // --></div>
<?php
echo $LACNE->library["admin_view"]->html_modal_close();
?>

<!-- #Modal // --></section>

<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>