<?php
use lacne\core\model\Post;
use lacne\core\model\PostMeta;

/**-------------------------------------------------------------
 *  
 * 記事データ表示用
 *
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// -------------------------------------------------------------

class Lacne_output
{
    var $LACNE;
    var $post_data;
    var $post_data_cnt;
    var $template_setting;
    var $param;
    var $preview_mode = false;
    
    function Lacne_output($_LACNE)
    {
        $this->LACNE = $_LACNE;
    }
    
    /**
     * 表示対象となる記事データを取得する
     * @param object $param //データ所得時の各種指定パラメータ
     *    |-number $num //表示件数(all : 全て）※page_limitを指定する場合は空に
     *    |-number $category //カテゴリ指定(ID)
     *    |-number $page_limit //1ページ単位の表示数
     *    |-string $date_target //日付指定しての絞込み(yyyy-mm-dd形式 絞りこみする場合、yyyyは必須 yyyy%やyyyy-mm% などが可能) 
     * @param object $search_param 
     * @param boolean $getCnt //記事データ数のみ取得する場合はtrueに
     * @return mixed 
     */
    function get_post_list($param , $search_param = array() , $getCnt = false)
    {
        
        $search_param_arr = array(
            "post.output_flag =" => 1,
            "post.output_date <=" => fn_get_date()
        );
        if($this->preview_mode) $search_param_arr = array(); //previewモードなら表示対象をすべてとする
        
        if(isset($param["category"]) && is_numeric($param["category"]))
        {
            $search_param_arr["post.category ="] = $param["category"];
        }
        if(isset($param["date_target"]) && $param["date_target"])
        {
            $search_param_arr["post.output_date LIKE"] = "%".$param["date_target"]."%";
        }
        //category , date_target以外でも他のデータ絞り込み条件があればそれをマージ
        if($search_param)
        {
            $search_param_arr = array_merge($search_param_arr , $search_param);
        }
        
        $cnt = with(new Post())->data_cnt($search_param_arr);
        
        //データのカウントだけなら
        if($getCnt)
        {
            return $cnt;
        }
        
        if($cnt){
        
            $sql_order = array("key"=>"post.output_date","by"=>"DESC","val"=>"sort_date");
            
            //データ取得（ページャー非対応）
            //================================
            if(!isset($param["pager"]) || !$param["pager"]){
                
                $sql = with(new Post())->get_list_sql($search_param_arr , $sql_order);

                if($param["num"] && is_numeric($param["num"]) && $param["num"] != "all"){
                    $sql .= " LIMIT ".$param["num"];
                }

                $this->post_data = with(new Post())->_fetchAll($sql , array());

            //データ取得（ページャー対応）
            //================================
            }else{
                
                //現在のページ
                $page = 1;

                if($_GET['page']){
                    $page = fn_now_page_set($_GET['page']);
                }
                
                $page_limit = NUM_LIST;
                if(isset($param["page_limit"]) && is_numeric($param["page_limit"])) $page_limit = $param["page_limit"];
                
                //総ページ数取得
                $page_num = fn_getPages($cnt,$page_limit);
                
                if($page > $page_num){
                    $page = $page_num;
                }
                
                $this->post_data = with(new Post())->get_list($page , $page_limit , $search_param_arr , $sql_order);

            }
            
            $this->set_var_post_data_cnt($cnt);
            return $this->post_data;
        }
        
        return false;
    }
    
    
    /**
     * 詳細（個別）記事モード
     */
    function get_post_data($post_id)
    {
        if(is_numeric($post_id))
        {
            $this->post_data = $this->LACNE->library["post"]->get_postdata($post_id);
            if($this->post_data)
            {
                return $this->post_data;
            }
        }
        return false;
    }
    
    /**
     * 公開されている記事で一番新しい表示日時の記事データIDを取得
     */
    function get_newer_post_id($search_param = array())
    {
        
        $search_param = array_merge($search_param , array(
            "post.output_flag =" => 1,
            "post.output_date <=" => fn_get_date()
        ));
        
        $order = array(
            "val" => "sort_date",
            "key" => "post.output_date",
            "by" => "desc"
        );
        $newer_post_data = with(new Post())->get_list(1 , 1 ,$search_param , $order);
        
        if(isset($newer_post_data[0]["id"])) return $newer_post_data[0]["id"];
        return 0;
    }
    
    
    function set_imagepath($html)
    {
        return $this->LACNE->library["output"]->set_http_path($html);
    }
    
    function get($key)
    {
        if(isset($this->post_data[$key]))
        {
            return $this->post_data[$key];
        }
        
        return false;
    }
    
    /**
     * 表示展開するテンプレートファイルを指定しセットする
     * @param string $template
     * @return void
     */
    function set_template($template = "")
    {
        
        $TEMPLATE_SETTING = array();
        
        //表示用テンプレートの指定があればそれを読み込み
        if($template && file_exists(LACNE_APP_DIR_OUTPUT_TEMPLATE."temp_".$template.".php"))
        {
            require_once(LACNE_APP_DIR_OUTPUT_TEMPLATE."temp_".$template.".php");
        }
        //指定がなければbase_template.phpを読み込み
        else if(file_exists(LACNE_APP_DIR_OUTPUT_TEMPLATE."base_template.php"))
        {
            require_once(LACNE_APP_DIR_OUTPUT_TEMPLATE."base_template.php");
        }
        
        if($TEMPLATE_SETTING) $this->template_setting = $TEMPLATE_SETTING;
        
        return;
    }
    
    
    /**
     * 展開するテンプレートファイル中の$TEMPLATE_SETTINGの指定した値を取得(set_templateメソッド実行後に利用）
     * @param string $key (テンプレートKEY)
     * @param string $device (デバイス指定 smph , mobi)
     *
     * @return string;
     */
    function get_template_value($key , $device="")
    {
        if($device && isset($this->template_setting[$device][$key]))
        {
            return $this->template_setting[$device][$key];
        }
        else if(isset($this->template_setting[$key]))
        {
            return $this->template_setting[$key];
        }

        return "";
    }
    
    function set_var_post_data($_post_data)
    {
        $this->post_data = $_post_data;
    }
    
    function get_var_post_data()
    {
        if($this->post_data) return $this->post_data;
        return false;
    }
    
    function set_var_post_data_cnt($_post_data_cnt)
    {
        $this->post_data_cnt = $_post_data_cnt;
    }
    
    function get_var_post_data_cnt()
    {
        if($this->post_data_cnt) return $this->post_data_cnt;
        return 0;
    }
    
    /**
     * プレビューリクエストかどうかをチェック
     * （POSTでtitleデータが送信されている、もしくはGETでpreview値が送信されてきている場合）
     * @return void
     */
    function preview_chk()
    {
        if((isset($_POST["title"]) && $_POST["title"]) || (isset($_GET["preview"]) && $_GET["preview"]))
        {
            //previewはログイン状態でない場合exitさせる
            $this->LACNE->session->sessionStart();
            $this->LACNE->load_library(array('login'));
            $login_id = $this->LACNE->library["login"]->IsSuccess(false);
            if(!$login_id) exit;
            $this->preview_mode = true;
        }
        return;
    }
    
}


/**
 *  記事リストを表示
 *  @params  object $params //データ所得時の各種指定パラメータ
 *    |-number $num //表示件数(all : 全て）※page_limitを指定する場合は空に
 *    |-number $category //カテゴリ指定(ID)
 *    |-number $page_limit //1ページ単位の表示数
 *    |-string $date_target //日付指定しての絞込み(yyyy-mm-dd形式 絞りこみする場合、yyyyは必須 yyyy%やyyyy-mm% などが可能) 
 *    |-boolean $use_frame //フレームモードで出力する場合に使用
 *    |-boolean $postmeta //拡張データ項目も取得するか(postmetaテーブルのデータ）
 *  @param  string $template //テンプレートファイル名指定（output／template／以下のファイル名※拡張子付けない)
 *  @param  string $device   //テンプレートファイルでデバイス指定する場合( smph : スマートフォン , mobi :ガラケー）
 *
 *  @return string
 */
function printList($params , $template = "" , $device ="") {
    
    global $LACNE;
    global $Output;
    
    //デバイス情報取得（mobile output拡張オプションが有効の場合のみ）
    if(!$device)
    {
        $device = $LACNE->library["output"]->is_mobile();
    }
    
    $Output->set_template($template);
    
    $param = array();
    /*==================================================================
     *  ここから書き出すテンプレートタグを取り出し
     *==================================================================*/
    $param["start_tag"]     = $Output->get_template_value("TEMPLATE_TAG_START" , $device);
    $param["end_tag"]       = $Output->get_template_value("TEMPLATE_TAG_END" , $device);
    $param["loop_tag"]      = $Output->get_template_value("TEMPLATE_TAG_LOOP" , $device);
    $param["loop_tag_top"]  = $Output->get_template_value("TEMPLATE_TAG_LOOP_TOP" , $device);
    $param["loop_tag_last"] = $Output->get_template_value("TEMPLATE_TAG_LOOP_LAST" , $device);
    $param["format_date"]   = $Output->get_template_value("TEMPLATE_FORMAT_DATE" , $device);
    $param["error_txt"]     = $Output->get_template_value("TEMPLATE_TAG_ERROR" , $device);
    
    //取得対象とするカテゴリID指定
    $param["category"] = "";
    if(isset($params["category"]) && is_numeric($params["category"])){
        $param["category"] = $params["category"];
    }
    
    //取得対象とする年月指定
    $param["date_target"] = "";
    if(isset($params["date_target"]) && $params["date_target"]){
        $param["date_target"] = $params["date_target"];
    }
    
    //その他、データ取得時に絞込み条件を付けるのであれば
    $search_param = array();
    
    $param["newicon"]       = $Output->get_template_value("TEMPLATE_NEWICON" , $device);
    $param["newicon_no"]    = $Output->get_template_value("TEMPLATE_NEWICON_NO" , $device);
    //pagerによるページ分割に対応させるか
    $param["pager"]         = $Output->get_template_value("TEMPLATE_PAGER" , $device);
    
    //表示件数
    $param["num"] = "all";
    if(isset($params["num"]))
    {
        $param["num"] = $params["num"];
    }
    //1ページあたりの記事数指定
    if(isset($params["page_limit"]) && is_numeric($params["page_limit"])){
        $param["page_limit"] = $params["page_limit"];
    }else{
        $param["page_limit"] = $Output->get_template_value("TEMPLATE_PAGE_LIMIT" , $device);
    }
    //Newアイコン設定
    $param["newicon_item"] = $Output->get_template_value("TEMPLATE_NEWICON_ITEM" , $device);
    
    $post_data = $Output->get_post_list($param , $search_param);
    
    if($post_data && count($post_data)){
        
        $output_tag = $param["start_tag"];

        $i = 1;
        $last = count($post_data);
        foreach($post_data as $post)
        {
            $replace_pattern = array();
            
            //body , historyデータは外す
            $unset_keys = array('body' , 'history');
            fn_unset_form_param($post , $unset_keys);
            
            if(is_array($post))
            {
                foreach($post as $key=>$value)
                {
                    $replace_pattern[$key] = '/\['.$key.'\]/';
                }
            }
            
            
            $post = fn_sanitize($post);
            $link = 0;
            
            $output_date = $post["output_date"];
            $post["output_date"] = date($param["format_date"],strtotime($post["output_date"]));
            
            //テンプレート内の埋め込み変数変換パターンにパーマリンクURL値も追加
            if(!isset($replace_pattern["detail_linktag_url"])) //detail_linktag_url : URLのみ
            {
                $replace_pattern["detail_linktag_url"] = '/\[detail_linktag_url\]/';
            }
            $post["detail_linktag_url"] = $LACNE->url_setting->get_pageurl($post["id"],$post["category"],$device);
            
            if(!isset($replace_pattern["detail_linktag_param"])) //detail_linktag_param : aタグリンクの属性値
            {
                $replace_pattern["detail_linktag_param"] = '/\[detail_linktag_param\]/';
            }
			
            if(!isset($replace_pattern["detail_linktag"])) //detail_linktag_param : 詳細リンクaタグ
            {
                $replace_pattern["detail_linktag"] = '/\[detail_linktag\]/';
            }
            if(!isset($replace_pattern["detail_linktag_close"])) //detail_linktag_param : 詳細リンクaタグ閉じ
            {
                $replace_pattern["detail_linktag_close"] = '/\[detail_linktag_close\]/';
            }
			
            $linktag_param = "";
            if($post["link"]){
                $linktag_param = " href=\"".$post["link"]."\"";
            }
            else
            {
                $linktag_param = " href=\"".$post["detail_linktag_url"]."\"";
            }
            
            if($post["link_window"]){
                $linktag_param .= " target=\"_blank\"";
            }
            else if($post["link"] || $post["body"])
            {
                //IFRAME対応タイプの場合、target=_topで
                if((defined("USE_FRAME") && USE_FRAME) || (isset($param["use_frame"]) && $param["use_frame"])) {
                    $linktag_param .= " target=\"_top\"";
                }
            }
            
            $post["detail_linktag_param"] = $linktag_param;
			
            $post["detail_linktag"] = (!empty($post["body"]) || !empty($post["link"]))?"<a".$linktag_param.">":"";
            $post["detail_linktag_close"] = (!empty($post["body"]) || !empty($post["link"]))?"</a>":"";
			
                    
            if($param["newicon"]) {
                //テンプレート内の埋め込み変数変換パターンにnewiconも追加
                if(!isset($replace_pattern["newicon"]))
                {
                    $replace_pattern["newicon"] = '/\[newicon\]/';
                }
                $post["newicon"] = "";
                $daydiff = (strtotime(date("Y/m/d"))-strtotime(date("Y/m/d" , strtotime($output_date))))/(3600*24);
                if($daydiff >= 0 && $daydiff <= $param["newicon"] && count($param["newicon_item"]) && isset($param["newicon_item"][$param["newicon_no"]])) {
                    $post["newicon"] = $param["newicon_item"][$param["newicon_no"]];
                }
            }
            
            //拡張項目のデータ取得
            //================================
            if(isset($params["postmeta"]) && $params["postmeta"])
            {
                $post_meta_data = with(new PostMeta())->fetchByPostID($post["id"]);
                if($post_meta_data && count($post_meta_data))
                {
                    foreach($post_meta_data as $post_meta)
                    {
                        //テンプレート内の埋め込み変数変換パターンに拡張項目も追加
                        //拡張項目は _meta_を前に付ける img1なら _meta_img1
                        $meta_key_str = "_meta_".$post_meta["meta_key"];
                        if(!isset($replace_pattern[$meta_key_str]))
                        {
                            $replace_pattern[$meta_key_str] = '/\['.$meta_key_str.'\]/';
                            $post[$meta_key_str] = $post_meta["meta_body"];
                        }
                    }
                }
            }
            
            
            if($i == 1 && $param["loop_tag_top"]){

                $output_tag .= preg_replace($replace_pattern,$post,$param["loop_tag_top"]);

            }else if($i == $last && $param["loop_tag_last"]){

                $output_tag .= preg_replace($replace_pattern,$post,$param["loop_tag_last"]);

            }else{

                $output_tag .= preg_replace($replace_pattern,$post,$param["loop_tag"]);

            }
            
            $i++;

        }

        $output_tag .= $param["end_tag"];

        return fn_change_stringcode($output_tag , STRINGCODE_OUTPUT , STRINGCODE_PHP);

    }else{

        return fn_change_stringcode($param["error_txt"] , STRINGCODE_OUTPUT , STRINGCODE_PHP);

    }
    
    return "";
}

//-----------------------------------------------------------------------------------
//
// ここから一覧ページ用の処理や関数いろいろ
// 
//-----------------------------------------------------------------------------------
/**
 * 一覧データを取得する
 * @global object $LACNE
 * @global object $Output
 * @param  object $param//データ所得時の各種指定パラメータ
 *    |-number $num //表示件数(all : 全て）※page_limitを指定する場合は空に
 *    |-number $category //カテゴリ指定(ID)
 *    |-number $page_limit //1ページ単位の表示数
 *    |-string $date_target //日付指定しての絞込み(yyyy-mm-dd形式 絞りこみする場合、yyyyは必須 yyyy%やyyyy-mm% などが可能) 
 *    |-boolean $postmeta //拡張データ項目も取得するか(postmetaテーブルのデータ）
 *    |-boolean $escape //返却データをエスケープするか（デフォルト true)
 */
function LACNE_PostList($params)
{
    global $LACNE;
    global $Output;
    
    
    //その他、データ取得時に絞込み条件を付けるのであればここに指定
    $search_param = array();
    
    //previewモードの場合(POSTデータがある場合）、そのデータを利用する
    if($Output->preview_mode && isset($_POST["title"]))
    {
        $set_post_data[] = fn_get_form_param($_POST);
        
        $Output->set_var_post_data($set_post_data);
        $Output->set_var_post_data_cnt(1);
    }
    else
    {
        if(isset($params["page_limit"]) && is_numeric($params["page_limit"])) $params["pager"] = 1; //page_limit指定があれば pager対応
        $post_data = $Output->get_post_list($params , $search_param);
        
        //拡張項目のデータ取得
        //================================
        if(isset($params["postmeta"]) && $params["postmeta"] && $post_data && count($post_data))
        {

            foreach($post_data as $key => $post)
            {
                $post_meta_data = with(new PostMeta())->fetchByPostID($post["id"]);
                if($post_meta_data && count($post_meta_data))
                {
                    foreach($post_meta_data as $post_meta)
                    {
                        $post_data[$key]["_meta_"][$post_meta["meta_key"]] = $post_meta["meta_body"];
                    }
                }
            }
        }
        $Output->set_var_post_data($post_data);
    }
    
    $result = $Output->get_var_post_data();
    if(!isset($params["escape"]) || $params["escape"])
    {
        $result = fn_esc($result);
    }
    return $result;
    
}

/**
 *  記事データ数を取得
 */
function getListCnt() {
    
    global $Output;
    
    $post_data_cnt = $Output->get_var_post_data_cnt();
    
    if($post_data_cnt)
    {
        return $post_data_cnt;
    }
    return 0;
}

function getListPageNum($page_limit)
{
    return fn_getPages(getListCnt() , $page_limit);
}

function getListCurrentPage($page_limit)
{
    $page = 1;
    if($_GET['page']){
        $page = fn_now_page_set($_GET['page']);
    }
    
    //総ページ数取得
    $page_num = fn_getPages(getListCnt(),$page_limit);
    
    if($page > $page_num){
        $page = $page_num;
    }
    
    return $page;
}
function getNumCurrentPage($page_limit)
{
    $num = getListCnt();
    $current_page = getListCurrentPage($page_limit);
    $page_num = getListPageNum($page_limit);
    
    $current_page_num1 = ($current_page)?($page_limit*($current_page-1)+1):0;
    $current_page_num2 = $page_limit*$current_page;
    
    if($current_page_num2 > $num) $current_page_num2 = $num;
    
    return array("num1"=>$current_page_num1 , "num2"=>$current_page_num2);
    
}
//Todo 通常のテンプレートではなく、Templateベースのviewファイルを利用するタイプでも表示できるように
function renderList($template , $device ="")
{
    global $LACNE;
    global $Output;
    //Templateファイルのフォルダ指定(通常読み込むtemplateフォルダの中のさらにaccount以下にあるため）
    $LACNE->template->setViewDir(LACNE_APP_DIR_OUTPUT_TEMPLATE);
    
    $render_data = array(
        "post_data" => $Output->get_var_post_data(),
        "device"    => $device
    );
    return $LACNE->render($template , $render_data , true);
}

function renderPager($page_limit , $template="pager" , $device ="" , $param = "")
{
    global $LACNE;
    global $Output;
    //Templateファイルのフォルダ指定(通常読み込むtemplateフォルダの中のさらにaccount以下にあるため）
    $LACNE->template->setViewDir(LACNE_APP_DIR_OUTPUT_TEMPLATE);
    
    $render_data = array(
        "page" => getListCurrentPage($page_limit),
        "page_limit" => $page_limit,
        "page_num" => getListPageNum($page_limit),
        "device"    => $device,
        "param"     => $param
    );
    return $LACNE->render($template , $render_data , true);
}


//-----------------------------------------------------------------------------------
//
// ここから詳細ページ用の処理や関数いろいろ
// 
//-----------------------------------------------------------------------------------
/**
 * 詳細データを取得する
 * @global object $LACNE
 * @global object $Output
 * @param number $post_id 
 */
function LACNE_Post($post_id = "")
{
    global $LACNE;
    global $Output;
    
    //previewモードの場合(POSTデータがある場合）、そのデータを利用する
    if($Output->preview_mode && isset($_POST["title"]))
    {
        $post_data = fn_get_form_param($_POST);
    }
    else 
    {
        if(!$post_id && isset($_GET["id"]) && is_numeric($_GET["id"]))
        {
            $post_id = $_GET["id"];
        }
        
        $post_data = $Output->get_post_data($post_id);
        
        //post_dataが取得できない、または、post_idが取得できなければ一番新しいデータのIDを取得する
        if(!$post_data || !$post_id)
        {
            $post_id = $Output->get_newer_post_id();
            $post_data = $Output->get_post_data($post_id);
        }
        
        //取得したデータが公開状態か、また公開日時が過ぎているかをチェック
        //ただしpreviewモードなら非公開もしくは公開日時が過ぎていなくても見れる
        if(!$Output->preview_mode && (!isset($post_data["output_flag"]) || !$post_data["output_flag"] || !isset($post_data["output_date"]) || $post_data["output_date"] > fn_get_date()))
        {
            $post_data = array();
        }
        
    }
    
    $Output->set_var_post_data($post_data);
    
    //ソーシャルボタン設置用のOGP情報をセット（social_btn拡張オプションが有効の場合のみ）
    if(method_exists($LACNE->library["social_btn"], "set_pageinfo"))
    {
        $LACNE->library["social_btn"]->set_pageinfo($Output);
    }
    
    return $post_data;
}

function get_field_data($key , $escape = true)
{
    global $Output;
    
    $target_data = $Output->get($key);
    if($target_data){
        if(!is_array($target_data))
        {
            if($escape) $target_data = fn_sanitize ($target_data);
            return fn_change_stringcode($target_data , STRINGCODE_OUTPUT , STRINGCODE_PHP);
        }
        return $target_data;
    }
    
    return "";
}

//データの存在チェック
function post_exists()
{
    $tdata = get_field_data("title");
    if($tdata != "") return true;
    return false;
}

function the_title($escape = true)
{
    return get_field_data("title" , $escape);
}
function the_category($escape = true)
{
    return get_field_data("category" , $escape);
}
function the_categoryname($escape = true)
{
    return get_field_data("category_name" , $escape);
}

function the_date($format="Y.m.d")
{
    global $Output;
    $target_data = $Output->get("output_date");
    if($target_data) return date($format , strtotime($target_data));
    
    return "";
}
function the_time($format="H:i:s")
{
    global $Output;
    $target_data = $Output->get("output_date");
    if($target_data) return date($format , strtotime($target_data));
    
    return "";
}
function the_datetime($format="Y.m.d H:i:s")
{
    global $Output;
    $target_data = $Output->get("output_date");
    if($target_data) return date($format , strtotime($target_data));
    
    return "";
}
function the_body($escape = false)
{
    global $LACNE;
    global $Output;
    
    //bodyはタグを含むのでエスケープしない
    $target_data = get_field_data("body" , $escape);
    if($target_data)
    {
        //imgタグにLightbox用のクラスを付ける (フロント出力時に余計な改行が入る問題があるため、次行のLightboxクラス付加の処理はやめる)
        //$target_data = $LACNE->library["output"]->set_modallink_for_img($target_data);
        
        //競売デバイス向けに画像を調整(mobile output拡張オプションが有効の場合のみ)
        $device = $LACNE->library["output"]->is_mobile();
        if($device)
        {
            $target_data = $LACNE->library["output"]->changedeviceImg($device , $target_data);
        }
        //さらにdeviceがmobiの場合はフューチャーフォン向けに出力コンテンツを変換
        if($device == "mobi")
        {
            $target_data = $LACNE->library["output"]->mobileHtmlConvert($target_data);
        }
        //flv埋め込みがある場合、objectタグ生成
        if(method_exists($LACNE->library["media"],"Lib_media_movie"))
        {
            $target_data = $LACNE->library["output"]->set_flv_objct_param($target_data);
        }
    }
    return $target_data;
}
function the_meta_keyword()
{
    return get_field_data("meta_keyword");
}
function the_meta_description() 
{
    return get_field_data("meta_description");
}
function the_field($key , $escape=true)
{
    return get_field_data($key , $escape);
}
function the_postmeta_field($key , $escape = true)
{
    $target_data = get_field_data("_meta_");
    if($target_data && isset($target_data[$key]))
    {
        $meta_data = $target_data[$key];
        if($escape) $meta_data = fn_sanitize($meta_data);
        return fn_change_stringcode($meta_data, STRINGCODE_OUTPUT , STRINGCODE_PHP);
    }
    return "";
}

/**
 * LPO書式データを登録したKEYを指定し、条件に合致したコンテンツを返す(LPO拡張オプションが有効な場合のみ）
 * @param string $key
 * @param boolean $escape 
 * @return string 
 */
function lpo_content($key , $escape = false)
{
    global $LACNE;
    $lpo_content = the_field($key , $escape); //LPOはhtmlコンテンツである可能性が高いためエスケープしない
    if(!$lpo_content)
    {
        //対象となるKEYでデータがなければ_meta_でもチェックしてみる
        $lpo_content = the_postmeta_field($key , $escape);
    }
    //LPO拡張が有効かどうか
    if($lpo_content && method_exists($LACNE->library["lpo_light"], "extractSettingData"))
    {
        $settingdata_arr =  $LACNE->library["lpo_light"]->extractSettingData($lpo_content);
        if(settingdata_arr)
        {
            return $LACNE->library["lpo_light"]->getTargetContents($settingdata_arr);
        }
    }
    
    return "";
}


//-----------------------------------------------------------------------------------
//
// ここからはその他汎用系
// 
//-----------------------------------------------------------------------------------
/**
 * カテゴリリストデータを取得
 * @global object $LACNE
 * @global object $Output
 * @param  boolean $escape
 * @return object
 */
function get_categories($escape = true)
{
    global $LACNE;
    global $Output;
    
    //カテゴリデータ一覧取得
    $category_list = $LACNE->library['post']->get_category_list();
    
    //記事データがカテゴリごとに何件あるか
    $cat_cnt_where = "output_flag = ? AND output_date <= ?";
    $cat_cnt_param = array(1 , fn_get_date());
    $cnt_category_by = with(new Post())->getnum_category_by($cat_cnt_where , $cat_cnt_param);
    
    $result = array();
    if($cnt_category_by && count($cnt_category_by))
    {
        foreach($cnt_category_by as $key => $num)
        {
            $category_name = (isset($category_list[$key]))?$category_list[$key]:"";
            if($escape) $category_name = fn_sanitize ($category_name);
            $result[$key] = array(
                "id"    => $key,
                "name"  => fn_change_stringcode($category_name, STRINGCODE_OUTPUT , STRINGCODE_PHP),
                "num"   => $num
            );
        }
    }
    
    return $result;
}

/**
 * カテゴリリストデータを取得
 * @global object $LACNE
 * @global object $Output
 * @param  boolean $escape
 * @return object
 */
function get_sub_categories($escape = true)
{
    global $LACNE;
    global $Output;
    
    //カテゴリデータ一覧取得
    $category_list = $LACNE->library['post']->get_sub_category_list();
    
    //記事データがカテゴリごとに何件あるか
    $cat_cnt_where = "output_flag = ? AND output_date <= ? AND post.sub_category > 0";
    $cat_cnt_param = array(1 , fn_get_date());
    $cnt_category_by = with(new Post())->getnum_sub_category_by($cat_cnt_where , $cat_cnt_param);
    
    $result = array();
    if($cnt_category_by && count($cnt_category_by))
    {
        foreach($cnt_category_by as $key => $num)
        {
            $category_name = (isset($category_list[$key]))?$category_list[$key]:"";
            if($escape) $category_name = fn_sanitize ($category_name);
            $result[$key] = array(
                "id"    => $key,
                "name"  => fn_change_stringcode($category_name, STRINGCODE_OUTPUT , STRINGCODE_PHP),
                "num"   => $num
            );
        }
    }
    
    return $result;
}

/**
 * カテゴリリストデータを取得
 * @global object $LACNE
 * @global object $Output
 * @return object
 */
function get_sub_category_datas()
{
    global $LACNE;
    
    //カテゴリデータ一覧取得
    $category_datas = $LACNE->library['post']->get_sub_category_data();
    
    return fn_sanitize($category_datas);
}

/**
 * モーダルリンク表示のためのJavascriptコードを書き出し
 * 要: jquery.js , fancybox.js , fancybox.css
 */
function set_jscode_modallink()
{
    if(defined('USE_SETTING_LIGHTBOX') && USE_SETTING_LIGHTBOX)
    {
        
        $modal_class = USE_SETTING_LIGHTBOX;
echo <<< JSCODE
$(function(){
    
    $("img.{$modal_class}").each(function(){
        var src = $(this).attr("src");
        $(this).wrap($("<a>").attr({"href":src , "class" :"fancylink"}));
    });
    $("a.fancylink").fancybox();
    
});    
JSCODE;
    
    }
    else
    {
        echo "";
    }
}

function get_device()
{
    global $LACNE;
    return $LACNE->library["output"]->is_mobile();
}
/**
 * 画像サイズをデバイス向けに最適化して返す (htmlコードで渡す）
 * @param string $html
 * @param string $device
 * @return string 
 */
function change_opt_device_img($html , $device = "")
{
    global $LACNE;
    if(!$device) $device = get_device();
    return $LACNE->library["output"]->changedeviceImg($device , $html);
}

/**
 * OGP情報を書きだす
 * (social_btn拡張オプションが有効の場合のみ）
 */
function output_ogp()
{
    global $LACNE;
    if(method_exists($LACNE->library["social_btn"], "output_ogp"))
    {
        return $LACNE->library["social_btn"]->output_ogp();
    }
    return "";
}
/**
 * 各種ソーシャルボタンを書きだす
 * (social_btn拡張オプションが有効の場合のみ）
 */
function set_social_btn_twitter()
{
    global $LACNE;
    if(method_exists($LACNE->library["social_btn"], "btn_twitter"))
    {
        return $LACNE->library["social_btn"]->btn_twitter();
    }
    return "";
}
function set_social_btn_facebook()
{
    global $LACNE;
    if(method_exists($LACNE->library["social_btn"], "btn_facebook"))
    {
        return $LACNE->library["social_btn"]->btn_facebook();
    }
    return "";
}
function set_social_btn_mixi()
{
    global $LACNE;
    if(method_exists($LACNE->library["social_btn"], "btn_mixi"))
    {
        return $LACNE->library["social_btn"]->btn_mixi();
    }
    return "";
}


//-----------------------------------------------------------------------------------
//
// ロード後の初期処理系
// 
//-----------------------------------------------------------------------------------
$LACNE->load_library(array('post' , 'output' , 'social_btn' ,'lpo_light' , 'media'));
$Output = new Lacne_output($LACNE);
//previewチェック
$Output->preview_chk();


//-----------------------------------------------------------------------------------
//
// js , ssiから呼び出して表示させる際に処理される
// 
//-----------------------------------------------------------------------------------
/**
 *
 *  js or ssi書き出し用
 *  $_GET["mode"] = js もしくは ssi で指定
 * 
 *  @param number $num //出力件数(空の場合すべて出力)
 *  @param number $category //カテゴリID(空の場合すべて出力)
 *  @param boolean $postmeta //拡張データ項目も取得するか(postmetaテーブルのデータ）
 *  @param string $template //テンプレートファイル名指定（output／template／以下のファイル名※拡張子付けない)
 *  @param string $device   //テンプレートファイルでデバイス指定する場合( smph : スマートフォン , mobi :ガラケー）
 */
if(isset($_GET["type"]) && ($_GET["type"] == "js" || $_GET["type"] == "ssi")) {

	$now_date = fn_get_date();
        
	//$num
	$num = "";
	if(isset($_GET["num"]) && is_numeric($_GET["num"])){
            $num = fn_sanitize($_GET["num"]);
	}
	//$category
	$category = "";
	if(isset($_GET["category"]) && is_numeric($_GET["category"])){
            $category = fn_sanitize($_GET["category"]);
	}
	//$postmeta
	$postmeta = "";
	if(isset($_GET["postmeta"])){
            $postmeta = fn_sanitize($_GET["postmeta"]);
	}
        
        //template file
	$template = "";
        if(isset($_GET["template"]) && $_GET["template"]){
            $template = fn_sanitize($_GET["template"]);
	}
        
        //device
	$device = "";
        if(isset($_GET["device"]) && $_GET["device"]){
            $device = fn_sanitize($_GET["device"]);
	}
        
	$params = array(
		"num" 		=> $num,
		"category" 	=> $category,
		"postmeta"      => ($postmeta)?true:false,
	);
        
        $output_str = printList($params , $template , $device);
        
        if($output_str && $_GET["type"] == "js") {
             echo 'document.write("'.str_replace("\"","\\\"",$output_str).'");';
        }else if($output_str && $_GET["type"] == "ssi") {
             echo $output_str;
        }
        
        exit;
}

?>
