<?php

/**
 * ローカル用の設定ファイル core側にない設定を記述
 *
 * @package  Lacne
 * @author  InVogue Inc.
 * @link  http://lacne.jp
 * @copyright  Copyright 2008- InVogue Inc. All rights reserved.
 */


/*----------------------------------------------
 *  基本情報
 *  [_CHECK_] 管理するコンテンツ名（ニュース管理、アイテム管理など）と
 *  サイドナビゲーションのID値などを指定（/share/include/template/temp_side）
 *---------------------------------------------*/
define ("LACNE_APP_ADMIN_PAGENAME" , "メインメニュー");
//define ("LACNE_APP_ADMIN_NAVI_ID" , "#Usually");
define ("LACNE_APP_ADMIN_NAVI_ID" , "#Extend00"); //サイドナビゲーションID
//サイトを見るでジャンプするフロント側のURL
define ("LACNE_APP_FRONTPAGE_URL" , "/");
//このコンテンツのINDEX（もしくはMAIN）ページURL
define ("LACNE_APP_INDEXPAGE_URL" , LACNE_APP_ADMIN_PATH."/back.php");

/*----------------------------------------------
 *  本文内の画像にモーダルウインドウ表示対応させるためのクラスを割り当てる
 *---------------------------------------------*/
define("USE_SETTING_LIGHTBOX" , "fancy_img");

/*----------------------------------------------
 *  「記事」という表現を変えたい場合にここを編集する（ex:商品、実績など）
 *---------------------------------------------*/
define("KEYWORD_KIJI" , "記事");
