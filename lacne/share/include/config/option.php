<?php

/**
 * ローカル用オプションファイルの指定
 * 
 * グローバルのoption.phpをローカル用で書き換える場合
 * ここに記述
 *
 * @package  Lacne
 * @author  InVogue Inc.
 * @link  http://lacne.jp
 * @copyright  Copyright 2008- InVogue Inc. All rights reserved.
 */


/*----------------------------------------------
 *  ローカル用オプション指定
 *  [_CHECK_] グローバルのoption.phpをローカル用で書き換える場合に利用
 *---------------------------------------------*/
class Option_setting extends Option_setting_base
{
    function Option_setting(){
    }
}