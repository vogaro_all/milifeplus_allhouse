CKEDITOR.plugins.add( 'marker', {
    icons: 'marker',
    init: function( editor ) {
        editor.addCommand( 'insertMarker', {
            exec: function( editor ) {
            	
            	var selection = editor.getSelection();
            	var element = selection.getStartElement();
            	var selectText = '';

            	// 選択しているテキストの取得
            	if( selection.getNative() ){
            	  selectText = selection.getNative();

            	} else{
            	  // 上記で取得できない（IEなど）の場合は下記から取得
            	  selectText = selection._.cache.selectedText;

            	}
                editor.insertHtml( '<span class="marker">'+selectText+'</span>');
            }
        });
        editor.ui.addButton( 'Marker', {
            label: 'マーカー',
            command: 'insertMarker',
            toolbar: 'insert'
        });
    }
});