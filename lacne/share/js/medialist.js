
//-----------------------------------------------------------------
//
// メディア選択画面用の処理
// 
//-----------------------------------------------------------------


function imgSelect(movie)
{

    var select_url = LACNE_APP_ADMIN_PATH+"/media/list.php";
    if(movie == 1 || movie == 'movie') select_url += "?movie=1";
    //$("#srcimg").val("d");
    OpenNewWindow('win1',select_url,'width=710,height=660,toolbar=no,location=no,menubar=no,scrollbars=yes');
    
}

//メディア選択画面で選択したメディアファイルをWYSIWYGエディタへ画像URLを渡す
function setImageURL(path , target_id){
    
	if( window.opener && !window.opener.closed ){
            window.opener.CKEditor_dialog_setValue(path);
            //$("#"+target_id, window.opener.document).val(path).focus();
	}
	window.close();
        return false;
		
}
//メディア選択画面の画像リスト ロールオーバー処理
function setImageRollOver() {
	$("ul.list-media li p.img").css("cursor","pointer").hover(
		function(){
			$("img",this).css("border","3px solid #FFCC33");
		},
		function(){
			$("img",this).css("border","3px solid #EFEFEF");
		}
	);
}

$(document).ready(
	function() {
		setImageRollOver();
	}
);
    
//CKEditor側のダイアログへ選択された画像（動画）パスを渡す
function CKEditor_dialog_setValue(path)
{
    var dialog = CKEDITOR.dialog.getCurrent();
    if(dialog.getName() == 'link'){
        dialog.setValueOf('info','url',path);  
    }else{
        dialog.setValueOf('info','txtUrl',path);  
    }
}

//-----------------------------------------------------------------
//
// 記事作成画面内に画像選択フィールドを使う場合に利用する
// 
//-----------------------------------------------------------------
$(function(){
    
    //画像選択ボタン（メディア選択画面起動）
    $(document).on('click', '.img_btn', function(){
        var tid = $("input" , this).attr("id");
        tid = tid.replace("img_select_" , "");

        var select_url = LACNE_APP_ADMIN_PATH+"/media/list.php?image=1&type=meta&field_name="+tid;
        OpenNewWindow('win1',select_url,'width=710,height=660,toolbar=no,location=no,menubar=no,scrollbars=yes');
    })
    
    //画像削除ボタン
    $(document).on('click', '.img_delete_btn', function(){
        var tid = $("input" , this).attr("id");
        tid = tid.replace("img_delete_" , "");
        var obj_hidden = $("#img_hidden_"+tid);
        var obj_image = $("#img_prev_"+tid);
        if($(obj_hidden).val())
        {
            $(obj_hidden).val("");
            $(obj_image).empty();
        }
    });
    
    //記事編集時にすでに選択・保存された画像ファイルをimgタグでプレビュー表示させる
    $(".img_hidden_value").each(function(){
        if($(this).val())
        {
            var tid = $(this).attr("id");
            tid = tid.replace("img_hidden_" , "");
            set_preview_img("#img_prev_"+tid , $(this).val());
        }
    })
    
});

function set_preview_img(obj_image , path)
{
    $(obj_image).empty().append('<img src="'+path+'" width="100px" />');
}

//メディア選択画面で選択したメディアファイルを METAフィールドの画像選択項目に画像セット
function setImageURL_META(path ,field_name){
	if( window.opener && !window.opener.closed ){
		var obj_hidden = $("#img_hidden_"+field_name,window.opener.document);
		var obj_image = $("#img_prev_"+field_name,window.opener.document);
		var obj_btn_del = $("#img_delete_"+field_name,window.opener.document);
		$(obj_hidden).val(path);
                set_preview_img(obj_image , path);
                $(obj_btn_del).show();
	}
	window.close();
        return false;
}
