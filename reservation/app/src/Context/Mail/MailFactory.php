<?php namespace Context\Mail;

use Infra\Mail\Mail;
use Infra\View\Template;

class MailFactory {

    private $fields;
    private $item;

    public function __construct($fields, $item) {
        $this->fields = $fields;
        $this->item = $item;
    }

    public function generate($config) {
        $mail = new Mail();

        //FROM
        $mail->setFrom($this->getEmail($config['sender']));

        //TO
        $mail->setTo($this->getEmail($config['receiver']));

        //件名
        $mail->setSubject(Template::render($config['template']['subject'], array(
            'item' => $this->item,
        ), false));

        //本文
        $mail->setBody(Template::render($config['template']['body'], array(
            'item' => $this->item,
        ), false));

        return $mail;
    }

    private function getEmail($client) {

        //振り分け設定が無効
        if (empty($client['switchingBy'])) {
            if (isset($client['email']) && isset($client['email'][0])) {
                return $client['email'][0];
            }
            if (isset($client['emailField']) && isset($client['emailField'][0])) {
                return $this->convertFieldNameToMail($client['emailField'][0]);
            }
            throw new \DomainException();
        }

        //振り分け設定が有効
        $fieldName = $client['switchingBy'];
        if (empty($this->item[$fieldName]) || empty( $this->fields[$fieldName]['selection'])) {
            throw new \DomainException();
        }
        $key = array_search($this->item[$fieldName], $this->fields[$fieldName]['selection']);
        if ($key === false) {
            throw new \DomainException();
        }
        if (isset($client['email']) && isset($client['email'][$key])) {
            return $client['email'][$key];
        }
        if (isset($client['emailField']) && isset($client['emailField'][$key])) {
            return $this->convertFieldNameToMail($client['emailField'][$key]);
        }
        throw new \DomainException();
    }

    private function convertFieldNameToMail($fieldNames) {
        $_ = array();
        foreach($fieldNames as $fieldName) {
            $_[] = $this->item[$fieldName];
        }
        return $_;
    }
}