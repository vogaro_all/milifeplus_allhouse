<?php namespace Infra\Http;

/**
 * Class Response
 * @package Infra\Http
 */
class Response
{
//	/**
//	 * @param \Closure fn
//	 */
//	public function echoJson($fn)
//	{
//		if(is_callable($fn)){
//			$response_data = call_user_func($fn, new Query(Query::getRequestParamater()));
//		}
//
//		header('Content-Type: application/json');
//		echo(json_encode($response_data));
//	}
	/**
	 * JSONの出力
	 * @param array $data
	 */
	public static function echoJson(array $data)
	{
		header('Content-Type: application/json');
		echo(json_encode($data));
		exit;
	}

	/**
	 * 301転送
	 * @param $path
	 */
	public static function redirect($path)
	{
		header("Location: ${path}", true, 301);
		exit;
	}

	/**
	 * 302転送
	 * @param $path
	 */
	public static function redirect302($path)
	{
		header("Location: ${path}", true, 302);
		exit;
	}

	/**
	 * 302転送し処理継続
	 * @param $path
	 */
	public static function redirect302NotExit($path)
	{
		while( ob_get_level() ) { ob_end_clean() ; }
		$out = "\r\n";
		header("Content-Length: ".strlen($out));
		header("Connection: close");
		header("Location: ". $path);
		echo $out;
		flush();
	}

	/**
	 * 404転送
	 * @param string $path
	 */
	public static function notfound404($path = '/404.html')
	{
		$host = parse_url($path, PHP_URL_HOST);
		if(!empty($path)){
			if(empty($host)){
				$path = SITE_URL. $path;
			}
			$data = file_get_contents($path);

			$statusCode = self::getStatusCode($http_response_header);
			if($statusCode && $statusCode == 200){
				if(self::isDeflate($http_response_header)){
					$data = self::inflate($data);
				}
				print($data);
			}
		}
		exit;
	}

	/**
	 * レスポンスのステータスコード取得
	 * @param array $headers
	 * @return int
	 */
	public static function getStatusCode($headers){
		if(preg_match('/HTTP\/1\.[0|1|x] ([0-9]{3})/', $headers[0], $matches)){
			return $matches[1];
		}
		return false;
	}

	/**
	 * Deflate(gzip圧縮)されているか
	 * @param array $headers
	 * @return bool
	 */
	public static function isDeflate($headers){
		foreach($headers as $header) {
			if (stristr($header, 'content-encoding') and stristr($header, 'gzip')) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Deflate(gzip圧縮)のデコード
	 * @param $data
	 * @return string
	 */
	public static function inflate($data){
		if(function_exists("gzdecode")) {
			return gzdecode($data);
		}
		return gzinflate(substr($data, 10, -8));
	}
}
