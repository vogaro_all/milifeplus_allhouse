<?php namespace Infra\Csv;


/**
 * Class CsvReader
 * @package Infra\Csv
 */
class CsvReader
{
	/**
	 * ファイルを読み込んで2次元配列に変換する
	 * @param string $filePath
	 * @return array
	 */
	public static function read($filePath){
		//存在チェック
		if(!file_exists($filePath)){
			throw new \RuntimeException("無効なCSVファイルです。");
		}

		$result = array();
		$fp = fopen($filePath, 'r');
		while($csvData = self::fgetCsvReg($fp, 256)){
			$result[] = $csvData;
		}
		fclose($fp);

		return $result;
	}

	/**
	 * バイナリを読み込んで2次元配列に変換する
	 * @param string $data
	 * @return array
	 */
	public static function readByBinary($data){
		if(!$data){
			throw new \RuntimeException("無効なCSVファイルです。");
		}

		$data = explode(CSV::LINE_BREAK_DELIMITER, $data);

		$result = array();
		$line = '';
		foreach($data as $item){
			$line .= $item;
			$itemCount = mb_substr_count($line, '"');
			if($itemCount % 2 == 0) {
				$result[] = self::format($line);
				$line = '';
			}else{
				$line .= CSV::LINE_BREAK_DELIMITER;
			}
		}

		//最終行が空であれば削除
		$last = array_pop($result);
		if(empty($result) || count($last) == count($result[0]) ){
			$result[] = $last;
		}
		return $result;
	}

	/**
	 * CSV読み込み時のfget関数の場合、先頭文字が文字化けしたままになるので独自関数実装
	 * @param $handle
	 * @param null $length
	 * @return array
	 */
	static public function fgetCsvReg(&$handle, $length = null) {
		$line = '';
		$eof = false;

		while ($eof != true) {
			$line .= fgets($handle, $length);
			$itemCount = mb_substr_count($line, '"');
			$eof = ($itemCount % 2 == 0);
		}

		return self::format($line);
	}

	/**
	 * CSVの整形・文字コード変換
	 * @param $data
	 * @return array
	 */
	static protected function format($data){
		$d = preg_quote(',');
		$e = preg_quote('"');
		$delimiter = preg_quote(CSV::LINE_BREAK_DELIMITER);

		$line = preg_replace('/(?:'. $delimiter. '|['. $delimiter. '])?$/', $d, trim($data));
		$pattern = '/('.$e.'[^'.$e.']*(?:'.$e.$e.'[^'.$e.']*)*'.$e.'|[^'.$d.']*)'.$d.'/';
		preg_match_all($pattern, $line, $matches);
		$data = $matches[1];

		return array_map(function($_) use ($e){
			mb_convert_variables('UTF-8', 'SJIS-win', $_);
			$_ = trim($_);
			$_ = preg_replace('/^'.$e.'(.*)'.$e.'$/s','$1',$_);
			return str_replace($e.$e, $e, $_);
		}, $data);
	}
}
