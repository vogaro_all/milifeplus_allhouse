<?php namespace Infra\Csv;

use Infra\Http\Request;

/**
 * Class CSV
 * @package Infra\Csv
 */
class CSV
{
	const LINE_BREAK_DELIMITER = "\r";
	const LINE_BREAK_DELIMITER_WINDOWS = "\n";
	const LINE_BREAK_DELIMITER_MAC = "\r\n";

	/**
	 * 出力バッファ
	 * @var string
	 */
	protected $buffer = '';

	/**
	 * ファイル名
	 * @var
	 */
	protected $filename;

    /**
     * CSV constructor.
     * @param $filename
     */
    public function __construct($filename)
    {
    	$this->filename = $filename;
//        $this->setHttpHeader($filename);
//        $this->outputCsvHeader();
    }

    /**
     * HTTPヘッダーの設定
     */
    private function setHttpHeader(){
        //マック用
//        if(Request::ua_mac()){
//            header("Content-Disposition: attachment; filename=$this->filename");
//            //IE8以下用
//        }else
		if(1 <= Request::ie_version() && Request::ie_version() <= 8){
            header('Content-Disposition: attachment; filename='.mb_convert_encoding($this->filename, 'SJIS-win', 'UTF-8'));
            //それ以外はURLエンコードで対応
        }else{
            header('Content-Disposition: attachment; filename*=UTF-8\'\''.rawurlencode($this->filename));
        }
        header("Content-Type: application/octet-stream");
    }


    /**
     * CSVヘッダー書き出し
     */
    private function outputCsvHeader() {
//        if(Request::ua_mac()){
//            //MAC Excel用のUTF-BOMあり対応
//            print("\xEF\xBB\xBF");
//        }
    }

    /**
     * 文字コード変換し、書き出し
     * @param array $data
     * @param string $to_encoding
     * @param string $from_encoding
     */
    public function outputCsvBody(array $data, $to_encoding='', $from_encoding='auto') {
        //文字コードの指定
        if(empty($to_encoding)){
            $to_encoding = $this->getCharset();
        }

        //指定した文字コードで変換し、書き出し
        if(!empty($data)){
            mb_convert_variables($to_encoding, $from_encoding, $data);
            $this->buffer .= '"';
			$data = array_map(function($v){
				return str_replace('"', '""', $v);
			}, $data);
            $this->buffer .= implode('","', $data);
			$this->buffer .= '"'. self::LINE_BREAK_DELIMITER;
        }
    }

    /**
     * OSごとの文字コード
     * @return string
     */
    private function getCharset(){
        //マックの場合はUTF-8
//        if(Request::ua_mac()){
//            return 'UTF-8';
//        }
        return 'SJIS-win';
    }

	/**
	 * セル内での改行コード取得
	 */
	public static function getLineBreakInCell()
	{
		//マック用
		if(Request::ua_windows()){
			return self::LINE_BREAK_DELIMITER_WINDOWS;
		}
		return self::LINE_BREAK_DELIMITER_MAC;
	}

	/**
	 * ダウンロード処理
	 */
	public function download(){
    	$this->setHttpHeader();
    	echo $this->buffer;
		exit;
	}

	/**
	 * 保存処理
	 * @param $directory
	 */
	public function save($directory){
		$directory = realpath($directory);
		$path = "{$directory}/{$this->filename}";
		file_put_contents($path, $this->buffer);
	}
}
