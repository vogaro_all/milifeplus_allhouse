<?php namespace Infra\Mail;

/**
 * Class Mail
 * @package Infra\Mail
 */
class Mail
{
    /**
     * @var string Wonderful Subject
     */
    protected $subject = "";

    /**
     * @var array array('john@doe.com' => 'John Doe')
     */
    protected $from = array();

    /**
     * @var array array('receiver@domain.org', 'other@domain.org' => 'A name')
     */
    protected $to = array();

    /**
     * @var string Here is the message itself
     */
    protected $body = "";

    /**
     * @var \Swift_SendmailTransport
     */
    protected $transport = null;

    /**
     * Mail constructor.
     */
    public function __construct()
    {
        \Swift::init(function () {
            \Swift_DependencyContainer::getInstance()
                ->register('mime.qpheaderencoder')
                ->asAliasOf('mime.base64headerencoder');
            \Swift_Preferences::getInstance()->setCharset('iso-2022-jp');
        });

        $sendmailCommand = getenv('SENDMAIL_PATH') ?: '/usr/sbin/sendmail -bs';

        $this->transport = new \Swift_SendmailTransport($sendmailCommand);
    }

    /**
     * メールの送信
     * @return int
     */
    public function send()
    {
        if (empty($this->from) || empty($this->to)) {
            return false;
        }
        $mailer = new \Swift_Mailer($this->transport);

        $message = with(new \Swift_Message( $this->subject ))
            ->setFrom( $this->from )
            ->setTo( $this->to )
            ->setBody( $this->body );

        return $mailer->send($message);
    }

    /**
     * @param $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @param array $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @param array $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

    /**
     * @param $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }
}
