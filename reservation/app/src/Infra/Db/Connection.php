<?php namespace Infra\Db;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;

/**
 * Class Connection
 * @package Infra\Db
 */
class Connection
{
    /**
     * @var  \Doctrine\DBAL\Connection
     */
    protected static $conn = null;

    /**
     * CMSDBRepository constructor.
     */
    public function __construct()
    {
        self::$conn = self::getConnection();
    }

    /**
     * DBのコネクションを取得
     * @return \Doctrine\DBAL\Connection|null
     * @throws \Doctrine\DBAL\DBALException
     */
    public static function getConnection()
    {
        if( !empty(self::$conn) ){
            return self::$conn;
        }

        $conn = array(
            "driver"    => 'pdo_mysql',
            "host"      => DB_HOST,
            "user"      => DB_USER,
            "password"  => DB_PASS,
            "dbname"    => DB_NAME,
            "charset"   => 'UTF8',
            'driverOptions' => array(
                1002 => 'SET NAMES UTF8'
            )
        );

        $config = new Configuration(array( \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC ));

        if(DBSetting::DEBUG_MODE){
            $config->setSQLLogger( new \Doctrine\DBAL\Logging\EchoSQLLogger() );
        }

        self::$conn = DriverManager::getConnection( $conn, $config );

        return self::$conn;
    }

    /**
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    public static function createQueryBuilder()
    {
        return self::getConnection()->createQueryBuilder();
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public static function clean()
    {
        $ret = self::getConnection()->fetchArray("show tables");
        $ret = empty($ret)? array(): $ret;

        foreach( $ret as $name ){
            self::getConnection()->exec("DROP TABLE " . $name);
        }

        passthru("mysql -u " . DB_USER ." -p" . DB_PASSWORD . " " . DB_NAME . " < " . DB_SQL_FILE);
    }

    /**
     * テーブル一覧の表示
     * @return array
     */
    protected static function tables()
    {
        $ret = self::getConnection()->fetchArray("show tables");
        if( empty( $ret ) ) return $ret;

        foreach( $ret as $name ){
            var_dump($name);
        }
    }

}
