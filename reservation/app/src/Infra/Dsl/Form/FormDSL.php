<?php namespace Infra\Dsl\Form;

use Infra\Util\Bag;

/**
 * フォームDSL
 * 標準的なフォームのエンジンを構築
 */
class FormDSL
{
    /**
     * @var array index confirm complete
     */
    protected $lazy = array();

    /**
     * トップページ
     */
    const INDEX     = "index";

    /**
     * 確認ページ
     */
    const CONFIRM   = "confirm";

    /**
     * 完了ページ
     */
    const COMPLETE  = "complete";

    /**
     * 検証
     */
    const VALID     = "valid";

    /**
     * 設定
     */
    const SETTING   = "setting";

    /**
     * 戻る
     */
    const BACK      = "back";

    /**
     * 特になし
     * FormDSL constructor.
     */
    public function __construct(){}

    /**
     * トップ・確認・完了・検証・設定関数の登録
     * @param $name
     * @param $param
     * @return $this
     */
    public function __call($name, $param)
    {
        $this->lazy[$name] = $param[0];
        return $this;
    }

    /**
     * @param $key
     * @return \Closure|mixed
     */
    public function resolve($key){ return array_key_exists($key, $this->lazy)? $this->lazy[$key]: function(){}; }

    public function run()
    {
        $_ = $this->resolve(self::INDEX);
        $postData = new Bag($_POST);
        $item = $postData->except(array(self::CONFIRM, self::COMPLETE, self::BACK));

        //入力フォーム初回表示時
        if( !$this->is_post() && !isset($item[self::BACK])){
            $item = array();
        }
        unset($item[self::BACK]);


        $errors = array();
        //確認画面か完了画面への遷移
        if( $this->is_post() && !isset($item[self::BACK])){
            $setting = $this->resolve(self::SETTING);
            $errors = call_user_func($this->resolve(self::VALID), $item, $setting($item));

            if(empty($errors)){
                if(isset($_POST[self::CONFIRM])){
                    $_ = $this->resolve(self::CONFIRM);
                }else if(isset($_POST[self::COMPLETE])){
                    $_ = $this->resolve(self::COMPLETE);
                }
            }
        }

        $setting = $this->resolve(self::SETTING);
        call_user_func($_, $item, $errors, $setting($item));

        //完了時の後処理
        if( $this->is_post() && !isset($item[self::BACK]) && empty($errors) && isset($_POST[self::COMPLETE]) ){
            //$query = $_GET ? '?'.http_build_query($_GET) : '';
            //header('location: complete'. $query);
        }
        exit;
    }

    /**
     * POST通信か判定
     * @return bool
     */
    protected function is_post()
    {
        return $_SERVER["REQUEST_METHOD"] == "POST";
    }

}
