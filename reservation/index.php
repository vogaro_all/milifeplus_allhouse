<?php
require_once(__DIR__. '/app/vendor/autoload.php');

ini_set('display_errors',1);

use Infra\View\Template;
use Infra\Dsl\Form\FormDSL;
use Infra\Validation\Validator;
use Context\Mail\MailFactory;

define("APP_ROOT_DIR", __DIR__);
function with($obj){ return $obj;}


/**
 * 各種お問い合わせフォームシステムの初期化
 */
return with(new FormDSL())
    /**
     * index,confirm,complete画面で$setting変数としてアクセスできるようになります。
     * フォーム全体の設定値などで使用します。
     */
    ->setting(function(){
        return json_decode(file_get_contents(__DIR__ . '/app_assets/form.json'), true);
    })

    /**
     * $qはフォームから送信されたきた値になり、この値を使用してフォーム値の検証処理を行います。
     * 確認画面・完了画面に遷移する場合にここに処理が入ります。
     * 戻り値をfalseの判定になるようなものを返すと次の確認画面に進みます。
     */
    ->valid(function($item, $setting){

        /**============================
         * 基本登録情報の検証処理
         *============================*/

        $validator = Validator::make();
        foreach($setting['fields'] as $key => $field) {
            if (empty($field['rule'])) {
                continue;
            }
            $rule = $validator->set($key, isset($item[$key]) ? $item[$key] : null, $field['label']);
            foreach($field['rule'] as $vRule => $vConf) {
                if (!$vConf) {
                    continue;
                }
                switch($vRule) {
                    case 'required':
                        $rule->required($setting['errors']['required']);
                        break;
                    case 'katakana':
                        $rule->katakana($setting['errors']['katakana']);
                        break;
                    case 'length_eq':
                        $rule->length('==', $vConf, $setting['errors']['length_eq']);
                        break;
                    case 'length_gteq':
                        $rule->length('>=', $vConf, $setting['errors']['length_gteq']);
                        break;
                    case 'length_lteq':
                        $rule->length('<=', $vConf, $setting['errors']['length_lteq']);
                        break;
                    case 'degit':
                        $rule->degit($setting['errors']['degit']);
                        break;
                    case 'tel':
                        $rule->tel($setting['errors']['tel']);
                        break;
                    case 'email':
                        $rule->email($setting['errors']['email']);
                        break;
                    case 'same':
                        $rule->custom(function($v) use($item, $vConf){
                            if(empty($item[$vConf])){
                                return false;
                            }
                            return $v != $item[$vConf];
                        }, $setting['errors']['same']);
                        break;
                    default:
                        break;
                }
            }
        }
        
        $err = $validator->valid()->getErr();
        
        // 日付入力チェック
        if (empty($item['vfHope1Month']) || empty($item['vfHope1Day']) || empty($item['vfHope1Time'])) {
            $err['vfHope1'] = '必須項目です。';
        }
        // 日付入力チェック
        if (!empty($item['vfHope2Month']) || !empty($item['vfHope2Day']) || !empty($item['vfHope2Time'])) {
            if (empty($item['vfHope2Month']) || empty($item['vfHope2Day']) || empty($item['vfHope2Time'])) {
                $err['vfHope2'] = '日時が正しくありません。';
            }
        }
        
        return $err;
    })

    /**
     * トップページの処理を記述します。
     * フォーム項目の初期化・検証エラーの初期化・フロント側で使用したい設定情報を設定します。
     */
    ->index(function($item, $err, $setting){
        $default = array();
        foreach($setting['fields'] as $key => $field) {
            if (isset($_GET[$key]) && isset($field['defaultByQueryString']) && $field['defaultByQueryString']) {
                $default[$key] = $_GET[$key];
            } else {
                $default[$key] = isset($field['default']) ? $field['default'] : '';
            }
        }
        echo( Template::render($setting['pages']['input'], array(
            'err'    => $err,
            'fields' => $setting['fields'],
            'item'   => empty($item) ? $default: $item
        )));
    })

    /**
     * 確認画面での処理を記述します。
     */
    ->confirm(function($item, $err, $setting){
        echo( Template::render($setting['pages']['confirm'], array(
            'item'   => $item,
            'fields' => $setting['fields'],
        )));
    })

    /**
     * 完了画面での処理を記述します。
     * 管理者・ユーザにメールを送信します。
     */
    ->complete(function($item, $err, $setting){

        $mailFactory = new MailFactory($setting['fields'], $item);

        //管理者にメールを送信します。
        $adminConfig = $setting['notifications']['admin'];
        if ($adminConfig['enable']) {
            $mail = $mailFactory->generate($adminConfig);
            $mail->send();
        }

        //ユーザにメールを送信します。
        $userConfig = $setting['notifications']['user'];
        if ($userConfig['enable']) {
            $mail = $mailFactory->generate($userConfig);
            $mail->send();
        }

        $query = $_GET ? '?'.http_build_query($_GET) : '';
        header('location: ' . $setting['pages']['complete'] . $query);
    })

    /**
     * 各メソッドて定義された内容を元にシステムを起動します。
     */
    ->run();
