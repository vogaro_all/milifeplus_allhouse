<footer class="l-footer">
  <div class="footer">
    <div class="footer__hdg l-row">
      <div class="l-col-md-6 l-col-sm-6 link link--01">
        <a href="/contact" class="link-content link-content--contact">
          <p class="contact-logo content-logo">
            <svg class="icon icon--01"><use xlink:href="#svg-icon-contact"></use></svg>
          </p>
          <p class="contact-txt content-txt">
            <picture>
              <source media="(min-width: 768px)" srcset="/assets/images/pages/common/footer_link01_md.png">
              <img src="/assets/images/pages/common/footer_link01.png" width="129" height="49" alt="お問い合わせはこちら">
            </picture>
          </p>
          <div class="content-btn u-d-md-block u-d-none">
            <button class="content-btn__circle c-circle"></button>
          </div>
        </a>
      </div>
      <div class="l-col-md-6 l-col-sm-6 link link--02">
        <a href="/reservation" class="link-content link-content--reserve">
          <p class="reserve-logo content-logo">
            <svg class="icon icon--02"><use xlink:href="#svg-icon-reserve"></use></svg>
          </p>
          <p class="contact-txt content-txt">
            <picture>
              <source media="(min-width: 768px)" srcset="/assets/images/pages/common/footer_link02_md.png">
              <img src="/assets/images/pages/common/footer_link02.png" width="87" height="49" alt="来店予約はこちら">
            </picture>
          </p>
          <div class="content-btn u-d-md-block u-d-none">
            <button class="content-btn__circle c-circle"></button>
          </div>
        </a>
      </div>
    </div>
    <div class="footer__content">
      <div class="content-wrap">
        <h2 class="content-ttl">
          <img src="/assets/images/pages/common/logo.svg" alt="保険とライフプランの相談デスク MILIFEPLUS by ALLHOUSE" class="logo-imgs__item--01"><br class="u-d-md-none">
            <img src="/assets/images/pages/common/logo02.svg" alt="保険とライフプランの相談デスク MILIFEPLUS by ALLHOUSE" class="logo-imgs__item--02">
        </h2>
        <p class="content-access">
          〒735-0012 広島県安芸郡府中町八幡1-4-23<br>
          TEL 082-236-6444 / FAX 082-890-1003<br>
          営業時間9:00~18:00（定休日 水曜）　駐車場 有
        </p>
      </div>
    </div>
    <div class="footer__bottom l-container">
      <div class="l-row btm-wrap">
        <div class="btm-wrap__link l-row">
          <a href="/privacy" class="privacy">
            プライバシーポリシー
          </a>
          <a href="https://www.facebook.com/milifeplus.official/" target="_blank" class="sns c-circle">
            <svg class="icon icon--01"><use xlink:href="#svg-icon-sns01"></use></svg>
          </a>
        </div>
        <p class="copyright c-en-txt">
          Copyright © MILIFEPLUS. All Rights Reserved.
        </p>
      </div>
    </div>
  </div>
</footer>