<header class="l-header l-row" id="js-header">
  <div class="l-header__wrap l-row">
    <div class="header-item header-item--left">
      <h1 class="header-logo">
        <a href="/" class="header-logo__img">
          <p class="logo-txt">保険とライフプランの相談デスク</p>
          <p class="logo-imgs l-row--inline">
            <img src="/assets/images/pages/common/logo.svg" alt="保険とライフプランの相談デスク MILIFEPLUS by ALLHOUSE" class="logo-imgs__item--01">
            <img src="/assets/images/pages/common/logo02.svg" alt="保険とライフプランの相談デスク MILIFEPLUS by ALLHOUSE" class="logo-imgs__item--02">
          </p>
        </a>
      </h1>
    </div>
    <div class="l-col-md-6 l-col-sm-full header-item header-item--right u-d-none u-d-md-block">
      <ul class="header-item__content l-row">
        <li class="content-item content-item--tel">
          <a href="tel:0822366444" class="content-item__link content-item__link--tel">
            <img src="/assets/images/pages/common/header_tel01.png" alt="0822366444"><br>
            <span class="work-time">営業時間 9:00~18:00（定休日 水曜）</span>
          </a>
        </li>
        <li class="content-item content-item--btn content-item--contact">
          <a href="/contact" class="content-item__btn c-link-btn c-link-btn--01">
            <p class="contact-logo btn-logo">
              <svg class="icon icon--01"><use xlink:href="#svg-icon-contact"></use></svg>
            </p>
            <p class="contact-txt btn-txt">
              お問い合わせ
            </p>
          </a>
        </li>
        <li class="content-item content-item--btn content-item--reserve">
          <a href="/reservation" class="content-item__btn c-link-btn c-link-btn--02">
            <p class="reserve-logo btn-logo">
              <svg class="icon icon--02"><use xlink:href="#svg-icon-reserve"></use></svg>
            </p>
            <p class="reserve-logo btn-txt">
              来店予約
            </p>
          </a>
        </li>
      </ul>
    </div>
  </div>
  <div class="l-header__hamburger">
    <button class="hamburger-menu js-header-toggler">
      <div class="hamburger-menu__wrap">
        <span class="line"></span>
        <span class="line"></span>
        <span class="line"></span>
      </div>
    </button>
  </div>
  <div class="l-header__menu l-row">
    <div class="menu-img l-col-md-6 u-bg-img u-d-none u-d-md-block"></div>
    <div class="menu-items l-col-md-6 l-col-sm-full">
      <div class="wrap">
        <ul class="menu-items__ls">
          <li class="ls">
            <a href="/" class="ls-link js-active-page">
              トップページ
            </a>
          </li>
          <li class="ls">
            <a href="/case" class="ls-link js-active-page">
              事例紹介
            </a>
          </li>
          <li class="ls">
            <a href="/staff" class="ls-link js-active-page">
              スタッフ紹介
            </a>
          </li>
        </ul>
        <div class="menu-items__other">
          <div class="item-sns">
            <a href="https://www.facebook.com/milifeplus.official/" target="_blank">
              <img src="/assets/images/pages/index/sns01.png" alt="facebook">
            </a>
          </div>
          <a href="/privacy"  class="privacy-policy">
            プライバシーポリシー
          </a>
        </div>
      </div>
    </div>
  </div>
</header><!-- /.header -->
