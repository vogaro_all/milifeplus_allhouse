<?php namespace Infra\View;

/**
 * テンプレートと変数の展開をサポートします。
 * 展開される変数はエスケープすることもできます。(デフォルトでは全てエスケープ)
 *
 * @author Vogaro Inc.
 *
 */
class Template
{
    /**
     * 再帰的にデータをエスケープする
     * ※scriptタグはエスケープされません。
     *
     * @param mixed $data
     * @return mixed
     */
    public static function escape($data){
        if ( is_array( $data ) ) {
            return array_map( array('self', 'escape'), $data );
        } else {
            if ( gettype( $data ) == "string" ) {
                return htmlspecialchars( $data, ENT_QUOTES );
            } else {
                return $data;
            }
        }
    }

    /**
     * Twigによるテンプレート読み込み
     * @param $path
     * @param $data
     * @param bool|string $escape
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public static function render($path, $data, $escape = 'html'){
        //twigの設定
        $loader = new \Twig_Loader_Filesystem(APP_ROOT_DIR);
        $twig   = new \Twig_Environment($loader, array('autoescape' => $escape, 'debug' => true));
        $twig->addExtension(new \Twig_Extension_Debug());

        //アクティブクラス出力
        $function = new \Twig_SimpleFunction('activeClass', array(__CLASS__, 'activeClass'));
        $twig->addFunction($function);

        $function = new \Twig_SimpleFunction('active', array(__CLASS__, 'active'));
        $twig->addFunction($function);

        //URLクエリ生成
        $function = new \Twig_SimpleFunction('urlParam', function($vars , $keys , $prefix=''){
            return fn_set_urlparam_prefix($vars , $keys , $prefix);
        });
        $twig->addFunction($function);

        //チェックボックス
        $function = new \Twig_SimpleFunction('checked', array(__CLASS__, 'checked'));
        $twig->addFunction($function);

        //セレクトボックス
        $function = new \Twig_SimpleFunction('selected', array(__CLASS__, 'selected'));
        $twig->addFunction($function);

        $function = new \Twig_SimpleFunction('arraySearch', array(__CLASS__, 'arraySearch'));
        $twig->addFunction($function);

        //フォーム用のhiddenパラメータ生成
        $function = new \Twig_SimpleFunction('hiddenParam', array(__CLASS__, 'hiddenParam'));
        $twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getLabel', array(__CLASS__, 'getLabel'));
        $twig->addFunction($function);

        //ファイルのインクルード
        $function = new \Twig_SimpleFunction('getContent', array(__CLASS__, 'getContent'));
        $twig->addFunction($function);


        return $twig->render($path, $data);
    }

    public static function active($cond, $active, $default = ''){
        if ($cond) {
            return $active;
        }
        return $default;
    }

    /**
     * アクティブクラス出力
     * @param $param
     * @param $key
     * @param $target
     * @param $class
     * @return string
     */
    public static function activeClass($param, $key, $target, $class){
        //ターゲットの指定がない場合・ターゲットとキーが一致した場合アクティブクラス出力
        if(isset($param[$key]) && !empty($param[$key])){
            if($param[$key] == $target){
                return $class;
            }
        }elseif((empty($target)) || ($target == 'all')){
            return $class;
        }
        return '';
    }

    /**
     * チェックボックス
     * @param $selectList
     * @param $target
     * @param string $echo
     * @return string
     */
    public static function checked($selectList, $target, $echo = 'checked'){
        //選択リストにターゲットが含まれていた場合アクティブクラス出力
        $target = (string) $target;
        if(is_array($selectList) && in_array($target, $selectList, true)){
            return $echo;
        }
        return '';
    }

    /**
     * セレクトボックス
     * @param $selected
     * @param $target
     * @param string $echo
     * @return string
     */
    public static function selected($selected, $target, $echo = 'selected'){
        //ターゲットの指定がない場合・ターゲットとキーが一致した場合アクティブクラス出力
        $target = (string) $target;
        if($target === $selected){
            return $echo;
        }
        return '';
    }

    public static function arraySearch($needle, $array) {
        return array_search($needle, $array);
    }

    public static function hiddenParam($list){
        if(count($list)){
            foreach($list as $key => $value){
                if(is_array($value)){
                    foreach($value as $key2 => $value2){
                        if(is_array($value2)){
                            foreach($value2 as $key3 => $value3){
                                echo "<input type=\"hidden\" name=\"".$key."[".$key2."][".$key3."]\" value=\"".$value3."\" />\n";
                            }
                        }else{
                            echo "<input type=\"hidden\" name=\"".$key."[".$key2."]\" value=\"".$value2."\" />\n";
                        }
                    }
                }else{

                    if($key != "comp" && $key != "back" && $key != "conf" && $key != "conf_x" && $key != "conf_y" && $key != "conm_x" && $key != "conm_y"){
                        echo "<input type=\"hidden\" name=\"$key\" value=\"$value\" />";
                    }

                }
            }
            echo "<input type=\"hidden\" name=\"charcode\" value=\"文字コード認識用の文字列\" />";
        }
    }

    public static function getLabel($value, $master, $default = ''){
        if (isset($master[$value])) {
            return $master[$value];
        }
        return $default;
    }
}
