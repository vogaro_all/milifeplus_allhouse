<?php
$lacne_path = dirname(__FILE__). "/../../lacne";

require_once($lacne_path . '/Base/system/composer/vendor/autoload.php');
require_once($lacne_path."/news/include/setup.php");

use lacne\service\output\OwnedMediaOtherAPI;
use lacne\domain\OwnedMedia\entity\MediaEntity;

$ownedMediaOtherAPI = new OwnedMediaOtherAPI();

// 記事＆セクション情報取得
$_post_data = $ownedMediaOtherAPI->details();

$MediaEntity = new MediaEntity( fn_esc($_post_data) );

$page_data = $_post_data['page_data'] ;

include_once('./index_template.html');

?>