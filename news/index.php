<?php

$lacne_path = dirname(__FILE__). "/../lacne";

require_once($lacne_path . '/Base/system/composer/vendor/autoload.php');
require_once($lacne_path."/news/include/setup.php");
use lacne\service\output\OwnedMediaHelper;
use lacne\core\Pager;

use lacne\service\output\OwnedMediaOtherAPI;
$ownedMediaOtherAPI = new OwnedMediaOtherAPI();

$limit 	= 12;
$params = array(
    'limit' => $limit,
//     'mv_flag' => 0,
);
$dataList = $ownedMediaOtherAPI->load("loadArticleList", $params);

//ページャ条件設定
$page 	= $_GET['page'];
$length = $dataList['ret']['cnt'];
$pages = (OwnedMediaHelper::pager( new Pager($page, $length, $limit) ));

include_once('./index_template.html');

?>