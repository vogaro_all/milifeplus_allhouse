<?php
require_once("./lacne/news/output/post.php");
use lacne\service\output\OwnedMediaOtherAPI;
use lacne\service\output\OwnedMediaHelper;
use lacne\core\Pager;
$ownedMediaOtherAPI = new OwnedMediaOtherAPI();

$limit 	= 4;
$params = array(
    'limit' => $limit,
//     'mv_flag' => 0,
);
$dataList = $ownedMediaOtherAPI->load("loadArticleList", $params);

include_once('./index_template.html');
?>